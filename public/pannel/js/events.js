$('#forget-password').click(function() {
    $('.login-form').hide();
    $('.forget-form').show();
});

$('#back-btn').click(function() {
    $('.login-form').show();
    $('.forget-form').hide();
});

$('#user_activation_btn').click(function(){
    SM.App.User.createPassword();
});

$('#sign-in-btn').click(function(){
    SM.App.User.login();
});

$('#user-login-email').keypress(function(event){
    if(event.which == '13'){
        SM.App.User.login();
    }
});

$('#user-login-password').keypress(function(event){
    if(event.which == '13'){
        SM.App.User.login();
    }
});

$('#user-logout').click(function(){
    SM.App.User.logout();
});

$('#forgot_send').click(function(){
    SM.App.User.forgotPassword();
});

$('#reset_btn').click(function(){
    SM.App.User.resetPassword();
});

//  user profile
$('#profile_password_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.User.updatePassword();
});

$('#profile_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.User.updateAccount();
});

// user management events
$('#user-list-limit').change(function(){
    SM.App.User.list();
});

$('#user_filter_by_status').change(function(){
    SM.App.User.list();
});

$('#user_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.User.list();
    }
});

$('#user_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.User.create();
});

$('#user_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.User.remove();
});

$('#user_status_btn').click(function(e){
    e.preventDefault();
    SM.App.User.updateStatus();
});

// dealers events
$('#dealer-list-limit').change(function(){
    SM.App.Dealers.list();
});

$('#dealer_filter_by_status').change(function(){
    SM.App.Dealers.list();
});

$('#dealer_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Dealers.create();
}); 

$('#dealer_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Dealers.remove();
});

$('#dealer_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Dealers.updateStatus();
});

$('#dealer_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Dealers.list();
    }
});

// type management events
$('#type-list-limit').change(function(){
    SM.App.Types.list();
});

$('#type_filter_by_status').change(function(){
    SM.App.Types.list();
});

$('#type_filter_by_type').change(function(){
    SM.App.Types.list();
});

$('#type_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Types.list();
    }
});

$('#type_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Types.create();
});

$('#type_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Types.remove();
});

$('#type_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Types.updateStatus();
});

// material management events
$('#material-list-limit').change(function(){
    SM.App.Materials.list();
});

$('#material_filter_by_status').change(function(){
    SM.App.Materials.list();
});

$('#material_filter_by_type').change(function(){
    SM.App.Materials.list();
});

$('#material_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Materials.list();
    }
});

$('#material_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Materials.create();
});

$('#material_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Materials.remove();
});

$('#material_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Materials.updateStatus();
});

// shape management events
$('#shape-list-limit').change(function(){
    SM.App.Shapes.list();
});

$('#shape_filter_by_status').change(function(){
    SM.App.Shapes.list();
});

$('#shape_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Shapes.list();
    }
});

$('#shape_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Shapes.create();
});

$('#shape_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Shapes.remove();
});

$('#shape_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Shapes.updateStatus();
});

// use management events
$('#use-list-limit').change(function(){
    SM.App.Uses.list();
});

$('#use_filter_by_status').change(function(){
    SM.App.Uses.list();
});

$('#use_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Uses.list();
    }
});

$('#use_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Uses.create();
});

$('#use_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Uses.remove();
});

$('#use_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Uses.updateStatus();
});

// client management events
$('#client-list-limit').change(function(){
    SM.App.Clients.list();
});

$('#client_filter_by_status').change(function(){
    SM.App.Clients.list();
});

$('#client_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Clients.list();
    }
});

$('#client_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Clients.create();
});

$('#client_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Clients.remove();
});

$('#client_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Clients.updateStatus();
});

// project management events
$('#project-list-limit').change(function(){
    SM.App.Projects.list();
});

$('#project_filter_by_status').change(function(){
    SM.App.Projects.list();
});

$('#project_filter_by_material').change(function(){
    SM.App.Projects.list();
});

$('#project_filter_by_client').change(function(){
    SM.App.Projects.list();
});

$('#project_filter_by_type').change(function(){
    SM.App.Projects.list();
});

$('#project_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Projects.list();
    }
});

$('#project_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Projects.create();
});

$('#project_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Projects.remove();
});

$('#project_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Projects.updateStatus();
});

// team management events
$('#team-list-limit').change(function(){
    SM.App.Teams.list();
});

$('#team_filter_by_status').change(function(){
    SM.App.Teams.list();
});

$('#team_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Teams.list();
    }
});

$('#team_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Teams.create();
});

$('#team_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Teams.remove();
});

$('#team_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Teams.updateStatus();
});

// settings
$('.setting_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Settings.update();
});

// quotes management events
$('#quote-list-limit').change(function(){
    SM.App.Quotes.list();
});

$('#quote_filter_by_status').change(function(){
    SM.App.Quotes.list();
});

$('#quote_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Quotes.list();
    }
});

// contact management events
$('#contact-list-limit').change(function(){
    SM.App.Contacts.list();
});

$('#contact_filter_by_status').change(function(){
    SM.App.Contacts.list();
});

$('#contact_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Contacts.list();
    }
});

$('#contact_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Contacts.create();
});

$('#contact_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Contacts.remove();
});

$('#contact_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Contacts.updateStatus();
});

// partners events
$('#partner-list-limit').change(function(){
    SM.App.Partners.list();
});

$('#partner_filter_by_status').change(function(){
    SM.App.Partners.list();
});

$('#partner_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Partners.create();
}); 

$('#partner_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Partners.remove();
});

$('#partner_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Partners.updateStatus();
});

$('#partner_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Partners.list();
    }
});

// process events
$('#process-list-limit').change(function(){
    SM.App.Process.list();
});

$('#process_filter_by_status').change(function(){
    SM.App.Process.list();
});

$('#process_submit_btn').click(function(e){
    e.preventDefault();
    SM.App.Process.create();
}); 

$('#process_remove_btn').click(function(e){
    e.preventDefault();
    SM.App.Process.remove();
});

$('#process_status_btn').click(function(e){
    e.preventDefault();
    SM.App.Process.updateStatus();
});

$('#process_search_keyword').keypress(function(event){
    if(event.which == '13'){
        SM.App.Process.list();
    }
});