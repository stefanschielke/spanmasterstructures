var SM = SM || {}; // "If SM is not defined, make it equal to an empty object"
SM.App = SM.App || {};
SM.Config = SM.Config || {};

var localStorage;
var localStorageData = localStorage.getItem('sm_admin');
var jsonLocalStorageData = JSON.parse(localStorageData);

SM.Config = (function(){
	if(window.location.host == 'localhost'){
		var apiUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/api/';
		var appUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/admin';
		var siteUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public';
		var imageUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/pannel/images/';
	} else {
		var apiUrl = window.location.protocol+'//'+window.location.host+'/api/';
		var appUrl = window.location.protocol+'//'+window.location.host+'/admin';
		var siteUrl = window.location.protocol+'//'+window.location.host+'';
		var imageUrl = window.location.protocol+'//'+window.location.host+'/pannel/images/';
	}

	var getApiUrl = function(){
		return apiUrl;
	};
	var getAppUrl = function(){
		return appUrl;
	};
	var getSiteUrl = function(){
		return siteUrl;
	};
	var getImageUrl = function(){
		return imageUrl;
	};

	return {
		getApiUrl:getApiUrl,
		getAppUrl:getAppUrl,
		getSiteUrl:getSiteUrl,
		getImageUrl:getImageUrl
	}
}());

SM.App = (function () {
	var config = SM.Config;

	var init = function () {
		if (!window.console) window.console = {log: function(obj) {}};
		console.log('Application has been initialized...');
	};

	var pagination = function (data) {

		var classDisabledPrev = '';
		var classDisabledNext = '';
		var paginationShow = '';

		if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
			$('.general-pagination').hide();
			//$('.general-limit-div').hide();
		} else {
			if(data.pagination.current==data.pagination.first){
				classDisabledPrev="disable";
			}
			if(data.pagination.current==data.pagination.last){
				classDisabledNext="disable";
			}
			paginationShow+='<li >\
							      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+data.pagination.previous+' href="javascript:;">Previous</a>\
							    </li>';
			paginationShow+= '<li >\
							      <a class="general-pagination-click '+classDisabledNext+'" data-page='+data.pagination.next+' href="javascript:;">Next</a>\
							    </li>';
			paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

			$('.general-pagination').html(paginationShow);
			$('.general-pagination').show();
			$('.general-limit-div').show();
		}

	};

	var imgError = function(image) {
        image.onerror = "";
        image.src = SM.Config.getImageUrl()+"image-not-found.png";
        return true;
    }

	var minLimit = function(min,value){
    	if(value.length < min) {
      		return false;
     	} else {
      		return true;
     	}
    };

	var toTitleCase = function(val){
	  var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;

	  return val.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title){
	    if (index > 0 && index + match.length !== title.length &&
	      match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
	      (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
	      title.charAt(index - 1).search(/[^\s-]/) < 0) {
	      return match.toLowerCase();
	    }

	    if (match.substr(1).search(/[A-Z]|\../) > -1) {
	      return match;
	    }

	    return match.charAt(0).toUpperCase() + match.substr(1);
	  });
	};

	var validateAlphabet = function(name) {

		var alphabetWithSpace = /^[a-zA-Z\s]*$/;
   		return alphabetWithSpace.test(name);
	}

	var isValidUrl = function (url){
		var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;

		var regex = new RegExp(expression);
		if (!url.match(regex)) {
			return false;
		}
		return true;
	};

	var isNumberOnly = function (value) {
    	//var er = /^[0-9]*$/;
    	var er = /^\d*\.?\d*$/;
    	return er.test(value);
	};

	var isEmail=function(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}

	var validatePhone=function(phoneNumber) {

		var phoneNumberPattern = /^[\s0-9]*([0-9][\s()+-]*){6,20}$/;
   		return phoneNumberPattern.test(phoneNumber);
	}

    var getTodaysDate = function() {

    	var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1; //January is 0!
	    var yyyy = today.getFullYear();

	    if(dd<10){
	        dd='0'+dd
	    }
	    if(mm<10){
	        mm='0'+mm
	    }

	  	//  var todayDate = yyyy+'-'+mm+'-'+dd;
	  	var todayDate = yyyy+'-'+mm+'-'+dd;
    	return todayDate;
    }

    var isAcceptFileTypes=function(fileTypes) {
		var acceptFileTypes = "(\.|\/)(png|jpg|jpeg|gif)$";
   		acceptFileTypes = new RegExp(acceptFileTypes, "i");
   		return acceptFileTypes.test(fileTypes);
	}

	return {
		init:init,
		pagination:pagination,
		minLimit:minLimit,
		validateAlphabet:validateAlphabet,
		isEmail:isEmail,
		validatePhone:validatePhone,
		isValidUrl:isValidUrl,
		isNumberOnly:isNumberOnly,
		isEmail:isEmail,
		validatePhone:validatePhone,
		getTodaysDate:getTodaysDate,
		toTitleCase:toTitleCase,
		isAcceptFileTypes:isAcceptFileTypes,
		imgError:imgError
	};
}());

/* User Authentication */
SM.App.User = (function() {

	var config = SM.Config;
	var userApiUrl = config.getApiUrl()+'user';

	var login = function () {

		var email = $('#user-login-email');
		var password = $('#user-login-password');
		var error_msg = $('#user-login-error-msg');
		var signin_btn = $('#sign-in-btn');
		
		error_msg.removeClass('alert-success').removeClass('alert-danger');

		var errors = [];

		if ($.trim(email.val()) == ''){
			errors.push('Please enter email address.');
			email.parent().addClass('has-error');
		} else if(!SM.App.isEmail(email.val())){
			errors.push('Please enter valid email address.');
			email.parent().addClass('has-error');
		} else {
			email.parent().removeClass('has-error');
		}

		if ($.trim(password.val()) == ''){
			errors.push('Please enter password');
			password.parent().addClass('has-error');
		}  else {
			password.parent().removeClass('has-error');
		}

		if(errors.length < 1) {
			var jsonData = {
								email:$.trim(email.val()),
								password:$.trim(password.val()),
								is_admin:1
							}

			var request = $.ajax({
				url: userApiUrl+'/login',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});

			signin_btn.addClass('prevent-click');
			$('#sign-in-loader').show();

			request.done(function(data){
				signin_btn.removeClass('prevent-click');
				$('#sign-in-loader').hide();
				if(data.error) {
					error_msg.addClass('alert-danger').html(data.error.messages).show();
				} else {
					localStorage.setItem('PS_admin', JSON.stringify(data.data));
					error_msg.addClass('alert-success').html('You have been logged in successfully.').show().delay(2000).fadeOut(function(){
						$(this).html('');
						window.location.href = config.getAppUrl()+'/users';
				    });
				}

			});

			request.fail(function(jqXHR, textStatus){
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var html = '';
				for(var i in jsonResponse.error.messages) {
					html += jsonResponse.error.messages[i];
				}
				signin_btn.removeClass('prevent-click');
				$('#sign-in-loader').hide();
				error_msg.removeClass('alert-success');
				error_msg.html(html).addClass('alert-danger').show();

			});

		}else{
			signin_btn.removeClass('prevent-click');
			$('#sign-in-loader').hide();
			error_msg.removeClass('alert-success');
			error_msg.html(errors[0]).addClass('alert-danger').show();
		}
	};

	var logout = function(){
		var request = $.ajax({

			url: userApiUrl+'/logout',
			data: {is_admin:1},
			type: 'POST',
			dataType:'json'
		});

		request.done(function(data){
			window.localStorage.removeItem('ps_admin');
		    var url = config.getAppUrl();
		    window.location.href = url;
		});
	};

	var createPassword = function () {

		var code 				= '';	
		var password 			= $('#create_password');
		var confirm_password 	= $('#create_confirm_password');
		var create_btn			= $('#user_activation_btn');

		code = $('#activation_key');		
		
		var errors = [];

		if ($.trim(code.val()) == ''){
			errors.push('Activation code is not found.');
		} 

		if ($.trim(password.val()) == '') {
			errors.push('Please enter new password');
			$('#new_password').parent().parent().addClass('has-error');
		}  else {
			if (!SM.App.minLimit(6,$.trim(password.val()))) {
	    		$('#new_password').parent().parent().addClass('has-error');
	    		errors.push('Password must be 6 characters long.');
	   		} else {
	   			$('#new_password').parent().parent().removeClass('has-error');
	   		}
		}

		if ($.trim(confirm_password.val()) == '') {
			errors.push('Please enter confirm password');
			$('#confirm_password').parent().parent().addClass('has-error');
		} else {
			if (!SM.App.minLimit(6,$.trim(confirm_password.val()))) {
	    		$('#confirm_password').parent().parent().addClass('has-error');
	    		errors.push('Password must be 6 characters long.');
	   		} else if ($.trim(password.val()) != $.trim(confirm_password.val())) {
	   			$('#confirm_password').parent().parent().addClass('has-error');
	    		errors.push('Confirm Password must be same as New Password.');
	   		}else {
	    		$('#confirm_password').parent().parent().removeClass('has-error');
	   		}
		}
		
		if (errors.length < 1) {
			var jsonData = {
							code:$.trim(code.val()),
							password:$.trim(password.val()),
							confirm_password:$.trim(confirm_password.val()),
							}

			var request = $.ajax({
				
				url: userApiUrl+'/create-password',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});

			create_btn.addClass('prevent-click');
			$('#submit_loader').show();
			request.done(function(data){
				$('#submit_loader').hide();
				if(data.response) {
				 	if(data.response.code == 200) {
				 		$('#msg_div').removeClass('alert-danger').addClass('alert-success').html(data.response.messages).show().delay(2000).fadeOut(function(){;
							$(this).html('');
							create_btn.removeClass('prevent-click');
							window.location.href = config.getAppUrl()+'';
				    	});
				 	}
				} else if(data.error) {
				 	if(data.error.code == 401) {
				 		$('#msg_div').removeClass('alert-success').addClass('alert-danger').html(data.error.messages).show().delay(2000).fadeOut(function(){;
							$(this).html('');
							window.location.href = config.getAppUrl()+'';
				    	});
				 	}
				 	create_btn.removeClass('prevent-click');
					var errors = 'An error occurred while creating your password.';
					if (data.error.messages && data.error.messages.length > 0) {
						errors = data.error.messages[0];
					}
				 	$('#msg_div').removeClass('alert-success').addClass('alert-danger').html(errors).show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				create_btn.removeClass('prevent-click');
				$('#submit_loader').hide();

				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred while creating your password.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');		
				$('#msg_div').html(error).addClass('alert-danger');
			});
		}else{
			create_btn.removeClass('prevent-click');
			$('#submit_loader').hide();
			$('#msg_div').removeClass('alert-success');		
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
		}
	};

	var forgotPassword = function () {
		var email = $('#forgot_email');
		var forgot_btn=$('#forgot_send');
		var errors = [];

		if ($.trim(email.val()) == ''){
			errors.push('Please enter email');
			email.parent().addClass('has-error');
		} else if(!SM.App.isEmail(email.val())){
			errors.push('Please enter valid email address.');
			email.parent().addClass('has-error');
		} else {
			email.parent().removeClass('has-error');
		}
		if(errors.length < 1) {
			var jsonData = {
								email:$.trim(email.val())
							}

			var request = $.ajax({
				url: userApiUrl+'/forgot-password',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});
			forgot_btn.addClass('prevent-click');
			$('#forgot-loader').show();
			request.done(function(data){
				forgot_btn.removeClass('show-spinner');
				$('#forgot-loader').hide();
				if(data.success) {
				 	if(data.success.code == 200) {
						$('#forgot-error-message').removeClass('alert-danger').addClass('alert-success').html(data.success.messages).show().delay(2000).fadeOut(function(){;
							$(this).html('');
							email.val('');
							forgot_btn.removeClass('prevent-click');
							$('.forget-form').hide();
							$('.login-form').show();
    						
							/*$('.forget-form').addClass('slideOutRight').removeClass('slideInRight show');
							$(".login-form").addClass('slideInLeft').removeClass('slideOutLeft hide');*/
					    });
				 	}
				} else if(data.error) {
					forgot_btn.removeClass('prevent-click');
						$('#forgot-error-message').removeClass('alert-success').addClass('alert-danger').html(data.error.messages).show().delay(2000).fadeOut(function(){;
							$(this).html('');
					    });
				}

			});

			request.fail(function(jqXHR, textStatus){
				forgot_btn.removeClass('prevent-click');
				$('#forgot-loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var html = '';
				for(var i in jsonResponse.error.messages) {
					html += jsonResponse.error.messages[i];
				}
				$('#forgot-error-message').removeClass('alert-success');
				$('#forgot-error-message').html(html).addClass('alert-danger').show();
			});
		} else {
			forgot_btn.removeClass('prevent-click');
			$('#forgot-loader').hide();
			$('#forgot-error-message').removeClass('alert-success');
			$('#forgot-error-message').html(errors[0]).addClass('alert-danger').show();
		}
	};

	var resetPassword = function () {

		var code 				= '';
		var password 			= $('#new_password');
		var confirm_password 	= $('#confirm_password');
		var reset_btn			=$('#reset_btn');

		code = $('#recover_password_key');

		var errors = [];

		if ($.trim(code.val()) == ''){
			errors.push('Recover code is not found.');
		}

		if ($.trim(password.val()) == '') {
			errors.push('Please enter new password');
			password.parent().parent().addClass('has-error');
		}  else {
			if (!SM.App.minLimit(6,$.trim(password.val()))) {
	    		password.parent().parent().addClass('has-error');
	    		errors.push('Password must be 6 characters long.');
	   		} else {
	   			password.parent().parent().removeClass('has-error');
	   		}
		}

		if ($.trim(confirm_password.val()) == '') {
			errors.push('Please enter confirm password');
			confirm_password.parent().parent().addClass('has-error');
		} else {
			if (!SM.App.minLimit(6,$.trim(confirm_password.val()))) {
	    		confirm_password.parent().parent().addClass('has-error');
	    		errors.push('Password must be 6 characters long.');
	   		} else if ($.trim(password.val()) != $.trim(confirm_password.val())) {
	   			confirm_password.parent().parent().addClass('has-error');
	    		errors.push('Confirm Password must be same as New Password.');
	   		} else {
	    		confirm_password.parent().parent().removeClass('has-error');
	   		}
		}

		if (errors.length < 1) {
			var jsonData = {
							code:$.trim(code.val()),
							password:$.trim(password.val()),
							confirm_password:$.trim(confirm_password.val())
							}

			var request = $.ajax({
				url: userApiUrl+'/reset-password',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});

			reset_btn.addClass('prevent-click');
			$('#reset_submit_loader').show();
			request.done(function(data){
				$('#reset_submit_loader').hide();
				if(data.response) {
				 	if(data.response.code == 200) {
				 		$('#user-reset-error-msg').removeClass('alert-danger').addClass('alert-success').html(data.response.messages).show().delay(2000).fadeOut(function(){;
							$(this).html('');
							reset_btn.removeClass('prevent-click');
							window.location.href = config.getAppUrl()+'';
				    	});
				 	}
				} else if(data.error) {
					var errors = 'An error occurred while resetting your password.';
					if (data.error.messages && data.error.messages.length > 0) {
						errors = data.error.messages[0];
					}
					reset_btn.removeClass('prevent-click');
				 	$('#user-reset-error-msg').removeClass('alert-success').addClass('alert-danger').html(errors).show();
				}

			});

			request.fail(function(jqXHR, textStatus){
				reset_btn.removeClass('prevent-click');
				$('#reset_submit_loader').hide();

				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred while creating your password.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#user-reset-error-msg').removeClass('alert-success');		
				$('#user-reset-error-msg').html(error).addClass('alert-danger').show();
			});
		}else{
			reset_btn.removeClass('prevent-click');
			$('#user-reset-error-msg').removeClass('alert-success');
			$('#user-reset-error-msg').html(errors[0]).addClass('alert-danger').show();
		}
	};

	var updatePassword = function (){
		var errors = [];

		var old_password 		= $('#old_password');
		var new_password 		= $('#new_password');
		var confirm_password 	= $('#confirm_password');
			
		if ($.trim(old_password.val()) == '') {
			errors.push('Please enter current password.');
			old_password.parent().addClass('has-error');    
		}else{
			old_password.parent().removeClass('has-error');
		}

		if ($.trim(new_password.val()) == '') {
			errors.push('Please enter new password.');
			new_password.parent().addClass('has-error');    
		}else{
			new_password.parent().removeClass('has-error');
		}

		if ($.trim(confirm_password.val()) == '') {
			errors.push('Please enter confirm password.');
			confirm_password.parent().addClass('has-error');    
		}else{
			confirm_password.parent().removeClass('has-error');
		}

		if($.trim(new_password.val()) != $.trim(confirm_password.val()) ){
			errors.push('Password does not match.');
		} else {
			if($.trim(new_password.val()).length < 6){
				errors.push('Password must be atleast 6 characters long.');
			}
		}
	
		if (errors.length < 1 ) {

			var jsonData = {};
			jsonData.old_password = $.trim(old_password.val());
			jsonData.new_password = $.trim(confirm_password.val());

			var request = $.ajax({	
				url: userApiUrl+'/update-password',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});
            	
            $('#profile_password_submit_btn').addClass('prevent-click');
            $('#profile_password_submit_loader').show();
			request.done(function(data){
				$('#profile_password_submit_loader').hide();
				if(data.success) {		
					$('#save_account_password_msg').removeClass('alert-danger show-alert-message');
					$('#save_account_password_msg').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function() {
						$(this).html('');
				    	$(this).removeClass('alert-danger');
				    	$('#profile_password_submit_btn').removeClass('prevent-click');
				    	new_password.val('');
				    	old_password.val('');
				    	confirm_password.val('');
				    });		
				} else if(data.error) {
					$('#profile_password_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#save_account_password_msg').removeClass('alert-success');
					$('#save_account_password_msg').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#profile_password_submit_btn').removeClass('prevent-click');
				$('#profile_password_submit_loader').hide();

				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred while updating password.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#save_account_password_msg').html(error).addClass('alert-danger').show();
			});

		} else {
			$('#profile_password_submit_btn').removeClass('prevent-click');
			$('#save_account_password_msg').removeClass('alert-success');
			$('#save_account_password_msg').html(errors[0]).addClass('alert-danger').show();
		}		
	};

	var updateAccount = function (){
		var errors = [];

		var name 		= $('#profile_name');
			
		if ($.trim(name.val()) == '') {
			errors.push('Please enter full name.');
			name.parent().addClass('has-error');    
		}else{
			name.parent().removeClass('has-error');
		}

		if (errors.length < 1 ) {

			var jsonData = {};
			jsonData.name = $.trim(name.val());

			var request = $.ajax({	
				url: userApiUrl+'/update-account',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});
            	
            $('#profile_submit_btn').addClass('prevent-click');
            $('#profile_submit_loader').show();
			request.done(function(data){
				$('#profile_submit_loader').hide();
				if(data.success) {		
					$('#profile_msg').removeClass('alert-danger show-alert-message');
					$('#profile_msg').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function() {
						$(this).html('');
				    	$(this).removeClass('alert-danger');
				    	$('#profile_submit_btn').removeClass('prevent-click');
				    });		
				} else if(data.error) {
					$('#profile_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#profile_msg').removeClass('alert-success');
					$('#profile_msg').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#profile_submit_btn').removeClass('prevent-click');
				$('#profile_submit_loader').hide();

				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred while updating profile information.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#profile_msg').html(error).addClass('alert-danger').show();
			});

		} else {
			$('#profile_submit_btn').removeClass('prevent-click');
			$('#profile_msg').removeClass('alert-success');
			$('#profile_msg').html(errors[0]).addClass('alert-danger').show();
		}		
	};

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#user_filter_by_status').val() || '';
		var keyword 		= $('#user_search_keyword').val() || '';
		var limit 			= $('#user-list-limit').val() || 0;
		var data 			= {};
		var total_users 	= $('#total_users');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: userApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var users = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_users.html(paginations.total);

			if(users.length > 0) {

				$('#users_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Full Name </th>\
		                                        <th> Email </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(users).each(function(index, user){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof user.name != 'undefined' && typeof user.name !== null && user.name!='' ) {
						user.name = user.name;
					} else {
						user.name = 'N/A';									
					}

					if (typeof user.email != 'undefined' && typeof user.email !== null && user.email!='' ) {
						user.email = user.email;
					} else {
						user.email = 'N/A';									
					}

					if (typeof user.is_active != 'undefined' && typeof user.is_active !== null ) {
						if(user.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					
					html += '<tr>\
                                <td class="highlight"> '+user.id+' </td>\
                                <td class="hidden-xs"> '+user.name+' </td>\
                                <td> '+user.email+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/user?id='+user.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_user" data-id="'+user.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' user_status" data-id="'+user.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#users_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#users_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.user-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.User.list(page);
		    });

		    $('.delete_user').unbind('click').bind('click',function(e){
				e.preventDefault();
				var user_id  = $(this).attr('data-id');
				$('#hidden_action_user_id').val(user_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.user_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var user_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_user_id').val(user_id);
				$('#hidden_action_user_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving users.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#users_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var user_id = $('#updated_user_id').val();
		var name 	= $('#user_name');
		var email 	= $('#user_email');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter full name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(email.val()) == '') {
			errors.push('Please enter email.');
			email.parent().addClass('has-error');
		} else {
			if(!SM.App.isEmail(email.val())){
				errors.push('Please enter valid email address.');
			} else {
				email.parent().removeClass('has-error');
			}			
		}

		if (errors.length < 1) {

			var jsonData = {
								id:user_id,
								name:$.trim(name.val()),
								email:$.trim(email.val())															
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: userApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: userApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#user_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/users';
					    });		 	
				} else if(data.error) {
					$('#user_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#user_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#user_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_user_id').val();
		$('#user_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: userApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'User has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#user_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.User.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this user.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#user_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#user_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this user.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_user_id').val();
		jsonData.status = $('#hidden_action_user_status').val();

		var request = $.ajax({
			url: userApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#user_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#user_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.User.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating user status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#user_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#user_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating user status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};
	return {
		createPassword:createPassword,
		forgotPassword:forgotPassword,
		resetPassword:resetPassword,
		login:login,
		logout:logout,
		updatePassword:updatePassword,
		updateAccount:updateAccount,
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus
	}
}());

/* Dealers Management */
SM.App.Dealers = (function() {

	var config = SM.Config;
	var dealerApiUrl = config.getApiUrl()+'dealer';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#dealer_filter_by_status').val() || '';
		var keyword 		= $('#dealer_search_keyword').val() || '';
		var limit 			= $('#dealer-list-limit').val() || 0;
		var data 			= {};
		var total_dealers 	= $('#total_dealers');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: dealerApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var dealers = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_dealers.html(paginations.total);

			if(dealers.length > 0) {

				$('#dealers_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th> Image </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(dealers).each(function(index, dealer){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof dealer.name != 'undefined' && typeof dealer.name !== null && dealer.name!='' ) {
						dealer.name = dealer.name;
					} else {
						dealer.name = 'N/A';									
					}

					if (typeof dealer.image != 'undefined' && dealer.image!=null ) {
						dealer.image = dealer.image;
					} else {
						dealer.image = 'image-not-found.png';									
					}
					
					if (typeof dealer.is_active != 'undefined' && typeof dealer.is_active !== null ) {
						if(dealer.is_active == 1){
							statusText = 'Active';
							is_active = 0;
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else if (dealer.is_active == 0){
							statusText = 'InActive';
							is_active = 1;
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}

					html += '<tr>\
                                <td class="highlight"> '+dealer.id+' </td>\
                                <td class="hidden-xs"> '+dealer.name+' </td>\
                                <td> <img src="'+config.getSiteUrl()+'/files/dealer/'+dealer.image+'" width="170px" height="60px"></td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/dealer?id='+dealer.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_dealer" data-id="'+dealer.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' dealer_status" data-id="'+dealer.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#dealers_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#dealers_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.dealer-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Dealers.list(page);
		    });

		    $('.delete_dealer').unbind('click').bind('click',function(e){
				e.preventDefault();
				var dealer_id  = $(this).attr('data-id');
				$('#hidden_action_dealer_id').val(dealer_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.dealer_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var dealer_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_dealer_id').val(dealer_id);
				$('#hidden_action_dealer_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving dealers.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#dealers_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var dealer_id = $('#updated_dealer_id').val();
		var name 	= $('#dealer_name');
		var image 	= $('#hidden-dealer-image');

		if ($.trim(image.val()) == '') {
			errors.push('Please enter image.');
			image.parent().addClass('has-error');
		} else {
			image.parent().removeClass('has-error');	
		}

		if (errors.length < 1) {

			var jsonData = {
								id:dealer_id,
								name:$.trim(name.val()),
								image:$.trim(image.val())														
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: dealerApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: dealerApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#dealer_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/dealers';
					    });		 	
				} else if(data.error) {
					$('#dealer_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#dealer_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#dealer_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var view = function(id){
        var jsonData = {id:id};
		var request = $.ajax({
			url: dealerApiUrl+'/view',
			data: jsonData,
			type: 'GET',
			dataType:'json'
		});

		request.success(function(data){
			$('#dealer_name').val(data.name)
			
		});

		request.fail(function(jqXHR, textStatus){
			// do something
		});
    };

	var remove = function (){
		var id = $('#hidden_action_dealer_id').val();
		$('#dealer_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: dealerApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'dealer has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#dealer_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    SM.App.Dealers.list();	
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this dealer.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#dealer_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#dealer_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this dealer.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		
		var jsonData = {};
		jsonData.id = $('#hidden_action_dealer_id').val();
		jsonData.status = $('#hidden_action_dealer_status').val();

		var request = $.ajax({
			url: dealerApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#dealer_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#dealer_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Dealers.list();		
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating dealer status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#dealer_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#dealer_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating dealer status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		list:list,
		create:create,
		remove:remove,
		updateStatus:updateStatus,
		view:view
	}
}());

/* Type Management */
SM.App.Types = (function() {

	var config = SM.Config;
	var typeApiUrl = config.getApiUrl()+'type';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#type_filter_by_status').val() || '';
		//var filterByType 	= $('#type_filter_by_type').val() || '';
		var keyword 		= $('#type_search_keyword').val() || '';
		var limit 			= $('#type-list-limit').val() || 0;
		var data 			= {};
		var total_types 	= $('#total_types');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		//data.filter_by_type = filterByType;
		
		var request = $.ajax({
			url: typeApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var types = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_types.html(paginations.total);

			if(types.length > 0) {

				$('#types_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th width="25%"> Description </th>\
		                                        <th> Associated Projects </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(types).each(function(index, type){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof type.name != 'undefined' && typeof type.name !== null && type.name!='' ) {
						type.name = type.name;
					} else {
						type.name = 'N/A';									
					}

					if (typeof type.type != 'undefined' && typeof type.type !== null && type.type!='' ) {
						type.type = type.type;
					} else {
						type.type = 'N/A';									
					}

					if (typeof type.description != 'undefined' && typeof type.description !== null && type.description!='' ) {
						type.description = type.description;
					} else {
						type.description = 'N/A';									
					}

					if (typeof type.associated_projects != 'undefined' && typeof type.associated_projects !== null && type.associated_projects!='' ) {
						type.associated_projects = type.associated_projects;
					} else {
						type.associated_projects = '0';									
					}

					if (typeof type.is_active != 'undefined' && typeof type.is_active !== null ) {
						if(type.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					
					html += '<tr>\
                                <td class="highlight"> '+type.id+' </td>\
                                <td class="hidden-xs"> '+type.name+' </td>\
                                <td> '+type.description+' </td>\
                                <td> '+type.associated_projects+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/type?id='+type.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_type" data-id="'+type.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' type_status" data-id="'+type.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#types_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#types_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.type-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Types.list(page);
		    });

		    $('.delete_type').unbind('click').bind('click',function(e){
				e.preventDefault();
				var type_id  = $(this).attr('data-id');
				$('#hidden_action_type_id').val(type_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.type_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var type_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_type_id').val(type_id);
				$('#hidden_action_type_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving types.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#types_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var type_id = $('#updated_type_id').val();
		var name 	= $('#type_name');
		var description 	= $('#type_description');
		var type 	= $('#type_type');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(description.val()) == '') {
			errors.push('Please enter description.');
			description.parent().addClass('has-error');
		} else {
			description.parent().removeClass('has-error');		
		}

		/*if ($.trim(type.val()) == '') {
			errors.push('Please select type.');
			type.parent().parent().addClass('has-error');
		} else {
			type.parent().parent().removeClass('has-error');	
		}*/

		if (errors.length < 1) {

			var jsonData = {
								id:type_id,
								name:$.trim(name.val()),
								description:$.trim(description.val()),
								//type:$.trim(type.val())															
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: typeApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: typeApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#type_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/types';
					    });		 	
				} else if(data.error) {
					$('#type_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#type_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#type_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_type_id').val();
		$('#type_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: typeApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'Type has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.Types.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this type.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#type_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#type_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this role.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_type_id').val();
		jsonData.status = $('#hidden_action_type_status').val();

		var request = $.ajax({
			url: typeApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#type_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Types.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating type status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#type_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#type_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating type status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus
	}
}());

/* Material Management */
SM.App.Materials = (function() {

	var config = SM.Config;
	var materialApiUrl = config.getApiUrl()+'material';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#material_filter_by_status').val() || '';
		//var filterByType 	= $('#material_filter_by_type').val() || '';
		var keyword 		= $('#material_search_keyword').val() || '';
		var limit 			= $('#material-list-limit').val() || 0;
		var data 			= {};
		var total_materials 	= $('#total_materials');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		//data.filter_by_type = filterByType;
		
		var request = $.ajax({
			url: materialApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var materials = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_materials.html(paginations.total);

			if(materials.length > 0) {

				$('#materials_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th width="25%"> Description </th>\
		                                        <th> Associated Projects </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(materials).each(function(index, material){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof material.name != 'undefined' && typeof material.name !== null && material.name!='' ) {
						material.name = material.name;
					} else {
						material.name = 'N/A';									
					}

					if (typeof material.type != 'undefined' && typeof material.type !== null && material.type!='' ) {
						material.type = material.type;
					} else {
						material.type = 'N/A';									
					}

					if (typeof material.description != 'undefined' && typeof material.description !== null && material.description!='' ) {
						material.description = material.description;
					} else {
						material.description = 'N/A';									
					}

					if (typeof material.associated_projects != 'undefined' && typeof material.associated_projects !== null && material.associated_projects!='' ) {
						material.associated_projects = material.associated_projects;
					} else {
						material.associated_projects = '0';									
					}

					if (typeof material.is_active != 'undefined' && typeof material.is_active !== null ) {
						if(material.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					
					html += '<tr>\
                                <td class="highlight"> '+material.id+' </td>\
                                <td class="hidden-xs"> '+material.name+' </td>\
                                <td> '+material.description+' </td>\
                                <td> '+material.associated_projects+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/material?id='+material.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_material" data-id="'+material.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' material_status" data-id="'+material.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#materials_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#materials_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.material-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Materials.list(page);
		    });

		    $('.delete_material').unbind('click').bind('click',function(e){
				e.preventDefault();
				var material_id  = $(this).attr('data-id');
				$('#hidden_action_material_id').val(material_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.material_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var material_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_material_id').val(material_id);
				$('#hidden_action_material_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving materials.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#materials_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var material_id = $('#updated_material_id').val();
		var name 	= $('#material_name');
		var description 	= $('#material_description');
		var type 	= $('#material_type');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(description.val()) == '') {
			errors.push('Please enter description.');
			description.parent().addClass('has-error');
		} else {
			description.parent().removeClass('has-error');		
		}

		/*if ($.trim(type.val()) == '') {
			errors.push('Please select type.');
			type.parent().parent().addClass('has-error');
		} else {
			type.parent().parent().removeClass('has-error');	
		}*/

		if (errors.length < 1) {

			var jsonData = {
								id:material_id,
								name:$.trim(name.val()),
								description:$.trim(description.val()),
								//type:$.trim(type.val())															
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: materialApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: materialApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#material_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/materials';
					    });		 	
				} else if(data.error) {
					$('#material_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#material_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#material_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_material_id').val();
		$('#material_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: materialApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'Material has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#material_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.Materials.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this material.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#material_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#material_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this material.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_material_id').val();
		jsonData.status = $('#hidden_action_material_status').val();

		var request = $.ajax({
			url: materialApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#material_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Materials.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating material status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#material_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#material_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating material status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus
	}
}());

/* Shape Management */
SM.App.Shapes = (function() {

	var config = SM.Config;
	var shapeApiUrl = config.getApiUrl()+'shapes';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#shape_filter_by_status').val() || '';
		var keyword 		= $('#shape_search_keyword').val() || '';
		var limit 			= $('#shape-list-limit').val() || 0;
		var data 			= {};
		var total_shapes 	= $('#total_shapes');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: shapeApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var shapes = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_shapes.html(paginations.total);

			if(shapes.length > 0) {

				$('#shapes_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th width="25%"> Description </th>\
		                                        <th> Associated Projects </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(shapes).each(function(index, shape){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof shape.name != 'undefined' && typeof shape.name !== null && shape.name!='' ) {
						shape.name = shape.name;
					} else {
						shape.name = 'N/A';									
					}

					if (typeof shape.description != 'undefined' && typeof shape.description !== null && shape.description!='' ) {
						shape.description = shape.description;
					} else {
						shape.description = 'N/A';									
					}

					if (typeof shape.associated_projects != 'undefined' && typeof shape.associated_projects !== null && shape.associated_projects!='' ) {
						shape.associated_projects = shape.associated_projects;
					} else {
						shape.associated_projects = '0';									
					}

					if (typeof shape.is_active != 'undefined' && typeof shape.is_active !== null ) {
						if(shape.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					
					html += '<tr>\
                                <td class="highlight"> '+shape.id+' </td>\
                                <td class="hidden-xs"> '+shape.name+' </td>\
                                <td> '+shape.description+' </td>\
                                <td> '+shape.associated_projects+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/shape?id='+shape.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_shape" data-id="'+shape.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' shape_status" data-id="'+shape.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#shapes_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#shapes_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.shape-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Shapes.list(page);
		    });

		    $('.delete_shape').unbind('click').bind('click',function(e){
				e.preventDefault();
				var shape_id  = $(this).attr('data-id');
				$('#hidden_action_shape_id').val(shape_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.shape_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var shape_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_shape_id').val(shape_id);
				$('#hidden_action_shape_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving shapes.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#shapes_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var shape_id = $('#updated_shape_id').val();
		var name 	= $('#shape_name');
		var description 	= $('#shape_description');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(description.val()) == '') {
			errors.push('Please enter description.');
			description.parent().addClass('has-error');
		} else {
			description.parent().removeClass('has-error');		
		}

		if (errors.length < 1) {

			var jsonData = {
								id:shape_id,
								name:$.trim(name.val()),
								description:$.trim(description.val())												
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: shapeApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: shapeApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#shape_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/shapes';
					    });		 	
				} else if(data.error) {
					$('#shape_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#shape_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#shape_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_shape_id').val();
		$('#shape_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: shapeApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'Shape has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#shape_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.Shapes.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this shape.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#shape_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#shape_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this shape.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_shape_id').val();
		jsonData.status = $('#hidden_action_shape_status').val();

		var request = $.ajax({
			url: shapeApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#shape_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Shapes.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating shape status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#shape_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#shape_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating shape status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus
	}
}());

/* Use Management */
SM.App.Uses = (function() {

	var config = SM.Config;
	var useApiUrl = config.getApiUrl()+'uses';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#use_filter_by_status').val() || '';
		var keyword 		= $('#use_search_keyword').val() || '';
		var limit 			= $('#use-list-limit').val() || 0;
		var data 			= {};
		var total_uses 	= $('#total_uses');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: useApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var uses = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_uses.html(paginations.total);

			if(uses.length > 0) {

				$('#uses_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th width="25%"> Description </th>\
		                                        <th> Associated Projects </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(uses).each(function(index, use){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof use.name != 'undefined' && typeof use.name !== null && use.name!='' ) {
						use.name = use.name;
					} else {
						use.name = 'N/A';									
					}

					if (typeof use.description != 'undefined' && typeof use.description !== null && use.description!='' ) {
						use.description = use.description;
					} else {
						use.description = 'N/A';									
					}

					if (typeof use.associated_projects != 'undefined' && typeof use.associated_projects !== null && use.associated_projects!='' ) {
						use.associated_projects = use.associated_projects;
					} else {
						use.associated_projects = '0';									
					}

					if (typeof use.is_active != 'undefined' && typeof use.is_active !== null ) {
						if(use.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					
					html += '<tr>\
                                <td class="highlight"> '+use.id+' </td>\
                                <td class="hidden-xs"> '+use.name+' </td>\
                                <td> '+use.description+' </td>\
                                <td> '+use.associated_projects+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/use?id='+use.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_use" data-id="'+use.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' use_status" data-id="'+use.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#uses_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#uses_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.use-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Uses.list(page);
		    });

		    $('.delete_use').unbind('click').bind('click',function(e){
				e.preventDefault();
				var use_id  = $(this).attr('data-id');
				$('#hidden_action_use_id').val(use_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.use_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var use_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_use_id').val(use_id);
				$('#hidden_action_use_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving uses.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#uses_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var use_id = $('#updated_use_id').val();
		var name 	= $('#use_name');
		var description 	= $('#use_description');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(description.val()) == '') {
			errors.push('Please enter description.');
			description.parent().addClass('has-error');
		} else {
			description.parent().removeClass('has-error');		
		}

		if (errors.length < 1) {

			var jsonData = {
								id:use_id,
								name:$.trim(name.val()),
								description:$.trim(description.val())												
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: useApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: useApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#use_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/uses';
					    });		 	
				} else if(data.error) {
					$('#use_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#use_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#use_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_use_id').val();
		$('#use_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: useApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'Usage has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#use_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.Uses.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this usage.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#use_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#use_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this usage.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_use_id').val();
		jsonData.status = $('#hidden_action_use_status').val();

		var request = $.ajax({
			url: useApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#use_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Uses.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating usage status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#use_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#use_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating usage status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus
	}
}());

/* Clients Management */
SM.App.Clients = (function() {

	var config = SM.Config;
	var clientApiUrl = config.getApiUrl()+'client';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#client_filter_by_status').val() || '';
		var keyword 		= $('#client_search_keyword').val() || '';
		var limit 			= $('#client-list-limit').val() || 0;
		var data 			= {};
		var total_clients 	= $('#total_clients');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: clientApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var clients = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_clients.html(paginations.total);

			if(clients.length > 0) {

				$('#clients_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th> Associated Projects </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(clients).each(function(index, client){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof client.name != 'undefined' && typeof client.name !== null && client.name!='' ) {
						client.name = client.name;
					} else {
						client.name = 'N/A';									
					}

					if (typeof client.designation != 'undefined' && typeof client.designation !== null && client.designation!='' ) {
						client.designation = client.designation;
					} else {
						client.designation = 'N/A';									
					}

					if (typeof client.associated_projects != 'undefined' && typeof client.associated_projects !== null && client.associated_projects!='' ) {
						client.associated_projects = client.associated_projects;
					} else {
						client.associated_projects = '0';									
					}

					if (typeof client.is_active != 'undefined' && typeof client.is_active !== null ) {
						if(client.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					
					html += '<tr>\
                                <td class="highlight"> '+client.id+' </td>\
                                <td class="hidden-xs"> '+client.name+' </td>\
                                <td class="hidden-xs"> '+client.associated_projects+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/client?id='+client.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_client" data-id="'+client.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' client_status" data-id="'+client.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#clients_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#clients_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.client-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.clients.list(page);
		    });

		    $('.delete_client').unbind('click').bind('click',function(e){
				e.preventDefault();
				var client_id  = $(this).attr('data-id');
				$('#hidden_action_client_id').val(client_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.client_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var client_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_client_id').val(client_id);
				$('#hidden_action_client_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving manufacturers.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#clients_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var client_id = $('#updated_client_id').val();
		var name 	= $('#client_name');
		var designation 	= $('#client_designation');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		/*if ($.trim(designation.val()) == '') {
			errors.push('Please enter designation.');
			designation.parent().addClass('has-error');
		} else {
			designation.parent().removeClass('has-error');		
		}*/

		if (errors.length < 1) {

			var jsonData = {
								id:client_id,
								name:$.trim(name.val()),
								designation:$.trim(designation.val())													
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: clientApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: clientApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#client_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/clients';
					    });		 	
				} else if(data.error) {
					$('#client_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#client_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#client_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_client_id').val();
		$('#client_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: clientApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'Manufacturer has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#client_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.Clients.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this manufacturer.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#client_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#client_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this manufacturer.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_client_id').val();
		jsonData.status = $('#hidden_action_client_status').val();

		var request = $.ajax({
			url: clientApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#client_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Clients.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating manufacturer status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#client_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#client_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating manufacturer status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus
	}
}());

/* Projects Management */
SM.App.Projects = (function() {

	var config = SM.Config;
	var projectApiUrl = config.getApiUrl()+'project';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#project_filter_by_status').val() || '';
		var filterByType 	= $('#project_filter_by_type').val() || '';
		var filterByMaterial 	= $('#project_filter_by_material').val() || '';
		var filterByClient 	= $('#project_filter_by_client').val() || '';
		var keyword 		= $('#project_search_keyword').val() || '';
		var limit 			= $('#project-list-limit').val() || 0;
		var data 			= {};
		var total_projects 	= $('#total_projects');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		data.filter_by_type = filterByType;
		data.filter_by_material = filterByMaterial;
		data.filter_by_client = filterByClient;
		
		var request = $.ajax({
			url: projectApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var projects = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_projects.html(paginations.total);

			if(projects.length > 0) {

				$('#projects_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th> Size </th>\
		                                        <th> Start date </th>\
		                                        <th> End Date </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(projects).each(function(index, project){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof project.name != 'undefined' && typeof project.name !== null && project.name!='' ) {
						project.name = project.name;
					} else {
						project.name = 'N/A';									
					}

					if (typeof project.size != 'undefined' && typeof project.size !== null && project.size!='' ) {
						project.size = project.size;
					} else {
						project.size = 'N/A';									
					}

					if (typeof project.description != 'undefined' && typeof project.description !== null && project.description!='' ) {
						project.description = project.description;
					} else {
						project.description = 'N/A';									
					}

					if (typeof project.start_date_formatted != 'undefined' && typeof project.start_date_formatted !== null && project.start_date_formatted!='' ) {
						project.start_date_formatted = project.start_date_formatted;
					} else {
						project.start_date_formatted = 'N/A';									
					}

					if (typeof project.end_date_formatted != 'undefined' && typeof project.end_date_formatted !== null && project.end_date_formatted!='' ) {
						project.end_date_formatted = project.end_date_formatted;
					} else {
						project.end_date_formatted = 'N/A';									
					}

					if (typeof project.is_active != 'undefined' && typeof project.is_active !== null ) {
						if(project.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					
					html += '<tr>\
                                <td class="highlight"> '+project.id+' </td>\
                                <td class="hidden-xs"> '+project.name+' </td>\
                                <td> '+project.size+' </td>\
                                <td> '+project.start_date_formatted+' </td>\
                                <td> '+project.end_date_formatted+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                	<a href="'+config.getAppUrl()+'/view-project?id='+project.id+'" class="btn btn-outline btn-circle yellow btn-sm">\
                                        <i class="fa fa-eye"></i> View </a>\
                                    <a href="'+config.getAppUrl()+'/project?id='+project.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_project" data-id="'+project.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' project_status" data-id="'+project.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#projects_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#projects_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.project-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Projects.list(page);
		    });

		    $('.delete_project').unbind('click').bind('click',function(e){
				e.preventDefault();
				var project_id  = $(this).attr('data-id');
				$('#hidden_action_project_id').val(project_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.project_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var project_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_project_id').val(project_id);
				$('#hidden_action_project_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving projects.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#projects_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var project_id 		= $('#updated_project_id').val();
		var name 			= $('#project_name');
		var size 			= $('#project_size');
		var description 	= $('#project_description').parent().parent().find('.jqte_editor');
		var recent_description 	= $('#project_recent_description').parent().parent().find('.jqte_editor');
		var testimonial 	= $('#project_testimonial').parent().parent().find('.jqte_editor');
		var client_name 			= $('#project_client_name');
		var company_name 			= $('#project_company_name');
		var company_logo 			= $('.hidden-company-logo-image');
		var uses 			= $('#project_uses');
		var start_date		= $('#start_date');
		var end_date		= $('#end_date');
		var type_id			= $('#project_type_id');
		var client_id		= $('#project_client_id');
		var shape_id			= $('#project_shape_id');
		var material_rows	= $('.material_rows');
		var use_rows		= $('.use_rows');
		var image_rows		= $('.image_rows');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(size.val()) == '') {
			errors.push('Please enter size.');
			size.parent().addClass('has-error');
		} else {
			size.parent().removeClass('has-error');		
		}

		if ($.trim(description.html()) == '') {
			errors.push('Please enter description.');
			description.parent().addClass('has-error');
		} else {
			description.parent().removeClass('has-error');		
		}

		if ($.trim(start_date.val()) == '') {
			errors.push('Please enter start date.');
			start_date.parent().addClass('has-error');
		} else {
			start_date.parent().removeClass('has-error');		
		}

		if ($.trim(end_date.val()) == '') {
			errors.push('Please enter end date.');
			end_date.parent().addClass('has-error');
		} else {
			end_date.parent().removeClass('has-error');		
		}

		if ($.trim(type_id.val()) == '0') {
			errors.push('Please select type.');
			type_id.parent().parent().addClass('has-error');
		} else {
			type_id.parent().parent().removeClass('has-error');	
		}

		if ($.trim(client_id.val()) == '0') {
			errors.push('Please select manufacturer.');
			client_id.parent().parent().addClass('has-error');
		} else {
			client_id.parent().parent().removeClass('has-error');	
		}

		if ($.trim(shape_id.val()) == '0') {
			errors.push('Please select shape.');
			shape_id.parent().parent().addClass('has-error');
		} else {
			shape_id.parent().parent().removeClass('has-error');	
		}

		var uses_ids = [];
    	$('.project_uses').each(function(){
    		/*if ($(this).prop("checked")) {
    			uses_ids.push($(this).val());
    		}*/
    		if ($(this).parent().hasClass("checked")) {
    			uses_ids.push($(this).val());
    		}
    	});

    	if (uses_ids.length == 0) {
    		errors.push('Please select atleast one usage.');
    	}

		var material_ids = [];
		$(material_rows).each(function(index, element){
			var materialId = $(this).find('.project_material_ids').val();
			if (materialId > 0) {
				if($.inArray(materialId, material_ids) !== -1) {
					errors.push('Please select distinct materials.');
					$(this).parent().addClass('has-error');
				} else {
					material_ids.push(materialId);
					$(this).parent().removeClass('has-error');
				}
				
			} else {
				errors.push('Please select atleast one material.');
				$(this).parent().addClass('has-error');
			}
		});

		var images = [];
		$(image_rows).each(function(index, element){
			var image = $(this).find('.hidden-project-image').val();
			var real_image = $(this).find('.hidden-project-real-image').val();
			var image_title = $(this).find('.image_title').val();
			if (image != '' && image_title != '') {
					var imageObj = {};
					imageObj.image = image;
					imageObj.real_image = real_image;
					imageObj.title = image_title;
					images.push(imageObj);
					$(this).find('.hidden-project-image').parent().removeClass('has-error');
					$(this).find('.image_title').parent().removeClass('has-error');
				
			} else {

				if (image_title == '') {
					errors.push('Please enter image title.');
					$(this).find('.image_title').parent().addClass('has-error');
				} else {
					$(this).find('.image_title').parent().removeClass('has-error');
				}

				if (image == '') {
					errors.push('Please select image.');
					$(this).find('.hidden-project-image').parent().addClass('has-error');
				} else {
					$(this).find('.hidden-project-image').parent().removeClass('has-error');
				}
							
			}
		});


		if (errors.length < 1) {

			var jsonData = {
								id:project_id,
								name:$.trim(name.val()),
								description:$.trim(description.html()),
								recent_description:$.trim(recent_description.html()),
								testimonial:$.trim(testimonial.html()),
								client_name:$.trim(client_name.val()),
								company_name:$.trim(company_name.val()),
								company_logo:$.trim(company_logo.val()),
								type_id:$.trim(type_id.val()),
								shape_id:$.trim(shape_id.val()),
								client_id:$.trim(client_id.val()),
								size:$.trim(size.val()),
								uses:$.trim(uses.val()),
								start_date:$.trim(start_date.val()),
								end_date:$.trim(end_date.val()),														
								material_ids:material_ids,
								images:images,
								uses_ids:uses_ids
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: projectApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: projectApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#project_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/projects';
					    });		 	
				} else if(data.error) {
					$('#project_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#project_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#project_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_project_id').val();
		$('#project_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: projectApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'project has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#project_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.Projects.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this project.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#project_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#project_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this project.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_project_id').val();
		jsonData.status = $('#hidden_action_project_status').val();

		var request = $.ajax({
			url: projectApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#project_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Projects.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating project status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#project_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#project_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating project status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var view = function(id){
        var jsonData = {id:id};
		var request = $.ajax({
			url: projectApiUrl+'/view',
			data: jsonData,
			type: 'GET',
			dataType:'json'
		});

		request.success(function(data){
			var projectData = data;
			if (projectData != null) {
		        $('#project_type_id').val(projectData.type.id);
		        $('#project_client_id').val(projectData.client.id);
		        $('#project_shape_id').val(projectData.shape.id);

		        if (projectData.materials.length > 0) {
		            $(projectData.materials).each(function(index, element){
		                
		                if (index > 0) {
		                    var clonedElement = $('.add-more-material').prev().clone();

		                    clonedElement.find('.project_material_ids').val(element.id);
		                    clonedElement.find('.project_material_ids').parent().parent().removeClass('has-error');
		                    clonedElement.find('.delete-project-material').remove();
		                    clonedElement.find('.project_label').remove();
		                    clonedElement.append('<a href="javascript:;" class="delete-project-material"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
		                    clonedElement.insertAfter($('.add-more-material').prev());
		                } else {
		                    $('.project_material_ids').val(element.id);
		                }
		                
		                $('.page-container').on('click', '.delete-project-material', function(e) {
		                    e.preventDefault();
		                    $(this).parent().remove();
		                });
		            });
		        }

		        $('.project_uses').each(function(){
		            $(this).parent().removeClass('checked');
		        });
		        if (projectData.usages.length > 0) {

		            //$("input[name=project_uses]").attr('checked', false);
		            $(projectData.usages).each(function(index, element) {
		                $("input[name=project_uses][value="+element.id+"]").attr('checked', true);
		                $("input[name=project_uses][value="+element.id+"]").parent().addClass('checked');
		            });
		        }   

		        if (projectData.images.length > 0) {
		            $(projectData.images).each(function(index, element){
		                if (index > 0) {
		                    var clonedElement = $('.add-more-images').prev().clone();

		                    clonedElement.find('.hidden-project-image').val(element.image);
		                    clonedElement.find('.hidden-project-real-image').val(element.real_image);
		                    clonedElement.find('.image_title').val(element.title);
		                    clonedElement.find('.image_title').parent().removeClass('has-error');
		                    clonedElement.find('.hidden-project-image').parent().removeClass('has-error');
		                    clonedElement.find('.project-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
		                    clonedElement.find('.file-span-msg-project-image').attr('placeholder', element.real_image);
		                    clonedElement.find('.delete-project-image').remove();
		                    clonedElement.find('.project_label').remove();
		                    clonedElement.find('.image_browse').css('margin-bottom', '19px');
		                    clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-project-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
		                    clonedElement.insertAfter($('.add-more-images').prev());

		                } else {
		                    $('.file-span-msg-project-image').attr('placeholder', element.real_image);
		                    $('.hidden-project-image').val(element.image);
		                    $('.hidden-project-real-image').val(element.real_image);
		                    $('.image_title').val(element.title);
		                }
		            });
		        }
		    }
			
		});

		request.fail(function(jqXHR, textStatus){
			// do something
		});
    };

	return {
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus,
		view:view
	}
}());

/* Team Members Management */
SM.App.Teams = (function() {

	var config = SM.Config;
	var teamApiUrl = config.getApiUrl()+'team';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#team_filter_by_status').val() || '';
		var keyword 		= $('#team_search_keyword').val() || '';
		var limit 			= $('#team-list-limit').val() || 0;
		var data 			= {};
		var total_teams 	= $('#total_teams');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: teamApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var teams = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_teams.html(paginations.total);

			if(teams.length > 0) {
				/*<th width="25%"> Description </th>\
		                                        <th> Email </th>\
		                                        <th> Linked In </th>\*/
				$('#teams_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th> Designation </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(teams).each(function(index, team){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof team.name != 'undefined' && typeof team.name !== null && team.name!='' ) {
						team.name = team.name;
					} else {
						team.name = 'N/A';									
					}

					if (typeof team.designation != 'undefined' && typeof team.designation !== null && team.designation!='' ) {
						team.designation = team.designation;
					} else {
						team.designation = 'N/A';									
					}

					if (typeof team.description != 'undefined' && typeof team.description !== null && team.description!='' ) {
						team.description = team.description;
					} else {
						team.description = 'N/A';									
					}

					if (typeof team.email != 'undefined' && typeof team.email !== null && team.email!='' ) {
						team.email = team.email;
					} else {
						team.email = 'N/A';									
					}

					if (typeof team.linked_in != 'undefined' && typeof team.linked_in !== null && team.linked_in!='' ) {
						team.linked_in = '<a href="http://www.linkedin.com/in/'+team.linked_in+'" target="_blank">'+team.linked_in+'</a>';
					} else {
						team.linked_in = 'N/A';									
					}

					if (typeof team.is_active != 'undefined' && typeof team.is_active !== null ) {
						if(team.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					/*<img src="'+config.getSiteUrl()+'/files/team/'+team.image+'" width="30px" height="30px">
					<td class="hidden-xs"> '+team.description+' </td>\
                                <td class="hidden-xs"> '+team.email+' </td>\
                                <td class="hidden-xs"> '+team.linked_in+' </td>\*/
					html += '<tr>\
                                <td class="highlight"> '+team.id+' </td>\
                                <td class="hidden-xs">  '+team.name+' </td>\
                                <td class="hidden-xs"> '+team.designation+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/team?id='+team.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_team" data-id="'+team.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' team_status" data-id="'+team.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#teams_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#teams_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.team-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.teams.list(page);
		    });

		    $('.delete_team').unbind('click').bind('click',function(e){
				e.preventDefault();
				var team_id  = $(this).attr('data-id');
				$('#hidden_action_team_id').val(team_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.team_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var team_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_team_id').val(team_id);
				$('#hidden_action_team_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving manufacturers.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#teams_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var team_id = $('#updated_team_id').val();
		var name 	= $('#team_name');
		var designation 	= $('#team_designation');
		var description 	= $('#team_description').parent().parent().find('.jqte_editor');
		var email 			= $('#team_email');
		var linked_in 		= $('#team_linked_in');
		var image 			= $('#hidden-team-image');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(designation.val()) == '') {
			errors.push('Please enter designation.');
			designation.parent().addClass('has-error');
		} else {
			designation.parent().removeClass('has-error');		
		}

		/*if ($.trim(email.val()) != '') {
			if(!SM.App.isEmail(email.val())){
				errors.push('Please enter valid email address.');
				email.parent().addClass('has-error');
			} else {
				email.parent().removeClass('has-error');
			}
		}

		if ($.trim(description.html()) == '') {
			errors.push('Please enter description.');
			description.parent().addClass('has-error');
		} else {
			description.parent().removeClass('has-error');		
		}

		if ($.trim(image.val()) == '') {
			errors.push('Please select image.');
			image.parent().addClass('has-error');
		} else {
			image.parent().removeClass('has-error');		
		}*/

		if (errors.length < 1) {

			var jsonData = {
								id:team_id,
								name:$.trim(name.val()),
								designation:$.trim(designation.val()),
								/*description:$.trim(description.html()),
								email:$.trim(email.val()),
								linked_in:$.trim(linked_in.val()),
								image:$.trim(image.val())*/												
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: teamApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: teamApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#team_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/teams';
					    });		 	
				} else if(data.error) {
					$('#team_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#team_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#team_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_team_id').val();
		$('#team_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: teamApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'Team member has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#team_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.Teams.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this manufacturer.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#team_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#team_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this team member.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_team_id').val();
		jsonData.status = $('#hidden_action_team_status').val();

		var request = $.ajax({
			url: teamApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#team_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Teams.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating team member status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#team_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#team_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating team member status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus
	}
}());

/* Contact Members Management */
SM.App.Contacts = (function() {

	var config = SM.Config;
	var contactApiUrl = config.getApiUrl()+'contact';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#contact_filter_by_status').val() || '';
		var keyword 		= $('#contact_search_keyword').val() || '';
		var limit 			= $('#contact-list-limit').val() || 0;
		var data 			= {};
		var total_contacts 	= $('#total_contacts');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: contactApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var contacts = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_contacts.html(paginations.total);

			if(contacts.length > 0) {

				$('#contacts_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th> Designation </th>\
		                                        <th> Email </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(contacts).each(function(index, contact){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof contact.name != 'undefined' && typeof contact.name !== null && contact.name!='' ) {
						contact.name = contact.name;
					} else {
						contact.name = 'N/A';									
					}

					if (typeof contact.designation != 'undefined' && typeof contact.designation !== null && contact.designation!='' ) {
						contact.designation = contact.designation;
					} else {
						contact.designation = 'N/A';									
					}

					if (typeof contact.email != 'undefined' && typeof contact.email !== null && contact.email!='' ) {
						contact.email = contact.email;
					} else {
						contact.email = 'N/A';									
					}

					if (typeof contact.is_active != 'undefined' && typeof contact.is_active !== null ) {
						if(contact.is_active == '1'){
							statusText = 'Active';
							is_active = '0';
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else{
							statusText = 'InActive';
							is_active = '1';
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}
					
					html += '<tr>\
                                <td class="highlight"> '+contact.id+' </td>\
                                <td class="hidden-xs"> '+contact.name+' </td>\
                                <td class="hidden-xs"> '+contact.designation+' </td>\
                                <td class="hidden-xs"> '+contact.email+' </td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/contact?id='+contact.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_contact" data-id="'+contact.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' contact_status" data-id="'+contact.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#contacts_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#contacts_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.contact-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Contacts.list(page);
		    });

		    $('.delete_contact').unbind('click').bind('click',function(e){
				e.preventDefault();
				var contact_id  = $(this).attr('data-id');
				$('#hidden_action_contact_id').val(contact_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.contact_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var contact_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_contact_id').val(contact_id);
				$('#hidden_action_contact_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving contact members.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#contacts_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var contact_id 		= $('#updated_contact_id').val();
		var name 			= $('#contact_name');
		var designation 	= $('#contact_designation');
		var email 			= $('#contact_email');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(designation.val()) == '') {
			errors.push('Please enter designation.');
			designation.parent().addClass('has-error');
		} else {
			designation.parent().removeClass('has-error');		
		}

		if ($.trim(email.val()) == '') {
			errors.push('Please enter email address.');
			email.parent().addClass('has-error');
			
		} else if(!SM.App.isEmail(email.val())){
			errors.push('Please enter valid email address.');
			email.parent().addClass('has-error');
		} else {
			email.parent().removeClass('has-error');
		}

		if (errors.length < 1) {

			var jsonData = {
								id:contact_id,
								name:$.trim(name.val()),
								designation:$.trim(designation.val()),
								email:$.trim(email.val()),												
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: contactApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: contactApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#contact_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/contacts';
					    });		 	
				} else if(data.error) {
					$('#contact_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#contact_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#contact_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var remove = function (){
		var id = $('#hidden_action_contact_id').val();
		$('#contact_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: contactApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'Contact member has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#contact_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    	SM.App.Contacts.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this contact member.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#contact_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#contact_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this contact member.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		var jsonData = {};
		jsonData.id = $('#hidden_action_contact_id').val();
		jsonData.status = $('#hidden_action_contact_status').val();

		var request = $.ajax({
			url: contactApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#contact_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#type_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Contacts.list();
				    
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating contact member status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#contact_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#contact_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating contact member status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		create:create,
		remove:remove,
		list:list,
		updateStatus:updateStatus
	}
}());

/* Settings */
SM.App.Settings = (function() {

	var config = SM.Config;
	var settingApiUrl = config.getApiUrl()+'setting';

	var update = function (){
		var errors = [];
		var home_heading 					= $('#home_heading').parent().parent().find('.jqte_editor').html();
		var why_choose_title 				= $('#why_choose_title').val();
		var why_choose_description 			= $('#why_choose_description').parent().parent().find('.jqte_editor').html();
		var about_us_title 					= $('#about_us_title').val();
		var about_us_description 			= $('#about_us_description').parent().parent().find('.jqte_editor').html();
		var mission_title 					= $('#mission_title').val();
		var mission_description 			= $('#mission_description').parent().parent().find('.jqte_editor').html();
		var sponsorship_title 				= $('#sponsorship_title').val();
		var sponsorship_description 		= $('#sponsorship_description').parent().parent().find('.jqte_editor').html();
		var certification_title 			= $('#certification_title').val();
		var certification_description 		= $('#certification_description').parent().parent().find('.jqte_editor').html();
		var about_us_image 					= $('#hidden-about_us-image').val();
		//var sponsorship_image 				= $('#hidden-sponsorship-image').val();
		//var certification_image 			= $('#hidden-certification-image').val();
		var inquiry_phone 					= $('#inquiry_phone').val();
		var inquiry_toll_free 				= $('#inquiry_toll_free').val();
		var inquiry_email 					= $('#inquiry_email').val();

		var repairs_heading 				= $('#repairs_heading').val();
		var repairs_description 			= $('#repairs_description').parent().parent().find('.jqte_editor').html();

		var repairs_who_is_title 				= $('#repairs_who_is_title').val();
		var repairs_who_is_description 			= $('#repairs_who_is_description').parent().parent().find('.jqte_editor').html();

		var repairs_who_is_it_for_title 		= $('#repairs_who_is_it_for_title').val();
		var repairs_who_is_it_for_description 	= $('#repairs_who_is_it_for_description').parent().parent().find('.jqte_editor').html();

		var repairs_why_you_love_title 			= $('#repairs_why_you_love_title').val();
		var repairs_why_you_love_descripton 	= $('#repairs_why_you_love_descripton').parent().parent().find('.jqte_editor').html();
		
		var repairs_who_is_image 				= $('#hidden-repairs_who_is-image').val();
		var repairs_who_is_it_for_image 		= $('#hidden-repairs_who_is_it_for-image').val();
		var repairs_why_you_love_image 			= $('#hidden-repairs_why_you_love-image').val();

		var slider_rows 						= $('.slider_rows');
		var images = [];
		$(slider_rows).each(function(index, element){
			var image = $(this).find('.hidden-slider-image').val();
			var real_image = $(this).find('.hidden-slider-real-image').val();
			var image_title = $(this).find('.image_title').val();
			if (image != '' && image_title != '') {
					var imageObj = {};
					imageObj.image = image;
					imageObj.real_image = real_image;
					imageObj.title = image_title;
					images.push(imageObj);
					$(this).find('.hidden-slider-image').parent().removeClass('has-error');
					$(this).find('.image_title').parent().removeClass('has-error');
				
			} else {

				if (image != '' && image_title == '') {
					errors.push('Please enter image title.');
					$(this).find('.image_title').parent().addClass('has-error');
				} else {
					$(this).find('.image_title').parent().removeClass('has-error');
				}

				if (image_title != '' && image == '') {
					errors.push('Please select image.');
					$(this).find('.hidden-slider-image').parent().addClass('has-error');
				} else {
					$(this).find('.hidden-slider-image').parent().removeClass('has-error');
				}
							
			}
		});

		var sponsorship_images 						= $('.sponsorship_images');
		var sponsorshipImages = [];
		$(sponsorship_images).each(function(index, element){
			var image = $(this).find('.hidden-sponsorship-image').val();
			var real_image = $(this).find('.hidden-sponsorship-real-image').val();
			var sponsorship_link = $(this).find('.sponsorship_link').val();
			
			if (image != '' && sponsorship_link != '') {
					var imageObj = {};
					imageObj.image = image;
					imageObj.real_image = real_image;
					imageObj.link = sponsorship_link;
					sponsorshipImages.push(imageObj);
					$(this).find('.hidden-sponsorship-image').parent().removeClass('has-error');
					$(this).find('.sponsorship_link').parent().removeClass('has-error');
				
			} else {

				if (image != '' && sponsorship_link == '') {
					errors.push('Please enter link.');
					$(this).find('.sponsorship_link').parent().addClass('has-error');
				} else {
					$(this).find('.sponsorship_link').parent().removeClass('has-error');
				}

				if (sponsorship_link != '' && image == '') {
					errors.push('Please select image.');
					$(this).find('.hidden-sponsorship-image').parent().addClass('has-error');
				} else {
					$(this).find('.hidden-sponsorship-image').parent().removeClass('has-error');
				}
							
			}
		});

		var certification_images 						= $('.certification_images');
		var certificationImages = [];
		$(certification_images).each(function(index, element){
			var image = $(this).find('.hidden-certification-image').val();
			var real_image = $(this).find('.hidden-certification-real-image').val();
			var certification_link = $(this).find('.certification_link').val();
			if (image != '' && certification_link != '') {
					var imageObj = {};
					imageObj.image = image;
					imageObj.real_image = real_image;
					imageObj.link = certification_link;
					certificationImages.push(imageObj);
					$(this).find('.hidden-certification-image').parent().removeClass('has-error');
					$(this).find('.certification_link').parent().removeClass('has-error');
				
			} else {

				if (image != '' && certification_link == '') {
					errors.push('Please enter link.');
					$(this).find('.certification_link').parent().addClass('has-error');
				} else {
					$(this).find('.certification_link').parent().removeClass('has-error');
				}

				if (certification_link != '' && image == '') {
					errors.push('Please select image.');
					$(this).find('.hidden-certification-image').parent().addClass('has-error');
				} else {
					$(this).find('.hidden-certification-image').parent().removeClass('has-error');
				}
							
			}
		});

		if ($.trim(inquiry_email) != '') {
			if(!SM.App.isEmail(inquiry_email)){
				errors.push('Please enter valid email address.');
				$('#inquiry_email').parent().addClass('has-error');
			} else {
				$('#inquiry_email').parent().removeClass('has-error');
			}
		} else {
			$('#inquiry_email').parent().removeClass('has-error');
		}

		if (errors.length < 1 ) {

			var jsonData = {};
			jsonData.home_images			= images;
			jsonData.home_heading 			= home_heading;
			jsonData.why_choose_title 		= why_choose_title;
			jsonData.why_choose_description	= why_choose_description;
			jsonData.about_us_title 		= about_us_title;
			jsonData.about_us_description 	= about_us_description;
			jsonData.mission_title 			= mission_title;
			jsonData.mission_description 	= mission_description;
			jsonData.sponsorship_title 		= sponsorship_title;
			jsonData.sponsorship_description = sponsorship_description;
			jsonData.certification_title 	= certification_title;
			jsonData.certification_description = certification_description;
			jsonData.about_us_image 		= about_us_image;
			jsonData.sponsorship_images 		= sponsorshipImages;
			jsonData.certification_images 	= certificationImages;
			jsonData.inquiry_phone 			= inquiry_phone;
			jsonData.inquiry_toll_free 		= inquiry_toll_free;
			jsonData.inquiry_email 			= inquiry_email;
			jsonData.repairs_heading 			= repairs_heading;
			jsonData.repairs_description 			= repairs_description;
			jsonData.repairs_who_is_title 			= repairs_who_is_title;
			jsonData.repairs_who_is_description 			= repairs_who_is_description;
			jsonData.repairs_who_is_it_for_title 			= repairs_who_is_it_for_title;
			jsonData.repairs_who_is_it_for_description 			= repairs_who_is_it_for_description;
			jsonData.repairs_why_you_love_title 			= repairs_why_you_love_title;
			jsonData.repairs_why_you_love_descripton 			= repairs_why_you_love_descripton;
			jsonData.repairs_who_is_image 			= repairs_who_is_image;
			jsonData.repairs_who_is_it_for_image 			= repairs_who_is_it_for_image;
			jsonData.repairs_why_you_love_image 			= repairs_why_you_love_image;

			var request = $.ajax({	
				url: settingApiUrl+'/update',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});
            	
            $('.setting_submit_btn').addClass('prevent-click');
            $('.submit_loader').show();
			request.done(function(data){
				$('.submit_loader').hide();
				if(data.success) {		
					$('.msg_div').removeClass('alert-danger show-alert-message');
					$('.msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function() {
						$(this).html('');
				    	$(this).removeClass('alert-danger');
				    	$('.setting_submit_btn').removeClass('prevent-click');
				    	window.location.reload(true);
				    });		
				} else if(data.error) {
					$('.setting_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('.msg_div').removeClass('alert-success');
					$('.msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('.setting_submit_btn').removeClass('prevent-click');
				$('.submit_loader').hide();

				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred while updating settings.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('.msg_div').html(error).addClass('alert-danger').show();
			});

		} else {
			$('.setting_submit_btn').removeClass('prevent-click');
			$('.msg_div').removeClass('alert-success');
			$('.msg_div').html(errors[0]).addClass('alert-danger').show();
		}		
	};

	return {
		update:update
	}
}());

/* Quotes Management */
SM.App.Quotes = (function() {

	var config = SM.Config;
	var quoteApiUrl = config.getApiUrl()+'quote';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#quote_filter_by_status').val() || '';
		var keyword 		= $('#quote_search_keyword').val() || '';
		var limit 			= $('#quote-list-limit').val() || 0;
		var data 			= {};
		var total_quotes 	= $('#total_quotes');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		//data.filter_by_type = filterByType;
		
		var request = $.ajax({
			url: quoteApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var quotes = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_quotes.html(paginations.total);

			if(quotes.length > 0) {

				$('#quotes_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th> Company Name </th>\
		                                        <th> Email </th>\
		                                        <th> Phone </th>\
		                                        <th> Address </th>\
		                                        <th> City </th>\
		                                        <th> Requested At </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(quotes).each(function(index, quote){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof quote.name != 'undefined' && typeof quote.name !== null && quote.name!='' ) {
						quote.name = quote.name;
					} else {
						quote.name = 'N/A';									
					}

					if (typeof quote.company_name != 'undefined' && typeof quote.company_name !== null && quote.company_name!='' ) {
						quote.company_name = quote.company_name;
					} else {
						quote.company_name = 'N/A';									
					}

					if (typeof quote.email != 'undefined' && typeof quote.email !== null && quote.email!='' ) {
						quote.email = quote.email;
					} else {
						quote.email = 'N/A';									
					}

					if (typeof quote.phone != 'undefined' && typeof quote.phone !== null && quote.phone!='' ) {
						quote.phone = quote.phone;
					} else {
						quote.phone = 'N/A';									
					}

					if (typeof quote.address != 'undefined' && typeof quote.address !== null && quote.address!='' ) {
						quote.address = quote.address;
					} else {
						quote.address = 'N/A';									
					}

					if (typeof quote.city != 'undefined' && typeof quote.city !== null && quote.city!='' ) {
						quote.city = quote.city;
					} else {
						quote.city = 'N/A';									
					}
					
					html += '<tr>\
                                <td class="highlight"> '+quote.id+' </td>\
                                <td class="hidden-xs"> '+quote.name+' </td>\
                                <td> '+quote.company_name+' </td>\
                                <td> '+quote.email+' </td>\
                                <td> '+quote.phone+' </td>\
                                <td> '+quote.address+' </td>\
                                <td> '+quote.city+' </td>\
                                <td> '+quote.created_at+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/quote?id='+quote.id+'" class="btn btn-outline btn-circle yellow btn-sm">\
                                        <i class="fa fa-edit"></i> View </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#quotes_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#quotes_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.quote-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Quotes.list(page);
		    });

		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving quotes.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#quotes_list').html(html);
		});		
	};

	return {
		list:list
	}
}());

/* Partners Management */
SM.App.Partners = (function() {

	var config = SM.Config;
	var partnerApiUrl = config.getApiUrl()+'partner';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#partner_filter_by_status').val() || '';
		var keyword 		= $('#partner_search_keyword').val() || '';
		var limit 			= $('#partner-list-limit').val() || 0;
		var data 			= {};
		var total_partners 	= $('#total_partners');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: partnerApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var partners = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_partners.html(paginations.total);

			if(partners.length > 0) {

				$('#partners_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th> Image </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(partners).each(function(index, partner){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof partner.name != 'undefined' && typeof partner.name !== null && partner.name!='' ) {
						partner.name = partner.name;
					} else {
						partner.name = 'N/A';									
					}

					if (typeof partner.image != 'undefined' && partner.image!=null ) {
						partner.image = partner.image;
					} else {
						partner.image = 'image-not-found.png';									
					}
					
					if (typeof partner.is_active != 'undefined' && typeof partner.is_active !== null ) {
						if(partner.is_active == 1){
							statusText = 'Active';
							is_active = 0;
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else if (partner.is_active == 0){
							statusText = 'InActive';
							is_active = 1;
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}

					html += '<tr>\
                                <td class="highlight"> '+partner.id+' </td>\
                                <td class="hidden-xs"> '+partner.name+' </td>\
                                <td> <img src="'+config.getSiteUrl()+'/files/partner/'+partner.image+'" width="170px" height="60px"></td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/partner?id='+partner.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_partner" data-id="'+partner.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' partner_status" data-id="'+partner.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#partners_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#partners_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.partner-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Partners.list(page);
		    });

		    $('.delete_partner').unbind('click').bind('click',function(e){
				e.preventDefault();
				var partner_id  = $(this).attr('data-id');
				$('#hidden_action_partner_id').val(partner_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.partner_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var partner_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_partner_id').val(partner_id);
				$('#hidden_action_partner_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving partners.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#partners_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var partner_id = $('#updated_partner_id').val();
		var name 	= $('#partner_name');
		var image 	= $('#hidden-partner-image');

		if ($.trim(image.val()) == '') {
			errors.push('Please enter image.');
			image.parent().addClass('has-error');
		} else {
			image.parent().removeClass('has-error');	
		}

		if (errors.length < 1) {

			var jsonData = {
								id:partner_id,
								name:$.trim(name.val()),
								image:$.trim(image.val())														
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: partnerApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: partnerApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#partner_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/partners';
					    });		 	
				} else if(data.error) {
					$('#partner_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#partner_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#partner_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var view = function(id){
        var jsonData = {id:id};
		var request = $.ajax({
			url: partnerApiUrl+'/view',
			data: jsonData,
			type: 'GET',
			dataType:'json'
		});

		request.success(function(data){
			$('#partner_name').val(data.name)
			
		});

		request.fail(function(jqXHR, textStatus){
			// do something
		});
    };

	var remove = function (){
		var id = $('#hidden_action_partner_id').val();
		$('#partner_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: partnerApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'partner has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#partner_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    SM.App.Partners.list();	
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this partner.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#partner_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#partner_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this partner.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		
		var jsonData = {};
		jsonData.id = $('#hidden_action_partner_id').val();
		jsonData.status = $('#hidden_action_partner_status').val();

		var request = $.ajax({
			url: partnerApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#partner_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#partner_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Partners.list();		
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating partner status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#partner_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#partner_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating partner status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		list:list,
		create:create,
		remove:remove,
		updateStatus:updateStatus,
		view:view
	}
}());

/* Process Management */
SM.App.Process = (function() {

	var config = SM.Config;
	var processApiUrl = config.getApiUrl()+'process';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByStatus 	= $('#process_filter_by_status').val() || '';
		var keyword 		= $('#process_search_keyword').val() || '';
		var limit 			= $('#process-list-limit').val() || 0;
		var data 			= {};
		var total_processes 	= $('#total_processes');

		data.pagination 	= pagination;
		data.page 			= page;
		data.limit 			= limit;
		data.keyword 		= keyword;
		data.filter_by_status = filterByStatus;
		
		var request = $.ajax({
			url: processApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var paginationShow = '';
			var processes = data.data;
			var classDisabledPrev = "";
			var classDisabledNext = "";
			var paginations = data.pagination;
			total_processes.html(paginations.total);

			if(processes.length > 0) {

				$('#processes_list_head').html('<tr>\
		                                        <th> ID </th>\
		                                        <th> Name </th>\
		                                        <th> Description </th>\
		                                        <th> Image </th>\
		                                        <th> Status </th>\
		                                        <th> Action</th>\
		                                    </tr>');

				$(processes).each(function(index, process){

					var archiveText = '-';	
					var archiveClass = '';					
					var is_active = '';
					var statusText = 'N/A';

					if (typeof process.name != 'undefined' && typeof process.name !== null && process.name!='' ) {
						process.name = process.name;
					} else {
						process.name = 'N/A';									
					}

					if (typeof process.image != 'undefined' && process.image!=null ) {
						process.image = process.image;
					} else {
						process.image = 'image-not-found.png';									
					}
					
					if (typeof process.is_active != 'undefined' && typeof process.is_active !== null ) {
						if(process.is_active == 1){
							statusText = 'Active';
							is_active = 0;
							archiveText = 'Deactivate';
							archiveClass = 'blue';
						}else if (process.is_active == 0){
							statusText = 'InActive';
							is_active = 1;
							archiveText = 'Activate';
							archiveClass = 'green-jungle';
						}
					}

					html += '<tr>\
                                <td class="highlight"> '+process.id+' </td>\
                                <td class="hidden-xs"> '+process.name+' </td>\
                                <td class="hidden-xs"> '+process.description+' </td>\
                                <td> <img src="'+config.getSiteUrl()+'/files/process/'+process.image+'" width="170px" height="60px"></td>\
                                <td> '+statusText+' </td>\
                                <td>\
                                    <a href="'+config.getAppUrl()+'/process?id='+process.id+'" class="btn btn-outline btn-circle dark btn-sm black">\
                                        <i class="fa fa-edit"></i> Edit </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm red delete_process" data-id="'+process.id+'">\
                                        <i class="fa fa-trash-o"></i> Delete </a>\
                                    <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm '+archiveClass+' process_status" data-id="'+process.id+'" data-status="'+is_active+'">\
                                        <i class="fa fa-trash-o"></i> '+archiveText+' </a>\
                                </td>\
                            </tr>';

				});
			} else {
				$('#processes_list_head').html('');
				html  += '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>Nothing Here Yet.</h3>\
                    <p>We couldn\'t find any record related to the defined criteria. Please try again later.</p></div>';
			}

			$('#processes_list').html(html);

            if(data.pagination.current >= data.pagination.next && data.pagination.current==1) {
				$('.general-pagination').hide();
				$('.process-pagination-limit').hide();
			} else {
				if(data.pagination.current==data.pagination.first){
					classDisabledPrev="disable";
				}
				if(data.pagination.current==data.pagination.last){
					classDisabledNext="disable";
				}
				paginationShow+='<li >\
								      <a class="general-pagination-click  '+classDisabledPrev+'" data-page='+paginations.previous+' href="javascript:;">Previous</a>\
								    </li>';
				paginationShow+= '<li >\
								      <a class=" general-pagination-click '+classDisabledNext+'" data-page='+paginations.next+' href="javascript:;">Next</a>\
								    </li>';
				paginationShow+= '<li class="hidden-xs">Showing '+data.pagination.to+' - '+data.pagination.from+' of total '+data.pagination.total+' records</li>';

				$('.general-pagination').html(paginationShow);
				$('.general-pagination').show();
				$('.general-pagination-limit').show();
			}

			$('.general-pagination-click').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = $(this).data('page');
				SM.App.Process.list(page);
		    });

		    $('.delete_process').unbind('click').bind('click',function(e){
				e.preventDefault();
				var process_id  = $(this).attr('data-id');
				$('#hidden_action_process_id').val(process_id);
				$('#removeConfirmation').modal('show');
		    });

		    $('.process_status').unbind('click').bind('click',function(e){
				e.preventDefault();
				var process_id  = $(this).attr('data-id');
				var status  = $(this).attr('data-status');
				$('#hidden_action_process_id').val(process_id);
				$('#hidden_action_process_status').val(status);

				if (status == 1) {
					$('#update_status_text').text('Activate');
				} else if (status == 0) {
					$('#update_status_text').text('Deactivate');
				}
				$('#updateStatusConfirmation').modal('show');
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving processes.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="blank-data">\
                	<img src="'+config.getImageUrl()+'emptystate@2x.png" class="img-responsive">\
                    <h3>'+error+'</h3></div>';
            $('#processes_list').html(html);
		});		
	};

	var create = function (){

		var errors = [];
		var process_id = $('#updated_process_id').val();
		var name 	= $('#process_name');
		var description 	= $('#process_description').parent().parent().find('.jqte_editor');
		var image 	= $('#hidden-process-image');

		if ($.trim(name.val()) == '') {
			errors.push('Please enter name.');
			name.parent().addClass('has-error');
		} else {
			name.parent().removeClass('has-error');	
		}

		if ($.trim(description.html()) == '') {
			errors.push('Please enter description.');
			description.parent().addClass('has-error');
		} else {
			description.parent().removeClass('has-error');	
		}

		if ($.trim(image.val()) == '') {
			errors.push('Please enter image.');
			image.parent().addClass('has-error');
		} else {
			image.parent().removeClass('has-error');	
		}

		if (errors.length < 1) {

			var jsonData = {
								id:process_id,
								name:$.trim(name.val()),
								description:$.trim(description.html()),
								image:$.trim(image.val())														
							}

			if (jsonData.id == 0) {
				var request = $.ajax({
					url: processApiUrl+'/create',
					data: jsonData,
					type: 'post',
					dataType:'json'
				});
			} else {
				var request = $.ajax({	
					url: processApiUrl+'/update',
					data: jsonData,
					type: 'pOST',
					dataType:'json'
				});
			}
			
			$('#process_submit_btn').addClass('prevent-click');
			$('#submit_loader').show();

			request.done(function(data){
				
				$('#submit_loader').hide();
				if(data.success) {		 
						$('#msg_div').removeClass('alert-danger');
						$('#msg_div').html(data.success.messages[0]).addClass('alert-success').show().delay(2000).fadeOut(function()
						{
							window.location.href = config.getAppUrl()+'/processes';
					    });		 	
				} else if(data.error) {
					$('#process_submit_btn').removeClass('prevent-click');
					var message_error = data.error.messages[0];
					$('#msg_div').removeClass('alert-success');
					$('#msg_div').html(message_error).addClass('alert-danger').show();
				}
				
			});

			request.fail(function(jqXHR, textStatus){
				$('#process_submit_btn').removeClass('prevent-click');
				$('#submit_loader').hide();
				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var error = 'An error occurred.';
				if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
					error = jsonResponse.error.messages[0];
				}
				$('#msg_div').removeClass('alert-success');
				$('#msg_div').html(error).addClass('alert-danger').show();
			});	

		} else {
			$('#msg_div').removeClass('alert-success');
			$('#msg_div').html(errors[0]).addClass('alert-danger').show();
			$('#process_submit_btn').removeClass('prevent-click');
			$('#submit_loader').hide();
		}		
	};

	var view = function(id){
        var jsonData = {id:id};
		var request = $.ajax({
			url: processApiUrl+'/view',
			data: jsonData,
			type: 'GET',
			dataType:'json'
		});

		request.success(function(data){
			$('#process_name').val(data.name)
			$('#process_description').html(data.description)
			
		});

		request.fail(function(jqXHR, textStatus){
			// do something
		});
    };

	var remove = function (){
		var id = $('#hidden_action_process_id').val();
		$('#process_remove_btn').addClass('prevent-click');
		$('#remove_submit_loader').show();

		var request = $.ajax({
			url: processApiUrl+'/remove',
			data: {id:id},
			type: 'delete',
			dataType:'json'
		});

		request.done(function(data){
			
			$('#remove_submit_loader').hide();
			if (data.success) {
				var html = 'Process has been deleted successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#remove_msg_div').removeClass('alert-danger');
		 		$('#remove_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#process_remove_btn').removeClass('prevent-click');
				    $('#removeConfirmation').modal('hide');
				    SM.App.Process.list();	
			    });

			} else if (data.error) {

				var error = 'An error occurred while deleting this process.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#process_remove_btn').removeClass('prevent-click');
				$('#remove_msg_div').removeClass('alert-success');
				$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
				    $(this).html('');
				    $('#remove_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#process_remove_btn').removeClass('prevent-click');
			$('#remove_submit_loader').hide();

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while deleting this process.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#remove_msg_div').html(error).addClass('alert-danger').show().delay(3000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	var updateStatus = function (){
		
		var jsonData = {};
		jsonData.id = $('#hidden_action_process_id').val();
		jsonData.status = $('#hidden_action_process_status').val();

		var request = $.ajax({
			url: processApiUrl+'/update-status',
			data: jsonData,
			type: 'pOST',
			dataType:'json'
		});

		$('#process_status_btn').addClass('prevent-click');
		$('#status_submit_loader').show();

		request.done(function(data){
			
			$('#status_submit_loader').hide();
			
			if (data.success) {
				var html = 'Status has been updated successfully.';
				
				if (data.success.messages && data.success.messages.length > 0 ) {
					html = data.success.messages[0];
				}

				$('#status_msg_div').removeClass('alert-danger');
		 		$('#status_msg_div').html(html).addClass('alert-success').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $(this).removeClass('alert-success');
				    $('#process_status_btn').removeClass('prevent-click');
				    $('#updateStatusConfirmation').modal('hide');
				    SM.App.Process.list();		
			    });

			} else if (data.error) {

				var error = 'An error occurred while updating process status.';
				if (data.error.messages && data.error.messages.length > 0) {
					error = data.error.messages[0];
				}
				$('#process_status_btn').removeClass('prevent-click');
				$('#status_msg_div').removeClass('alert-success');
				$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
				    $(this).html('');
				    $('#status_msg_div').removeClass('alert-danger');
				});
			}
				
		});

		request.fail(function(jqXHR, textStatus){
			$('#process_status_btn').removeClass('prevent-click');
			$('#status_submit_loader').hide();
			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while updating process status.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}
			$('#status_msg_div').html(error).addClass('alert-danger').show().delay(2000).fadeOut(function(){
			    $(this).html('');
			    $(this).removeClass('alert-danger');
			});
		});		
	};

	return {
		list:list,
		create:create,
		remove:remove,
		updateStatus:updateStatus,
		view:view
	}
}());