var SM = SM || {}; // "If SM is not defined, make it equal to an empty object"
SM.App = SM.App || {};
SM.Config = SM.Config || {};

var global_project_page = 1;
var global_product_page = 1;
SM.Config = (function(){
	if(window.location.host == 'localhost'){
		var apiUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/api/';
		var appUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/';
		var imageUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/files/';
		var imageAdminUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/pannel/';
		var imageFrontUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/front/';
		var assetUrl = window.location.protocol+'//'+window.location.host+'/spanmaster/public/';
	} else {
		var apiUrl = window.location.protocol+'//'+window.location.host+'/api/';
		var appUrl = window.location.protocol+'//'+window.location.host+'/';
		var imageUrl = window.location.protocol+'//'+window.location.host+'/files/';
		var imageAdminUrl = window.location.protocol+'//'+window.location.host+'/pannel/';
		var imageFrontUrl = window.location.protocol+'//'+window.location.host+'/front/';
		var assetUrl = window.location.protocol+'//'+window.location.host+'/';
	}

	var getApiUrl = function(){
		return apiUrl;
	};
	var getAppUrl = function(){
		return appUrl;
	};
	var getImageUrl = function(){
		return imageUrl;
	};
	var getAdminImageUrl = function(){
		return imageAdminUrl;
	};
	var getAssetUrl = function(){
		return assetUrl;
	};

	var getFrontImageUrl = function(){
		return imageFrontUrl;
	};

	return {
		getApiUrl:getApiUrl,
		getAppUrl:getAppUrl,
		getImageUrl:getImageUrl,
		getAssetUrl:getAssetUrl,
		getAdminImageUrl:getAdminImageUrl,
		getFrontImageUrl:getFrontImageUrl
	}
}());

SM.App = (function () {
	var config = SM.Config;

	var init = function () {
		if (!window.console) window.console = {log: function(obj) {}};
		console.log('Application has been initialized...');
	};

	var minLimit = function(min,value){
    	if(value.length < min) {
      		return false;
     	} else {
      		return true;
     	}
    };

    var imgError = function(image) {
	    image.onerror = "";
	    image.src = config.getFrontImageUrl()+'images/image-not.png';
	    return true;
	}

	var toTitleCase = function(val){
	  var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;

	  return val.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title){
	    if (index > 0 && index + match.length !== title.length &&
	      match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
	      (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
	      title.charAt(index - 1).search(/[^\s-]/) < 0) {
	      return match.toLowerCase();
	    }

	    if (match.substr(1).search(/[A-Z]|\../) > -1) {
	      return match;
	    }

	    return match.charAt(0).toUpperCase() + match.substr(1);
	  });
	};

	var validateAlphabet = function(name) {

		var alphabetWithSpace = /^[a-zA-Z\s]*$/;
   		return alphabetWithSpace.test(name);
	}

	var isValidUrl = function (url){
		var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;

		var regex = new RegExp(expression);
		if (!url.match(regex)) {
			return false;
		}
		return true;
	};

	var isNumberOnly = function (value) {
    	//var er = /^[0-9]*$/;
    	var er = /^\d*\.?\d*$/;
    	return er.test(value);
	};

	var isEmail=function(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}

	var resetFrom = function(){
		$('input').removeClass('validate_error');
	}

	var isAcceptFileTypes=function(fileTypes) {
		var acceptFileTypes = "(\.|\/)(png|jpg|jpeg)$";
   		acceptFileTypes = new RegExp(acceptFileTypes, "i");
   		return acceptFileTypes.test(fileTypes);
	}

	return {
			init:init,
			minLimit:minLimit,
			validateAlphabet:validateAlphabet,
			isEmail:isEmail,
			isValidUrl:isValidUrl,
			isNumberOnly:isNumberOnly,
			toTitleCase:toTitleCase,
			imgError:imgError,
			resetFrom:resetFrom,
			isAcceptFileTypes:isAcceptFileTypes,
			imgError:imgError
	};
}());

/* Projects */
SM.App.Projects = (function() {

	var config = SM.Config;
	var projectApiUrl = config.getApiUrl()+'project';

	var list = function (page) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= true;
		var filterByShape 	= $('#project_filter_by_shape').val() || '0';
		var filterByType 	= $('#project_filter_by_type').val() || '0';
		var data 			= {};

		data.pagination 	= pagination;
		data.page 			= page;
		data.filter_by_shape = filterByShape;
		data.filter_by_type = filterByType;
		data.from = 'site';
		
		var request = $.ajax({
			url: projectApiUrl+'/list?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var projects = data.data;
			var paginations = data.pagination;

			if(projects.length > 0) {

				$(projects).each(function(index, project){

					var key = index +1;
					if (typeof project.name != 'undefined' && typeof project.name !== null && project.name!='' ) {
						if(project.name.length > 30) project.name = project.name.substr(0,30)+'...';
					} else {
						project.name = 'N/A';									
					}
					
					html += '<div class="image_grid mix project-type-'+project.type.id+'" data-my-order="'+key+'">\
                            <a href="'+config.getAppUrl()+'project/'+project.id+'" title="'+project.name+'">\
                                <div class="thumb_zoom"><img src="'+config.getImageUrl()+'project/'+project.images[0].small_image+'" class="img-responsive" >'+project.name+'</div>\
                            </a>\
                        </div>';

				});
			} else {
				html  += '<div class="text-center">\
                            <h2>No Project Found</h2>\
                        </div>';
			}

			$('#projects_list').html(html);

			function mixitup() {
		        $("#projects_list").mixItUp({
		            animation: {
		                duration: 300,
		                effects: "fade translateZ(-360px) stagger(34ms)",
		                easing: "ease"
		            }
		        });
		    }

		    mixitup();

            if(data.pagination.current == data.pagination.last) {
				$('.load-more').hide();
			} else {
				$('.load-more').show ();
			}

			$('.load-more').unbind('click').bind('click',function(e){
				e.preventDefault();
				var page  = global_project_page++;
				SM.App.Projects.list(page);
		    });
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving projects.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="text-center">\
                            <h2>'+error+'</h2>\
                        </div>';
            $('#projects_list').html(html);
		});		
	};

	var listProduct = function (page, type) {

		$('.spinner-section').show();
		var page 			= page || 1;
		var pagination 		= false;
		var data 			= {};

		data.pagination 	= pagination;
		data.page 			= page;
		data.from = 'site';
		data.filter_by_product = type || 'all';

		var request = $.ajax({
			url: projectApiUrl+'/listProducts?page='+page,
			data:data,
			type: 'GET',
			dataType:'json'
		});

		request.done(function(data){
			$('.spinner-section').hide();
			var html = '';
			var products = data;

			if(products.length > 0) {

				$(products).each(function(index, product){

					var key = index +1;
					if (typeof product.name != 'undefined' && typeof product.name !== null && product.name!='' ) {
						if(product.name.length > 30) product.name = product.name.substr(0,30)+'...';
					} else {
						product.name = 'N/A';									
					}
					
					var productImage = '';
					
					if (typeof product.project_image != 'undefined' && typeof product.project_image[0] != 'undefined' && typeof product.project_image[0].small_image != 'undefined') {
						productImage = config.getImageUrl()+'project/'+product.project_image[0].small_image;
					} else {
						productImage = config.getAppUrl()+'front/images/image-not.png';									
					}

					html += '<div class="image_grid mix filter-'+product.type+' category-'+product.type.id+'" data-my-order="'+key+'">\
                            <a href="'+config.getAppUrl()+'product/'+product.type+'/'+product.id+'" title="'+product.name+'">\
                                <div class="thumb_zoom"><img src="'+productImage+'" class="img-responsive" >'+product.name+'</div>\
                            </a>\
                        </div>';

				});
			} else {
				html  += '<div class="text-center">\
                            <h2>No Product Found</h2>\
                        </div>';
			}

			/*if(products.length > 0) {

				$(products).each(function(index, product){

					var key = index +1;
					if (typeof product.name != 'undefined' && typeof product.name !== null && product.name!='' ) {
						product.name = product.name;
					} else {
						product.name = 'N/A';									
					}

					var productImage = '';
					
					if (typeof product.project_image != 'undefined' && typeof product.project_image[0] != 'undefined' && typeof product.project_image[0].small_image != 'undefined') {
						productImage = config.getImageUrl()+'project/'+product.project_image[0].small_image;
					} else {
						productImage = config.getAppUrl()+'front/images/image-not.png';									
					}
					
					html += '<div class="image_grid mix filter-'+product.type+' category-'+key+'" data-my-order="'+key+'">\
                            <a href="'+config.getAppUrl()+'product/'+product.type+'/'+product.id+'" title="'+product.name+'">\
                                <div class="thumb_zoom"><img width="40%" height="30%" src="'+productImage+'" class="img-responsive" onerror="'+SM.App.imgError(this)+'">'+product.name+'</div>\
                            </a>\
                        </div>';

				});
			} else {
				html  += '<div class="text-center">\
                            <h2>No Product Found</h2>\
                        </div>';
			}*/

			$('#products_list').html(html);

			function mixitup() {
		        $("#products_list").mixItUp({
		            animation: {
		                duration: 300,
		                effects: "fade translateZ(-360px) stagger(34ms)",
		                easing: "ease"
		            }
		        });
		    }

		    mixitup();
		});

		request.fail(function(jqXHR, textStatus){

			var jsonResponse = $.parseJSON(jqXHR.responseText);
			var error = 'An error occurred while retrieving products.';
			if (jsonResponse.error.messages && jsonResponse.error.messages.length > 0) {
				error = jsonResponse.error.messages[0];
			}

			var html = '<div class="text-center">\
                            <h2>'+error+'</h2>\
                        </div>';
            $('#products_list').html(html);
		});		
	};

	return {
		list:list,
		listProduct:listProduct
	}
}());
// Cob=ntact
SM.App.Contact = (function() {

	var config = SM.Config;
	var contactApiUrl = config.getApiUrl()+'contact';

	var send = function () {

		var errors = [];
    	var name = $('#name').val();
    	var company = $('#company').val();
    	var phone = $('#phone').val();
    	var email = $('#email').val();
    	var subject = $('#subject').val();
    	var message = $('#message').val();
    	var best_time_to_call = $('#best_time_to_call').val();
    	  	
    	if ($.trim(name) == '') {
    		errors.push('Please enter name');
    		$('#name').addClass('validate_error');
    	} else {
    		$('#name').removeClass('validate_error');
    	}

    	if ($.trim(email) == '') {
    		errors.push('Please enter email address');
    		$('#email').addClass('validate_error');
    	} else if (!SM.App.isEmail($.trim(email))) {
    		errors.push('Please enter valid email address');
    		$('#email').addClass('validate_error');
    	} else {
    		$('#email').removeClass('validate_error');
    	}

    	if ($.trim(phone) == '') {
    		errors.push('Please enter phone');
    		$('#phone').addClass('validate_error');
    	} else {
    		$('#phone').removeClass('validate_error');
    	}

    	if ($.trim(subject) == '') {
    		errors.push('Please enter subject');
    		$('#subject').addClass('validate_error');
    	} else {
    		$('#subject').removeClass('validate_error');
    	}

    	if ($.trim(message) == '') {
    		errors.push('Please enter message');
    		$('#message').addClass('validate_error');
    	} else {
    		$('#message').removeClass('validate_error');
    	}

    	if ($.trim(best_time_to_call) == '') {
    		errors.push('Please select best time to call');
    		$('#best_time_to_call').addClass('validate_error');
    	} else {
    		$('#best_time_to_call').removeClass('validate_error');
    	}
    	if (errors.length < 1) {

    		var jsonData = {};
    		jsonData.best_time_to_call = $.trim(best_time_to_call);
			jsonData.name = $.trim(name);
			jsonData.company = $.trim(company);
			jsonData.email = $.trim(email);
			jsonData.phone = $.trim(phone);
			jsonData.subject = $.trim(subject);
			jsonData.message = $.trim(message);

			$('.submit_contact').addClass('prevent-click');
			$('#contact_loader').show();
			var request = $.ajax({
				url: contactApiUrl+'/send',
				data: jsonData,
				type: 'post',
				dataType:'json'
			});

			request.done(function(data){
				$('#contact_loader').hide();
				// $('#msg_div').html(data.success.messages[0]).show().removeClass('error').addClass('success').delay(3000).fadeOut(function(){
				// 	$(this).html('');
				// 	$(this).removeClass('success');
				// 	$('.submit_contact').removeClass('prevent-click');
				// 	window.location.reload();
				// });
				$('#success_msg').html(data.success.messages[0]);
				$('#response_modal').modal('show');

				$('form input:text,form textarea').val('');

				$('.submit_contact').removeClass('prevent-click');
			});

			request.fail(function(jqXHR, textStatus){
				$('#contact_loader').hide();
				$('.submit_contact').removeClass('prevent-click');

				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var html = '';
				for(var i in jsonResponse.error.messages) {
					html += jsonResponse.error.messages[i];
				}

				$('#msg_div').html(html).show().addClass('error').delay(3000).fadeOut(function(){
					$(this).html('');
					$(this).removeClass('error');
				});
			});

    	} else {

    		/*$('#msg_div').removeClass('success');
			$('#msg_div').html(errors[0]).show().addClass('error');
			$('.submit_contact').removeClass('prevent-click');*/

    	}
		
	};

	return {
		send:send
	}
}());

// Quote
SM.App.Quote = (function() {

	var config = SM.Config;
	var quoteApiUrl = config.getApiUrl()+'quote';

	var send = function () {

		var errors = [];
    	var name = $('#name').val();
    	var company_name = $('#company_name').val();
    	var phone = $('#phone').val();
    	var email = $('#email').val();
    	var fax = $('#fax').val();
    	var address = $('#address').val();
    	var city = $('#city').val();
    	var postal_code = $('#postal_code').val();
    	var use = $('#use').val();
    	var other_use = $('#other_use').val();
    	var installation_period = $('#installation_period').val();
    	var comments = $('#comments').val();
    	var about_us = $('#about_us').val();
    	var best_time_to_call = $('#best_time_to_call').val();
    	if (about_us != '') {
    		var reasonId = $('#about_us option:selected').attr('data-id');
    		var about_us_reason = $('#about_us_reason'+reasonId+'').val();
    	} else {
    		var about_us_reason = '';
    	}

    	if ($.trim(name) == '') {
    		errors.push('Please enter name');
    		$('#name').addClass('validate_error');
    	} else {
    		$('#name').removeClass('validate_error');
    	}

    	if ($.trim(phone) == '') {
    		errors.push('Please enter phone');
    		$('#phone').addClass('validate_error');
    	} else {
    		$('#phone').removeClass('validate_error');
    	}

    	if ($.trim(email) == '') {
    		errors.push('Please enter email address');
    		$('#email').addClass('validate_error');
    	} else if (!SM.App.isEmail($.trim(email))) {
    		errors.push('Please enter valid email address');
    		$('#email').addClass('validate_error');
    	} else {
    		$('#email').removeClass('validate_error');
    	}

    	if ($.trim(city) == '') {
    		errors.push('Please enter city');
    		$('#city').addClass('validate_error');
    	} else {
    		$('#city').removeClass('validate_error');
    	}
    	/*
    	if ($.trim(best_time_to_call) == '') {
    		errors.push('Please enter best time to call');
    		$('#best_time_to_call').addClass('validate_error');
    	} else {
    		$('#best_time_to_call').removeClass('validate_error');
    	}*/

    	if (errors.length < 1) {

    		var jsonData = {};
			jsonData.name = $.trim(name);
			jsonData.best_time_to_call = $.trim(best_time_to_call);
			jsonData.address = $.trim(address);
			jsonData.company_name = $.trim(company_name);
			jsonData.email = $.trim(email);
			jsonData.phone = $.trim(phone);
			jsonData.fax = $.trim(fax);
			jsonData.city = $.trim(city);
			jsonData.postal_code = $.trim(postal_code);
			jsonData.use = $.trim(use);
			jsonData.other_use = $.trim(other_use);
			jsonData.installation_period = $.trim(installation_period);
			jsonData.comments = $.trim(comments);
			jsonData.about_us = $.trim(about_us);
			jsonData.about_us_reason = $.trim(about_us_reason);

			$('#submit_quote').addClass('prevent-click');
			$('#quote_loader').show();
			var request = $.ajax({
				url: quoteApiUrl+'/send',
				data: jsonData,
				type: 'post',
				dataType:'json'
			});

			request.done(function(data){
				$('#quote_loader').hide();
				// $('#msg_div').html(data.success.messages[0]).show().removeClass('alert-danger').addClass('alert-success')

				//$('#success_msg').html(data.success.messages[0]);
				//$('#response_modal').modal('show');

				/*.delay(3000).fadeOut(function(){
					$(this).html('');
					$(this).removeClass('success');
					$('#submit_quote').removeClass('prevent-click');
					window.location.reload();
				})*/

				$('#success_msg').html(data.success.messages[0]);
				$('#response_modal').modal('show');

				$('form input:text,form textarea').val('');

				$('#submit_quote').removeClass('prevent-click');

			});

			request.fail(function(jqXHR, textStatus){
				$('#quote_loader').hide();
				$('#submit_quote').removeClass('prevent-click');

				var jsonResponse = $.parseJSON(jqXHR.responseText);
				var html = '';
				for(var i in jsonResponse.error.messages) {
					html += jsonResponse.error.messages[i];
				}

				$('#msg_div').html(html).show().addClass('alert-danger')/*.delay(3000).fadeOut(function(){
					$(this).html('');
					$(this).removeClass('error');
				})*/;
			});

    	} else {

    		/*$('#msg_div').removeClass('success');
			$('#msg_div').html(errors[0]).show().addClass('error');
			$('#submit_quote').removeClass('prevent-click');*/

    	}
		
	};

	return {
		send:send
	}
}());