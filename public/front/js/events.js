$(document).ready(function (){
    $('.contact-back').find('a').attr('target', '_blank');
    var $infomail = $('.infomail');
    var $footermail = $('.footermail');
    var $robertmail = $('.robertmail');
    var $brianmail = $('.brianmail');
    var $barrymail = $('.barrymail');
    var $donmail = $('.donmail');
    var $robertlink = $('.robertlink');
    var $brianlink = $('.brianlink');
    var $barrylink = $('.barrylink');
    var $donlink = $('.donlink');
    if ($infomail.length){
        var username = "info";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + linktext + " </a>").appendTo($infomail);
    }
    if ($footermail.length){
        var username = "info";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'><i class='fa fa-lg fa-envelope'></i></a>").appendTo($footermail);
    }
    if ($robertmail.length){
        var username = "robert";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + linktext + " </a>").appendTo($robertmail);
    }
    if ($brianmail.length){
        var username = "brian";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + linktext + " </a>").appendTo($brianmail);
    }
    if ($barrymail.length){
        var username = "barry";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + linktext + " </a>").appendTo($barrymail);
    }
    if ($donmail.length){
        var username = "don";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + linktext + " </a>").appendTo($donmail);
    }
    if ($robertlink.length){
        var username = "robert";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + "<i class='fa fa-lg fa-envelope'></i>" + "</a>").appendTo($robertlink);
    }
    if ($brianlink.length){
        var username = "brian";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + "<i class='fa fa-lg fa-envelope'></i>" + "</a>").appendTo($brianlink);
    }
    if ($barrylink.length){
        var username = "barry";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + "<i class='fa fa-lg fa-envelope'></i>" + "</a>").appendTo($barrylink);
    }
    if ($donlink.length){
        var username = "don";
        var hostname = "spanmaster.ca";
        var linktext = username + "@" + hostname;
        $("<a href='mailto:" + linktext + "'>" + "<i class='fa fa-lg fa-envelope'></i>" + "</a>").appendTo($donlink);
    }
});

// contact
$('#contact_submit').click(function(){
    SM.App.Contact.send();
});

// quote
$('#quote_submit').click(function(){
    SM.App.Quote.send();
});
/*$('.filter_by_product').unbind('click').bind('click', function(){
    var type = $(this).attr('data-filter');
    global_product_page = 1;
    SM.App.Projects.listProduct(global_product_page, type)
});*/