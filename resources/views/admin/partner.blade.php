@extends('admin.layouts.inside')
@section('content')
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        @include('admin.common.sidebar')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE BAR -->
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{URL::to('admin/partner')}}">Partners Management</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>{{($partner_id > 0)?'Modify':'Add'}} Partner</span>
                        </li>
                    </ul>

                    <div class="form-actions pull-right margin-top-5px margin-bottom-5px">
                        <a href="{{URL::to('admin/partners')}}"><button type="button" class="btn green"><i class="fa fa-arrow-left fa-fw"></i> Go Back To Partners</button></a>
                    </div>
                </div>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                
                
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-8 ">
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase">{{($partner_id > 0)?'Modify':'Add New'}} partner</span>
                                </div>
                            </div>
                            <div class="portlet-body form">

                                <div class="msg_divs alert" id="msg_div"></div>
                               
                                <div class="form-body">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <div class="input-group margin-top-10 col-md-8">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Enter Name" id="partner_name" value="{{($partner_id > 0)?$partner->name:''}}"> </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group fg-file-upload-detail">
                                        <label for="partner-image" class="control-label">Image:</label>
                                        <p class="image-error partner-upload-error"></p>
                                        <input type="text" class="form-control input-lg input-file-path pull-left" placeholder="Select file to upload" id="file-span-msg-partner">
                                        <input type="file" class="custom_input_file" id="partnerImage"  multiple="true">
                                         <span class="input_label" style="margin-bottom: -46px;margin-right: -229px;">Browse</span>
                                        <input type="hidden" value="{{($partner_id > 0)?$partner->image:''}}" id="hidden-partner-image">
                                        <?php
                                            if (isset($partner->image) && $partner->image != '') {
                                        ?>
                                            <img class="image_preview" id="partner_preview" src="{{url('files/partner/'.$partner->image)}}" style="margin-right: -215px;"/>
                                        <?php
                                            }  else {
                                        ?>
                                            <img class="image_preview" id="partner_preview" src="{{url('pannel/images/image-not-found.png')}}" style="margin-right: -215px;"/>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-actions right">
                                    <a href="javascript:;" id="partner_submit_btn"><button type="button" class="btn green">Submit</button></a>
                                    <img class="button_spinners" src="{{URL::to('pannel/images/loader.gif')}}" id="submit_loader">
                                </div>
                               
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        
    </div>
    <!-- END CONTAINER -->

<input type="hidden" value="{{$partner_id}}" id="updated_partner_id"/>

@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {

    $('#partnerImage').fileupload({
        dataType: 'json',
        paramName:'file',
        formData:{'from':'partner'},
        limitMultiFileUploads:1,
        acceptFileTypes:/(\.|\/)(png|jpg|jpeg|gif)$/i,
        url:SM.Config.getApiUrl()+'image/upload',
        add: function (e, data) {
            
            $('.partner-upload-error').html('');
            
            // for file format validation
            if (!SM.App.isAcceptFileTypes(data.files[0]['type'])) {
                $('.partner-upload-error').removeClass('success');
                $('.partner-upload-error').addClass('error');
                $('.partner-upload-error').html('Invalid File Format.').delay(2000).fadeIn();
                return false;
            }

            // for file size validation
            var maxFileSize = 10000000; // 10MB
            if (data.files[0]['size'] > maxFileSize) {
                $('.partner-upload-error').removeClass('success');
                $('.partner-upload-error').addClass('error');
                $('.partner-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                return false;
            }

            var jqXHR = data.submit()
                .success(function (result, textStatus, jqXHR) {
                    if (result.status) {

                        $('#hidden-partner-image').val(result.file);
                        var html = 'Image has been uploaded successfully.';
                        if (jqXHR.responseJSON.messages){
                            var messages = jqXHR.responseJSON.messages;
                            var html =  messages;
                        }
                        //$('.partner-upload-error').removeClass('error');
                        //$('.partner-upload-error').addClass('success');
                        //$('.partner-upload-error').html(html).delay(2000).fadeIn();
                        $('#file-span-msg-partner').attr('placeholder',html);
                        $('#file-browse').addClass('new_label').text('Uploaded');
                        $('#hidden-partner-image').parent().removeClass('has-error');
                        $('#partner_preview').attr('src', SM.Config.getSiteUrl()+'/files/partner/'+result.file);
                    } else if(result.error) {
                        var html = 'An error occurred.';
                        if (result.error.messages && result.error.messages.length > 0){
                            var messages = jqXHR.responseJSON.error.messages;
                            var html =  messages[0];
                        }

                        $('.partner-upload-error').removeClass('success');
                        $('.partner-upload-error').addClass('error');
                        $('.partner-upload-error').html(html).delay(2000).fadeIn();
                    }
                })
                .error(function (jqXHR, textStatus, errorThrown) {
                    var html = 'An error occurred.';
                    if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0){
                        var messages = jqXHR.responseJSON.error.messages;
                        var html =  messages[0];
                    }
                    $('.partner-upload-error').removeClass('success');
                    $('.partner-upload-error').addClass('error');
                    $('.partner-upload-error').html(html).delay(2000).fadeIn();
 
                })
                .complete(function (result, textStatus, jqXHR) {
                    $('#hidden-partner-image').parent().removeClass('has-error');
                });
        },
        progressall: function (e, data) {
           
            var progress = parseInt(data.loaded / data.total * 100, 10);
            if (progress > 99) {
                progress = 99;
            };              
            $('#file-span-msg-partner').attr('placeholder','Upload '+progress+'% Complete');
        },
        done: function (e, data) {
            $('#hidden-partner-image').parent().removeClass('has-error');
        }
    });
});
</script> 
@endsection