@extends('admin.layouts.inside')
@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('admin.common.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{URL::to('admin/projects')}}">Projects Management</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Project Details</span>
                    </li>
                </ul>

                <div class="form-actions pull-right margin-top-5px margin-bottom-5px">
                    <a href="{{URL::to('admin/project?id='.$project_id)}}"><button type="button" class="btn green"><i class="fa fa-edit fa-fw"></i>Edit Details</button></a>
                </div>
            </div>
            <!-- END PAGE BAR -->

             <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">{{$project->name}}</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#project_details_tab" data-toggle="tab">Project Details</a>
                                    </li>

                                    <li class="">
                                        <a href="#project_materials_tab" data-toggle="tab">Materials</a>
                                    </li>

                                    <li class="">
                                        <a href="#project_uses_tab" data-toggle="tab">Uses</a>
                                    </li>

                                    <li class="">
                                        <a href="#project_images_tab" data-toggle="tab">Images</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- project details TAB -->
                                    <div class="tab-pane active" id="project_details_tab">
                                        <div class="mt-comments">
                                            <div class="mt-comment">
                                                <div class="mt-comment-body">
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Project Name</span>
                                                        <span class="mt-comment-text project-view-right"> {{(isset($project->name) && $project->name != '')?$project->name:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Description</span>
                                                        <span class="mt-comment-text project-view-right"> <?php echo (isset($project->description) && $project->description != '')?$project->description:'N/A' ?></span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Size</span>
                                                        <span class="mt-comment-text project-view-right"> {{(isset($project->size) && $project->size != '')?$project->size:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Type</span>
                                                        <span class="mt-comment-text project-view-right"> {{(isset($project->type->name) && $project->type->name != '')?$project->type->name:'N/A'}} 
                                                         {{(isset($project->type->description) && $project->type->description != '')?' | '.$project->type->description:''}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Shape</span>
                                                        <span class="mt-comment-text project-view-right"> {{(isset($project->shape->name) && $project->shape->name != '')?$project->shape->name:'N/A'}} {{(isset($project->shape->description) && $project->shape->description != '')?' | '.$project->shape->description:''}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Manufacturer</span>
                                                        <span class="mt-comment-text project-view-right"> {{(isset($project->client->name) && $project->client->name != '')?$project->client->name:'N/A'}} </span>
                                                    </div>   
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Testimonial</span>
                                                        <span class="mt-comment-text project-view-right"> <?php echo (isset($project->client->testimonial) && $project->client->testimonial != '')?$project->client->testimonial:'N/A'?> </span>
                                                    </div>    
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Client</span>
                                                        <span class="mt-comment-text project-view-right"> {{(isset($project->client_name) && $project->client_name != '')?$project->client_name:'N/A'}} </span>
                                                    </div> 
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Company</span>
                                                        <span class="mt-comment-text project-view-right">
                                                        <?php
                                                            if (isset($project->company_logo) && $project->company_logo != '') {
                                                        ?>  
                                                            <img src="{{url('files/company/'.$project->company_logo)}}" width="10%" height="10%">
                                                        <?php
                                                            }

                                                        ?>

                                                         {{(isset($project->company_name) && $project->company_name != '')?$project->company_name:'N/A'}} </span>
                                                    </div>                                         
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Start Date</span>
                                                        <span class="mt-comment-text project-view-right"> {{(isset($project->start_date_formatted) && $project->start_date_formatted != '')?$project->start_date_formatted:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">End Date</span>
                                                        <span class="mt-comment-text project-view-right"> {{(isset($project->end_date_formatted) && $project->end_date_formatted != '')?$project->end_date_formatted:'N/A'}} </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END project details TAB -->
 
                                    <!-- Materials TAB -->
                                    <div class="tab-pane" id="project_materials_tab">
                                        <div class="mt-comments">
                                            <div class="mt-comment">
                                                <div class="mt-comment-body">
                                                    
                                                    <?php
                                                        if (count($project->materials) > 0) {
                                                            foreach ($project->materials as $key => $material) {
                                                        ?>
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author" style="width:25%">{{$material->name}}</span>
                                                                <span class="mt-comment-text project-view-right"> {{(isset($material->description) && $material->description != '')?$material->description:'N/A'}} </span>
                                                            </div>
                                                            <hr/>
                                                        <?php
                                                            }
                                                        } else {
                                                    ?>
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author" style="text-align: center;">No Materials Found</span>
                                                        </div>
                                                    <?php
                                                        }
                                                    ?>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Materials TAB -->

                                     <!-- Uses TAB -->
                                    <div class="tab-pane" id="project_uses_tab">
                                        <div class="mt-comments">
                                            <div class="mt-comment">
                                                <div class="mt-comment-body">
                                                    
                                                    <?php
                                                        if (count($project->usages) > 0) {
                                                            foreach ($project->usages as $key => $use) {
                                                        ?>
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author" style="width:25%">{{$use->name}}</span>
                                                                <span class="mt-comment-text project-view-right"> {{(isset($use->description) && $use->description != '')?$use->description:'N/A'}} </span>
                                                            </div>
                                                            <hr/>
                                                        <?php
                                                            }
                                                        } else {
                                                    ?>
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author" style="text-align: center;">No Uses Found</span>
                                                        </div>
                                                    <?php
                                                        }
                                                    ?>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Uses TAB -->

                                    <!-- Images TAB -->
                                    <div class="tab-pane" id="project_images_tab">
                                        <div class="mt-comments">
                                            <div class="mt-comment">
                                                <div class="mt-comment-body">
                                                    
                                                    <?php
                                                        if (count($project->images) > 0) {
                                                            foreach ($project->images as $key => $image) {
                                                        ?>
                                                            <div class="image-thumb">
                                                                <p style="text-align: center;">{{(isset($image->title) && $image->title != '')?$image->title:''}}</p>
                                                                <img src="{{url('files/project/'.$image->image)}}" width="150px" height="100px">
                                                                
                                                            </div>
                                                        <?php
                                                            }
                                                        } else {
                                                    ?>
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author" style="text-align: center;">No Image Found</span>
                                                        </div>
                                                    <?php
                                                        }
                                                    ?>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Images TAB -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    
</div>
<!-- END CONTAINER -->

@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
});
</script> 
@endsection