@extends('admin.layouts.inside')
@section('content')
<link href="{{URL::to('pannel/css/jquery-te-1.4.0.css')}}" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        @include('admin.common.sidebar')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE BAR -->
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{URL::to('admin/teams')}}">About</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>{{($team_id > 0)?'Modify':'Add'}} Team Member</span>
                        </li>
                    </ul>

                    <div class="form-actions pull-right margin-top-5px margin-bottom-5px">
                        <a href="{{URL::to('admin/teams')}}"><button type="button" class="btn green"><i class="fa fa-arrow-left fa-fw"></i> Go Back To Team Members</button></a>
                    </div>
                </div>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                
                
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase">{{($team_id > 0)?'Modify':'Add New'}} Team Member</span>
                                </div>
                            </div>
                            <div class="portlet-body form">

                                <div class="msg_divs alert" id="msg_div"></div>
                               
                                <div class="form-body">

                                    <div class="row">
                                        <div class="form-group col-md-5">
                                            <label>Name</label>
                                            <div class="margin-top-10">
                                                <input type="text" class="form-control" placeholder="Enter Name" id="team_name" value="{{($team_id > 0)?$team->name:''}}"> </div>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label>Designation</label>
                                            <div class="margin-top-10">
                                                <input type="text" class="form-control" placeholder="Enter Designation" id="team_designation" value="{{($team_id > 0)?$team->designation:''}}"> </div>
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="form-group col-md-5">
                                            <label>Email</label>
                                            <div class="margin-top-10">
                                                <input type="text" class="form-control" placeholder="Enter Email" id="team_email" value="{{($team_id > 0)?$team->email:''}}"> </div>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label>Linked In Account</label>
                                            <div class="margin-top-10">
                                                <input type="text" class="form-control" placeholder="Enter Linked In account" id="team_linked_in" value="{{($team_id > 0)?$team->linked_in:''}}"> </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Description</label>
                                            <div class="margin-top-10">
                                            <textarea class="" id="team_description" style="width:100%">
                                                <?php
                                                    echo ($team_id > 0)?$team->description:'';
                                                ?>
                                            </textarea></div>
                                        </div>
                                        
                                        <div class="form-group col-md-5 fg-file-upload-detail">
                                            <label for="team-image" class="control-label">Image</label>
                                            <p class="image-error team-upload-error"></p>
                                            <input type="text" class="form-control input-lg input-file-path pull-left" placeholder="Browse image" id="file-span-msg-team">
                                            <input type="file" class="custom_input_file" id="teamImage"  multiple="true">
                                            <input type="hidden" value="{{($team_id > 0)?$team->image:''}}" id="hidden-team-image">
                                            <?php
                                                if (isset($team->image) && $team->image != '') {
                                            ?>
                                                <img class="image_preview_team" id="team_preview" src="{{url('files/team/'.$team->image)}}"/>
                                            <?php
                                                }  else {
                                            ?>
                                                <img class="image_preview_team" id="team_preview" src="{{url('pannel/images/image-not-found.png')}}"/>
                                            <?php
                                                }
                                            ?>
                                        </div> -->

                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <a href="javascript:;" id="team_submit_btn"><button type="button" class="btn green">Submit</button></a>
                                    <img class="button_spinners" src="{{URL::to('pannel/images/loader.gif')}}" id="submit_loader">
                                </div>
                               
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        
    </div>
    <!-- END CONTAINER -->

<input type="hidden" value="{{$team_id}}" id="updated_team_id"/>

@endsection
@section('scripts')
<script src="{{URL::to('pannel/js/jquery-te-1.4.0.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#team_description').jqte();
    $('#teamImage').fileupload({
        dataType: 'json',
        paramName:'file',
        formData:{'from':'team'},
        limitMultiFileUploads:1,
        acceptFileTypes:/(\.|\/)(png|jpg|jpeg|gif)$/i,
        url:SM.Config.getApiUrl()+'image/upload',
        add: function (e, data) {
            
            $('.team-upload-error').html('');
            
            // for file format validation
            if (!SM.App.isAcceptFileTypes(data.files[0]['type'])) {
                $('.team-upload-error').removeClass('success');
                $('.team-upload-error').addClass('error');
                $('.team-upload-error').html('Invalid File Format.').delay(2000).fadeIn();
                return false;
            }

            // for file size validation
            var maxFileSize = 10000000; // 10MB
            if (data.files[0]['size'] > maxFileSize) {
                $('.team-upload-error').removeClass('success');
                $('.team-upload-error').addClass('error');
                $('.team-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                return false;
            }

            var jqXHR = data.submit()
                .success(function (result, textStatus, jqXHR) {
                    if (result.status) {

                        $('#hidden-team-image').val(result.file);
                        var html = 'Image has been uploaded successfully.';
                        if (jqXHR.responseJSON.messages){
                            var messages = jqXHR.responseJSON.messages;
                            var html =  messages;
                        }
                        //$('.team-upload-error').removeClass('error');
                        //$('.team-upload-error').addClass('success');
                        //$('.team-upload-error').html(html).delay(2000).fadeIn();
                        $('#file-span-msg-team').attr('placeholder',html);
                        $('#file-browse').addClass('new_label').text('Uploaded');
                        $('#hidden-team-image').parent().removeClass('has-error');
                        $('#team_preview').attr('src', SM.Config.getSiteUrl()+'/files/team/'+result.file);
                    } else if(result.error) {
                        var html = 'An error occurred.';
                        if (result.error.messages && result.error.messages.length > 0){
                            var messages = jqXHR.responseJSON.error.messages;
                            var html =  messages[0];
                        }

                        $('.team-upload-error').removeClass('success');
                        $('.team-upload-error').addClass('error');
                        $('.team-upload-error').html(html).delay(2000).fadeIn();
                    }
                })
                .error(function (jqXHR, textStatus, errorThrown) {
                    var html = 'An error occurred.';
                    if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0){
                        var messages = jqXHR.responseJSON.error.messages;
                        var html =  messages[0];
                    }
                    $('.team-upload-error').removeClass('success');
                    $('.team-upload-error').addClass('error');
                    $('.team-upload-error').html(html).delay(2000).fadeIn();
 
                })
                .complete(function (result, textStatus, jqXHR) {
                    $('#hidden-team-image').parent().removeClass('has-error');
                });
        },
        progressall: function (e, data) {
           
            var progress = parseInt(data.loaded / data.total * 100, 10);
            if (progress > 99) {
                progress = 99;
            };              
            $('#file-span-msg-team').attr('placeholder','Upload '+progress+'% Complete');
        },
        done: function (e, data) {
            $('#hidden-team-image').parent().removeClass('has-error');
        }
    });
});
</script> 
@endsection