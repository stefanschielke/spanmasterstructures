@extends('admin.layouts.inside')
@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('admin.common.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{URL::to('admin/quotes')}}">Requested Quotes</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Quote Details</span>
                    </li>
                </ul>

                <div class="form-actions pull-right margin-top-5px margin-bottom-5px">
                    <a href="{{URL::to('admin/quotes')}}"><button type="button" class="btn green"><i class="fa fa-arrow-left fa-fw"></i>Go Back To Quotes</button></a>
                </div>
            </div>
            <!-- END PAGE BAR -->

             <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">{{$quote->name}}</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- quote details TAB -->
                                    <div class="tab-pane active" id="quote_details_tab">
                                        <div class="mt-comments">
                                            <div class="mt-comment">
                                                <div class="mt-comment-body">
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Name</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->name) && $quote->name != '')?$quote->name:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Company Name</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->company_name) && $quote->company_name != '')?$quote->company_name:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Email</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->email) && $quote->email != '')?$quote->email:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Phone</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->phone) && $quote->phone != '')?$quote->phone:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Fax</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->fax) && $quote->fax != '')?$quote->fax:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Address</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->address) && $quote->address != '')?$quote->address:'N/A'}} </span>
                                                    </div>   
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Use</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->use) && $quote->use != '')?$quote->use:'N/A'}} </span>
                                                    </div>                                            
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Other Use</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->other_use) && $quote->other_use != '')?$quote->other_use:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Installation Period</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->installation_period) && $quote->installation_period != '')?$quote->installation_period:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">About Us</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->about_us) && $quote->about_us != '')?$quote->about_us:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">About Us Reason</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->about_us_reason) && $quote->about_us_reason != '')?$quote->about_us_reason:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Comments</span>
                                                        <span class="mt-comment-text"> {{(isset($quote->comments) && $quote->comments != '')?$quote->comments:'N/A'}} </span>
                                                    </div>
                                                    <div class="mt-comment-info">
                                                        <span class="mt-comment-author" style="width:25%">Requested At</span>
                                                        <span class="mt-comment-text"> {{(date('d M, Y', strtotime($quote->created_at)))}} </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END quote details TAB -->
 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    
</div>
<!-- END CONTAINER -->

@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
});
</script> 
@endsection