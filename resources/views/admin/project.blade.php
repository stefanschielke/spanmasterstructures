@extends('admin.layouts.inside')
@section('content')

<link href="{{URL::to('pannel/css/jquery-te-1.4.0.css')}}" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('pannel/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('admin.common.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{URL::to('admin/projects')}}">Projects Management</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>{{($project_id > 0)?'Modify':'Add'}} Project</span>
                    </li>
                </ul>

                <div class="form-actions pull-right margin-top-5px margin-bottom-5px">
                    <a href="{{URL::to('admin/projects')}}"><button type="button" class="btn green"><i class="fa fa-arrow-left fa-fw"></i> Go Back To Projects</button></a>
                </div>
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            
            
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12 ">
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject bold uppercase">{{($project_id > 0)?'Modify':'Add New'}} Project</span>
                            </div>
                        </div>
                        <div class="portlet-body form">

                            <div class="msg_divs alert" id="msg_div"></div>
                           
                            <div class="form-body">

                                <div class="row">
                                    <div class="form-group col-md-5">
                                        <label>Name</label>
                                        <div class="margin-top-10">
                                            <input type="text" class="form-control" placeholder="Enter Name" id="project_name" value="{{($project_id > 0)?$project->name:''}}"> </div>
                                    </div>

                                    <div class="form-group col-md-5">
                                        <label>Size</label>
                                        <div class="margin-top-10">
                                            <input type="text" class="form-control" placeholder="Enter Size" id="project_size" value="{{($project_id > 0)?$project->size:''}}"> </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group ">
                                      <label for="">Description <span class="primary-text-color">(Select/Highlight text for formating)</span></label>
                                      <!-- <div class="editable-text" id="project_description">
                                            <?php
                                                echo ($project_id > 0)?$project->description:'';
                                            ?>
                                      </div> -->
                                      <textarea class="" id="project_description" style="width:100%">
                                            <?php
                                                echo ($project_id > 0)?$project->description:'';
                                            ?>
                                      </textarea>
                                    </div>

                                    <div class="form-group ">
                                      <label for="">Short Description<span class="primary-text-color">(Select/Highlight text for formating)</span></label>
                                      <!-- <div class="editable-text" id="project_description">
                                            <?php
                                                echo ($project_id > 0)?$project->description:'';
                                            ?>
                                      </div> -->
                                      <textarea class="" id="project_recent_description" style="width:100%">
                                            <?php
                                                echo ($project_id > 0)?$project->recent_description:'';
                                            ?>
                                      </textarea>
                                    </div>

                                    <div class="form-group  ">
                                      <label for="">Testimonial <span class="primary-text-color">(Select/Highlight text for formating)</span></label>
                                      <!-- <div class="editable-text" id="project_testimonial">
                                            <?php
                                                echo ($project_id > 0)?$project->client->testimonial:'';
                                            ?>
                                      </div> -->
                                      <textarea id="project_testimonial" style="width:100%">
                                            <?php
                                                echo ($project_id > 0)?$project->client->testimonial:'';
                                            ?>
                                      </textarea>
                                    </div>

                                </div>
                                
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label>Client Name</label>
                                        <div class="margin-top-10">
                                            <input type="text" class="form-control" placeholder="Enter Client Name" id="project_client_name" value="{{($project_id > 0)?$project->client_name:''}}"> </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label>Company Name</label>
                                        <div class="margin-top-10">
                                            <input type="text" class="form-control" placeholder="Enter Company Name" id="project_company_name" value="{{($project_id > 0)?$project->company_name:''}}"> </div>
                                    </div>
                                    
                                    <div class="form-group fg-file-upload-detail col-md-4">
                                        <label for="company-logo-image" class="control-label company-logo_label">Company Logo</label>
                                        <p class="image-error company-logo-upload-error"></p>
                                        <input type="text" class="form-control input-lg input-file-path pull-left file-span-msg-company-logo-image" placeholder="Select file to upload">
                                        <input type="file" class="custom_input_file company-logoImage"  multiple="true">
                                        <span class="input_label image_browse">Browse</span>
                                        <input type="hidden" class="hidden-company-logo-image" value="{{($project_id > 0)?$project->company_logo:''}}">
                                        
                                            <!-- <img class="image_preview" id="shop_preview" src="{{url('pannel/images/image-not-found.png')}}"/> -->
                                        
                                    </div>

                                </div>
                                <div class="row">

                                        <div class="form-group col-md-3">
                                            <label>Start Date</label>
                                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="{{($project_id > 0)?$project->start_date:''}}" id="start_date" name="start_date" placeholder="Select start date">
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label>End Date</label>
                                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="{{($project_id > 0)?$project->end_date:''}}" id="end_date" name="end_date" placeholder="Select end date">
                                        </div>
                                        
                                    
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label>Type</label>
                                        <div class="margin-top-10">
                                            <select class="bs-select form-control" id="project_type_id">
                                            
                                                <?php 
                                                    if (count($types) > 0) {
                                                ?>
                                                    <option value="0">Select Type</option>
                                                    <?php
                                                        foreach ($types as $key => $type) {
                                                    ?>
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                    <?php
                                                        }
                                                    ?>
                                                <?php
                                                    } else {
                                                ?>
                                                    <option value="0">No Type Found</option>
                                                <?php
                                                    }
                                                ?> 
                                            
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Manufacturer</label>
                                        <div class="margin-top-10">
                                            <select class="bs-select form-control" id="project_client_id">
                                            
                                                <?php 
                                                    if (count($clients) > 0) {
                                                ?>
                                                    <option value="0">Select Manufacturer</option>
                                                    <?php
                                                        foreach ($clients as $key => $client) {
                                                    ?>
                                                        <option value="{{$client->id}}">{{$client->name}}</option>
                                                    <?php
                                                        }
                                                    ?>
                                                <?php
                                                    } else {
                                                ?>
                                                    <option value="0">No Manufacturer Found</option>
                                                <?php
                                                    }
                                                ?> 
                                            
                                            </select>
                                        </div>
                                        
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label>Shape</label>
                                        <div class="margin-top-10">
                                            <select class="bs-select form-control" id="project_shape_id">
                                            
                                                <?php 
                                                    if (count($shapes) > 0) {
                                                ?>
                                                    <option value="0">Select Shape</option>
                                                    <?php
                                                        foreach ($shapes as $key => $shape) {
                                                    ?>
                                                        <option value="{{$shape->id}}">{{$shape->name}}</option>
                                                    <?php
                                                        }
                                                    ?>
                                                <?php
                                                    } else {
                                                ?>
                                                    <option value="0">No Shape Found</option>
                                                <?php
                                                    }
                                                ?> 
                                            
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="project_label">Uses</label>
                                        <div class="margin-top-10 use_rows">
                                            
                                                <?php 
                                                    if (count($uses) > 0) {
                                                        foreach ($uses as $key => $use) {
                                                    ?>
                                                        <div class="auto-grid-uses">
                                                            <label title="{{$use->id}}" class="custom-checkbox-1"><input type="checkbox" class="project_uses" name="project_uses" value="{{$use->id}}">{{$use->name}}</label>
                                                        </div>
                                                    <?php
                                                        }
                                                    } else {
                                                ?>
                                                    <div class="auto-grid-uses">No Usage Found</div>
                                                <?php
                                                    }
                                                ?> 
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-5">
                                        <label class="project_label">Material</label>
                                        <div class="margin-top-10 material_rows">
                                            <select class="bs-select form-control project_material_ids">
                                            
                                                <?php 
                                                    if (count($materials) > 0) {
                                                ?>
                                                    <option value="0">Select Material</option>
                                                    <?php
                                                        foreach ($materials as $key => $material) {
                                                    ?>
                                                        <option value="{{$material->id}}">{{$material->name}}</option>
                                                    <?php
                                                        }
                                                    ?>
                                                <?php
                                                    } else {
                                                ?>
                                                    <option value="0">No Material Found</option>
                                                <?php
                                                    }
                                                ?> 
                                            
                                            </select>
                                        </div>
                                        <a class="add-more-material pull-right" href="javascript:;"><i class="glyphicon glyphicon-plus"></i> Add more materials</a>
                                    </div>
                                </div>

                                <div class="row image_rows">
                                    <div class="form-group col-md-5">
                                        <label class="project_label">Image Title</label>
                                        <div class="margin-top-10">
                                            <input type="text" class="form-control image_title" placeholder="Enter Title" value=""> </div>
                                    </div>

                                    <div class="form-group fg-file-upload-detail col-md-5">
                                        <label for="project-image" class="control-label project_label">Image</label>
                                        <p class="image-error project-upload-error"></p>
                                        <input type="text" class="form-control input-lg input-file-path pull-left file-span-msg-project-image" placeholder="Select file to upload">
                                        <input type="file" class="custom_input_file projectImage"  multiple="true">
                                        <span class="input_label image_browse">Browse</span>
                                        <input type="hidden" value="" class="hidden-project-image">
                                        <input type="hidden" value="" class="hidden-project-real-image">
                                        
                                            <!-- <img class="image_preview" id="shop_preview" src="{{url('pannel/images/image-not-found.png')}}"/> -->
                                        
                                    </div>
                                </div>
                                <a class="add-more-images pull-right" href="javascript:;"><i class="glyphicon glyphicon-plus"></i> Add more images</a>
                                

                            </div>
                            <div class="form-actions right">
                                <a href="javascript:;" id="project_submit_btn"><button type="button" class="btn green">Submit</button></a>
                                <img class="button_spinners" src="{{URL::to('pannel/images/loader.gif')}}" id="submit_loader">
                            </div>
                           
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    
</div>
<!-- END CONTAINER -->

<input type="hidden" value="{{$project_id}}" id="updated_project_id"/>

@endsection
@section('scripts')

 <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('pannel/assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('pannel/assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('pannel/js/jquery-te-1.4.0.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script type="text/javascript">
var projectId = '<?php echo $project_id?>'
//var project = '<?php echo json_encode($project)?>'
$(document).ready(function() {
    // content live editor
    //$('#project_description').froalaEditor();
    //$('#project_testimonial').froalaEditor();
    $('#project_description').jqte();
    $('#project_recent_description').jqte();
     $('#project_testimonial').jqte();
    //grande.bind(document.querySelectorAll(".editable-text"));
    /*var projectData = JSON.parse(project);
        
    if (projectData != null) {
        $('#project_type_id').val(projectData.type.id);
        $('#project_client_id').val(projectData.client.id);
        $('#project_shape_id').val(projectData.shape.id);

        if (projectData.materials.length > 0) {
            $(projectData.materials).each(function(index, element){
                
                if (index > 0) {
                    var clonedElement = $('.add-more-material').prev().clone();

                    clonedElement.find('.project_material_ids').val(element.id);
                    clonedElement.find('.project_material_ids').parent().parent().removeClass('has-error');
                    clonedElement.find('.delete-project-material').remove();
                    clonedElement.find('.project_label').remove();
                    clonedElement.append('<a href="javascript:;" class="delete-project-material"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
                    clonedElement.insertAfter($('.add-more-material').prev());
                } else {
                    $('.project_material_ids').val(element.id);
                }
                
                $('.page-container').on('click', '.delete-project-material', function(e) {
                    e.preventDefault();
                    $(this).parent().remove();
                });
            });
        }

        $('.project_uses').each(function(){
            $(this).parent().removeClass('checked');
        });
        if (projectData.usages.length > 0) {

            //$("input[name=project_uses]").attr('checked', false);
            $(projectData.usages).each(function(index, element) {
                $("input[name=project_uses][value="+element.id+"]").attr('checked', true);
                $("input[name=project_uses][value="+element.id+"]").parent().addClass('checked');
            });
        }   

        if (projectData.images.length > 0) {
            $(projectData.images).each(function(index, element){
                if (index > 0) {
                    var clonedElement = $('.add-more-images').prev().clone();

                    clonedElement.find('.hidden-project-image').val(element.image);
                    clonedElement.find('.hidden-project-real-image').val(element.real_image);
                    clonedElement.find('.image_title').val(element.title);
                    clonedElement.find('.image_title').parent().removeClass('has-error');
                    clonedElement.find('.hidden-project-image').parent().removeClass('has-error');
                    clonedElement.find('.project-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
                    clonedElement.find('.file-span-msg-project-image').attr('placeholder', element.real_image);
                    clonedElement.find('.delete-project-image').remove();
                    clonedElement.find('.project_label').remove();
                    clonedElement.find('.image_browse').css('margin-bottom', '19px');
                    clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-project-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
                    clonedElement.insertAfter($('.add-more-images').prev());

                } else {
                    $('.file-span-msg-project-image').attr('placeholder', element.real_image);
                    $('.hidden-project-image').val(element.image);
                    $('.hidden-project-real-image').val(element.real_image);
                    $('.image_title').val(element.title);
                }
            });
        }
    }*/
    if (projectId > 0) {
        SM.App.Projects.view(projectId);
    }
    
    $('#start_date').datepicker({
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });
    $('#end_date').datepicker({
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });

    $('.date-picker').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });

    $('.page-container').on('click', '.add-more-material', function(e) {
        e.preventDefault();
        var clonedElement = $(this).prev().clone();

        clonedElement.find('.project_material_ids').val('0');
        clonedElement.find('.project_material_ids').parent().parent().removeClass('has-error');
        clonedElement.find('.delete-project-material').remove();
        clonedElement.find('.project_label').remove();
        clonedElement.append('<a href="javascript:;" class="delete-project-material"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
        clonedElement.insertAfter($(this).prev());
    });

    $('.page-container').on('click', '.delete-project-material', function(e) {
        e.preventDefault();
        $(this).parent().remove();
    });

    $('.page-container').on('click', '.add-more-images', function(e) {
        e.preventDefault();
        var clonedElement = $(this).prev().clone();

        clonedElement.find('.hidden-project-image').val('');
        clonedElement.find('.hidden-project-real-image').val('');
        clonedElement.find('.hidden-project-image').parent().removeClass('has-error');
        clonedElement.find('.image_title').val('');
        clonedElement.find('.image_title').parent().removeClass('has-error');
        clonedElement.find('.project-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
        clonedElement.find('.file-span-msg-project-image').attr('placeholder', 'Select file to upload');
        clonedElement.find('.delete-project-image').remove();
        clonedElement.find('.project_label').remove();
        clonedElement.find('.image_browse').css('margin-bottom', '19px');
        clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-project-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
        clonedElement.insertAfter($(this).prev());
    });

    $('.page-container').on('click', '.delete-project-image', function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    });

    jQuery(".page-container").on("click", '.projectImage', function() {

        var that = $(this).parent();
        $(this).fileupload({
            dataType: 'json',
            paramName: 'file',
            formData:{'from':'project'},
            acceptFileTypes: /(\.|\/)(png|jpg|jpeg|gif)$/i,
            limitMultiFileUploads: 1,
            url: SM.Config.getApiUrl() + 'image/upload',
            add: function(e, data) {

                that.find('.project-upload-error').html('');
                var acceptFileTypes = "(\.|\/)(png|jpg|jpeg|gif)$";
                acceptFileTypes = new RegExp(acceptFileTypes, "i");
                if (!acceptFileTypes.test(data.files[0]['type'])) {
                    $('.project-upload-error').removeClass('alert-success');
                    $('.project-upload-error').addClass('alert-danger');
                    $('.project-upload-error').html('Invalid File Format').fadeIn();
                    return false;
                }

                // for file size validation
                var maxFileSize = 10000000; // 10MB
                if (data.files[0]['size'] > maxFileSize) {
                    that.find('.project-upload-error').removeClass('alert-success');
                    that.find('.project-upload-error').addClass('alert-danger');
                    that.find('.project-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                    return false;
                }

                var jqXHR = data.submit()
                    .success(function(result, textStatus, jqXHR) {
                        if (result.status) {

                            that.find('.hidden-project-image').val(result.file);
                            that.find('.hidden-project-real-image').val(result.file_real_name);
                            var html = 'Image has been uploaded successfully.';
                            if (jqXHR.responseJSON.messages) {
                                var messages = jqXHR.responseJSON.messages;
                                var html = messages;
                            }
                            that.find('.file-span-msg-project-image').attr('placeholder', result.file_real_name);
                            that.find('.hidden-project-image').parent().removeClass('has-error');
                            that.find('.project-upload-error').removeClass('alert-danger');
                            that.find('.project-upload-error').addClass('alert-success');
                            //$('.project-upload-error').html(html).delay(2000).fadeOut();
                        } else if (result.error) {
                            var html = 'An error occurred.';
                            if (result.error.messages && result.error.messages.length > 0) {
                                var messages = jqXHR.responseJSON.error.messages;
                                var html = messages[0];
                            }

                            that.find('.file-span-msg-project-image').attr('placeholder', "Select Image to upload").addClass('alert-danger');
                            that.find('.project-upload-error').removeClass('alert-success');
                            that.find('.project-upload-error').addClass('alert-danger');
                            that.find('.project-upload-error').html(html).show().delay(2000).fadeOut();
                        }
                    })
                    .error(function(jqXHR, textStatus, errorThrown) {
                        var html = 'An error occurred.';
                        if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0) {
                            var messages = jqXHR.responseJSON.error.messages;
                            var html = messages[0];
                        }
                        that.find('.project-upload-error').removeClass('alert-success');
                        that.find('.project-upload-error').addClass('alert-danger');
                        that.find('.project-upload-error').html(html).delay(2000).fadeIn();

                    })
                    .complete(function(result, textStatus, jqXHR) {
                        $('.hidden-project-image').parent().removeClass('has-error');
                    });
            },
            progressall: function(e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                if (progress > 99) {
                    progress = 99;
                };
                that.find('.file-span-msg-project-image').attr('placeholder', 'Upload ' + progress + '% complete');
            },
            done: function(e, data) {
                that.find('.hidden-project-image').parent().removeClass('has-error');

            }
        });

    });

    jQuery(".page-container").on("click", '.company-logoImage', function() {

        var that = $(this).parent();
        $(this).fileupload({
            dataType: 'json',
            paramName: 'file',
            formData:{'from':'company'},
            acceptFileTypes: /(\.|\/)(png|jpg|jpeg|gif)$/i,
            limitMultiFileUploads: 1,
            url: SM.Config.getApiUrl() + 'image/upload',
            add: function(e, data) {

                that.find('.company-logo-upload-error').html('');
                var acceptFileTypes = "(\.|\/)(png|jpg|jpeg|gif)$";
                acceptFileTypes = new RegExp(acceptFileTypes, "i");
                if (!acceptFileTypes.test(data.files[0]['type'])) {
                    $('.company-logo-upload-error').removeClass('alert-success');
                    $('.company-logo-upload-error').addClass('alert-danger');
                    $('.company-logo-upload-error').html('Invalid File Format').fadeIn();
                    return false;
                }

                // for file size validation
                var maxFileSize = 10000000; // 10MB
                if (data.files[0]['size'] > maxFileSize) {
                    that.find('.company-logo-upload-error').removeClass('alert-success');
                    that.find('.company-logo-upload-error').addClass('alert-danger');
                    that.find('.company-logo-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                    return false;
                }

                var jqXHR = data.submit()
                    .success(function(result, textStatus, jqXHR) {
                        if (result.status) {

                            that.find('.hidden-company-logo-image').val(result.file);
                            var html = 'Image has been uploaded successfully.';
                            if (jqXHR.responseJSON.messages) {
                                var messages = jqXHR.responseJSON.messages;
                                var html = messages;
                            }
                            that.find('.file-span-msg-company-logo-image').attr('placeholder', 'Image has been uploaded');
                            that.find('.hidden-company-logo-image').parent().removeClass('has-error');
                            that.find('.company-logo-upload-error').removeClass('alert-danger');
                            that.find('.company-logo-upload-error').addClass('alert-success');
                            //$('.company-logo-upload-error').html(html).delay(2000).fadeOut();
                        } else if (result.error) {
                            var html = 'An error occurred.';
                            if (result.error.messages && result.error.messages.length > 0) {
                                var messages = jqXHR.responseJSON.error.messages;
                                var html = messages[0];
                            }

                            that.find('.file-span-msg-company-logo-image').attr('placeholder', "Select Image to upload").addClass('alert-danger');
                            that.find('.company-logo-upload-error').removeClass('alert-success');
                            that.find('.company-logo-upload-error').addClass('alert-danger');
                            that.find('.company-logo-upload-error').html(html).show().delay(2000).fadeOut();
                        }
                    })
                    .error(function(jqXHR, textStatus, errorThrown) {
                        var html = 'An error occurred.';
                        if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0) {
                            var messages = jqXHR.responseJSON.error.messages;
                            var html = messages[0];
                        }
                        that.find('.company-logo-upload-error').removeClass('alert-success');
                        that.find('.company-logo-upload-error').addClass('alert-danger');
                        that.find('.company-logo-upload-error').html(html).delay(2000).fadeIn();

                    })
                    .complete(function(result, textStatus, jqXHR) {
                        $('.hidden-company-logo-image').parent().removeClass('has-error');
                    });
            },
            progressall: function(e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                if (progress > 99) {
                    progress = 99;
                };
                that.find('.file-span-msg-company-logo-image').attr('placeholder', 'Upload ' + progress + '% complete');
            },
            done: function(e, data) {
                that.find('.hidden-company-logo-image').parent().removeClass('has-error');

            }
        });

    });

});
</script> 
@endsection