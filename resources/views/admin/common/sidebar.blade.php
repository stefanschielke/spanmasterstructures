<?php
    $page = Route::getCurrentRoute()->getPath();
    $page = explode('/', $page);
    $page = array_pop($page);
?>
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            

            <li class="nav-item start <?php if($page=='users' || $page=='user' || $page=='partners' || $page=='partner' || $page=='dealers' || $page=='dealer'){ echo "active";} ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">General</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <ul class="sub-menu">
                        <li class="nav-item start <?php if($page=='users' || $page=='user'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/users')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">User Management</span>
                                <span class=""></span>
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='partners' || $page=='partner'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/partners')}}" class="nav-link nav-toggle">
                                <i class="icon-briefcase"></i>
                                <span class="title">Partners Management</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='dealers' || $page=='dealer'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/dealers')}}" class="nav-link nav-toggle">
                                <i class="icon-briefcase"></i>
                                <span class="title">Dealers Management</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                    </ul>
                </a>
            </li>

            <li class="nav-item start <?php if($page=='settings-home'){ echo "active";} ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Home Page</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <ul class="sub-menu">
                        <li class="nav-item start <?php if($page=='settings-home'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/settings-home')}}" class="nav-link nav-toggle">
                                <i class="icon-pencil"></i>
                                <span class="title">Content Management</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                    </ul>
                </a>
            </li>

            <li class="nav-item start <?php if($page=='settings-about' || $page=='teams' || $page=='team'){ echo "active";} ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">About Page</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <ul class="sub-menu">
                        <li class="nav-item start <?php if($page=='settings-about'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/settings-about')}}" class="nav-link nav-toggle">
                                <i class="icon-pencil"></i>
                                <span class="title">Content Management</span>
                                <span class=""></span>
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='teams' || $page=='team'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/teams')}}" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Team Members</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                    </ul>
                </a>
            </li>

            <li class="nav-item start <?php if($page=='quotes' || $page=='quote'){ echo "active";} ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-info"></i>
                    <span class="title">Request a Quote Page </span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <ul class="sub-menu">
                        <li class="nav-item start <?php if($page=='quotes' || $page=='quote'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/quotes')}}" class="nav-link nav-toggle">
                                <span class="title">Requested Quotes</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                    </ul>
                </a>
            </li>

            <li class="nav-item start <?php if($page=='types' || $page=='type' || $page=='materials' || $page=='material' || $page=='shapes' || $page=='shape' || $page=='uses' || $page=='use' || $page=='clients' || $page=='client' || $page=='projects' || $page=='project'){ echo "active";} ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Projects Page </span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <ul class="sub-menu">
                        <li class="nav-item start <?php if($page=='types' || $page=='type'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/types')}}" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">Types</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='materials' || $page=='material'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/materials')}}" class="nav-link nav-toggle">
                                <i class="icon-wrench"></i>
                                <span class="title">Materials</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='shapes' || $page=='shape'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/shapes')}}" class="nav-link nav-toggle">
                                <i class="icon-globe"></i>
                                <span class="title">Shapes</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='uses' || $page=='use'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/uses')}}" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Uses</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='clients' || $page=='client'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/clients')}}" class="nav-link nav-toggle">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Manufacturer</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='projects' || $page=='project'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/projects')}}" class="nav-link nav-toggle">
                                <i class="icon-layers"></i>
                                <span class="title">Projects</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                    </ul>
                </a>
            </li>
            <li class="nav-item start <?php if($page=='repairs-maintenance'){ echo "active";} ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-rocket"></i>
                    <span class="title">Repairs & Maintenance Page</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <ul class="sub-menu">
                        <li class="nav-item start <?php if($page=='repairs-maintenance'){ echo "active open";} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Requests</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                    </ul>
                </a>
            </li>

            <li class="nav-item start <?php if($page=='contacts' || $page=='contact' || $page=='inquiry'){ echo "active";} ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Contact Page</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <ul class="sub-menu">
                        <li class="nav-item  <?php if($page=='inquiry'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/inquiry')}}" class="nav-link ">
                                <span class="title">General Inquiry</span>
                            </a>
                        </li>
                        <li class="nav-item  <?php if($page=='contacts' || $page=='contact'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/contacts')}}" class="nav-link ">
                                <span class="title">Contact Members</span>
                            </a>
                        </li>
                    </ul>
                </a>
            </li>

            <li class="nav-item start <?php if($page=='fabric-structure' || $page=='processes' || $page=='process'){ echo "active";} ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-rocket"></i>
                    <span class="title">Landing Page</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <ul class="sub-menu">
                        <li class="nav-item  <?php if($page=='fabric-structure'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/fabric-structure')}}" class="nav-link ">
                                <span class="title">Pre-engineered Fabric Structures</span>
                            </a>
                        </li>
                        <li class="nav-item start <?php if($page=='processes' || $page=='process'){ echo "active open";} ?>">
                            <a href="{{URL::to('admin/processes')}}" class="nav-link nav-toggle">
                                <span class="title">Our Process</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                        </li>
                    </ul>
                </a>
            </li>
            
            <li class="nav-item start <?php if($page=='user-profile'){ echo "active";} ?>">
                <a href="{{URL::to('admin/user-profile')}}" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">Profile Settings</span>
                    <span class=""></span>
                    <!-- <span class="arrow open"></span> -->
                </a>
            </li>
            <!-- <li class="nav-item start <?php if($page=='settings'){ echo "active";} ?>">
                <a href="{{URL::to('admin/settings')}}" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Settings</span>
                    <span class=""></span>
                    <!-- <span class="arrow open"></span> -->
                </a>
            </li> -->
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR