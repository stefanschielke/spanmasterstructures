@extends('admin.layouts.inside')
@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('admin.common.sidebar')

    <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE BAR -->
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{URL::to('admin/contacts')}}">Contact Page</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>General Inquiry</span>
                        </li>
                    </ul>
                </div>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                
                
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light bordered">
                             <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">General Inquiry</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <!-- <li class="active">
                                        <a href="#contact_tab" data-toggle="tab">General Inquiry </a>
                                    </li> -->
                                    <!-- <li>
                                        <a href="#contact_tab" data-toggle="tab">General Inquiry</a>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="portlet-body form">
                                <div class="tab-content">

                                    <div class="tab-pane active" id="contact_tab">
                                        <div class="msg_divs alert msg_div"></div>
                               
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Phone Number</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter phone number" id="inquiry_phone" value="{{(isset($settings->inquiry_phone) && $settings->inquiry_phone != '')?$settings->inquiry_phone:''}}"> </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Toll Free Number</label>
                                                    <div class="margin-top-10">
                                                    <input type="text" class="form-control" placeholder="Enter tolle free number" id="inquiry_toll_free" value="{{(isset($settings->inquiry_toll_free) && $settings->inquiry_toll_free != '')?$settings->inquiry_toll_free:''}}"></div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Email</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter email" id="inquiry_email" value="{{(isset($settings->inquiry_email) && $settings->inquiry_email != '')?$settings->inquiry_email:''}}"> </div>
                                                </div>
                                                
                                            </div>

                                        </div>

                                        <div class="form-actions right">
                                            <a href="javascript:;" class="setting_submit_btn"><button type="button" class="btn green">Save</button></a>
                                            <img class="button_spinners submit_loader" src="{{URL::to('pannel/images/loader.gif')}}">
                                        </div>
                                    </div>
                                </div>
                                
                               
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->


@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {

});
</script> 
@endsection