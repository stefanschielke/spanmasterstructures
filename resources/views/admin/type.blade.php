@extends('admin.layouts.inside')
@section('content')
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        @include('admin.common.sidebar')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE BAR -->
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{URL::to('admin/types')}}">Types Management</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>{{($type_id > 0)?'Modify':'Add'}} Type</span>
                        </li>
                    </ul>

                    <div class="form-actions pull-right margin-top-5px margin-bottom-5px">
                        <a href="{{URL::to('admin/types')}}"><button type="button" class="btn green"><i class="fa fa-arrow-left fa-fw"></i> Go Back To Types</button></a>
                    </div>
                </div>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                
                
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-8 ">
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase">{{($type_id > 0)?'Modify':'Add New'}} Type</span>
                                </div>
                            </div>
                            <div class="portlet-body form">

                                <div class="msg_divs alert" id="msg_div"></div>
                               
                                <div class="form-body">

                                    <div class="form-group col-md-8">
                                        <label>Name</label>
                                        <div class="margin-top-10">
                                            <input type="text" class="form-control" placeholder="Enter Name" id="type_name" value="{{($type_id > 0)?$type->name:''}}"> </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-8">
                                        <label>Description</label>
                                        <div class="margin-top-10">
                                            <textarea class="form-control" id="type_description" placeholder="Enter description" rows="3" value="{{($type_id > 0)?$type->description:''}}">{{($type_id > 0)?$type->description:''}}</textarea></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!-- <div class="form-group col-md-8">
                                        <label>Type</label>
                                        <div class="margin-top-10">
                                            <select class="bs-select form-control" id="type_type">
                                            
                                                <option value="">Select Type</option>
                                                <option value="project">Project</option>
                                                <option value="product">Product</option>
                                                <option value="both">Both</option>
                                            
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="clearfix"></div> -->
                                </div>
                                <div class="form-actions right">
                                    <a href="javascript:;" id="type_submit_btn"><button type="button" class="btn green">Submit</button></a>
                                    <img class="button_spinners" src="{{URL::to('pannel/images/loader.gif')}}" id="submit_loader">
                                </div>
                               
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        
    </div>
    <!-- END CONTAINER -->

<input type="hidden" value="{{$type_id}}" id="updated_type_id"/>

@endsection
@section('scripts')
<script type="text/javascript">
var type = '<?php echo json_encode($type)?>'
$(document).ready(function() {
    /*var typeData = JSON.parse(type);
    $('#type_type').val(typeData.type);*/
});
</script> 
@endsection