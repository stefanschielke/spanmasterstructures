<?php
    ob_start();
    include($_SERVER['DOCUMENT_ROOT'].'/common/header.php');
?>

<title>SpanMaster Structures Ltd. &bull; 1-886-935-4888 or 250-835-4888</title></head>
<meta name="robots" content="index, follow, archive" />
<meta name="description" content="SpanMaster Structures Ltd. is the leading British Columbia dealer of pre-engineered
    steel framed, fabric and metal covered buildings for the largest building manufacturers in North America."/>

<meta property="og:locale" content="en_CA" />
<meta property="og:type" content="website" />
<meta property="og:title" content="SpanMaster Structures Ltd. &bull; 1-886-935-4888 or 250-835-4888" />
<meta property="og:description" content="SpanMaster Structures Ltd. is the leading British Columbia dealer of
    pre-engineered steel framed, fabric and metal covered buildings for the largest building manufacturers in North America."/>
<meta property="og:url" content="http://spanmaster.ca/" />
<meta property="og:site_name" content="Spanmaster Structures Ltd." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="SpanMaster Structures Ltd. is the leading British Columbia dealer of
    pre-engineered steel framed, fabric and metal covered buildings for the largest building manufacturers in North America."/>
<meta name="twitter:title" content="SpanMaster Structures Ltd. &bull; 1-886-935-4888 or 250-835-4888"/>
<meta name="twitter:image" content="http://spanmaster.ca//common/img/................" />
<meta name="twitter:url" content="http://spanmaster.ca/" />

<?php
    include($_SERVER['DOCUMENT_ROOT'].'/common/navbar.php');
?>

<style>
</style>

<div class="first bot-30">
    <ul class="cb-slideshow">
        <li><span>Image 01</span><div><h4>Building Type 1</h4></div></li>
        <li><span>Image 02</span><div><h4>Building Type 2</h4></div></li>
        <li><span>Image 03</span><div><h4>Building Type 3</h4></div></li>
        <li><span>Image 04</span><div><h4>Building Type 4</h4></div></li>
        <li><span>Image 05</span><div><h4>Building Type 5</h4></div></li>
        <li><span>Image 06</span><div><h4>Building Type 6</h4></div></li>
    </ul>
</div>

<div class="top-30">
    <div>
        <div class="row">
            <img src="common/img/bars.png" alt="Spanmaster Structures Ltd. Logo" class="top-30 center-block img-responsive">
        </div>
        <div class="row">
            <h1 class="text-center red">
                If you can dream it, we can build it<br />
                no project is too small or too big!
            </h1>
        </div>
    </div>
</div>

<div class="container top-30">
    <div class="well well-lg">
        <div class="padded-20">
            <div class="row">
                <h2>Why choose Spanmaster Structures for your project?</h2>
            </div>
            <div class="row top-30">
                <h3>
                    Our locally qualified installers and sales staff will ensure that all your projects needs are met.
                    We listen to your requirements, ask the right questions and make sure that your Spanmaster Structure
                    will provide you with satisfaction for years to come.<br />
                    With over 20 years industry experience, you can be sure that our green products and environmentally
                    sound solutions will meet all of your expectations and regulatory requirements.
                </h3>
            </div>
            <div class="row top-20">
                <div class="col-xs-4 text-center">
                    <a href="#"><button type="button" class="btn btn-primary btn-lg gradient">About Us</button></a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="#"><button type="button" class="btn btn-danger btn-lg gradient">Request a Quote</button></a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="#"><button type="button" class="btn btn-success btn-lg gradient">Contact Us</button></a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row text-center">
        <h2>Recent Projects</h2>
    </div>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-md-offset-0 col-xs-10 col-xs-offset-1">
                <div class="row">
                    <a href="/services/web-solutions/" class="normal">
                        <div class="col-md-6 col-md-offset-5 wrap2 text-center thumbnail">
                            <i class="fa fa-5x fa-chrome"></i>
                            <h3>Website Design</h3>
                            <p>An interactive website can set you apart from your competition. Create a lasting impression on your customers.</p>
                            <button type="button" class="btn btn-primary">Learn more...</button>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-md-offset-0 col-xs-10 col-xs-offset-1">
                <div class="row">
                    <a href="/services/tech-support/" class="normal">
                        <div class="col-md-6 col-md-offset-1 col-xs-12 wrap2 thumbnail">
                            <i class="fa fa-5x fa-desktop"></i>
                            <h3>Technical Support</h3>
                            <p>Computer problems can bring your business to a standstill. Let us help you get your business back on track.<br />FAST</p>
                            <button type="button" class="btn btn-primary">Learn more...</button>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 text-center top-15">
            <a href="/services/">
                <button type="button" class="btn btn-primary active btn-lg">VIEW ALL SERVICES</button>
            </a>
        </div>
    </div>
</div>
<!-- third section -->

<!-- blue section -->
<div id="blue-section" class="pad-section">
</div>
<!-- blue section -->

<?php
    include($_SERVER['DOCUMENT_ROOT'].'/common/footer.php');
?>
