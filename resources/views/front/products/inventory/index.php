<?php
ob_start();
include($_SERVER['DOCUMENT_ROOT'].'/common/header.php');
?>

<title>Projects | SpanMaster Structures Ltd.</title></head>
<meta name="robots" content="index, follow, archive" />
<meta name="description" content="SpanMaster Structures Ltd. is the leading British Columbia dealer of pre-engineered
    steel framed, fabric and metal covered buildings for the largest building manufacturers in North America."/>

<!-- page level css Start -->
<link rel="stylesheet" type="text/css" href="/common/css/portfolio.css">
<link rel="stylesheet" type="text/css" href="/common/vendors/fancybox/source/jquery.fancybox.css">
<link rel="stylesheet" type="text/css" href="/common/vendors/fancybox/source/helpers/jquery.fancybox-buttons.css">
<!-- //page level css End -->

<style>
    .content {
        margin-left:auto;
        margin-right:auto;
    }
    .contact-back {
        background: url(/common/img/001a.jpg) no-repeat center center fixed;
        display: table;
        height: 100%;
        position: relative;
        width: 100%;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;

    }
    .mix a {
        color: #000000;
    }
</style>

<?php
include($_SERVER['DOCUMENT_ROOT'].'/common/navbar.php');
?>


<div class="contact-back">
<div class="container top-30">
    <!-- Imags Section Start -->
    <div class="row text-center">
        <div id="gallery">
            <div class="content">
                <div class="mix category-1" data-my-order="1">
                    <a href="/products/inventory/item/" title='Project 1'>
                        <div class="thumb_zoom"><img src="/common/img/inventory.jpg" class="img-responsive">Britespan Accent Building</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- //Imags Section Start End -->
</div>
</div>


<!--<div class="padded top-30 left-30 right-30">
    <embed src="/examples/FilterFunctionality/filter.html" style="width:100%; height: 1000px;">
</div>
-->

<?php
include($_SERVER['DOCUMENT_ROOT'].'/common/footer.php');
?>


<?php
include($_SERVER['DOCUMENT_ROOT'].'/common/scripts.php');
?>

<!-- page level js Start -->
    <script type="text/javascript" src="/common/vendors/mixitup/src/jquery.mixitup.js"></script>
    <script type="text/javascript" src="/common/vendors/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/common/vendors/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="/common/vendors/fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <script type="text/javascript" src="/common/js/portfolio.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox({
                helpers : {
                    title: {
                        type: 'over',
                        position: 'top'
                    }
                }
            });
        });
    </script>
<!-- //page level js End -->


<?php
include($_SERVER['DOCUMENT_ROOT'].'/common/bottom.php');
?>