<?php
    ob_start();
    include($_SERVER['DOCUMENT_ROOT'].'/common/header.php');
?>

<title>Current Inventory | SpanMaster Structures Ltd.</title></head>
<meta name="robots" content="index, follow, archive" />
<meta name="description" content="SpanMaster Structures Ltd. is the leading British Columbia dealer of pre-engineered
    steel framed, fabric and metal covered buildings for the largest building manufacturers in North America."/>

<?php
    include($_SERVER['DOCUMENT_ROOT'].'/common/navbar.php');
?>

<style>
    #menu1, #menu2, #menu3 {
        display: none;
    }

</style>

<!-- Container Section Start -->
<div class="container top-30">
    <!-- Project Description Section Satrt -->
    <h2 class="text-primary text-center">
        Britespan Accent Building
    </h2>
    <!-- //Project Description Section End -->
    <!-- Row Start -->
    <div class="row">
        <!-- Paragraph Text Start -->
        <div class="col-md-5 top-20 vcenter">
            <p class="text-justify bot-10">
                This building comes with one 10'x12' roll up fabric door and would be of good use as a personal storage unit.
            </p>
            <p>
                <br /><br />
            </p>
            <p>
                *please note: this photo is of a similar building, not necessarily the building described*
            </p>
            <div class="row bot-10">
                <div class="col-sm-4 text-center">
                    Option 1<br />
                    <button class="btn btn-info btn-md gradient" id="option1s">Show</button><!--&nbsp;&nbsp;&nbsp;<button class="btn btn-info btn-md gradient" id="option1h">Hide</button>-->
                </div>
                <div class="col-sm-4 text-center">
                    Option 2<br />
                    <button class="btn btn-info btn-md gradient" id="option2s">Show</button><!--&nbsp;&nbsp;&nbsp;<button class="btn btn-info btn-md gradient" id="option2h">Hide</button><br>-->
                </div>
                <div class="col-sm-4 text-center">
                    Option 3<br />
                    <button class="btn btn-info btn-md gradient" id="option3s">Show</button><!--&nbsp;&nbsp;&nbsp;<button class="btn btn-info btn-md gradient" id="option3h">Hide</button>-->
                </div>
            </div>

            <div class="row top-20" id="menu1">
                <div class="col-xs-12">
                    <dl class="dl-horizontal">
                        <dt>Type</dt>
                        <dd>Accent utility building</dd>
                        <dt>Material</dt>
                        <dd>Single pipe non engineered</dd>
                        <dt>Size</dt>
                        <dd>26' W x 50' L with 2 ends</dd>
                        <dt>Uses</dt>
                        <dd>Multi-Use</dd>
                    </dl>
                </div>
            </div>


            <div class="row top-20" id="menu2">
                <div class="col-xs-12">
                    <dl class="dl-horizontal">
                        <dt><span class="text-danger"><i class="fa fa-building pull-left"></i>Type</span></dt>
                        <dd>Accent utility building</dd>
                        <dt><span class="text-danger"><i class="fa fa-gears pull-left"></i>Material</span></dt>
                        <dd>Single pipe non engineered</dd>
                        <dt><span class="text-danger"><i class="fa fa-expand  pull-left"></i>Size</span></dt>
                        <dd>26' W x 50' L with 2 ends</dd>
                        <dt><span class="text-danger"><i class="fa fa-cubes pull-left"></i>Uses</span></dt>
                        <dd>Multi-Use</dd>
                    </dl>
                </div>
            </div>


            <div class="row top-20">
                <div class="col-xs-12">
                    <div class="box" id="menu3">
                        <h4 class="row box-bot">
                            <div class="col-xs-1 text-danger text-center"><i class="fa fa-arrow-circle-right"></i></div>
                            <div class="col-xs-3 box-lr">Type</div>
                            <div class="col-xs-7">Accent utility building</div>
                        </h4>
                        <h4 class="row box-bot">
                            <div class="col-xs-1 text-danger text-center"><i class="fa fa-arrow-circle-right"></i></div>
                            <div class="col-xs-3 box-lr">Material</div>
                            <div class="col-xs-7">Single pipe non engineered</div>
                        </h4>
                        <h4 class="row box-bot">
                            <div class="col-xs-1 text-danger text-center"><i class="fa fa-arrow-circle-right"></i></div>
                            <div class="col-xs-3 box-lr">Size</div>
                            <div class="col-xs-7">26' W x 50' L with 2 ends</div>
                        </h4>
                        <h4 class="row box-bot-non">
                            <div class="col-xs-1 text-danger text-center"><i class="fa fa-arrow-circle-right"></i></div>
                            <div class="col-xs-3 box-lr">Uses</div>
                            <div class="col-xs-7">Multi-Use</div>
                        </h4>
                    </div>
                </div>
            </div>

        </div>
        <!-- //Paragraph Text End -->
        <!-- Filmstrip Section Start -->
        <div class="col-md-6 img-responsive vcenter">
            <img src="/common/img/inventory.jpg" alt="ISO 9001 Certified" class="img-responsive">
        </div>
        <!-- //Filmstrip Section End -->
    </div>
    <!-- //Row End -->
</div>
<!-- //Container Section End -->


<?php
include($_SERVER['DOCUMENT_ROOT'].'/common/footer.php');
?>


<?php
include($_SERVER['DOCUMENT_ROOT'].'/common/scripts.php');
?>

<script>
    jQuery(document).ready(function($) {
        $("#option1s").click(function(){
            $("#menu1").show();
            $("#menu2").hide();
            $("#menu3").hide();
        });
        $("#option2s").click(function(){
            $("#menu1").hide();
            $("#menu2").show();
            $("#menu3").hide();
        });
        $("#option3s").click(function(){
            $("#menu1").hide();
            $("#menu2").hide();
            $("#menu3").show();
        });
    });
</script>

<?php
include($_SERVER['DOCUMENT_ROOT'].'/common/bottom.php');
?>

