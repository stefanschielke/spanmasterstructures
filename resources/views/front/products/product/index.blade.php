@extends('front.layouts.main')
@section('content')
<style>
    .content {
        margin-left:auto;
        margin-right:auto;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{url('front/css/portfolio.css')}}">
<!-- Container Section Start -->
<div class="container top-30">
    <div class="row services">
        <h2>
            <label class="border-bottom">Portfolio Item
            </label>
        </h2>
    </div>
    <!-- Project Description Section Satrt -->
    <h3 class="text-primary">
        {{(isset($product->name) && $product->name != '')?$product->name:''}}
    </h3>
    <!-- //Project Description Section End -->
    <!-- Row Start -->
    <div class="row">
        <!-- Paragraph Text Start -->
        <div class="col-md-6">
            <p class="text-justify">
                {{(isset($product->description) && $product->description != '')?$product->description:''}}
            </p>
            <br />
            <div>
                
                <?php
                    $projectType = [];
                    $projectSize = [];
                    $projectMaterial = [];
                    $projectClient = [];
                    $projectUse = [];
                    if (count($product_projects) > 0) {
                        $typeId = [];
                        $clientId = [];
                        $materialId = [];
                        $usesId = [];
                        foreach ($product_projects as $key => $product_project) {
                            $projectSize[] = $product_project->size;
                            if (!in_array($product_project->type->id, $typeId)) {
                                $projectType[] = $product_project->type->name;
                            }
                            if (!in_array($product_project->client->id, $clientId)) {
                                $projectClient[] = $product_project->client->name;
                            }

                            if (count($product_project->materials) > 0) {
                                foreach ($product_project->materials as $key => $material) {
                                    if (!in_array($material->id, $materialId)) {
                                        $projectMaterial[] = $material->name;
                                    }
                                    $materialId[] = $material->id;
                                } 
                            }

                            if (count($product_project->usages) > 0) {
                                foreach ($product_project->usages as $key => $usage) {
                                    if (!in_array($usage->id, $usesId)) {
                                        $projectUse[] = $usage->name;
                                    }
                                    $usesId[] = $usage->id;
                                }  
                                
                            }

                            $typeId[] = $product_project->type->id;
                            $clientId[] = $product_project->client->id;
                        }
                    }
                ?>

                <?php
                    if (count($projectType) > 0) {
                    ?>
                        <ul style="list-style-type:none; margin-left: -40px; "><span style="font-weight: bold;">Type</span>:

                    <?php
                        foreach ($projectType as $key => $type) {
                    ?>
                        <li style="margin-left: 30px;">{{$type}}</li>
                        <!-- <h4><span class="text-success"><i class="fa fa-html5"></i></span>&nbsp;&nbsp;{{$type}}</h4> -->
                    <?php
                        }
                        ?>

                        </ul>
                        <?php
                    }
                ?>

                <?php
                    if (count($projectSize) > 0) {
                    ?>
                        <ul style="list-style-type:none; margin-left: -40px; "><span style="font-weight: bold;">Size</span>:

                    <?php
                        foreach ($projectSize as $key => $size) {
                    ?>
                            <li style="margin-left: 30px;">{{$size}}</li>
                        <!-- <h4><span class="text-success"><i class="fa fa-comments"></i></span>&nbsp;&nbsp;{{$size}}</h4>  -->
                    <?php
                        }
                        ?>

                        </ul>
                        <?php
                    }
                ?>

                <?php
                    if (count($projectClient) > 0) {
                    ?>
                        <ul style="list-style-type:none; margin-left: -40px; "><span style="font-weight: bold;">Manufacturer</span>:

                    <?php
                        foreach ($projectClient as $key => $client) {
                    ?>
                            <li style="margin-left: 30px;">{{$client}}</li>
                       <!--  <h4><span class="text-success"><i class="fa fa-cog"></i></span>&nbsp;&nbsp;{{$client}}</h4> --> 
                    <?php
                        }
                        ?>

                        </ul>
                        <?php
                    }
                ?>

                <?php
                    if (count($projectMaterial) > 0) {
                        ?>
                        <ul style="list-style-type:none; margin-left: -40px; "><span style="font-weight: bold;">Material</span>:

                    <?php
                        foreach ($projectMaterial as $key => $material) {
                    ?>
                            <li style="margin-left: 30px;">{{$material}}</li>
                        <!-- <h4><span class="text-success"><i class="fa fa-bell"></i></span>&nbsp;&nbsp;{{$material}}</h4>  -->
                    <?php
                        }
                        ?>

                        </ul>
                        <?php
                    }
                ?>

                <?php
                    if (count($projectUse) > 0) {
                    ?>
                        <ul style="list-style-type:none; margin-left: -40px; "><span style="font-weight: bold;">Uses</span>:

                    <?php
                        foreach ($projectUse as $key => $use) {
                    ?>
                            <li style="margin-left: 30px;">{{$use}}</li>
                        <!-- <h4><span class="text-success"><i class="fa fa-heart"></i></span>&nbsp;&nbsp;{{$use}}</h4>  -->
                    <?php
                        }
                        ?>

                        </ul>
                        <?php
                    }
                ?>
                
                
            </div>
        </div>
        <!-- //Paragraph Text End -->
        <!-- Slider Section Start -->

        <?php
            if (count($product_projects) > 0) {
        ?>
            <div class="col-md-6">
                <div id="carousel-example-generic" class="carousel slide bot-20" data-ride="carousel">
                    <!-- Indicators -->
                    <!-- <ol class="carousel-indicators">
                        <?php
                            if (count($product_projects) > 0) {
                                foreach ($product_projects as $key => $product_project) {
                                    if (count($product_project->images) > 0) {
                                        foreach ($product_project->images as $key => $image) {
                                            $activeClass = '';
                                            if ($key == 0) {
                                                $activeClass = 'active';
                                            } else {
                                                $activeClass = '';
                                            }
                        ?>
                                    <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="{{$activeClass}}"></li>
                        <?php
                                        }
                                    }
                                }
                            }

                        ?>
                    </ol> -->
                    <!-- Wrapper for slides -->

                    <?php
                /*    echo "<pre>";
                    print_r($product_projects);
                    echo "</pre>";*/
                        ?>
                    <div class="carousel-inner" role="listbox" style="">
                        <?php

                            if (count($product_projects) > 0) {
                                $flag=true;
                                foreach ($product_projects as $key => $product_project) {
                                    if (count($product_project->images) > 0) {
                                        foreach ($product_project->images as $key => $image) {
                                            $activeClass = '';
                                            if ($flag) {
                                                $activeClass = 'active';
                                                $flag=false;
                                            }

                        ?>
                                    <div class="item {{$activeClass}}">
                                        <a href="{{url('project/'.$product_project->id)}}"><img src="{{url('files/project/'.$image->image)}}" alt="{{$image->title}}" class="img-responsive"></a>
                                    </div>
                        <?php
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="text-center">
                    <a href="{{url('quote')}}" class="top-20 text-center"><button type="button" class="btn btn-danger btn-lg gradient">Request a Quote</button></a>
                </div>

            </div>
        <?php
            }
        ?>
        
        <!-- //Slider Section End -->
    </div>
    <!-- //Row End -->
</div>
<!-- //Container Section End -->

@endsection
@section('scripts')

<script type="text/javascript">
$(document).ready(function() {
   
});
</script> 
@endsection
