@extends('front.layouts.main')
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('front/css/portfolio.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('front/vendors/fancybox/source/jquery.fancybox.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('front/vendors/fancybox/source/helpers/jquery.fancybox-buttons.css')}}">
<div class="contact-back top-30">
<div class="container top-30">
    <!-- Imags Section Start -->
    <div class="row text-center">
        <div id="gallerys">
            <div class="bot-10">
                <button class="btn filter btn-danger btn-lg gradient filter_by_product" data-filter="all">All</button>
                <button class="btn filter btn-danger btn-lg gradient filter_by_product" data-filter=".filter-types">Type</button>
                <button class="btn filter btn-danger btn-lg gradient filter_by_product" data-filter=".filter-uses">Use</button>
                <button class=" btn filter btn-danger btn-lg gradient filter_by_product" data-filter=".filter-shapes">Shape</button>
                <button class=" btn filter btn-danger btn-lg gradient filter_by_product" data-filter=".filter-materials">Material</button>
            </div>
            <div class="content" id="products_list">
            </div>
        </div>
    </div>
    <!-- //Imags Section Start End -->
</div>
</div>

@endsection
@section('scripts')

<!-- page level js Start -->
    <script type="text/javascript" src="{{url('front/vendors/mixitup/src/jquery.mixitup.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/jquery.fancybox.pack.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/helpers/jquery.fancybox-buttons.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/helpers/jquery.fancybox-media.js')}}"></script>
    <script type="text/javascript" src="{{url('front/js/portfolio.js')}}"></script>
<!-- //page level js End -->

<script type="text/javascript">
$(document).ready(function() {
    $(".fancybox").fancybox({
        helpers : {
            title: {
                type: 'over',
                position: 'top'
            }
        }
    });
    SM.App.Projects.listProduct(global_product_page, 'all');
});
</script> 
@endsection