@extends('front.layouts.main')
@section('content')

    @include('front.common.slider')

<style>

</style>

<div class="top-30">
    <div>
<!--        <div class="row">
            <img src="common/img/bars.png" alt="Spanmaster Structures Ltd. Logo" class="top-30 center-block img-responsive">
        </div>
-->        <div class="row">
            <h1 class="text-center red">
                <?php
                    $home_heading = 'If you can dream it, we can build it.<br />
                    No project is too big, or too small!';
                    if (isset($settings->home_heading) && $settings->home_heading != '') {
                        $home_heading = $settings->home_heading;
                    } 
                ?>
                <?php echo $home_heading?>
            </h1>
        </div>
    </div>
</div>

<div class="well">
<!--    <div class="well well-lg">
        <div class="padded-20">-->
            <div class="row lef-rig-30">
                <?php
                    $why_choose_title = 'Why choose Spanmaster Structures for your project?';
                    if (isset($settings->why_choose_title) && $settings->why_choose_title != '') {
                        $why_choose_title = $settings->why_choose_title;
                    } 
                ?>
                <h2>{{$why_choose_title}}</h2>
            </div>
            <div class="row lef-rig-30 why-choose">
                <?php
                $why_choose_description = 'Our local, qualified installers and building professionals will ensure that all your projects needs are met.
                    We listen to your requirements, ask the right questions, and make sure that your Spanmaster Structure
                    will provide you with satisfaction for years to come.<br /><br />
                    With more than half a century of industry experience, you can be sure that our green products and environmentally
                    sound solutions will meet all your expectations and regulatory requirements.';
                if (isset($settings->why_choose_description) && $settings->why_choose_description != '') {
                    $why_choose_description = $settings->why_choose_description;
                } 
            ?>
                <h3><?php echo $why_choose_description?></h3>
            </div>
            <div class="row top-20">
                <div class="col-xs-4 text-center">
                    <a href="{{url('about-us')}}"><button type="button" class="btn btn-danger btn-lg gradient btn-responsive">About Us</button></a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="{{url('quote')}}"><button type="button" class="btn btn-danger btn-lg gradient btn-responsive">Request a Quote</button></a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="{{url('contact')}}"><button type="button" class="btn btn-danger btn-lg gradient btn-responsive">Contact Us</button></a>
                </div>
            </div>
<!--        </div>
    </div>-->
</div>


    @include('front.common.recent-projects')
    <hr>
    @include('front.common.partners')


@endsection
@section('scripts')

<!-- <script type="text/javascript">
var slider_images = '<?php echo json_encode($slider_images)?>'
$(document).ready(function() {
    var sliderImages = JSON.parse(slider_images);  

    if (sliderImages.length > 0) {
        var animation = 0;
        $(sliderImages).each(function(index, element){
            var eq = index + parseInt(1);
            console.log(eq);
            console.log(animation);
            if (index == 0) {
                $('.cb-slideshow').eq(eq).find('span').css({"background-image": "url(files/slider/"+element.image+")"});
            } else {
                $('.cb-slideshow').eq(eq).find('span').css({"background-image": "url(files/slider/"+element.image+")",
                                                                "-webkit-animation-delay": ""+animation+"s",
                                                                "-moz-animation-delay": ""+animation+"s",
                                                                "-o-animation-delay": ""+animation+"s",
                                                                "-ms-animation-delay": ""+animation+"s",
                                                                "animation-delay": ""+animation+"s"});
                $('.cb-slideshow').eq(eq).find('div').css({"-webkit-animation-delay": ""+animation+"s",
                                                                "-moz-animation-delay": ""+animation+"s",
                                                                "-o-animation-delay": ""+animation+"s",
                                                                "-ms-animation-delay": ""+animation+"s",
                                                                "animation-delay": ""+animation+"s"});
            }
            

            animation = animation + parseInt(6);
        });
    } else {
        /*var imageArray = ['001.jpg', '002.jpg', '003.jpg', '004.jpg', '005.jpg'];
        var animation = 1;
        $(imageArray).each(function(index ,element){
            console.log(element);
            var eq = index + 1;
            if (index == 0) {
                $('.cb-slideshow').eq(eq).find('span').css({"background-image": "url(../img/"+element+")"});
            } else {
                $('.cb-slideshow').eq(eq).find('span').css({"background-image": "url(../img/"+element+")",
                                                                "-webkit-animation-delay": ""+animation+"s",
                                                                "-moz-animation-delay": ""+animation+"s",
                                                                "-o-animation-delay": ""+animation+"s",
                                                                "-ms-animation-delay": ""+animation+"s",
                                                                "animation-delay": ""+animation+"s"});
                $('.cb-slideshow').eq(eq).find('div').css({"-webkit-animation-delay": ""+animation+"s",
                                                                "-moz-animation-delay": ""+animation+"s",
                                                                "-o-animation-delay": ""+animation+"s",
                                                                "-ms-animation-delay": ""+animation+"s",
                                                                "animation-delay": ""+animation+"s"});
            }
            

            animation = animation + parseInt(6);

        });*/
    }
});
</script>  -->
@endsection