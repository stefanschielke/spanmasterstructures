<?php
    $page = Route::getCurrentRoute()->getPath();
    $page = explode('/', $page);
    $page = array_pop($page);

?>
<?php
    if ($page != 'quote' && $page != 'contact') {
?>
<a href="{{url('quote')}}" class="cta-right-bot">
   <span>Get your free quote today!</span>
   <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="66px" height="20.564px" viewBox="0 0 66 20.564" enable-background="new 0 0 66 20.564" xml:space="preserve">
<polygon fill="#E3CF6D " points="0.829,1.161 16.75,10.863 0.829,20.568 66,20.568 66,1.161 "/><polygon fill="#ED1C24 " points="0,0 15.92,9.702 0,19.406 65.171,19.406 65.171,0 "/></svg>
</a>

<?php
    }
?>

<footer class="left-30 right-30 top-30 bot-30">
    <div class="border-between bot-20">
        <div class="col-sm-4 col-xs-12 to_centre">
            3901 Myers Frontage Road<br />
            Tappen, B.C. V0E 2X3<br />
            Office: <a href="tel:12508354888">(250) 835-4888</a><br />
            Toll free: <a href="tel:18669354888">(866) 935-4888</a><br />
            Email: <span class="infomail"></span>
        </div>
        <div class="row visible-xs hidden-sm hidden-md-block hidden-lg-block">
            <div class="col-md-0 col-xs-offset-2 col-xs-8">
                <hr>
            </div>
        </div>
        <div class="clearfix visible-xs-block"></div>
        <div class="col-sm-4 col-xs-12 text-center">
            <div>&#169; Copyright <?php echo date("Y"); ?> All Rights Reserved <br/>
                Spanmaster Structures Ltd.</div>
            <div class="top-5"><a href="#">Terms of use</a></div>
            <div><a href="#">Privacy Policy</a></div>
            <div><a href="#">Site Map</a></div>
        </div>
        <div class="clearfix visible-xs-block"></div>
        <div class="row visible-xs hidden-sm hidden-md-block hidden-lg-block">
            <div class="col-md-0 col-xs-offset-2 col-xs-8">
                <hr>
            </div>
        </div>
        <div class="col-sm-4 col-xs-12 text-center">
            <div>
                <a href="{{url('quote')}}"><button type="button" class="btn btn-danger btn-sm gradient">Request a Quote</button></a>
                <a href="{{url('contact')}}"><button type="button" class="btn btn-danger btn-sm gradient">Contact Us</button></a>
                <a href="https://www.google.ca/maps/dir//Spanmaster+Structures+Ltd,+Myers+Frontage+Road,+Tappen,+BC/@50.8268592,-119.3542464,13z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x4d594fc10290a0fb:0x46578d4bf924e65b!2m2!1d-119.3192271!2d50.8268102" target="_blank">
                    <button type="button" class="btn btn-danger btn-sm gradient">Directions</button>
                </a>
            </div>
            <div class="row social">
                <ul class="list-unstyled">
                    <li><span class="footermail"></span></li>
                    <li><a href="https://www.facebook.com/SpanMaster-Structures-Ltd-227828957302800/" target="_blank" rel="me"><i class="fa fa-lg fa-facebook"></i></a></li>
                    <li><a href="https://plus.google.com/107353454812876509275" target="_blank" rel="me"><i class="fa fa-lg fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/" target="_blank" rel="me"><i class="fa fa-lg fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row"> </div>
<!--    <div class="top-20 text-center">Site by <a href="http://www.intechrity.ca/" target="_blank">inTechrity</a></div>
-->
</footer>

</div>




