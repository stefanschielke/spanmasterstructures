<?php
    if (count($team_members) > 0) {
?>
<div class="team-bg bot-30">
    <div class="about-headings">
        <h1>Our Management Team</h1>
    </div>
    <div class="team bot-20"> <!-- class="flex-element" -->
        <?php
            foreach ($team_members as $key => $team_member) {
        ?>
            <div class="col-xs-4">
                    <h4>{{$team_member->name}}</h4>
                    <h5>{{$team_member->designation}}</h5>
            </div>
        <?php
            }
        ?>
    </div>
</div>
<?php
    }
?>
