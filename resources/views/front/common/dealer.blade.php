<?php 
    if (count($dealers) > 0) {
?>
<div class="bot-20">
    <div class="strike">
        <h3 class="gold">
            Authorized Dealer For
        </h3>
    </div>
</div>

<div class="row bot-20 flex-element">
    <?php 
        if (count($dealers) > 0) {
            foreach ($dealers as $key => $dealer) {
                $firstClass = '';
                if ($key == 0) {
                    $firstClass = '';
                } else {
                    $firstClass = '';
                }
    ?>
            <div class="col-xs-2 {{$firstClass}}">
                <img src="{{url('files/dealer/'.$dealer->image)}}" alt="{{$dealer->name}}" class="center-block img-responsive">
            </div>
    <?php
            }
        } else {
    ?>
        <div class="text-center">
            <h2>No Dealers Found</h2>
        </div>
    <?php
        }
    ?>
</div>

    <!--<div class="row social text-center">
        <ul class="list-unstyled">
            <li><a href="https://www.facebook.com/intechritysolutions/" target="_blank" rel="me"><i class="fa fa-lg fa-facebook"></i></a></li>
            <li><a href="https://plus.google.com/107353454812876509275" target="_blank" rel="me"><i class="fa fa-lg fa-google-plus"></i></a></li>
            <li><a href="https://www.linkedin.com/company/intechrity-business-solutions" target="_blank" rel="me"><i class="fa fa-lg fa-linkedin"></i></a></li>
        </ul>
    </div>-->

<?php
        }
?>
<hr>