<?php
    if (count($recent_projects) > 0) {
?>
    <div class="container">
    <div class="row text-center">
        <h2>Recent Projects</h2>
    </div>
    <div id="myCarousel" class="carousel slide top-20" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php
                if (count($recent_projects) > 0) {
                    foreach ($recent_projects as $key => $recent_project) {
                        $activeClass = '';
                        if ($key == 0) {
                            $activeClass = 'active';
                        }
            ?>
                <li data-target="#myCarousel" data-slide-to="{{$key}}" class="{{$activeClass}}"></li>
            <?php
                    }
                }
            ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner text-center" role="listbox">
            <?php
                if (count($recent_projects) > 0) {
                    foreach ($recent_projects as $key => $recent_project) {
                        $activeClass = '';
                        if ($key == 0) {
                            $activeClass = 'active';
                        }
            ?>
                <div class="item {{$activeClass}}">
                    <a href="javascript:;">
                        <img src="{{url('files/project/'.$recent_project->images[0]->medium_image)}}" alt="{{$recent_project->name}}">
                        <div class="carousel-caption">
                            <h2>{{$recent_project->name}}</h2>
                            <p><?php echo $recent_project->recent_description?></p>
                        </div>
                    </a>
                </div>
            <?php
                    }
                } else {
            ?>
                <div class="text-center">
                    <h2>No Recent Projects Found</h2>
                </div>
            <?php
                }
            ?>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>
<?php
    }
?>
