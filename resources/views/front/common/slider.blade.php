<?php
/* echo "<pre>";
print_r($slider_images );
echo "</pre>";*/
?>

<?php
$total= 6;
$diffrence=6;
$totalAnimationTime= $total * $diffrence;
$totalPercentage = round(100/$total);
$totalPercentage;
if($totalPercentage<=25)
    $totalPercentage=25;


?>
<style>


    .cb-slideshow li span {
        -webkit-animation: imageAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
        -moz-animation: imageAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
        -o-animation: imageAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
        -ms-animation: imageAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
        animation: imageAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
    }
    .cb-slideshow li div {
        -webkit-animation: titleAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
        -moz-animation: titleAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
        -o-animation: titleAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
        -ms-animation: titleAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
        animation: titleAnimation <?php echo $totalAnimationTime; ?>s linear infinite 0s;
    }


    <?php
        for ($i=2 ; $i <= $total ; $i++){

            $second = $diffrence* ($i-1);

            ?>
               .cb-slideshow li:nth-child(<?php echo $i;?>) span,.cb-slideshow li:nth-child(<?php echo $i;?>) div {
                -webkit-animation-delay: <?php echo $second;?>s;
                -moz-animation-delay: <?php echo $second;?>s;
                -o-animation-delay: <?php echo $second;?>s;
                -ms-animation-delay: <?php echo $second;?>s;
                animation-delay: <?php echo $second;?>s;
            }
    <?php
}
?>






 /* Animation for the slideshow images */
    @-webkit-keyframes imageAnimation {
        0% {
            opacity: 0;
            -webkit-animation-timing-function: ease-in;
        }
        8% {
            opacity: 1;
            -webkit-transform: scale(1.05);
            -webkit-animation-timing-function: ease-out;
        }
        17% {
            opacity: 1;
            -webkit-transform: scale(1.1);
        }
    <?php echo  $totalPercentage ?>% {
            opacity: 0;
            -webkit-transform: scale(1.1);
        }
        100% { opacity: 0 }
    }
    @-moz-keyframes imageAnimation {
        0% {
            opacity: 0;
            -moz-animation-timing-function: ease-in;
        }
        8% {
            opacity: 1;
            -moz-transform: scale(1.05);
            -moz-animation-timing-function: ease-out;
        }
        17% {
            opacity: 1;
            -moz-transform: scale(1.1);
        }
    <?php echo  $totalPercentage ?>% {
            opacity: 0;
            -moz-transform: scale(1.1);
        }
        100% { opacity: 0 }
    }
    @-o-keyframes imageAnimation {
        0% {
            opacity: 0;
            -o-animation-timing-function: ease-in;
        }
        8% {
            opacity: 1;
            -o-transform: scale(1.05);
            -o-animation-timing-function: ease-out;
        }
        17% {
            opacity: 1;
            -o-transform: scale(1.1);
        }
    <?php echo  $totalPercentage ?>% {
            opacity: 0;
            -o-transform: scale(1.1);
        }
        100% { opacity: 0 }
    }
    @-ms-keyframes imageAnimation {
    0% {
        opacity: 0;
        -ms-animation-timing-function: ease-in;
    }
    8% {
        opacity: 1;
        -ms-transform: scale(1.05);
        -ms-animation-timing-function: ease-out;
    }
    17% {
        opacity: 1;
        -ms-transform: scale(1.1);
    }
    <?php echo  $totalPercentage ?>% {
        opacity: 0;
        -ms-transform: scale(1.1);
    }
    100% { opacity: 0 }
    }
    @keyframes imageAnimation {
        0% {
            opacity: 0;
            animation-timing-function: ease-in;
        }
        8% {
            opacity: 1;
            transform: scale(1.05);
            animation-timing-function: ease-out;
        }
        17% {
            opacity: 1;
            transform: scale(1.1);
        }
    <?php echo  $totalPercentage ?>% {
            opacity: 0;
            transform: scale(1.1);
        }
        100% { opacity: 0 }
    }
    /* Animation for the title */
    @-webkit-keyframes titleAnimation {
        0% {
            opacity: 0;
            -webkit-transform: translateY(200px);
        }
        8% {
            opacity: 1;
            -webkit-transform: translateY(0px);
        }
        17% {
            opacity: 1;
            -webkit-transform: scale(1);
        }
        19% { opacity: 0 }
    <?php echo  $totalPercentage ?>% {
            opacity: 0;
            -webkit-transform: scale(10);
        }
        100% { opacity: 0 }
    }
    @-moz-keyframes titleAnimation {
        0% {
            opacity: 0;
            -moz-transform: translateY(200px);
        }
        8% {
            opacity: 1;
            -moz-transform: translateY(0px);
        }
        17% {
            opacity: 1;
            -moz-transform: scale(1);
        }
        19% { opacity: 0 }
    <?php echo  $totalPercentage ?>% {
            opacity: 0;
            -moz-transform: scale(10);
        }
        100% { opacity: 0 }
    }
    @-o-keyframes titleAnimation {
        0% {
            opacity: 0;
            -o-transform: translateY(200px);
        }
        8% {
            opacity: 1;
            -o-transform: translateY(0px);
        }
        17% {
            opacity: 1;
            -o-transform: scale(1);
        }
        19% { opacity: 0 }
    <?php echo  $totalPercentage ?>% {
            opacity: 0;
            -o-transform: scale(10);
        }
        100% { opacity: 0 }
    }
    @-ms-keyframes titleAnimation {
    0% {
        opacity: 0;
        -ms-transform: translateY(200px);
    }
    8% {
        opacity: 1;
        -ms-transform: translateY(0px);
    }
    17% {
        opacity: 1;
        -ms-transform: scale(1);
    }
    19% { opacity: 0 }
    <?php echo  $totalPercentage ?>% {
        opacity: 0;
        -webkit-transform: scale(10);
    }
    100% { opacity: 0 }
    }
    @keyframes titleAnimation {
        0% {
            opacity: 0;
            transform: translateY(200px);
        }
        8% {
            opacity: 1;
            transform: translateY(0px);
        }
        17% {
            opacity: 1;
            transform: scale(1);
        }
        19% { opacity: 0 }
    <?php echo  $totalPercentage ?>% {
            opacity: 0;
            transform: scale(10);
        }
        100% { opacity: 0 }
    }
    /* Show at least something when animations not supported */




</style>

<div class="first bot-30">
    <ul class="cb-slideshow">
        <?php
        $final_images = [];
        if (count($slider_images) > 0) {
            $final_images = $slider_images;
        } else {

            $tmp = ['001.jpg'=>'Custom Fabric', '002.jpg'=>'Custom Metal', '003.jpg'=>'Engineered Arch', '004.jpg'=>'Engineered Peak', '005.jpg'=>'Utility Building'];
            foreach ($tmp as $image => $title) {
                $obj = new \StdClass;
                $obj->image = $image;
                $obj->default = 'yes';
                $obj->title = $title;
                $final_images[] = $obj;
            }
            
        }
        foreach ($final_images as $key => $image) {
            $imageLink = '';
            if (isset($image->default) && $image->default == 'yes') {
                $imageLink = "url('front/img/$image->image')";
            } else {
                $imageLink = "url('files/slider/$image->image')";
            }
        ?>
            <li> <span style="background-image:  <?php echo $imageLink?>; ">{{$image->title}}</span><div><h4>{{$image->title}}</h4></div></li>
        <?php
        }
        ?>
    </ul>
</div>