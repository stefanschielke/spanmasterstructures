<?php
    $page = Route::getCurrentRoute()->getPath();
    $page = explode('/', $page);
    if (isset($page[1]) && $page[1] == "{id}") {
        $page = $page[0];
    } else if (isset($page[2]) && $page[2] == "{id}") {
        $page = $page[0];
    } else {
        $page = array_pop($page);
    }
?>
<div class="top-banner">

</div>

<!-- navigation panel -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container bot-10">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand my-logo" href="{{url('/')}}"><img src="{{url('front/img/logogold.png')}}"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse top-10">
            <ul class="nav navbar-right nav-pills">
                <li class="<?php if($page==''){ echo "active";} ?>"><a href="{{url('/')}}">Home</a></li>
                <li class="<?php if($page=='about-us'){ echo "active";} ?>"><a href="{{url('about-us')}}">About</a></li>
                <li class="<?php if($page=='products' || $page=='product'){ echo "active";} ?>"><a href="{{url('products')}}">Products</a></li>
<!--                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Products<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li <?php /*if ($url == '/products/') { */?>class="active"<?php /*} */?>><a href="/products/">All products</a></li>
                        <li <?php /*if ($url == '/products/type/') { */?>class="active"<?php /*} */?>><a href="/products/type/">Browse by Type</a></li>
                        <li <?php /*if ($url == '/products/use/') { */?>class="active"<?php /*} */?>><a href="/products/use/">Browse by Use</a></li>
                        <li <?php /*if ($url == '/products/shape/') { */?>class="active"<?php /*} */?>><a href="/products/shape/">Browse by Shape</a></li>
                        <li <?php /*if ($url == '/products/current_inventory/') { */?>class="active"<?php /*} */?>><a href="/products/inventory/">Current Inventory</a></li>
                    </ul>
                </li>-->
                <li class="<?php if($page=='quote'){ echo "active";} ?>"><a href="{{url('quote')}}">Request a Quote</a></li>
                <li class="<?php if($page=='projects' || $page=='project'){ echo "active";} ?>"><a href="{{url('projects')}}">Projects</a></li>
                <li class="<?php if($page=='repairs'){ echo "active";} ?>"><a href="{{url('repairs')}}">Repairs and Maintenance</a></li>
                <li class="<?php if($page=='contact'){ echo "active";} ?>"><a href="{{url('contact')}}">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- navigation panel -->

