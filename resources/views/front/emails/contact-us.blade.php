<!DOCTYPE html>
<html>
<head>

</head>
<body>
<p>Hello!</p> 

<p>{{$data['name']}} has sent a contact request, details are:</p>

<p>Name : {{$data['name']}}</p>
<p>Company : {{(isset($data['company']) && $data['company'] != '')?$data['company']:''}}</p>
<p>Email : {{$data['email']}}</p>
<p>Phone : {{$data['phone']}}</p>
<p>Best time to call : {{($data['best_time_to_call'] && $data['best_time_to_call'] != '')?$data['best_time_to_call']:'N/A'}}</p>
<p>Message : <?php echo nl2br($data['message']) ?></p>

</body>
</html>
