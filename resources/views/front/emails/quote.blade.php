<!DOCTYPE html>
<html>
<head>

</head>
<body>
<p>Hello!</p> 

<p>{{$data['name']}} has sent a quote request, details are:</p>

<p>Name : {{$data['name']}}</p>
<p>Company : {{(isset($data['company_name']) && $data['company_name'] != '')?$data['company_name']:'N/A'}}</p>
<p>Email : {{$data['email']}}</p>
<p>Phone : {{$data['phone']}}</p>
<p>Fax : {{(isset($data['fax']) && $data['fax'] != '')?$data['fax']:'N/A'}}</p>
<p>Mailing Address : {{(isset($data['address']) && $data['address'] != '')?$data['address']:'N/A'}}</p>
<p>City : {{(isset($data['city']) && $data['city'] != '')?$data['city']:'N/A'}}</p>
<p>Postal Code : {{(isset($data['postal_code']) && $data['postal_code'] != '')?$data['postal_code']:'N/A'}}</p>
<?php
	if (isset($data['use']) && $data['use'] != '') {
		$data['use'] = ucfirst(str_replace('_', ' ', $data['use']));
	}

	if (isset($data['about_us']) && $data['about_us'] != '') {
		$data['about_us'] = ucfirst(str_replace('_', ' ', $data['about_us']));
	}

	if (isset($data['installation_period']) && $data['installation_period'] != '') {
		$data['installation_period'] = ucfirst(str_replace('_', ' ', $data['installation_period']));
	}
?>
<p>Use : {{(isset($data['use']) && $data['use'] != '')?$data['use']:'N/A'}}</p>
<p>Other use : {{(isset($data['other_use']) && $data['other_use'] != '')?$data['other_use']:'N/A'}}</p>
<p>About Us : {{(isset($data['about_us']) && $data['about_us'] != '')?$data['about_us']:'N/A'}}</p>
<p>About Us Description: {{(isset($data['about_us_reason']) && $data['about_us_reason'] != '')?$data['about_us_reason']:'N/A'}}</p>
<p>Installation Period : {{(isset($data['installation_period']) && $data['installation_period'] != '')?$data['installation_period']:'N/A'}}</p>
<p>Comments : <?php echo (isset($data['comments']) && $data['comments'] != '')?nl2br($data['comments']):'N/A' ?></p>
<p>Best time to call : {{(isset($data['best_time_to_call']) && $data['best_time_to_call'] != '')?$data['best_time_to_call']:'N/A'}}</p>
</body>
</html>
