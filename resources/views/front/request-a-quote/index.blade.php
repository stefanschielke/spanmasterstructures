@extends('front.layouts.main')
@section('content')
<link href="{{URL::to('pannel/css/jquery-te-1.4.0.css')}}" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('pannel/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />

<div class="contact-back">
    <div class="padded top-30 left-30 right-30">
        <div class="row">
            <div class="col-sm-10">
                <form class="form-horizontal" role="form">

                    <div class="form-group">
                        <label for="uses" class="col-sm-5 control-label">What are you going to use the building for?</label>
                        <div class="col-sm-7">
                            <select list="uses" name="use" id="use">
                                <option value="" selected disabled>Please select</option>
                                <option value="agriculture">Agriculture</option>
                                <option value="commercial">Commercial</option>
                                <option value="forestry">Forestry</option>
                                <option value="horse_equine">Horse & Equine</option>
                                <option value="industrial">Industrial</option>
                                <option value="marine_storage">Marine Storage</option>
                                <option value="mining">Mining</option>
                                <option value="personal_use_storage">Personal Use / Storage</option>
                                <option value="sports_complex">Sports Complex</option>
                                <option value="water_treatment">Water Treatment</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" style="display: none;" id="specify_for_other_use_div">
                        <label for="other" class="col-sm-5 control-label">Please Specify any other use:</label>
                        <div class="col-sm-7">
                            <input type="textarea" class="form-control input-sm" id="other_use" name="other_use" placeholder="For construction of NASA space shuttle" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="readiness" class="col-sm-5 control-label">When would you like your building installed?</label>
                        <div class="col-sm-7">
                            <select list="readiness" name="ready" id="installation_period">
                                <option value="" selected disabled>Please select</option>
                                <option value="asap">As soon as possible</option>
                                <option value="next_6_months">In the next 6 months</option>
                                <option value="next_year">In the next year</option>
                                <option value="just_looking">Just looking</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="heard" class="col-sm-5 control-label">How did you find out about us?</label>
                        <div class="col-sm-7">
                            <select list="heard" name="hear" id="about_us">
                                <option value="" selected disabled>Please select</option>
                                <option value="online_ad" data-id="1">Online ad</option>
                                <option value="print_ad" data-id="2">Print ad</option>
                                <option value="saw_building" data-id="3">Saw a building</option>
                                <option value="personal_referral" data-id="4">Personal referral</option>
                                <option value="other" data-id="5">Other</option>
                            </select>
                        </div>
                    </div>

                    <div id="option">
                        <div class="tab tab-open"></div>
                        <div class="tab" id="online_ad">
                            <div class="form-group">
                                <label for="onlinepub" class="col-sm-5 control-label">Website Name:</label>
                                <div class="col-sm-7">
                                    <input type="textarea" class="form-control input-sm" id="about_us_reason1" name="onlinepub" placeholder="Google ads">
                                </div>
                            </div>
                        </div>

                        <div class="tab" id="print_ad">
                            <div class="form-group">
                                <label for="printpub" class="col-sm-5 control-label">Publication Name:</label>
                                <div class="col-sm-7">
                                    <input type="textarea" class="form-control input-sm" id="about_us_reason2" name="printpub" placeholder="Architectural Digest">
                                </div>
                            </div>
                        </div>

                        <div class="tab" id="personal_referral">
                            <div class="form-group">
                                <label for="referral" class="col-sm-5 control-label">Name of the person or company that referred you?</label>
                                <div class="col-sm-7">
                                    <input type="textarea" class="form-control input-sm" id="about_us_reason4" name="referral" placeholder="Mr. Joe Example">
                                </div>
                            </div>
                        </div>

                        <div class="tab" id="saw_building">
                            <div class="form-group">
                                <label for="location" class="col-sm-5 control-label">Where did you see the building?</label>
                                <div class="col-sm-7">
                                    <input type="textarea" class="form-control input-sm" id="about_us_reason3" name="location" placeholder="Tappen Pulp Mill">
                                </div>
                            </div>
                        </div>

                        <div class="tab" id="other">
                            <div class="form-group">
                                <label for="otherad" class="col-sm-5 control-label">Other Place:</label>
                                <div class="col-sm-7">
                                    <input type="textarea" class="form-control input-sm" id="about_us_reason5" name="otherad" placeholder="Radio">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="message" class="col-sm-5 control-label">Comments or Questions:</label>
                        <div class="col-sm-7">
                            <textarea class="form-control input-sm" rows="4" name="message" id="comments"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-5 control-label">Name <span class="red">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control input-sm" id="name" name="name" placeholder="First & Last Name" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="company" class="col-sm-5 control-label">Company</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control input-sm" id="company_name" name="company_name" placeholder="Company name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-5 control-label">Phone <span class="red">*</span></label>
                        <div class="col-sm-7">
                            <input type="tel" class="form-control input-sm" id="phone" name="phone" placeholder="555-555-1212" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5">Best time to call</label>
                        <div class="col-md-3">
                            <div class="input-group">
                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="best_time_to_call">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-clock-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fax" class="col-sm-5 control-label">Fax</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="555-555-1212">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-5 control-label">Email <span class="red">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control input-sm" id="email" name="email" placeholder="someone@example.com" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-5 control-label">Mailing address</label>
                        <div class="col-sm-7">
                            <textarea class="form-control input-sm" rows="3" name="mailing" id="address"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-sm-5 control-label">City <span class="red">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control input-sm" name="city" id="city">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="postalcode" class="col-sm-5 control-label">Postal Code</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control input-sm" name="postalcode" id="postal_code">
                        </div>
                    </div>

                    <div class="row nothing">
                        <label for="nothing" class="col-sm-5 control-label">Leave this empty</label>
                        <div class="col-sm-7">
                            <input type="checkbox" id="urll" name="urll">
                        </div>
                    </div>
                    <div class="form-group top-15">
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <div class="alert quote-msg" id="msg_div">  </div>
                            </div>
                        </div>

                        <div class="col-sm-1 col-sm-offset-5">
                            <img class="button_spinners quote_loader" src="{{url('front/images/ajax-loader_dark.gif')}}" id="quote_loader">
                            <input id="quote_submit" name="submit" type="button" value="Send" class="btn btn-primary">
                        </div>
                        <div class="col-sm-5 col-sm-offset-1">
                            <p>All fields marked with <span class="red">*</span> are required.</p>
                            <p>We respect your privacy - your information will not be shared or sold to third parties.</p>
                            <p>We respond to quote requests within 48 hours. If you do not receive a reply within 48 hours please <a href="{{url('contact')}}">contact us</a>.</p>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')

 <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('pannel/assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('pannel/assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#best_time_to_call').timepicker();
    $('#best_time_to_call').val('');
    $("#use").change(function(){
        // document.getElementById("MyEdit").innerHTML = document.getElementById("options").value;
        var v = $(this).val();
        if (v== 'other') {
            $('#specify_for_other_use_div').show();
        } else {
            $('#specify_for_other_use_div').hide();
        }
    });

    $("#about_us").change(function(){
        // document.getElementById("MyEdit").innerHTML = document.getElementById("options").value;
        var v = document.getElementById("about_us").value;
        $('#option .tab-open').removeClass('tab-open');
        $('#'+ v).addClass('tab-open');
    });
});
</script> 
@endsection