@extends('front.layouts.main')
@section('content')
<!-- page level css Start -->
<link rel="stylesheet" type="text/css" href="{{url('front/css/portfolio.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('front/vendors/fancybox/source/jquery.fancybox.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('front/vendors/fancybox/source/helpers/jquery.fancybox-buttons.css')}}">
<!-- //page level css End -->

<div class="contact-back">
    <div class="container top-30">
        <!-- Imags Section Start -->
        <div class="row text-center">
            <div id="gallery">
                <div class="bot-10">
                    <button class="btn filter btn-danger btn-lg gradient filter_by_type btn-responsive" data-filter="all" data-type="0">All</button>
                    <?php
                        if (count($types) > 0) {
                            foreach ($types as $key => $type) {
                    ?>
                        <button class="btn filter btn-danger btn-lg gradient filter_by_type btn-responsive" data-filter=".project-type-{{$type->id}}" data-type="{{$type->id}}">{{$type->name}}</button>
                    <?php
                            }
                        }
                    ?>
                </div>
                <div class="content" id="projects_list">
                    
                    
                </div>

                <button class="btn filter btn-danger btn-lg gradient load-more" style="display: block;">Load More</button>
            </div>
        </div>
        <!-- //Imags Section Start End -->
    </div>
</div>

@endsection
@section('scripts')

<!-- page level js Start -->
    <script type="text/javascript" src="{{url('front/vendors/mixitup/src/jquery.mixitup.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/jquery.fancybox.pack.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/helpers/jquery.fancybox-buttons.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/helpers/jquery.fancybox-media.js')}}"></script>
    <script type="text/javascript" src="{{url('front/js/portfolio.js')}}"></script>
<!-- //page level js End -->

<script type="text/javascript">
$(document).ready(function() {
    $(".fancybox").fancybox({
        helpers : {
            title: {
                type: 'over',
                position: 'top'
            }
        }
    });

    SM.App.Projects.list();
});
</script> 
@endsection