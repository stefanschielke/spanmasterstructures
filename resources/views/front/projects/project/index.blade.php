@extends('front.layouts.main')
@section('content')
<!--page level css Start -->
<link rel="stylesheet" type="text/css" href="{{url('front/vendors/filmstrip/jquery.filmstrip.css')}}">
<!-- //page level css End -->
<style>
    .content {
        margin-left:auto;
        margin-right:auto;
    }
</style>

<!-- Container Section Start -->
<div class="container top-30">
    <!-- Project Description Section Satrt -->
    <h2 class="text-primary text-center">
        {{(isset($project->name)?$project->name:'')}}
    </h2>
    <!-- //Project Description Section End -->
    <!-- Row Start -->
    <div class="row">
        <!-- Paragraph Text Start -->
        <div class="col-md-6 top-20">
            <p class="text-justify bot-10">
                <?php
                    echo (isset($project->description) && $project->description != '')?$project->description:'';
                ?>
            </p>
            <!--   <div class="row bot-10">
                <div class="col-sm-4 text-center">
                    Option 1<br />
                    <button class="btn btn-info btn-md gradient" id="option1s">Show</button>
                </div>
                <div class="col-sm-4 text-center">
                    Option 2<br />
                    <button class="btn btn-info btn-md gradient" id="option2s">Show</button>
                </div>
                <div class="col-sm-4 text-center">
                    Option 3<br />
                    <button class="btn btn-info btn-md gradient" id="option3s">Show</button>
                </div>
            </div>-->

            <div class="row top-20" id="menu1">
                <div class="col-xs-offset-1 col-xs-10">
                    <dl class="dl-horizontal">
                        <dt>Type</dt>
                            <dd>{{(isset($project->type->name)?$project->type->name:'')}}</dd>
                        <dt>Manufacturer</dt>
                            <dd>{{(isset($project->client->name) && $project->client->name != '')?$project->client->name:''}}</dd>
                        <dt>Size</dt>
                            <dd>{{(isset($project->size)?$project->size:'')}}</dd>
                        <dt>Material</dt>
                            <?php
                                if (count($project->materials) > 0) {
                                    foreach ($project->materials as $key => $material) {
                            ?>  
                                <dd>{{$material->name}}</dd>
                            <?php
                                    }
                                } else {
                            ?>
                                 <dd>N/A</dd>
                            <?php
                                }
                            ?>
                        <dt>Uses</dt>
                            <?php
                                if (count($project->usages) > 0) {
                                    foreach ($project->usages as $key => $usage) {
                            ?>  
                                <dd>{{$usage->name}}</dd>
                            <?php
                                    }
                                } else {
                            ?>
                                <dd>N/A</dd>
                            <?php
                                }
                            ?>
                    </dl>
                </div>
            </div>
                <!--

            <div class="row top-20" id="menu2">
                <div class="col-xs-offset-2 col-xs-9">
                    <dl class="dl-horizontal">
                        <dt><span class="text-danger"><i class="fa fa-building pull-left"></i>Type</span></dt>
                        <dd>Robertson</dd>
                        <dt><span class="text-danger"><i class="fa fa-gears pull-left"></i>Material</span></dt>
                        <dd>Metal</dd>
                        <dt><span class="text-danger"><i class="fa fa-expand  pull-left"></i>Size</span></dt>
                        <dd>40 x 45 15' on center</dd>
                        <dt><span class="text-danger"><i class="fa fa-cubes pull-left"></i>Uses</span></dt>
                        <dd>Multi-Use Storage</dd>
                    </dl>
                </div>
            </div>


            <div class="row top-20">
                <div class="col-xs-offset-1 col-xs-10">
                    <div class="box" id="menu3">
                        <h4 class="row box-bot">
                            <div class="col-xs-1 text-danger text-center"><i class="fa fa-arrow-circle-right"></i></div>
                            <div class="col-xs-3 box-lr">Type</div>
                            <div class="col-xs-7">Robertson</div>
                        </h4>
                        <h4 class="row box-bot">
                            <div class="col-xs-1 text-danger text-center"><i class="fa fa-arrow-circle-right"></i></div>
                            <div class="col-xs-3 box-lr">Material</div>
                            <div class="col-xs-7">Metal</div>
                        </h4>
                        <h4 class="row box-bot">
                            <div class="col-xs-1 text-danger text-center"><i class="fa fa-arrow-circle-right"></i></div>
                            <div class="col-xs-3 box-lr">Size</div>
                            <div class="col-xs-7">40 x 45 15' on center</div>
                        </h4>
                        <h4 class="row box-bot-non">
                            <div class="col-xs-1 text-danger text-center"><i class="fa fa-arrow-circle-right"></i></div>
                            <div class="col-xs-3 box-lr">Uses</div>
                            <div class="col-xs-7">Multi-Use Storage</div>
                        </h4>
                    </div>
                </div>
            </div>
                -->
        </div>
        <!-- //Paragraph Text End -->
        <!-- Filmstrip Section Start -->
        <div class="col-md-6">
            <div id="filmstrip" class="filmstrip slide">
                <div class="filmstrip-top">
                    <div class="filmstrip-inner">
                        <?php
                            if (count($project->images) > 0) {
                                foreach ($project->images as $key => $image) {
                                    $activeClass = '';
                                    if ($key == 0) {
                                        $activeClass = 'active';
                                    }
                        ?>
                            <div class="item {{$activeClass}} main_image">
                                <img src="{{url('files/project/'.$image->image)}}">
                            </div>
                        <?php
                                }
                            }
                        ?>
                    </div>
                    <a class="left filmstrip-control" href="#filmstrip" data-filmstrip="prev">&lsaquo;</a>
                    <a class="right filmstrip-control" href="#filmstrip" data-filmstrip="next">&rsaquo;</a>
                </div>
                <div class="filmstrip-thumbs">

                    <?php
                        if (count($project->images) > 0) {
                            foreach ($project->images as $key => $image) {
                                $activeClass = '';
                                $pointerDisplay = 'none';
                                if ($key == 0) {
                                    $activeClass = 'selector';
                                    $pointerDisplay = 'block';
                                }
                    ?>
                        <a class="thumb {{$activeClass}}" href="#filmstrip" data-filmstrip="selector">
                            <img src="{{url('files/project/'.$image->image)}}">
                            <div class="pointer" style="display: {{$pointerDisplay}}"></div>
                        </a>

                    <?php
                            }
                        }
                    ?>
                </div>
            </div>



            <!--            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
            <!--                <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <!-- Wrapper for slides -->
            <!--                <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="{{url('front/imgs/1_600.jpg')}}" alt="portfolio image" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="{{url('front/imgs/2_600.jpg')}}" alt="portfolio image" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="{{url('front/imgs/3_600.jpg')}}" alt="portfolio image" class="img-responsive">
                                </div>
                            </div>
                            <!-- Controls -->
            <!--                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>-->
        </div>
        <!-- //Filmstrip Section End -->
    </div>
    <div class="row top-20">
        <div class="col-xs-offset-1 col-xs-10">
            <h3>Client Testimonial</h3>
            <div class="testimonial text-justify">
                <p class="editable-text">
                <?php
                    echo (isset($project->client->testimonial) && $project->client->testimonial != '')?$project->client->testimonial:'';
                ?></p>
            </div>
            <!-- <p class="pull-right">
                <b>{{(isset($project->client->name) && $project->client->name != '')?$project->client->name:''}}</b><br />
                <i>{{(isset($project->client->designation) && $project->client->designation != '')?$project->client->designation:''}}</i>
            </p> -->
        </div>
    </div>

    <!-- //Row End -->
</div>
<!-- //Container Section End -->

@endsection
@section('scripts')

<!-- page level js Start -->
<script type="text/javascript" src="{{url('front/vendors/filmstrip/jquery.filmstrip.js')}}"></script>
<!-- //page level js End -->

<script type="text/javascript">
$(document).ready(function() {
           
            $('#filmstrip').filmstrip({
                interval: 3000
            });
});
</script> 
@endsection


<!--<script>
    jQuery(document).ready(function($) {
        $('#filmstrip').filmstrip({
            interval: 3000
        });
/*
        $("#option1h").click(function(){
            $("#menu1").hide();
        });
*/
        $("#option1s").click(function(){
            $("#menu1").show();
            $("#menu2").hide();
            $("#menu3").hide();
        });
/*
        $("#option2h").click(function(){
            $("#menu2").hide();
        });
*/
        $("#option2s").click(function(){
            $("#menu1").hide();
            $("#menu2").show();
            $("#menu3").hide();
        });
/*
        $("#option3h").click(function(){
            $("#menu3").hide();
        });
*/
        $("#option3s").click(function(){
            $("#menu1").hide();
            $("#menu2").hide();
            $("#menu3").show();
        });
    });
</script>
-->

