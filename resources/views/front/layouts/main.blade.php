<?php 
if(isset($title)&&$title!=''){
    $title = $title;
}else{
    $title = 'SpanMaster';
}
?>
<!DOCTYPE html>
<html lang="en" >
<head>
    <title>{{$title}}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" type="image/png" href="{{url('favicon.ico')}}">

    <meta name="keywords" content="SpanMaster, Span, Master, Structures, Coverall, Cover-All, Cover, All, Buildings,
    pre-engineered, engineered, steel, frame, framed, fabric, metal, covered, A660, CSA S367, Building Systems, Legacy
    Building Solutions, Norseman Structures, Robertson Building Systems, Salmon Arm, Tappen, Thompson, Okanagan, Fraser
    Valley, British Columbia" />
    <meta name="revisit-after" content="1 month" />
    <meta name="abstract" content="Shupswap website and computer experts" />
    <meta name="author" content="inTechrity Business Solutions - www.inTechrity.ca" />
    <meta name="copyright" content="SpanMaster Structures Ltd." />
    <meta name="distribution" content="global" />
    <meta name="language" content="english" />
    <meta http-equiv="Cache-control" content="public" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1 " />


@include('front.common.css')
</head>
<body class="">

<div class="modal fade" id="response_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body">
          <p id="success_msg">Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  



    @include('front.common.header')
    <!--main content start-->
        @yield('content')
    <!--main content end--> 
    @include('front.common.dealer')
    @include('front.common.footer')
    @include('front.common.js')
@yield('scripts')
</body>
</html>

