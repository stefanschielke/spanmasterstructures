@extends('front.layouts.main')
@section('content')

<style>
    div span ul li a {
        background-color: transparent !important;
        text-decoration: underline;
    }
</style>

<div class="contact-back">
    <div class="container">
        <div class="row top-30">
            <div class="col-sm-6 col-xs-12 about-cnt vcenter">
                <?php
                    $about_us_title = 'About us';
                    if (isset($settings->about_us_title) && $settings->about_us_title != '') {
                        $about_us_title = $settings->about_us_title;
                    } 
                ?>

                <h1 class="text-center">{{$about_us_title}}</h1>

                <?php
                $about_us_description = '<p>
                    SpanMaster Structures Ltd. is a new corporate entity that benefits from having retained the experience and expertise
                    of many of the previous Cover-All Buildings BC staff and ownership. We offer over 55 years of combined experience in
                    the industry and provide expert local service, support and project management to each and every customer in British
                    Columbia. Our staffs experience with the Cover- All product as well as many other brands allows us to be a valuable
                    resource in the decision making process for your building requirements. SpanMaster Structures Ltds service does
                    not end with the purchase and installation of a building, but extends through the entire process and well beyond.
                </p>
                <p>
                    Attention to each and every detail has made us the go to choice for pre-engineered structures.
                </p>
                <p>
                    While Cover-All Building Systems Inc no longer exists as a corporation, BC is a showcase for the many proud Cover-All
                    building owners; and so the name will undoubtedly endure. SpanMaster Structures Ltd. offers a complete solution for
                    the repairs and maintenance needs of Cover-All buildings in British Columbia. Our building suppliers offer warrantied
                    OEM parts for your structure.
                </p>';
                if (isset($settings->about_us_description) && $settings->about_us_description != '') {
                    $about_us_description = $settings->about_us_description;
                } 
            ?>
                <p><?php echo $about_us_description?></p>
            </div>
            <div class="col-sm-5 col-xs-12 vcenter">

                <?php
                    $about_us_image = url('front/img/office.png');
                    if (isset($settings->about_us_image) && $settings->about_us_image != '') {
                        $about_us_image = url('files/about_us/'.$settings->about_us_image);
                    } 
                ?>

                <img src="{{$about_us_image}}" alt="outside office" class="img-responsive">
            </div>
        </div>
        <div class="row top-30">
{{--            <div class="col-sm-4 col-xs-0 vcenter">
                <!-- <img src="{{url('front/img/iso9001.png')}}" alt="ISO 9001 Certified" class="img-responsive">-->
            </div>--}}
            <hr>
            <div class="col-xs-12 about-cnt vcenter">
{{--                <h1 class="text-center">Mission Statement</h1>--}}
                <?php
                    $mission_title = 'ONE SENTENCE MISSION STATEMENT';
                    if (isset($settings->mission_title) && $settings->mission_title != '') {
                        $mission_title = $settings->mission_title;
                    } 
                ?>
                <h2>{{$mission_title}}</h2>

                <?php
                    $mission_description = '<p>
                        We at SpanMaster are dedicated to the service and support of the over 2,000 unique Cover-All building projects in BC,
                        and to bringing you innovative, durable and affordable building options now and in the future. We strive to exceed
                        our customers expectations by working with them to meet their individual building needs, and then providing the
                        right world class building solution.
                    </p>
                    <p>
                        The buildings we offer meet the National Building Code A660 - 10 rating and CSA S367 design criteria. This
                        certification is new, mandatory and very stringent: it is your guarantee that the products we deliver will last for
                        many years to come.
                    </p>
                    <p>
                        If you are considering the purchase of a building for any application, we would very much appreciate the opportunity
                        to introduce you to what SpanMaster Structures Ltd. has to offer.
                    </p>
                    <p>
                        The management and staff of SpanMaster Structures Ltd. look forward to assisting you in the future.
                    </p>';
                    if (isset($settings->mission_description) && $settings->mission_description != '') {
                        $mission_description = $settings->mission_description;
                    } 
                ?>
                    <p><?php echo $mission_description?></p>
            </div>
        </div>
        <hr>

        <div class="row top-30">
            <div class="col-sm-8 col-xs-12 about-cnt vcenter">
{{--                <h1 class="text-center">Sponsorships</h1>--}}
                <?php
                    $sponsorship_title = 'What we sponsor';
                    if (isset($settings->sponsorship_title) && $settings->sponsorship_title != '') {
                        $sponsorship_title = $settings->sponsorship_title;
                    } 
                ?>
                <h1>{{$sponsorship_title}}</h1>

                <?php
                    $sponsorship_description = 'What we sponsor';
                    if (isset($settings->sponsorship_description) && $settings->sponsorship_description != '') {
                        $sponsorship_description = $settings->sponsorship_description;
                    } 
                ?>
                <p><?php echo $sponsorship_description?></p>
            </div>
            <div class="col-sm-3 col-xs-12 vcenter">
                <?php
                    if (isset($sponsorship_images) && count($sponsorship_images) > 0) {
                        foreach ($sponsorship_images as $key => $sponsorship_image) {
                            $link = strstr('http', $sponsorship_image->link);
                            $link2 = strstr('https', $sponsorship_image->link);
                            if ($link == false && $link2 == false) {
                                $sponsorship_image->link = 'http://'.$sponsorship_image->link;
                            }
                    ?>
                        <a href="{{$sponsorship_image->link}}" target="_blank"><img src="{{url('files/sponsorship/'.$sponsorship_image->image)}}" alt="Sponsorship" class="img-responsive bot-15" width="70%" height="40%"></a>
                    <?php
                        }
                    } else {
                ?>
                    <img src="{{url('front/img/4h.jpg')}}" alt="Sponsorship" class="img-responsive" width="70%" height="40%">
                <?php
                    }
                ?>

            </div>
        </div>
        <hr>

        <div class="row top-30">
            <div class="col-sm-8 col-xs-12 about-cnt vcenter">
                <?php
                    $certification_title = 'Certifications';
                    if (isset($settings->certification_title) && $settings->certification_title != '') {
                        $certification_title = $settings->certification_title;
                    } 
                ?>
                <h1 class="text-center"><?php echo $certification_title ?></h1>

                <?php
                    $certification_description = 'Certifications';
                    if (isset($settings->certification_description) && $settings->certification_description != '') {
                        $certification_description = $settings->certification_description;
                    } 
                ?>
                <p><?php echo $certification_description ?></p>
            </div>
            <div class="col-sm-3 col-xs-0 vcenter">
                <?php
                if (isset($certification_images) && count($certification_images) > 0) {
                foreach ($certification_images as $key => $certification_image) {
                $link = strstr('http', $certification_image->link);
                $link2 = strstr('https', $certification_image->link);
                if ($link == false && $link2 == false) {
                    $certification_image->link = 'http://'.$certification_image->link;
                }
                ?>
                <a href="{{$certification_image->link}}" target="_blank"><img src="{{url('files/certification/'.$certification_image->image)}}" alt="Sponsorship" class="img-responsive bot-15" width="70%" height="40%"></a>
                <?php
                }
                } else {
                ?>
                <img src="{{url('front/img/csts.png')}}" alt="Certified" class="img-responsive" width="70%" height="40%">
                <?php
                }
                ?>
            </div>
        </div>

        <hr>

        @include('front.common.management-team')
    </div>
<div class="container">
    <hr>
    @include('front.common.partners')
    <hr>
</div>
</div>

@endsection
@section('scripts')

<script type="text/javascript">
$(document).ready(function() {

});
</script> 
@endsection