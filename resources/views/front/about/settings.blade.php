@extends('admin.layouts.inside')
@section('content')
<link href="{{URL::to('pannel/css/jquery-te-1.4.0.css')}}" rel="stylesheet" type="text/css" />
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('admin.common.sidebar')

    <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE BAR -->
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <?php
                            $activeClassHome= '';
                            $activeClassAbout= '';
                            $activeClassRM= '';
                            if (isset($from) && $from == 'home') {
                                $activeClassHome = 'active';
                                $activeClassAbout= '';
                                $activeClassRM= '';
                        ?>
                            <li>Home Page<i class="fa fa-circle"></i></li>
                            <li>
                            <span>Content Management</span>
                        </li>
                        <?php
                            } else if (isset($from) && $from == 'about_us'){
                                $activeClassAbout= 'active';
                                $activeClassHome= '';
                                $activeClassRM= '';
                        ?>
                            <li>About Page<i class="fa fa-circle"></i></li>
                            <li>
                            <span>Content Management</span>
                        </li>
                        <?php
                            } else if (isset($from) && $from == 'r_m'){
                                $activeClassRM= 'active';
                                $activeClassHome= '';
                                $activeClassAbout= '';
                        ?>
                            <li>Landing Pages<i class="fa fa-circle"></i></li>
                            <li>
                            <span>Pre-engineered Fabric Structures</span>
                        </li>
                        <?php
                            }
                        ?>

                        
                        
                    </ul>
                </div>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                
                
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light bordered">
                             <!-- <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Content Management</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <?php
                                        if (isset($from) && $from == 'home') {
                                    ?>
                                        <li class="active">
                                            <a href="#home_tab" data-toggle="tab">Home</a>
                                        </li>
                                    <?php
                                        } else if (isset($from) && $from == 'about_us'){
                                    ?>
                                        <li class="active">
                                            <a href="#about_us" data-toggle="tab">About Us </a>
                                        </li>
                                    <?php
                                        } else if (isset($from) && $from == 'r_m'){
                                    ?>
                                        <li class="active">
                                            <a href="#repairs_maintenance" data-toggle="tab">Repairs & Maintenance </a>
                                        </li>
                                    <?php
                                        }
                                    ?>
                                    
                                    
                                    
                                </ul>
                            </div> -->
                            <div class="portlet-body form">
                                <div class="tab-content">
                                    <div class="tab-pane {{$activeClassHome}}" id="home_tab">
                                        <div class="msg_divs alert msg_div"></div>
                               
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Sub Heading</label>
                                                    <div class="margin-top-10">
                                                        <textarea class="" id="home_heading" style="width:100%">
                                                            <?php
                                                                echo (isset($settings->home_heading) && $settings->home_heading != '')?$settings->home_heading:'';
                                                            ?>
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Why Choose Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="why_choose_title" value="{{(isset($settings->why_choose_title) && $settings->why_choose_title != '')?$settings->why_choose_title:''}}"> </div>
                                                </div>

                                                <div class="form-group col-md-12">
                                                    <label>Why Choose Description</label>
                                                    <div class="margin-top-10">
                                                        <textarea class="" id="why_choose_description" style="width:100%">
                                                            <?php
                                                                echo (isset($settings->why_choose_description) && $settings->why_choose_description != '')?$settings->why_choose_description:'';
                                                            ?>
                                                        </textarea>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row slider_rows">
                                                <div class="form-group col-md-5">
                                                    <label class="slider_label">Slider Image Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control image_title" placeholder="Enter Title" value=""> </div>
                                                </div>

                                                <div class="form-group fg-file-upload-detail col-md-5">
                                                    <label for="slider-image" class="control-label slider_label">Slider Image</label>
                                                    <p class="image-error slider-upload-error"></p>
                                                    <input type="text" class="form-control input-lg input-file-path pull-left file-span-msg-slider-image" placeholder="Select file to upload">
                                                    <input type="file" class="custom_input_file sliderImage"  multiple="true">
                                                    <span class="input_label image_browse">Browse</span>
                                                    <input type="hidden" value="" class="hidden-slider-image">
                                                    <input type="hidden" value="" class="hidden-slider-real-image">
                                                    
                                                        <!-- <img class="image_preview" id="shop_preview" src="{{url('pannel/images/image-not-found.png')}}"/> -->
                                                    
                                                </div>
                                            </div>
                                            <a class="add-more-images pull-right" href="javascript:;"><i class="glyphicon glyphicon-plus"></i> Add more images</a>

                                        </div>

                                        <div class="form-actions right">
                                            <a href="javascript:;" class="setting_submit_btn"><button type="button" class="btn green">Save</button></a>
                                            <img class="button_spinners submit_loader" src="{{URL::to('pannel/images/loader.gif')}}">
                                        </div>
                                    </div>

                                    <div class="tab-pane {{$activeClassAbout}}" id="about_us">
                                        <div class="msg_divs alert msg_div"></div>
                               
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>About Us Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="about_us_title" value="{{(isset($settings->about_us_title) && $settings->about_us_title != '')?$settings->about_us_title:''}}"> </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>About Us Description</label>
                                                    <div class="margin-top-10">
                                                    <textarea class="" id="about_us_description" style="width:100%">
                                                        <?php
                                                            echo (isset($settings->about_us_description) && $settings->about_us_description != '')?$settings->about_us_description:'';
                                                        ?>
                                                    </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Mission Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="mission_title" value="{{(isset($settings->mission_title) && $settings->mission_title != '')?$settings->mission_title:''}}"> </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Mission Description</label>
                                                    <div class="margin-top-10">
                                                    <textarea class="" id="mission_description" style="width:100%">
                                                        <?php
                                                            echo (isset($settings->mission_description) && $settings->mission_description != '')?$settings->mission_description:'';
                                                        ?>
                                                    </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label class="link_title">Sponsorship Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="sponsorship_title" value="{{(isset($settings->sponsorship_title) && $settings->sponsorship_title != '')?$settings->sponsorship_title:''}}"> </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Sponsorship Description</label>
                                                    <div class="margin-top-10">
                                                    <textarea class="" id="sponsorship_description" style="width:100%">
                                                        <?php
                                                            echo (isset($settings->sponsorship_description) && $settings->sponsorship_description != '')?$settings->sponsorship_description:'';
                                                        ?>
                                                    </textarea></div>
                                                </div>
                                                
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Certification Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="certification_title" value="{{(isset($settings->certification_title) && $settings->certification_title != '')?$settings->certification_title:''}}"> </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Certification Description</label>
                                                    <div class="margin-top-10">
                                                    <textarea class="" id="certification_description" style="width:100%">
                                                        <?php
                                                            echo (isset($settings->certification_description) && $settings->certification_description != '')?$settings->certification_description:'';
                                                        ?>
                                                    </textarea></div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5 fg-file-upload-detail">
                                                    <label for="about_us-image" class="control-label">About Us Image</label>
                                                    <p class="image-error about_us-upload-error"></p>
                                                    <input type="text" class="form-control input-lg input-file-path pull-left" placeholder="Browse image" id="file-span-msg-about_us">
                                                    <input type="file" class="custom_input_file" id="about_usImage"  multiple="true">
                                                    <input type="hidden" value="" id="hidden-about_us-image">
                                                    <?php
                                                        if (isset($settings->about_us_image) && $settings->about_us_image != '') {
                                                    ?>
                                                        <img class="image_preview" id="about_us_preview" src="{{url('files/about_us/'.$settings->about_us_image)}}"/>
                                                    <?php
                                                        }  else {
                                                    ?>
                                                        <img class="image_preview" id="about_us_preview" src="{{url('pannel/images/image-not-found.png')}}"/>
                                                    <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>

                                            
                                        </div>

                                        <div class="row sponsorship_images">
                                            <div class="form-group col-md-5">
                                                <label class="sponsorship_label">Link</label>
                                                <div class="margin-top-10">
                                                    <input type="text" class="form-control sponsorship_link" placeholder="Enter Link"> </div>
                                            </div>

                                            <div class="form-group col-md-5 fg-file-upload-detail">
                                                <label for="sponsorship-image" class="control-label sponsorship_label">Sponsorship Image</label>
                                                <p class="image-error sponsorship-upload-error"></p>
                                                <input type="text" class="form-control input-lg input-file-path pull-left file-span-msg-sponsorship" placeholder="Browse image">
                                                <input type="file" class="custom_input_file sponsorshipImage" multiple="true">
                                                <input type="hidden" value="" class="hidden-sponsorship-image">
                                                <input type="hidden" value="" class="hidden-sponsorship-real-image">
                                                    <!-- <img class="image_preview sponsorship_preview" src="{{url('pannel/images/image-not-found.png')}}"/> -->
                                            </div>
                                        </div>
                                        <a class="add-more-s-images pull-right" href="javascript:;"><i class="glyphicon glyphicon-plus"></i> Add more images</a>

                                        <div class="row certification_images">
                                            <div class="form-group col-md-5">
                                                <label class="certification_label">Link</label>
                                                <div class="margin-top-10">
                                                    <input type="text" class="form-control certification_link" placeholder="Enter Link"> </div>
                                            </div>
                                            <div class="form-group col-md-5 fg-file-upload-detail">
                                                <label for="certification-image" class="control-label certification_label">Certification Image</label>
                                                <p class="image-error certification-upload-error"></p>
                                                <input type="text" class="form-control input-lg input-file-path pull-left file-span-msg-certification" placeholder="Browse image">
                                                <input type="file" class="custom_input_file certificationImage"  multiple="true">
                                                <input type="hidden" value="" class="hidden-certification-image">
                                                <input type="hidden" value="" class="hidden-certification-real-image">
                                                
                                                    <!-- <img class="image_preview certification_preview" src="{{url('pannel/images/image-not-found.png')}}"/> -->
                                            </div>
                                        </div>
                                        <a class="add-more-c-images pull-right" href="javascript:;"><i class="glyphicon glyphicon-plus"></i> Add more images</a>

                                        <div class="clearfix"></div>
                                        <div class="form-actions right">
                                            <a href="javascript:;" class="setting_submit_btn"><button type="button" class="btn green">Save</button></a>
                                            <img class="button_spinners submit_loader" src="{{URL::to('pannel/images/loader.gif')}}">
                                        </div>
                                    </div>

                                    <div class="tab-pane {{$activeClassRM}}" id="repairs_maintenance">
                                        <div class="msg_divs alert msg_div"></div>
                               
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Main Heading</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="repairs_heading" value="{{(isset($settings->repairs_heading) && $settings->repairs_heading != '')?$settings->repairs_heading:''}}"> </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Main Description</label>
                                                    <div class="margin-top-10">
                                                    <textarea class="" id="repairs_description" style="width:100%">
                                                        <?php
                                                            echo (isset($settings->repairs_description) && $settings->repairs_description != '')?$settings->repairs_description:'';
                                                        ?>
                                                    </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Who Is Spanmaster Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="repairs_who_is_title" value="{{(isset($settings->repairs_who_is_title) && $settings->repairs_who_is_title != '')?$settings->repairs_who_is_title:''}}"> </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Who Is Spanmaster Description</label>
                                                    <div class="margin-top-10">
                                                    <textarea class="" id="repairs_who_is_description" style="width:100%">
                                                        <?php
                                                            echo (isset($settings->repairs_who_is_description) && $settings->repairs_who_is_description != '')?$settings->repairs_who_is_description:'';
                                                        ?>
                                                    </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Who Is It For Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="repairs_who_is_it_for_title" value="{{(isset($settings->repairs_who_is_it_for_title) && $settings->repairs_who_is_it_for_title != '')?$settings->repairs_who_is_it_for_title:''}}"> </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Who Is It For Description</label>
                                                    <div class="margin-top-10">
                                                    <textarea class="" id="repairs_who_is_it_for_description" style="width:100%">
                                                        <?php
                                                            echo (isset($settings->repairs_who_is_it_for_description) && $settings->repairs_who_is_it_for_description != '')?$settings->repairs_who_is_it_for_description:'';
                                                        ?>
                                                    </textarea></div>
                                                </div>
                                                
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>Why You'll Love Title</label>
                                                    <div class="margin-top-10">
                                                        <input type="text" class="form-control" placeholder="Enter Title" id="repairs_why_you_love_title" value="{{(isset($settings->repairs_why_you_love_title) && $settings->repairs_why_you_love_title != '')?$settings->repairs_why_you_love_title:''}}"> </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Why You'll Love Description</label>
                                                    <div class="margin-top-10">
                                                    <textarea class="" id="repairs_why_you_love_descripton" style="width:100%">
                                                        <?php
                                                            echo (isset($settings->repairs_why_you_love_descripton) && $settings->repairs_why_you_love_descripton != '')?$settings->repairs_why_you_love_descripton:'';
                                                        ?>
                                                    </textarea></div>
                                                </div>
                                            </div>

                                            <div class="row">
                                            <div class="form-group col-md-5 fg-file-upload-detail">
                                                <label for="repairs_who_is-image" class="control-label">Who Is Span Master Image</label>
                                                <p class="image-error repairs_who_is-upload-error"></p>
                                                <input type="text" class="form-control input-lg input-file-path pull-left" placeholder="Browse image" id="file-span-msg-repairs_who_is">
                                                <input type="file" class="custom_input_file" id="repairs_who_isImage"  multiple="true">
                                                <input type="hidden" value="" id="hidden-repairs_who_is-image">
                                                <?php
                                                    if (isset($settings->repairs_who_is_image) && $settings->repairs_who_is_image != '') {
                                                ?>
                                                    <img class="image_preview" id="repairs_who_is_preview" src="{{url('files/repairs/'.$settings->repairs_who_is_image)}}"/>
                                                <?php
                                                    }  else {
                                                ?>
                                                    <img class="image_preview" id="repairs_who_is_preview" src="{{url('pannel/images/image-not-found.png')}}"/>
                                                <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>

                                            <div class="row">
                                                <div class="form-group col-md-5 fg-file-upload-detail">
                                                    <label for="repairs_who_is_it_for-image" class="control-label">Who Is It For Image</label>
                                                    <p class="image-error repairs_who_is_it_for-upload-error"></p>
                                                    <input type="text" class="form-control input-lg input-file-path pull-left" placeholder="Browse image" id="file-span-msg-repairs_who_is_it_for">
                                                    <input type="file" class="custom_input_file" id="repairs_who_is_it_forImage"  multiple="true">
                                                    <input type="hidden" value="" id="hidden-repairs_who_is_it_for-image">
                                                    <?php
                                                        if (isset($settings->repairs_who_is_it_for_image) && $settings->repairs_who_is_it_for_image != '') {
                                                    ?>
                                                        <img class="image_preview" id="repairs_who_is_it_for_preview" src="{{url('files/repairs/'.$settings->repairs_who_is_it_for_image)}}"/>
                                                    <?php
                                                        }  else {
                                                    ?>
                                                        <img class="image_preview" id="repairs_who_is_it_for_preview" src="{{url('pannel/images/image-not-found.png')}}"/>
                                                    <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-5 fg-file-upload-detail">
                                                <label for="repairs_why_you_love-image" class="control-label">Why You'll Love Image</label>
                                                <p class="image-error repairs_why_you_love-upload-error"></p>
                                                <input type="text" class="form-control input-lg input-file-path pull-left" placeholder="Browse image" id="file-span-msg-repairs_why_you_love">
                                                <input type="file" class="custom_input_file" id="repairs_why_you_loveImage"  multiple="true">
                                                <input type="hidden" value="" id="hidden-repairs_why_you_love-image">
                                                <?php
                                                    if (isset($settings->repairs_why_you_love_image) && $settings->repairs_why_you_love_image != '') {
                                                ?>
                                                    <img class="image_preview" id="repairs_why_you_love_preview" src="{{url('files/repairs/'.$settings->repairs_why_you_love_image)}}"/>
                                                <?php
                                                    }  else {
                                                ?>
                                                    <img class="image_preview" id="repairs_why_you_love_preview" src="{{url('pannel/images/image-not-found.png')}}"/>
                                                <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <a href="javascript:;" class="setting_submit_btn"><button type="button" class="btn green">Save</button></a>
                                            <img class="button_spinners submit_loader" src="{{URL::to('pannel/images/loader.gif')}}">
                                        </div>
                                    </div>
                                </div>
                                
                               
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->


@endsection
@section('scripts')

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{URL::to('pannel/js/jquery-te-1.4.0.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
var slider_images = '<?php echo json_encode($slider_images)?>'
var sponsorship_images = '<?php echo json_encode($sponsorship_images)?>'
var certification_images = '<?php echo json_encode($certification_images)?>'
$(document).ready(function() {

    $('#home_heading').jqte();
    $('#why_choose_description').jqte();
    $('#about_us_description').jqte();
    $('#mission_description').jqte();
    $('#sponsorship_description').jqte();
    $('#certification_description').jqte();
    $('#repairs_description').jqte();
    $('#repairs_who_is_description').jqte();
    $('#repairs_who_is_it_for_description').jqte();
    $('#repairs_why_you_love_descripton').jqte();
    var sliderImages = JSON.parse(slider_images);  

    if (sliderImages.length > 0) {
        $(sliderImages).each(function(index, element){
            if (index > 0) {
                var clonedElement = $('.add-more-images').prev().clone();

                clonedElement.find('.hidden-slider-image').val(element.image);
                clonedElement.find('.hidden-slider-real-image').val(element.real_image);
                clonedElement.find('.image_title').val(element.title);
                clonedElement.find('.image_title').parent().removeClass('has-error');
                clonedElement.find('.hidden-slider-image').parent().removeClass('has-error');
                clonedElement.find('.slider-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
                clonedElement.find('.file-span-msg-slider-image').attr('placeholder', element.real_image);
                clonedElement.find('.delete-slider-image').remove();
                clonedElement.find('.slider_label').remove();
                clonedElement.find('.image_browse').css('margin-bottom', '19px');
                clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-slider-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
                clonedElement.insertAfter($('.add-more-images').prev());

            } else {
                $('.file-span-msg-slider-image').attr('placeholder', element.real_image);
                $('.hidden-slider-image').val(element.image);
                $('.hidden-slider-real-image').val(element.real_image);
                $('.image_title').val(element.title);
            }
        });
    }

    var sponsorshipImages = JSON.parse(sponsorship_images);  

    if (sponsorshipImages.length > 0) {
        $(sponsorshipImages).each(function(index, element){
            if (index > 0) {
                var clonedElement = $('.add-more-s-images').prev().clone();

                clonedElement.find('.hidden-sponsorship-image').val(element.image);
                clonedElement.find('.hidden-sponsorship-real-image').val(element.real_image);
                clonedElement.find('.sponsorship_link').val(element.link);
                clonedElement.find('.sponsorship_link').parent().removeClass('has-error');
                clonedElement.find('.hidden-sponsorship-image').parent().removeClass('has-error');
                clonedElement.find('.sponsorship-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
                clonedElement.find('.file-span-msg-sponsorship').attr('placeholder', element.real_image);
                clonedElement.find('.delete-sponsorship-image').remove();
                clonedElement.find('.sponsorship_label').remove();
                clonedElement.find('.image_browse').css('margin-bottom', '19px');
                clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-sponsorship-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
                clonedElement.insertAfter($('.add-more-s-images').prev());

            } else {
                $('.file-span-msg-sponsorship-image').attr('placeholder', element.real_image);
                $('.hidden-sponsorship-image').val(element.image);
                $('.hidden-sponsorship-real-image').val(element.real_image);
                $('.sponsorship_link').val(element.link);
            }
        });
    }

    var certificationImages = JSON.parse(certification_images);  

    if (certificationImages.length > 0) {
        $(certificationImages).each(function(index, element){
            if (index > 0) {
                var clonedElement = $('.add-more-c-images').prev().clone();

                clonedElement.find('.hidden-certification-image').val(element.image);
                clonedElement.find('.hidden-certification-real-image').val(element.real_image);
                clonedElement.find('.certification_link').val(element.link);
                clonedElement.find('.certification_link').parent().removeClass('has-error');
                clonedElement.find('.hidden-certification-image').parent().removeClass('has-error');
                clonedElement.find('.certification-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
                clonedElement.find('.file-span-msg-certification').attr('placeholder', element.real_image);
                clonedElement.find('.delete-certification-image').remove();
                clonedElement.find('.certification_label').remove();
                clonedElement.find('.image_browse').css('margin-bottom', '19px');
                clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-certification-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
                clonedElement.insertAfter($('.add-more-c-images').prev());

            } else {
                $('.file-span-msg-certification-image').attr('placeholder', element.real_image);
                $('.hidden-certification-image').val(element.image);
                $('.hidden-certification-real-image').val(element.real_image);
                $('.certification_link').val(element.link);
            }
        });
    }

    // settings
    $('#about_usImage').fileupload({
        dataType: 'json',
        paramName:'file',
        formData:{'from':'about_us'},
        limitMultiFileUploads:1,
        acceptFileTypes:/(\.|\/)(png|jpg|jpeg|gif)$/i,
        url:SM.Config.getApiUrl()+'image/upload',
        add: function (e, data) {
            
            $('.about_us-upload-error').html('');
            
            // for file format validation
            if (!SM.App.isAcceptFileTypes(data.files[0]['type'])) {
                $('.about_us-upload-error').removeClass('success');
                $('.about_us-upload-error').addClass('error');
                $('.about_us-upload-error').html('Invalid File Format.').delay(2000).fadeIn();
                return false;
            }

            // for file size validation
            var maxFileSize = 10000000; // 10MB
            if (data.files[0]['size'] > maxFileSize) {
                $('.about_us-upload-error').removeClass('success');
                $('.about_us-upload-error').addClass('error');
                $('.about_us-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                return false;
            }

            var jqXHR = data.submit()
                .success(function (result, textStatus, jqXHR) {
                    if (result.status) {

                        $('#hidden-about_us-image').val(result.file);
                        var html = 'Image has been uploaded successfully.';
                        if (jqXHR.responseJSON.messages){
                            var messages = jqXHR.responseJSON.messages;
                            var html =  messages;
                        }
                        //$('.about_us-upload-error').removeClass('error');
                        //$('.about_us-upload-error').addClass('success');
                        //$('.about_us-upload-error').html(html).delay(2000).fadeIn();
                        $('#file-span-msg-about_us').attr('placeholder',html);
                        $('#file-browse').addClass('new_label').text('Uploaded');
                        $('#hidden-about_us-image').parent().removeClass('has-error');
                        $('#about_us_preview').attr('src', SM.Config.getSiteUrl()+'/files/about_us/'+result.file);
                    } else if(result.error) {
                        var html = 'An error occurred.';
                        if (result.error.messages && result.error.messages.length > 0){
                            var messages = jqXHR.responseJSON.error.messages;
                            var html =  messages[0];
                        }

                        $('.about_us-upload-error').removeClass('success');
                        $('.about_us-upload-error').addClass('error');
                        $('.about_us-upload-error').html(html).delay(2000).fadeIn();
                    }
                })
                .error(function (jqXHR, textStatus, errorThrown) {
                    var html = 'An error occurred.';
                    if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0){
                        var messages = jqXHR.responseJSON.error.messages;
                        var html =  messages[0];
                    }
                    $('.about_us-upload-error').removeClass('success');
                    $('.about_us-upload-error').addClass('error');
                    $('.about_us-upload-error').html(html).delay(2000).fadeIn();
 
                })
                .complete(function (result, textStatus, jqXHR) {
                    $('#hidden-about_us-image').parent().removeClass('has-error');
                });
        },
        progressall: function (e, data) {
           
            var progress = parseInt(data.loaded / data.total * 100, 10);
            if (progress > 99) {
                progress = 99;
            };              
            $('#file-span-msg-about_us').attr('placeholder','Upload '+progress+'% Complete');
        },
        done: function (e, data) {
            $('#hidden-about_us-image').parent().removeClass('has-error');
        }
    });
    

    $('.page-container').on('click', '.add-more-s-images', function(e) {
        e.preventDefault();
        var clonedElement = $(this).prev().clone();

        clonedElement.find('.hidden-sponsorship-image').val('');
        clonedElement.find('.hidden-sponsorship-real-image').val('');
        clonedElement.find('.hidden-sponsorship-image').parent().removeClass('has-error');
        clonedElement.find('.s_image_title').val('');
        clonedElement.find('.link_title').remove();
        clonedElement.find('.sponsorship_link').val('');
        clonedElement.find('.sponsorship_link').parent().removeClass('has-error');
        clonedElement.find('.s_image_title').parent().removeClass('has-error');
        clonedElement.find('.sponsorship-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
        clonedElement.find('.file-span-msg-sponsorship').attr('placeholder', 'Select file to upload');
        clonedElement.find('.delete-sponsorship-image').remove();
        clonedElement.find('.sponsorship_label').remove();
        clonedElement.find('.image_browse').css('margin-bottom', '19px');
        clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-sponsorship-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
        clonedElement.insertAfter($(this).prev());
    });

    $('.page-container').on('click', '.delete-sponsorship-image', function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    });

    jQuery(".page-container").on("click", '.sponsorshipImage', function() {

        var that = $(this).parent();
        $(this).fileupload({
            dataType: 'json',
            paramName: 'file',
            formData:{'from':'sponsorship'},
            acceptFileTypes: /(\.|\/)(png|jpg|jpeg|gif)$/i,
            limitMultiFileUploads: 1,
            url: SM.Config.getApiUrl() + 'image/upload',
            add: function(e, data) {

                that.find('.sponsorship-upload-error').html('');
                var acceptFileTypes = "(\.|\/)(png|jpg|jpeg|gif)$";
                acceptFileTypes = new RegExp(acceptFileTypes, "i");
                if (!acceptFileTypes.test(data.files[0]['type'])) {
                    $('.sponsorship-upload-error').removeClass('alert-success');
                    $('.sponsorship-upload-error').addClass('alert-danger');
                    $('.sponsorship-upload-error').html('Invalid File Format').fadeIn();
                    return false;
                }

                // for file size validation
                var maxFileSize = 10000000; // 10MB
                if (data.files[0]['size'] > maxFileSize) {
                    that.find('.sponsorship-upload-error').removeClass('alert-success');
                    that.find('.sponsorship-upload-error').addClass('alert-danger');
                    that.find('.sponsorship-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                    return false;
                }

                var jqXHR = data.submit()
                    .success(function(result, textStatus, jqXHR) {
                        if (result.status) {

                            that.find('.hidden-sponsorship-image').val(result.file);
                            that.find('.hidden-sponsorship-real-image').val(result.file_real_name);
                            var html = 'Image has been uploaded successfully.';
                            if (jqXHR.responseJSON.messages) {
                                var messages = jqXHR.responseJSON.messages;
                                var html = messages;
                            }
                            that.find('.file-span-msg-sponsorship').attr('placeholder', result.file_real_name);
                            that.find('.hidden-sponsorship-image').parent().removeClass('has-error');
                            that.find('.sponsorship-upload-error').removeClass('alert-danger');
                            that.find('.sponsorship-upload-error').addClass('alert-success');
                            that.find('.sponsorship_preview').attr('src', SM.Config.getSiteUrl()+'/files/sponsorship/'+result.file);
                            //$('.sponsorship-upload-error').html(html).delay(2000).fadeOut();
                        } else if (result.error) {
                            var html = 'An error occurred.';
                            if (result.error.messages && result.error.messages.length > 0) {
                                var messages = jqXHR.responseJSON.error.messages;
                                var html = messages[0];
                            }

                            that.find('.file-span-msg-sponsorship').attr('placeholder', "Select Image to upload").addClass('alert-danger');
                            that.find('.sponsorship-upload-error').removeClass('alert-success');
                            that.find('.sponsorship-upload-error').addClass('alert-danger');
                            that.find('.sponsorship-upload-error').html(html).show().delay(2000).fadeOut();
                        }
                    })
                    .error(function(jqXHR, textStatus, errorThrown) {
                        var html = 'An error occurred.';
                        if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0) {
                            var messages = jqXHR.responseJSON.error.messages;
                            var html = messages[0];
                        }
                        that.find('.sponsorship-upload-error').removeClass('alert-success');
                        that.find('.sponsorship-upload-error').addClass('alert-danger');
                        that.find('.sponsorship-upload-error').html(html).delay(2000).fadeIn();

                    })
                    .complete(function(result, textStatus, jqXHR) {
                        $('.hidden-sponsorship-image').parent().removeClass('has-error');
                    });
            },
            progressall: function(e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                if (progress > 99) {
                    progress = 99;
                };
                that.find('.file-span-msg-sponsorship').attr('placeholder', 'Upload ' + progress + '% complete');
            },
            done: function(e, data) {
                that.find('.hidden-sponsorship-image').parent().removeClass('has-error');

            }
        });

    });

    $('.page-container').on('click', '.add-more-c-images', function(e) {
        e.preventDefault();
        var clonedElement = $(this).prev().clone();

        clonedElement.find('.hidden-certification-image').val('');
        clonedElement.find('.hidden-certification-real-image').val('');
        clonedElement.find('.hidden-certification-image').parent().removeClass('has-error');
        clonedElement.find('.s_image_title').val('');
        clonedElement.find('.link_title').remove();
        clonedElement.find('.certification_link').val('');
        clonedElement.find('.certification_link').parent().removeClass('has-error');
        clonedElement.find('.s_image_title').parent().removeClass('has-error');
        clonedElement.find('.certification-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
        clonedElement.find('.file-span-msg-certification').attr('placeholder', 'Select file to upload');
        clonedElement.find('.delete-certification-image').remove();
        clonedElement.find('.certification_label').remove();
        clonedElement.find('.image_browse').css('margin-bottom', '19px');
        clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-certification-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
        clonedElement.insertAfter($(this).prev());
    });

    $('.page-container').on('click', '.delete-certification-image', function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    });

    jQuery(".page-container").on("click", '.certificationImage', function() {

        var that = $(this).parent();
        $(this).fileupload({
            dataType: 'json',
            paramName: 'file',
            formData:{'from':'certification'},
            acceptFileTypes: /(\.|\/)(png|jpg|jpeg|gif)$/i,
            limitMultiFileUploads: 1,
            url: SM.Config.getApiUrl() + 'image/upload',
            add: function(e, data) {

                that.find('.certification-upload-error').html('');
                var acceptFileTypes = "(\.|\/)(png|jpg|jpeg|gif)$";
                acceptFileTypes = new RegExp(acceptFileTypes, "i");
                if (!acceptFileTypes.test(data.files[0]['type'])) {
                    $('.certification-upload-error').removeClass('alert-success');
                    $('.certification-upload-error').addClass('alert-danger');
                    $('.certification-upload-error').html('Invalid File Format').fadeIn();
                    return false;
                }

                // for file size validation
                var maxFileSize = 10000000; // 10MB
                if (data.files[0]['size'] > maxFileSize) {
                    that.find('.certification-upload-error').removeClass('alert-success');
                    that.find('.certification-upload-error').addClass('alert-danger');
                    that.find('.certification-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                    return false;
                }

                var jqXHR = data.submit()
                    .success(function(result, textStatus, jqXHR) {
                        if (result.status) {

                            that.find('.hidden-certification-image').val(result.file);
                            that.find('.hidden-certification-real-image').val(result.file_real_name);
                            var html = 'Image has been uploaded successfully.';
                            if (jqXHR.responseJSON.messages) {
                                var messages = jqXHR.responseJSON.messages;
                                var html = messages;
                            }
                            that.find('.file-span-msg-certification').attr('placeholder', result.file_real_name);
                            that.find('.hidden-certification-image').parent().removeClass('has-error');
                            that.find('.certification-upload-error').removeClass('alert-danger');
                            that.find('.certification-upload-error').addClass('alert-success');
                            that.find('.certification_preview').attr('src', SM.Config.getSiteUrl()+'/files/certification/'+result.file);
                            //$('.certification-upload-error').html(html).delay(2000).fadeOut();
                        } else if (result.error) {
                            var html = 'An error occurred.';
                            if (result.error.messages && result.error.messages.length > 0) {
                                var messages = jqXHR.responseJSON.error.messages;
                                var html = messages[0];
                            }

                            that.find('.file-span-msg-certification').attr('placeholder', "Select Image to upload").addClass('alert-danger');
                            that.find('.certification-upload-error').removeClass('alert-success');
                            that.find('.certification-upload-error').addClass('alert-danger');
                            that.find('.certification-upload-error').html(html).show().delay(2000).fadeOut();
                        }
                    })
                    .error(function(jqXHR, textStatus, errorThrown) {
                        var html = 'An error occurred.';
                        if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0) {
                            var messages = jqXHR.responseJSON.error.messages;
                            var html = messages[0];
                        }
                        that.find('.certification-upload-error').removeClass('alert-success');
                        that.find('.certification-upload-error').addClass('alert-danger');
                        that.find('.certification-upload-error').html(html).delay(2000).fadeIn();

                    })
                    .complete(function(result, textStatus, jqXHR) {
                        $('.hidden-certification-image').parent().removeClass('has-error');
                    });
            },
            progressall: function(e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                if (progress > 99) {
                    progress = 99;
                };
                that.find('.file-span-msg-certification').attr('placeholder', 'Upload ' + progress + '% complete');
            },
            done: function(e, data) {
                that.find('.hidden-certification-image').parent().removeClass('has-error');

            }
        });

    });

    $('.page-container').on('click', '.add-more-images', function(e) {
        e.preventDefault();
        var clonedElement = $(this).prev().clone();

        clonedElement.find('.hidden-slider-image').val('');
        clonedElement.find('.hidden-slider-real-image').val('');
        clonedElement.find('.hidden-slider-image').parent().removeClass('has-error');
        clonedElement.find('.image_title').val('');
        clonedElement.find('.image_title').parent().removeClass('has-error');
        clonedElement.find('.slider-upload-error').removeClass('alert-success').removeClass('alert-danger').html('');
        clonedElement.find('.file-span-msg-slider-image').attr('placeholder', 'Select file to upload');
        clonedElement.find('.delete-slider-image').remove();
        clonedElement.find('.slider_label').remove();
        clonedElement.find('.image_browse').css('margin-bottom', '19px');
        clonedElement.find('.fg-file-upload-detail').append('<a href="javascript:;" class="delete-slider-image"><i class="glyphicon glyphicon-minus"></i>Remove</a>');
        clonedElement.insertAfter($(this).prev());
    });

    $('.page-container').on('click', '.delete-slider-image', function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    });

    jQuery(".page-container").on("click", '.sliderImage', function() {

        var that = $(this).parent();
        $(this).fileupload({
            dataType: 'json',
            paramName: 'file',
            formData:{'from':'slider'},
            acceptFileTypes: /(\.|\/)(png|jpg|jpeg|gif)$/i,
            limitMultiFileUploads: 1,
            url: SM.Config.getApiUrl() + 'image/upload',
            add: function(e, data) {

                that.find('.slider-upload-error').html('');
                var acceptFileTypes = "(\.|\/)(png|jpg|jpeg|gif)$";
                acceptFileTypes = new RegExp(acceptFileTypes, "i");
                if (!acceptFileTypes.test(data.files[0]['type'])) {
                    $('.slider-upload-error').removeClass('alert-success');
                    $('.slider-upload-error').addClass('alert-danger');
                    $('.slider-upload-error').html('Invalid File Format').fadeIn();
                    return false;
                }

                // for file size validation
                var maxFileSize = 10000000; // 10MB
                if (data.files[0]['size'] > maxFileSize) {
                    that.find('.slider-upload-error').removeClass('alert-success');
                    that.find('.slider-upload-error').addClass('alert-danger');
                    that.find('.slider-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                    return false;
                }

                var jqXHR = data.submit()
                    .success(function(result, textStatus, jqXHR) {
                        if (result.status) {

                            that.find('.hidden-slider-image').val(result.file);
                            that.find('.hidden-slider-real-image').val(result.file_real_name);
                            var html = 'Image has been uploaded successfully.';
                            if (jqXHR.responseJSON.messages) {
                                var messages = jqXHR.responseJSON.messages;
                                var html = messages;
                            }
                            that.find('.file-span-msg-slider-image').attr('placeholder', result.file_real_name);
                            that.find('.hidden-slider-image').parent().removeClass('has-error');
                            that.find('.slider-upload-error').removeClass('alert-danger');
                            that.find('.slider-upload-error').addClass('alert-success');
                            //$('.slider-upload-error').html(html).delay(2000).fadeOut();
                        } else if (result.error) {
                            var html = 'An error occurred.';
                            if (result.error.messages && result.error.messages.length > 0) {
                                var messages = jqXHR.responseJSON.error.messages;
                                var html = messages[0];
                            }

                            that.find('.file-span-msg-slider-image').attr('placeholder', "Select Image to upload").addClass('alert-danger');
                            that.find('.slider-upload-error').removeClass('alert-success');
                            that.find('.slider-upload-error').addClass('alert-danger');
                            that.find('.slider-upload-error').html(html).show().delay(2000).fadeOut();
                        }
                    })
                    .error(function(jqXHR, textStatus, errorThrown) {
                        var html = 'An error occurred.';
                        if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0) {
                            var messages = jqXHR.responseJSON.error.messages;
                            var html = messages[0];
                        }
                        that.find('.slider-upload-error').removeClass('alert-success');
                        that.find('.slider-upload-error').addClass('alert-danger');
                        that.find('.slider-upload-error').html(html).delay(2000).fadeIn();

                    })
                    .complete(function(result, textStatus, jqXHR) {
                        $('.hidden-slider-image').parent().removeClass('has-error');
                    });
            },
            progressall: function(e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                if (progress > 99) {
                    progress = 99;
                };
                that.find('.file-span-msg-slider-image').attr('placeholder', 'Upload ' + progress + '% complete');
            },
            done: function(e, data) {
                that.find('.hidden-slider-image').parent().removeClass('has-error');

            }
        });

    });
    
    $('#repairs_who_isImage').fileupload({
        dataType: 'json',
        paramName:'file',
        formData:{'from':'repairs'},
        limitMultiFileUploads:1,
        acceptFileTypes:/(\.|\/)(png|jpg|jpeg|gif)$/i,
        url:SM.Config.getApiUrl()+'image/upload',
        add: function (e, data) {
            
            $('.repairs_who_is-upload-error').html('');
            
            // for file format validation
            if (!SM.App.isAcceptFileTypes(data.files[0]['type'])) {
                $('.repairs_who_is-upload-error').removeClass('success');
                $('.repairs_who_is-upload-error').addClass('error');
                $('.repairs_who_is-upload-error').html('Invalid File Format.').delay(2000).fadeIn();
                return false;
            }

            // for file size validation
            var maxFileSize = 10000000; // 10MB
            if (data.files[0]['size'] > maxFileSize) {
                $('.repairs_who_is-upload-error').removeClass('success');
                $('.repairs_who_is-upload-error').addClass('error');
                $('.repairs_who_is-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                return false;
            }

            var jqXHR = data.submit()
                .success(function (result, textStatus, jqXHR) {
                    if (result.status) {

                        $('#hidden-repairs_who_is-image').val(result.file);
                        var html = 'Image has been uploaded successfully.';
                        if (jqXHR.responseJSON.messages){
                            var messages = jqXHR.responseJSON.messages;
                            var html =  messages;
                        }
                        //$('.repairs_who_is-upload-error').removeClass('error');
                        //$('.repairs_who_is-upload-error').addClass('success');
                        //$('.repairs_who_is-upload-error').html(html).delay(2000).fadeIn();
                        $('#file-span-msg-repairs_who_is').attr('placeholder',html);
                        $('#file-browse').addClass('new_label').text('Uploaded');
                        $('#hidden-repairs_who_is-image').parent().removeClass('has-error');
                        $('#repairs_who_is_preview').attr('src', SM.Config.getSiteUrl()+'/files/repairs/'+result.file);
                    } else if(result.error) {
                        var html = 'An error occurred.';
                        if (result.error.messages && result.error.messages.length > 0){
                            var messages = jqXHR.responseJSON.error.messages;
                            var html =  messages[0];
                        }

                        $('.repairs_who_is-upload-error').removeClass('success');
                        $('.repairs_who_is-upload-error').addClass('error');
                        $('.repairs_who_is-upload-error').html(html).delay(2000).fadeIn();
                    }
                })
                .error(function (jqXHR, textStatus, errorThrown) {
                    var html = 'An error occurred.';
                    if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0){
                        var messages = jqXHR.responseJSON.error.messages;
                        var html =  messages[0];
                    }
                    $('.repairs_who_is-upload-error').removeClass('success');
                    $('.repairs_who_is-upload-error').addClass('error');
                    $('.repairs_who_is-upload-error').html(html).delay(2000).fadeIn();
 
                })
                .complete(function (result, textStatus, jqXHR) {
                    $('#hidden-repairs_who_is-image').parent().removeClass('has-error');
                });
        },
        progressall: function (e, data) {
           
            var progress = parseInt(data.loaded / data.total * 100, 10);
            if (progress > 99) {
                progress = 99;
            };              
            $('#file-span-msg-repairs_who_is').attr('placeholder','Upload '+progress+'% Complete');
        },
        done: function (e, data) {
            $('#hidden-repairs_who_is-image').parent().removeClass('has-error');
        }
    });

    $('#repairs_who_is_it_forImage').fileupload({
        dataType: 'json',
        paramName:'file',
        formData:{'from':'repairs'},
        limitMultiFileUploads:1,
        acceptFileTypes:/(\.|\/)(png|jpg|jpeg|gif)$/i,
        url:SM.Config.getApiUrl()+'image/upload',
        add: function (e, data) {
            
            $('.repairs_who_is_it_for-upload-error').html('');
            
            // for file format validation
            if (!SM.App.isAcceptFileTypes(data.files[0]['type'])) {
                $('.repairs_who_is_it_for-upload-error').removeClass('success');
                $('.repairs_who_is_it_for-upload-error').addClass('error');
                $('.repairs_who_is_it_for-upload-error').html('Invalid File Format.').delay(2000).fadeIn();
                return false;
            }

            // for file size validation
            var maxFileSize = 10000000; // 10MB
            if (data.files[0]['size'] > maxFileSize) {
                $('.repairs_who_is_it_for-upload-error').removeClass('success');
                $('.repairs_who_is_it_for-upload-error').addClass('error');
                $('.repairs_who_is_it_for-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                return false;
            }

            var jqXHR = data.submit()
                .success(function (result, textStatus, jqXHR) {
                    if (result.status) {

                        $('#hidden-repairs_who_is_it_for-image').val(result.file);
                        var html = 'Image has been uploaded successfully.';
                        if (jqXHR.responseJSON.messages){
                            var messages = jqXHR.responseJSON.messages;
                            var html =  messages;
                        }
                        //$('.repairs_who_is_it_for-upload-error').removeClass('error');
                        //$('.repairs_who_is_it_for-upload-error').addClass('success');
                        //$('.repairs_who_is_it_for-upload-error').html(html).delay(2000).fadeIn();
                        $('#file-span-msg-repairs_who_is_it_for').attr('placeholder',html);
                        $('#file-browse').addClass('new_label').text('Uploaded');
                        $('#hidden-repairs_who_is_it_for-image').parent().removeClass('has-error');
                        $('#repairs_who_is_it_for_preview').attr('src', SM.Config.getSiteUrl()+'/files/repairs/'+result.file);
                    } else if(result.error) {
                        var html = 'An error occurred.';
                        if (result.error.messages && result.error.messages.length > 0){
                            var messages = jqXHR.responseJSON.error.messages;
                            var html =  messages[0];
                        }

                        $('.repairs_who_is_it_for-upload-error').removeClass('success');
                        $('.repairs_who_is_it_for-upload-error').addClass('error');
                        $('.repairs_who_is_it_for-upload-error').html(html).delay(2000).fadeIn();
                    }
                })
                .error(function (jqXHR, textStatus, errorThrown) {
                    var html = 'An error occurred.';
                    if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0){
                        var messages = jqXHR.responseJSON.error.messages;
                        var html =  messages[0];
                    }
                    $('.repairs_who_is_it_for-upload-error').removeClass('success');
                    $('.repairs_who_is_it_for-upload-error').addClass('error');
                    $('.repairs_who_is_it_for-upload-error').html(html).delay(2000).fadeIn();
 
                })
                .complete(function (result, textStatus, jqXHR) {
                    $('#hidden-repairs_who_is_it_for-image').parent().removeClass('has-error');
                });
        },
        progressall: function (e, data) {
           
            var progress = parseInt(data.loaded / data.total * 100, 10);
            if (progress > 99) {
                progress = 99;
            };              
            $('#file-span-msg-repairs_who_is_it_for').attr('placeholder','Upload '+progress+'% Complete');
        },
        done: function (e, data) {
            $('#hidden-repairs_who_is_it_for-image').parent().removeClass('has-error');
        }
    });

    $('#repairs_why_you_loveImage').fileupload({
        dataType: 'json',
        paramName:'file',
        formData:{'from':'repairs'},
        limitMultiFileUploads:1,
        acceptFileTypes:/(\.|\/)(png|jpg|jpeg|gif)$/i,
        url:SM.Config.getApiUrl()+'image/upload',
        add: function (e, data) {
            
            $('.repairs_why_you_love-upload-error').html('');
            
            // for file format validation
            if (!SM.App.isAcceptFileTypes(data.files[0]['type'])) {
                $('.repairs_why_you_love-upload-error').removeClass('success');
                $('.repairs_why_you_love-upload-error').addClass('error');
                $('.repairs_why_you_love-upload-error').html('Invalid File Format.').delay(2000).fadeIn();
                return false;
            }

            // for file size validation
            var maxFileSize = 10000000; // 10MB
            if (data.files[0]['size'] > maxFileSize) {
                $('.repairs_why_you_love-upload-error').removeClass('success');
                $('.repairs_why_you_love-upload-error').addClass('error');
                $('.repairs_why_you_love-upload-error').html('Limit exceeds, maximium file size can be 10MB.').delay(2000).fadeIn();
                return false;
            }

            var jqXHR = data.submit()
                .success(function (result, textStatus, jqXHR) {
                    if (result.status) {

                        $('#hidden-repairs_why_you_love-image').val(result.file);
                        var html = 'Image has been uploaded successfully.';
                        if (jqXHR.responseJSON.messages){
                            var messages = jqXHR.responseJSON.messages;
                            var html =  messages;
                        }
                        //$('.repairs_why_you_love-upload-error').removeClass('error');
                        //$('.repairs_why_you_love-upload-error').addClass('success');
                        //$('.repairs_why_you_love-upload-error').html(html).delay(2000).fadeIn();
                        $('#file-span-msg-repairs_why_you_love').attr('placeholder',html);
                        $('#file-browse').addClass('new_label').text('Uploaded');
                        $('#hidden-repairs_why_you_love-image').parent().removeClass('has-error');
                        $('#repairs_why_you_love_preview').attr('src', SM.Config.getSiteUrl()+'/files/repairs/'+result.file);
                    } else if(result.error) {
                        var html = 'An error occurred.';
                        if (result.error.messages && result.error.messages.length > 0){
                            var messages = jqXHR.responseJSON.error.messages;
                            var html =  messages[0];
                        }

                        $('.repairs_why_you_love-upload-error').removeClass('success');
                        $('.repairs_why_you_love-upload-error').addClass('error');
                        $('.repairs_why_you_love-upload-error').html(html).delay(2000).fadeIn();
                    }
                })
                .error(function (jqXHR, textStatus, errorThrown) {
                    var html = 'An error occurred.';
                    if (jqXHR.responseJSON.error.messages && jqXHR.responseJSON.error.messages.length > 0){
                        var messages = jqXHR.responseJSON.error.messages;
                        var html =  messages[0];
                    }
                    $('.repairs_why_you_love-upload-error').removeClass('success');
                    $('.repairs_why_you_love-upload-error').addClass('error');
                    $('.repairs_why_you_love-upload-error').html(html).delay(2000).fadeIn();
 
                })
                .complete(function (result, textStatus, jqXHR) {
                    $('#hidden-repairs_why_you_love-image').parent().removeClass('has-error');
                });
        },
        progressall: function (e, data) {
           
            var progress = parseInt(data.loaded / data.total * 100, 10);
            if (progress > 99) {
                progress = 99;
            };              
            $('#file-span-msg-repairs_why_you_love').attr('placeholder','Upload '+progress+'% Complete');
        },
        done: function (e, data) {
            $('#hidden-repairs_why_you_love-image').parent().removeClass('has-error');
        }
    });
});
</script> 
@endsection