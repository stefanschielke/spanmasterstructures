@extends('front.layouts.main')
@section('content')

<style>
    .content {
        margin-left:auto;
        margin-right:auto;
    }
    .contact-back {
        background: url('front/img/001a.jpg') no-repeat center center fixed;
        display: table;
        height: 67%;
        position: relative;
        width: 100%;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    .mix a {
        color: #000000;
    }
    .big-icon-plane {
        display: inline-block;
        /*background: url('front/img/what_icon.png') no-repeat transparent;*/
        width: 123px;
        height: 123px;
    }

    .big-icon-wrench {
        display: inline-block;
        /*background: url('front/img/who_icon.png') no-repeat transparent;*/
        width: 123px;
        height: 123px;
    }

    .big-icon-heart {
        display: inline-block;
        /*background: url('front/img/why_icon.png') no-repeat transparent;*/
        width: 123px;
        height: 123px;
    }
</style>
<div class="contact-back top-30">
    <div class="container top-30">
        <div class="row text-center hero-unit">
            <?php
                $repairs_heading = 'Pre-Engineered Fabric Structures';
                if (isset($settings->repairs_heading) && trim($settings->repairs_heading) != '' && trim($settings->repairs_heading) != null) {
                    $repairs_heading = $settings->repairs_heading;
                } 
            ?>
            <h1>{{$repairs_heading}}</h1>
            <?php
                $repairs_description = 'If you are looking to purchase a Pre-Engineered Fabric Structure, we have the product-line to match your every need!';
                if (isset($settings->repairs_description) && trim($settings->repairs_description) != '' && trim($settings->repairs_description) != null) {
                    $repairs_description = $settings->repairs_description;
                } 
            ?>
            <p><?php echo $repairs_description?></p>
        </div>

        <hr>

        <div class="row text-center top-30">
            <div class="col-md-4">
                <div>
                <?php
                    $whoImage = url('front/img/what_icon.png');
                    if (isset($settings->repairs_who_is_image) && $settings->repairs_who_is_image != '' && $settings->repairs_who_is_image != null) {
                        $whoImage = url('files/repairs/'.$settings->repairs_who_is_image);
                    }
                ?>
                <img src="{{$whoImage}}" class="big-icon-plane">
                </div>
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <?php
                            $repairs_who_is_title = 'Who is SpanMaster Structures Ltd.?';
                            if (isset($settings->repairs_who_is_title) && trim($settings->repairs_who_is_title) != '' && trim($settings->repairs_who_is_title) != null) {
                                $repairs_who_is_title = trim($settings->repairs_who_is_title);
                            } 
                        ?>
                        <h3>{{$repairs_who_is_title}}</h3>

                        <?php
                            $repairs_who_is_description = 'SpanMaster boasts over 100 years of combined experience in Pre-engineered Fabric Structures.<br />
                        We provide professional project management and support to customers in Western Canada.';
                            if (isset($settings->repairs_who_is_description) && trim($settings->repairs_who_is_description) != ''  && trim($settings->repairs_who_is_description) != null) {
                                $repairs_who_is_description = $settings->repairs_who_is_description;
                            } 
                        ?>
                        <?php echo $repairs_who_is_description?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                <?php
                    $whoIsForImage = url('front/img/who_icon.png');
                    if (isset($settings->repairs_who_is_it_for_image) && $settings->repairs_who_is_it_for_image != '' && $settings->repairs_who_is_it_for_image != null) {
                        $whoIsForImage = url('files/repairs/'.$settings->repairs_who_is_it_for_image);
                    }
                ?>
                <img src="{{$whoIsForImage}}" class="big-icon-wrench">
                </div>
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">

                        <?php
                            $repairs_who_is_it_for_title = 'Who is it for?';
                            if (isset($settings->repairs_who_is_it_for_title) && trim($settings->repairs_who_is_it_for_title) != '' && trim($settings->repairs_who_is_it_for_title) != null) {
                                $repairs_who_is_it_for_title = $settings->repairs_who_is_it_for_title;
                            } 
                        ?>
                        <h3>{{$repairs_who_is_it_for_title}}</h3>

                        <?php
                            $repairs_who_is_it_for_description = 'Our buildings have unlimited uses, from riding arenas to distribution centers to repair shops to storage and warehousing.<br />
                        <b>If you can dream it, we can build it.</b>';
                            if (isset($settings->repairs_who_is_it_for_description) && trim($settings->repairs_who_is_it_for_description) != ''  && trim($settings->repairs_who_is_it_for_description) != null) {
                                $repairs_who_is_it_for_description = trim($settings->repairs_who_is_it_for_description);
                            } 
                        ?>
                        <?php echo $repairs_who_is_it_for_description?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <?php
                    $whyImage = url('front/img/why_icon.png');
                    if (isset($settings->repairs_why_you_love_image) && $settings->repairs_why_you_love_image != '' && $settings->repairs_why_you_love_image != null) {
                        $whyImage = url('files/repairs/'.$settings->repairs_why_you_love_image);
                    }
                ?>
                <img src="{{$whyImage}}" class="big-icon-heart">
                </div>
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <?php
                            $repairs_why_you_love_title = 'Why you\'ll love us';
                            if (isset($settings->repairs_why_you_love_title) && trim($settings->repairs_why_you_love_title) != '' && trim($settings->repairs_why_you_love_title) != null) {
                                $repairs_why_you_love_title = $settings->repairs_why_you_love_title;
                            } 
                        ?>
                        <h3>{{$repairs_why_you_love_title}}</h3>

                        <?php
                            $repairs_why_you_love_description = 'We make it easier to get your structure professionally built on time and on budget.<br />
                        Check out our client testimonials to see why we are the first-choice for pre-engineered structures.';
                            if (isset($settings->repairs_why_you_love_description) && trim($settings->repairs_why_you_love_description) != ''  && trim($settings->repairs_why_you_love_description) != null) {
                                $repairs_why_you_love_description = $settings->repairs_why_you_love_description;
                            } 
                        ?>
                        <?php echo $repairs_why_you_love_description?>
                    </div>
                </div>
            </div>
        </div>

        <?php
            if (count($processes) > 0) {
        ?>
                <hr>    
                <div class="row">
                    <h1>Our Process</h1>
                </div>
        <?php
                foreach ($processes as $key => $process) {
                    $rightDiv = '';
                    if (($key%2)==0) {
                        $rightDiv='left';           
                      } else {
                        $rightDiv ='right';    
                      }
        ?>
                <div class="row top-30">
                    <div class="col-sm-6" style="float:<?php echo $rightDiv?>">
                        <h2>{{$process->name}}</h2>
                        <?php echo $process->description; ?>
                    </div>
                    <div class="col-sm-6">
                        <img src="{{url('files/process/'.$process->image)}}" class="img-responsive">
                    </div>
                </div>
        <?php
                }
            }
        ?>

        <hr>

        <div class="row text-center top-bot-30">
            <h1>Contact us for your free quote today!</h1>
            <a href="{{url('quote')}}"><button type="button" class="btn btn-danger btn-lg gradient">Request a Quote</button></a>
            <h2>You'll be glad you did!</h2>
        </div>
        
        <hr>

    </div>
    @include('front.common.partners')

</div>

@endsection
@section('scripts')


<script type="text/javascript">
$(document).ready(function() {
    
});
</script> 
@endsection