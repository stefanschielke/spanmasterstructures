@extends('front.layouts.main')
@section('content')
<link href="{{URL::to('pannel/css/jquery-te-1.4.0.css')}}" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('pannel/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('pannel/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .contact-back {
        background: url(front/img/001a.jpg) no-repeat center center fixed;
        display: table;
        height: 600px;
        position: relative;
        width: 100%;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>

<div class="contact-back">
    <div class="padded top-30 left-30 right-30">
        <div class="row">
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-sm-10 box text-center">
                        <h2>General Inquiries</h2>
                        <h3>Phone: {{(isset($settings->inquiry_phone) && $settings->inquiry_phone != '')?$settings->inquiry_phone:'N/A'}}<br />
                            Toll Free: {{(isset($settings->inquiry_toll_free) && $settings->inquiry_toll_free != '')?$settings->inquiry_toll_free:'N/A'}} <br />
                            Email: <a href="mailto:{{(isset($settings->inquiry_email) && $settings->inquiry_email != '')?$settings->inquiry_email:''}}">{{(isset($settings->inquiry_email) && $settings->inquiry_email != '')?$settings->inquiry_email:'N/A'}}</a>
                        </h3>
                        <span>&nbsp;</span>
                    </div>
                </div>
                <div class="row">
                    <?php
                        if(count($contacts) > 0) {
                            foreach ($contacts as $key => $contact) {
                    ?>
                        <div class="col-sm-5 col-xs-12 box">
                            <h4>{{$contact->name}}</h4>
                            <h5>{{$contact->designation}}</h5>
                            <p><a href="mailto:{{$contact->email}}">{{$contact->email}}</a>
                            </p>
                        </div>
                    <?php
                            }
                        }
                    ?>
                </div>
            </div>
            <div class="col-sm-7">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Name <span class="red">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="company" class="col-sm-3 control-label">Company&nbsp;&nbsp;</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="company" name="company" placeholder="Company name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-3 control-label">Phone <span class="red">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="555-555-1212" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Best time to call</label>
                        <div class="col-md-3">
                            <div class="input-group">
                                <input type="text" class="form-control timepicker timepicker-no-seconds" id="best_time_to_call">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-clock-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email <span class="red">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="email" name="email" placeholder="someone@example.com" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="subject" class="col-sm-3 control-label">Subject <span class="red">*</span></label>
                        <div class="col-sm-9">
                            <input type="text"  class="form-control" rows="1" id="subject" name="subject"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message" class="col-sm-3 control-label">Message <span class="red">*</span></label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="4" id="message" name="message"></textarea>
                        </div>
                    </div>
                    <div class="row nothing">
                        <div class="col-sm-3 control-label">Leave this empty</div>
                        <div class="col-sm-9">
                            <input type="urll" class="form-control" id="urll" name="urll">
                        </div>
                    </div>
                    <div class="form-group top-15">
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="contact-msg" id="msg_div">  </div>
                        </div>
                        <div class="col-sm-1 col-sm-offset-3">
                            <img class="button_spinners contact_loader" src="{{url('front/images/ajax-loader_dark.gif')}}" id="contact_loader">
                            <input id="contact_submit" name="submit" type="button" value="Send" class="btn btn-danger btn-lg gradient submit_contact">
                            
                        </div>
                        <div class="col-sm-6 col-sm-offset-1">
                            <p>All fields marked with <span class="red">*</span> are required.</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')

    <script type="text/javascript" src="{{url('front/vendors/mixitup/src/jquery.mixitup.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/jquery.fancybox.pack.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/helpers/jquery.fancybox-buttons.js')}}"></script>
    <script type="text/javascript" src="{{url('front/vendors/fancybox/source/helpers/jquery.fancybox-media.js')}}"></script>
    <script type="text/javascript" src="{{url('front/js/portfolio.js')}}"></script>

     <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('pannel/assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('pannel/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('pannel/assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#best_time_to_call').timepicker();
    $('#best_time_to_call').val('');
    $(".fancybox").fancybox({
        helpers : {
            title: {
                type: 'over',
                position: 'top'
            }
        }
    });
});
</script> 
@endsection