<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        $user = new User;
        $user->name = 'Bushi Naz';
        $user->email = 'nazbushi@gmail.com';
        $user->password = Hash::make('bushinaz');
        $user->activation_key = Hash::make(time());
        $user->activated_on = Carbon::now();
        $user->save();
    }
}
