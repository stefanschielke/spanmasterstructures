<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
             $table->string('title', 255);
            $table->string('image', 255);
            $table->tinyInteger('is_featured')->default(0); 
            $table->timestamps();
        });

        Schema::table('project_images', function (Blueprint $table) {
           $table->foreign('project_id')
                    ->references('id')->on('projects')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_images');

        Schema::table('project_images', function (Blueprint $table) {
            $table->dropForeign('project_images_project_id_foreign');
        });
    }
}
