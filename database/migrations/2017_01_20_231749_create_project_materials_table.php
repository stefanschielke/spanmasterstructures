<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_materials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('material_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('project_materials', function (Blueprint $table) {
           $table->foreign('project_id')
                    ->references('id')->on('projects')
                    ->onDelete('cascade')
                    ->onUpdate('cascade'); 

            $table->foreign('material_id')
                    ->references('id')->on('materials')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_materials');

        Schema::table('project_materials', function (Blueprint $table) {
            $table->dropForeign('project_materials_project_id_foreign');
            $table->dropForeign('project_materials_material_id_foreign');
        });
    }
}
