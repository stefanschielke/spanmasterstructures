<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mission_title', 255)->nullable();
            $table->text('mission_description')->nullable();
            $table->string('sponsorship_title', 255)->nullable();
            $table->string('sponsorship_image', 255)->nullable();
            $table->text('sponsorship_description')->nullable();
            $table->string('certification_title', 255)->nullable();
            $table->string('certification_image', 255)->nullable();
            $table->text('certification_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
