<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('project_types', function (Blueprint $table) {
           $table->foreign('project_id')
                    ->references('id')->on('projects')
                    ->onDelete('cascade')
                    ->onUpdate('cascade'); 

            $table->foreign('type_id')
                    ->references('id')->on('types')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_types');

        Schema::table('project_types', function (Blueprint $table) {
            $table->dropForeign('project_types_project_id_foreign');
            $table->dropForeign('project_types_type_id_foreign');
        });
    }
}
