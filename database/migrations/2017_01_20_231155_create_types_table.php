<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->enum('type', ['project', 'product', 'both'])->default('both');
            $table->tinyInteger('is_active')->default(1); 
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('types', function (Blueprint $table) {
           $table->foreign('created_by')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');

        Schema::table('types', function (Blueprint $table) {
            $table->dropForeign('types_created_by_foreign');
        });
    }
}
