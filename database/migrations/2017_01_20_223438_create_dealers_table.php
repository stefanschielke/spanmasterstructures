<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('image', 255)->nullable();
            $table->tinyInteger('is_active')->default(1); 
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('dealers', function (Blueprint $table) {
           $table->foreign('created_by')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealers');

        Schema::table('dealers', function (Blueprint $table) {
            $table->dropForeign('dealers_created_by_foreign');
        });
    }
}
