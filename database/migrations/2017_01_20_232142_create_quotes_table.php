<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('company_name', 255)->nullable();
            $table->string('phone', 255)->nullable();
            $table->string('fax', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('postal_code', 255)->nullable();
            $table->enum('use', ['agriculture', 'commercial', 'forestry', 'horse_equine', 'industrial', 'marine_storage', 'mining', 'personal_use_storage', 'sports_complex', 'water_treatment', 'other'])->default('')->nullable();
            $table->enum('installation_period', ['asap', 'next_6_months', 'next_year', 'just_looking', 'other'])->default('')->nullable();
            $table->enum('about_us', ['online_ad', 'print_ad', 'saw_building', 'personal_referral', 'other'])->default('')->nullable();
            $table->text('other_use')->nullable();
            $table->text('about_us_reason')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
