<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('designation', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('email', 255)->nullable();
            $table->string('linked_in', 255)->nullable();
            $table->string('image', 255)->nullable();
            $table->tinyInteger('is_active')->default(1); 
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('team_members', function (Blueprint $table) {
           $table->foreign('created_by')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_members');

        Schema::table('team_members', function (Blueprint $table) {
            $table->dropForeign('team_members_created_by_foreign');
        });
    }
}
