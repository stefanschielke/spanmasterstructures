<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->text('testimonial')->nullable();
            $table->timestamps();
        });

        Schema::table('project_clients', function (Blueprint $table) {
           $table->foreign('project_id')
                    ->references('id')->on('projects')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');  

            $table->foreign('client_id')
                    ->references('id')->on('clients')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_clients');

        Schema::table('project_clients', function (Blueprint $table) {
            $table->dropForeign('project_clients_project_id_foreign');
            $table->dropForeign('project_clients_client_id_foreign');
        });
    }
}
