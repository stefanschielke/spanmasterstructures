<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure, \Session;

class AclWithAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        $loggedInUser = Auth::user();
        if ($loggedInUser != NULL) {
            
        } else {
            return redirect('/admin');
        }

        return $next($request);
       
    }
}
