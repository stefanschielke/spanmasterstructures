<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Models\Type;
use App\Models\Client;
use App\Models\TeamMember;
use App\Models\Setting;
use App\Models\Usage;
use App\Models\Shape;
use App\Models\HomeImage;
use App\Models\SponsorshipImage;
use App\Models\CertificationImage;
use \Validator, \Session;
class HomeController extends Controller {

    public $dealer_repo;
    public $user_repo;
    public $type_repo;
    public $material_repo;
    public $project_repo;
    public $client_repo;
    public $team_repo;
    public $setting_repo;
    public $shape_repo;
    public $use_repo;
    public $quote_repo;
    public $contact_repo;
    public $partner_repo;
    public $process_repo;

    public $material_model;
    public $type_model;
    public $client_model;
    public $use_model;
    public $shape_model;
    public $home_image_model;
    
    public function __construct(Material $material, Type $type, Client $client, Usage $use, Shape $shape, HomeImage $homeImage) {
        $this->dealer_repo = app()->make('DealerRepository');
        $this->user_repo = app()->make('UserRepository');
        $this->type_repo = app()->make('TypeRepository');
        $this->material_repo = app()->make('MaterialRepository');
        $this->project_repo = app()->make('ProjectRepository');
        $this->client_repo = app()->make('ClientRepository');
        $this->team_repo = app()->make('TeamRepository');
        $this->setting_repo = app()->make('SettingRepository');
        $this->shape_repo = app()->make('ShapesRepository');
        $this->use_repo = app()->make('UsesRepository');
        $this->quote_repo = app()->make('QuoteRepository');
        $this->contact_repo = app()->make('ContactRepository');
        $this->partner_repo = app()->make('PartnerRepository');
        $this->process_repo = app()->make('ProcessRepository');

        $this->home_image_model = $homeImage;
        $this->material_model = $material;
        $this->type_model = $type;
        $this->client_model = $client;
        $this->use_model = $use;
        $this->shape_model = $shape;
    }

 	public function showLogin(Request $request) {
        $title = 'SpanMaster | Admin | Login';
        return view('admin.login',['title'=>$title]);
    }
    public function showNotFound(Request $request) {
        $title = 'SpanMaster | Admin | 404';
        return view('admin.errors.404',['title'=>$title]);
    }
    public function showUnAuthorized(Request $request) {
        $title = 'SpanMaster | Admin | 401';
        return view('admin.errors.401',['title'=>$title]);
    }

    public function showActivation(Request $request) {
        
        $input = $request->only('code');

        $rules = ['code' => 'required'];

        $messages = ['code.required' => 'No Activation Code Found.'
                    ];      
        $errors = [];

        $validator = Validator::make($input, $rules, $messages); 
        $hasError = false;

        if($validator->fails()) {
            $hasError = true;
            $errors = $validator->messages()->all();
        } else {
            $user = $this->user_repo->findByAttribute('activation_key',$input['code']);
            if ($user == NULL) {
                $hasError = true;
                $errors[] = 'Invalid Activation Code';
            } else {
                if($user->activated_on != NULL) {
                    $hasError = true;
                    $errors[] = 'Account Already Activated';
                }
            }
        }
        return view('admin.activation',['code'=>$input['code'],'errors'=>$errors,'hasError'=>$hasError]);
    }

    public function showRecover(Request $request) {
        $input = $request->only('code');

        $rules = ['code' => 'required'];

        $messages = ['code.required' => 'No Forgot Password Code Found.'
                    ];      
        $errors = [];
        $validator = Validator::make($input, $rules, $messages); 
        $hasError = false;
        if($validator->fails()) {
            $errors = $validator->messages()->all();
            $hasError = true;
        } else {
            $user = $this->user_repo->findByAttribute('recover_password_key',$input['code']);
            if ($user == NULL) {
                $hasError = true;
                $errors[] = 'Invalid Forgot Password Code';
            }
        }
        return view('admin.recover-password',['code'=>$input['code'],'errors'=>$errors,'hasError'=>$hasError]);
    }

    /* for users listing */
    public function showUsers(Request $request) {
        $title  = 'SpanMaster | Admin | Users';
        $logged_in_user   = Auth::user();
        return view('admin.users',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update user */
    public function showUser(Request $request) {
        $title = 'SpanMaster | Admin | User';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $user_id = 0;
        $user = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $user_id = $input['id'];
            $user = $this->user_repo->findById($user_id);
            if ($user == NULL) {
                return redirect('/admin/404');
            }
        }

        return view('admin.user',['title'=>$title, 
                            'user_id' => $user_id, 
                            'user'=>$user,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for user's profule update */
    public function showUserProfile(Request $request) {
        $title = 'SpanMaster | Admin | Profile';
        $logged_in_user   = Auth::user();
        return view('admin.user-profile',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for dealers listing */
    public function showDealers(Request $request) {
        $title  = 'SpanMaster | Admin | Dealers';
        $logged_in_user   = Auth::user();
        return view('admin.dealers',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update dealer */
    public function showDealer(Request $request) {
        $title = 'SpanMaster | Admin | Dealer';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $dealer_id = 0;
        $dealer = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $dealer_id = $input['id'];
            $dealer = $this->dealer_repo->findById($dealer_id);
            if ($dealer == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.dealer',['title'=>$title, 
                            'dealer_id' => $dealer_id, 
                            'dealer'=>$dealer,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for partners listing */
    public function showPartners(Request $request) {
        $title  = 'SpanMaster | Admin | Partners';
        $logged_in_user   = Auth::user();
        return view('admin.partners',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update partner */
    public function showPartner(Request $request) {
        $title = 'SpanMaster | Admin | Partner';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $partner_id = 0;
        $partner = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $partner_id = $input['id'];
            $partner = $this->partner_repo->findById($partner_id);
            if ($partner == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.partner',['title'=>$title, 
                            'partner_id' => $partner_id, 
                            'partner'=>$partner,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for proceses listing */
    public function showProcesses(Request $request) {
        $title  = 'SpanMaster | Admin | Processes';
        $logged_in_user   = Auth::user();
        return view('admin.processes',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update process */
    public function showProcess(Request $request) {
        $title = 'SpanMaster | Admin | Process';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $process_id = 0;
        $process = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $process_id = $input['id'];
            $process = $this->process_repo->findById($process_id);
            if ($process == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.process',['title'=>$title, 
                            'process_id' => $process_id, 
                            'process'=>$process,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for types listing */
    public function showTypes(Request $request) {
        $title  = 'SpanMaster | Admin | Types';
        $logged_in_user   = Auth::user();
        return view('admin.types',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update type */
    public function showType(Request $request) {
        $title = 'SpanMaster | Admin | Type';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $type_id = 0;
        $type = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $type_id = $input['id'];
            $type = $this->type_repo->findById($type_id);
            if ($type == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.type',['title'=>$title, 
                            'type_id' => $type_id, 
                            'type'=>$type,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for materials listing */
    public function showMaterials(Request $request) {
        $title  = 'SpanMaster | Admin | Materials';
        $logged_in_user   = Auth::user();
        return view('admin.materials',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update material */
    public function showMaterial(Request $request) {
        $title = 'SpanMaster | Admin | Material';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $material_id = 0;
        $material = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $material_id = $input['id'];
            $material = $this->material_repo->findById($material_id);
            if ($material == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.material',['title'=>$title, 
                            'material_id' => $material_id, 
                            'material'=>$material,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for shapes listing */
    public function showShapes(Request $request) {
        $title  = 'SpanMaster | Admin | Shapes';
        $logged_in_user   = Auth::user();
        return view('admin.shapes',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update shape */
    public function showShape(Request $request) {
        $title = 'SpanMaster | Admin | Shape';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $shape_id = 0;
        $shape = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $shape_id = $input['id'];
            $shape = $this->shape_repo->findById($shape_id);
            if ($shape == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.shape',['title'=>$title, 
                            'shape_id' => $shape_id, 
                            'shape'=>$shape,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for uses listing */
    public function showUses(Request $request) {
        $title  = 'SpanMaster | Admin | Uses';
        $logged_in_user   = Auth::user();
        return view('admin.uses',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update use */
    public function showUse(Request $request) {
        $title = 'SpanMaster | Admin | Use';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $use_id = 0;
        $use = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $use_id = $input['id'];
            $use = $this->use_repo->findById($use_id);
            if ($use == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.use',['title'=>$title, 
                            'use_id' => $use_id, 
                            'use'=>$use,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for clients listing */
    public function showClients(Request $request) {
        $title  = 'SpanMaster | Admin | Clients';
        $logged_in_user   = Auth::user();
        return view('admin.clients',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update client */
    public function showClient(Request $request) {
        $title = 'SpanMaster | Admin | Client';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $client_id = 0;
        $client = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $client_id = $input['id'];
            $client = $this->client_repo->findById($client_id);
            if ($client == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.client',['title'=>$title, 
                            'client_id' => $client_id, 
                            'client'=>$client,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for projects listing */
    public function showProjects(Request $request) {
        $title  = 'SpanMaster | Admin | Projects';
        $logged_in_user   = Auth::user();

        $materials = $this->material_model->where('is_active', '=', 1)->get();
        $types = $this->type_model->where('is_active', '=', 1)->get();
        $clients = $this->client_model->where('is_active', '=', 1)->get();
        $uses = $this->use_model->where('is_active', '=', 1)->get();
        $shapes = $this->shape_model->where('is_active', '=', 1)->get();
        return view('admin.projects',['title'=>$title, 
                                        'logged_in_user'=>$logged_in_user,
                                        'materials'=>$materials,
                                        'types'=>$types,
                                        'clients'=>$clients,
                                        'uses'=>$uses,
                                        'shapes'=>$shapes
                                        ]);
    }

    /* for add/update project */
    public function showProject(Request $request) {
        $title = 'SpanMaster | Admin | Project';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $project_id = 0;
        $project = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $project_id = $input['id'];
            $project = $this->project_repo->findById($project_id);
            if ($project == NULL) {
                return redirect('/admin/404');
            }
        }

        $materials = $this->material_model->where('is_active', '=', 1)->get();
        $types = $this->type_model->where('is_active', '=', 1)->get();
        $clients = $this->client_model->where('is_active', '=', 1)->get();
        $uses = $this->use_model->where('is_active', '=', 1)->get();
        $shapes = $this->shape_model->where('is_active', '=', 1)->get();
        return view('admin.project',['title'=>$title, 
                            'project_id' => $project_id, 
                            'project'=>$project,
                            'materials'=>$materials,
                            'types'=>$types,
                            'clients'=>$clients,
                            'uses'=>$uses,
                            'shapes'=>$shapes,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for detail view project */
    public function showViewProject(Request $request) {
        $title = 'SpanMaster | Admin | Project Details';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $project_id = 0;
        $project = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $project_id = $input['id'];
            $project = $this->project_repo->findById($project_id);
            if ($project == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.view-project',['title'=>$title, 
                            'project_id' => $project_id, 
                            'project'=>$project,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for tema listing */
    public function showTeams(Request $request) {
        $title  = 'SpanMaster | Admin | Management Team Members';
        $logged_in_user   = Auth::user();
        return view('admin.teams',['title'=>$title, 
                                        'logged_in_user'=>$logged_in_user
                                        ]);
    }

    /* for add/update team */
    public function showTeam(Request $request) {
        $title = 'SpanMaster | Admin | Management Team Member';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $team_id = 0;
        $team = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $team_id = $input['id'];
            $team = $this->team_repo->findById($team_id);
            if ($team == NULL) {
                return redirect('/admin/404');
            }
        }

        return view('admin.team',['title'=>$title, 
                            'team_id' => $team_id, 
                            'team'=>$team,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for contact members listing */
    public function showContactMembers(Request $request) {
        $title  = 'SpanMaster | Admin | Contact Members';
        $logged_in_user   = Auth::user();
        return view('admin.contact-members',['title'=>$title, 
                                        'logged_in_user'=>$logged_in_user
                                        ]);
    }

    /* for add/update contact member */
    public function showContactMember(Request $request) {
        $title = 'SpanMaster | Admin | Contact Member';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $contact_id = 0;
        $contact = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $contact_id = $input['id'];
            $contact = $this->contact_repo->findById($contact_id);
            if ($contact == NULL) {
                return redirect('/admin/404');
            }
        }

        return view('admin.contact-member',['title'=>$title, 
                            'contact_id' => $contact_id, 
                            'contact'=>$contact,
                            'logged_in_user'=>$logged_in_user]);
    }

    /* for settings */
    public function showSettings(Request $request) {
        $title  = 'SpanMaster | Admin | Content Management';
        $logged_in_user   = Auth::user();

        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        $slider_images = $this->home_image_model->get();
        $sponsorship_images = SponsorshipImage::get();
        $certification_images = CertificationImage::get();
        return view('admin.settings',['title'=>$title, 'logged_in_user'=>$logged_in_user, 'settings'=>$settings, 'slider_images'=>$slider_images, 'sponsorship_images'=>$sponsorship_images, 'certification_images'=>$certification_images]);
    }
    public function showHomeSettings(Request $request) {
        $title  = 'SpanMaster | Admin | Content Management';
        $logged_in_user   = Auth::user();

        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        $slider_images = $this->home_image_model->get();
        $sponsorship_images = SponsorshipImage::get();
        $certification_images = CertificationImage::get();
        return view('admin.settings',['title'=>$title, 'logged_in_user'=>$logged_in_user, 'settings'=>$settings, 'slider_images'=>$slider_images, 'from'=>'home', 'sponsorship_images'=>$sponsorship_images, 'certification_images'=>$certification_images]);
    }
    public function showAboutSettings(Request $request) {
        $title  = 'SpanMaster | Admin | Content Management';
        $logged_in_user   = Auth::user();

        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        $slider_images = $this->home_image_model->get();
        $sponsorship_images = SponsorshipImage::get();
        $certification_images = CertificationImage::get();
        return view('admin.settings',['title'=>$title, 'logged_in_user'=>$logged_in_user, 'settings'=>$settings, 'slider_images'=>$slider_images, 'from'=>'about_us', 'sponsorship_images'=>$sponsorship_images, 'certification_images'=>$certification_images]);
    }

    public function showfabricStructure(Request $request) {
        $title  = 'SpanMaster | Admin | Pre-engineered Fabric Structures';
        $logged_in_user   = Auth::user();

        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        $slider_images = $this->home_image_model->get();
        return view('admin.settings',['title'=>$title, 'logged_in_user'=>$logged_in_user, 'settings'=>$settings, 'slider_images'=>$slider_images, 'from'=>'r_m']);
    }


    /* for inquiry */
    public function showInquiry(Request $request) {
        $title  = 'SpanMaster | Admin | General Inquiry';
        $logged_in_user   = Auth::user();

        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        return view('admin.inquiry',['title'=>$title, 'logged_in_user'=>$logged_in_user, 'settings'=>$settings]);
    }

    /* for quotes listing */
    public function showQuotes(Request $request) {
        $title  = 'SpanMaster | Admin | Quotes';
        $logged_in_user   = Auth::user();
        return view('admin.quotes',['title'=>$title, 'logged_in_user'=>$logged_in_user]);
    }

    /* for add/update quote */
    public function showQuote(Request $request) {
        $title = 'SpanMaster | Admin | Quote';
        $logged_in_user   = Auth::user();
        $input = $request->only('id');
        $quote_id = 0;
        $quote = NULL;
        if (isset($input['id']) && $input['id'] != '') {
            $quote_id = $input['id'];
            $quote = $this->quote_repo->findById($quote_id);
            if ($quote == NULL) {
                return redirect('/admin/404');
            }
        }
        return view('admin.quote',['title'=>$title, 
                            'quote_id' => $quote_id, 
                            'quote'=>$quote,
                            'logged_in_user'=>$logged_in_user]);
    }
}
