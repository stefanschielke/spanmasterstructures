<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Repositories\DealerRepository;
use \Validator, \Session;

class DealerController extends Controller {

    const PER_PAGE = 10;
    /**
     *
     * This will hold the instance of SettingsRepository class which is used for
     * fetching, modifying, creating and removing data from database.
     *
     * @var object
     * @access private
     *
     **/
    private $_repository;

    public function __construct() {
        $this->_repository = app()->make('DealerRepository');
    }

 	public function upload(Request $request){

        $input = [];
        $input['file'] = $request->file('file');
        $input['from'] = $request->only('from');

        $rules = array(
                        'file' => 'required|image|max:10000000|mimes:png,jpg,jpeg,gif',
                        );

        $validator = Validator::make($input,$rules);
        if ($validator->fails()) {
            $status = 400;
            $output = ['error' => array(
                                'messages'  => $validator->messages()->all(),
                                'type'      => 'BadRequest',
                                'status'      => $status
                )];
        } else {
            
            /*$file =$request->file('file');
            $fileName = md5(time()*rand()).'.'.strtolower($file->getClientOriginalExtension());
            $targetfolder = config('app.files.'.$input['from']['from'].'.full_path');
            $targetfolder = $targetfolder . basename( $fileName) ;
            if(move_uploaded_file($_FILES['file']['tmp_name'], $targetfolder)){
                     
                $status = 200;
                $output = array('status'=>$status,'file'=>$fileName,
                                        'url'=>url('/files/'.$input['from']['from'].'/'.$fileName), 
                                        'messages'=>"Image has been uploaded successfully.");
            }else {
                $status = 400;
                $output = array('error' => array(
                                    'messages'  => ['Unable to upload your Image. Try Again.'],
                                    'type'      => 'BadRequest',
                                    'status'      => $status
                    ));
            }*/
            
            $file =$request->file('file');
            $fileNameReal = $file->getClientOriginalName();
            $fileName = md5(time()*rand()).'.'.strtolower($file->getClientOriginalExtension());
            if($file->move(config('app.files.'.$input['from']['from'].'.full_path'), $fileName)) {
                $status = 200;
                $output = array('status'=>$status,'file'=>$fileName,'file_real_name'=>$fileNameReal,
                                            'url'=>url('/files/'.$input['from']['from'].'/'.$fileName), 
                                            'messages'=> "Image has been uploaded successfully.");
                
            } else {
                $status = 400;
                $output = array('error' => array(
                                    'messages'  => ['Unable to upload your Image. Try Again.'],
                                    'type'      => 'BadRequest',
                                    'status'      => $status
                    ));
            }  

        }
        return response()->json($output, $status);
    }

    /**
     *
     * This method will create a new dealer
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function create(Request $request) {
        
        $input = $request->only('name', 'image');
        $rules = [
                    'image'=>'required'
                ];
        $messages = [
                    'image'=> 'Please enter image'
                ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>$validator->messages()->all()]];
        } else {      
            $response = $this->_repository->create($input); 
            if($response == false) {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating dealer.']]];
            } else {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Dealer has been created successfully.']]];
            }
        } 
        return response()->json($output, $code);
    }

    /**
     *
     * This method will update existing dealer
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function update(Request $request){
        
        $input = $request->only('id','name', 'image');
        $rules = [
                    'id'            =>  'required|exists:dealers,id',
                    'image'         =>  'required'
                ];
        $messages = [
                    'id.required'       => 'Please enter dealer id',
                    'id.exists'         => 'Dealer not found',
                    'image.required'     => 'Please enter image'
                    ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>$validator->messages()->all()]];
        } else {
            $response = $this->_repository->update($input);
            if($response == NULL || $response == false) {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while updating dealer.']]];
            } else {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Dealer has been updated successfully ']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch data of individual dealer
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function view(Request $request){

        $input = $request->only('id');
        $rules = ['id'=>'required|exists:dealers,id'
                    ];

        $messages = ['id.required' => 'Please enter dealer id',
                    'id.exists' => 'Dealer not found'
                    ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>$validator->messages()->all()]];
        } else{
            $code = 200;
            $output = $this->_repository->findById($input['id']);
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch list of all dealers
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function all(Request $request) {
  
        $input = $request->only('pagination','keyword','limit', 'filter_by_status');
        $rules = [];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        
        // if validation passes
        } else {
            $code = 200;
            $pagination = false;
            if($input['pagination']) {
                $pagination = true;
            }
         
            $output = $this->_repository->findByAll($pagination, self::PER_PAGE, $input);
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will delete an existing dealer
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function remove(Request $request){

        $input = $request->only('id');
        $rules = ['id' => 'required|exists:dealers,id'];

        $messages = ['id.required' => 'Please enter dealer id.',
                    'id.exists' => 'Dealer not found.'
                    ];

        $validator = Validator::make($input,$rules,$messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code'=>$code,'messages'=>$validator->messages()->all()]];
        
        // if validation passes
        } else {
            $response = $this->_repository->deleteById($input['id']);

            if($response == 'success') {   
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Dealer has been deleted successfully.']]];
            } else if ($response == 'error') {
                $code = 405;
                $output = ['error' => ['code'=>$code,'messages'=>['An error occur while deleting this dealer.']]];
            } else if ($response == 'not_found') {
                $code = 404;
                $output = ['error' => ['code'=>$code,'messages'=>['Dealer not found.']]];
            } 
         }
         return response()->json($output, $code);
    }

    /**
     *
     * This method will change dealer status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus(Request $request) {

        // input parameters
        $input = $request->only('id', 'status');

        // define validation rules
        $rules = ['id'      => 'required | exists:dealers,id',
                  'status'  => 'required |in:1,0',
                ];

        $messages = [
                'id.required'           => 'Please enter dealer id.',
                'id.exists'             => 'Dealer not found.',
                'status.required'       => 'Please enter status.',
                'status.in'             => 'Status can only be 1 or 0.'
        ];

        $validator = Validator::make($input,$rules, $messages);

        if($validator->fails()){
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $response = $this->_repository->updateStatus($input);
            if ($response == 'success') {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Status has been updated successfully.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while updating status.']]];
            }
        }

        return response()->json($output, $code);
    }
}
