<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Repositories\SettingRepository;
use \Validator, \Session, Image, File;

class SettingController extends Controller {

    /**
     *
     * This will hold the instance of SettingRepository class which is used for
     * fetching, modifying, creating and removing data from database.
     *
     * @var object
     * @access private
     *
     **/
    private $_repository;

    public function __construct() {
        $this->_repository = app()->make('SettingRepository');
    }

    public function update(Request $request){

        $input = $request->only('mission_title', 'mission_description', 'sponsorship_title', 'sponsorship_description', 'sponsorship_images', 'certification_title', 'certification_description', 'certification_images', 'inquiry_phone', 'inquiry_toll_free', 'inquiry_email', 'about_us_title', 'about_us_description', 'about_us_image', 'why_choose_title', 'why_choose_description', 'home_heading', 'home_images', 'repairs_heading', 'repairs_description', 'repairs_who_is_title', 'repairs_who_is_description', 'repairs_who_is_it_for_title', 'repairs_who_is_it_for_description', 'repairs_why_you_love_title', 'repairs_why_you_love_descripton', 'repairs_who_is_image', 'repairs_who_is_it_for_image', 'repairs_why_you_love_image');
        $rules = [];
        $messages = [];

        $validator = Validator::make( $input, $rules, $messages);  

        // if validation fails
        if ( $validator->fails() ) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>$validator->messages()->all()]];

        } else { 
            if (isset($input['home_images']) && count($input['home_images']) > 0) {
                foreach ($input['home_images'] as $key => $image) {
                    $localFile  = config('app.files.slider.full_path').$image['image'];
                    $imageObj = Image::make($localFile);

                    $imageParts = explode('.', $image['image']);
                    $imageName = $imageParts[0];
                    $imageExtension = File::extension($localFile);
                    
                    // large image
                    $largeImageName = $imageName.'.'.$imageExtension;
                    $largeImage = config('app.files.slider.full_path').$largeImageName;
                    $imageObj->resize(1000,670)->save($largeImage);
                }
            }

            $response = $this->_repository->update($input);
            if($response) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Settings has been updated successfully.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while updating settings.']]];
            }
        }

        return response()->json($output, $code);
    }
}
