<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Data\Repositories\QuoteRepository;
use Illuminate\Http\Request;

use \Validator, \Session, Carbon\Carbon;;

class QuoteController extends Controller {

    const PER_PAGE = 10;
    /**
     *
     * This will hold the instance of QuoteRepository class which is used for
     * fetching, modifying, creating and removing data from database.
     *
     * @var object
     * @access private
     *
     **/
	private $_repository;

	public function __construct() {
		$this->_repository = app()->make('QuoteRepository');
	}   

    /**
     *
     * This method will create a new quote
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function create(Request $request){

        $input = $request->only('use','other_use', 'installation_period', 'about_us','about_us_reason', 'comments', 'name', 'company_name', 'phone', 'fax', 'email', 'address', 'city', 'postal_code', 'best_time_to_call');

        $rules = [
                    'name'              =>  'required',
                    'phone'             =>  'required',
                    'email'             =>  'required|email',
                    'city'              =>  'required',
                    'best_time_to_call' =>  ''
                ];
        $messages = [
                'name.required'         => 'Please enter name.',
                'phone.required'        => 'Please enter phone number.',
                'email.required'        => 'Please enter email address.',
                'email.email'           => 'Please enter valid email address.',
                'city.required'         => 'Please enter city.',
                'best_time_to_call.required'         => 'Please select best time to call.',

        ];

        $validator = Validator::make( $input, $rules, $messages);

        // if validation fails
        if ( $validator->fails() ) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->create($input);
            if($response == true) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Quote has been sent successfully.']]];
                
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while sending quote.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch list of all quotes
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function all(Request $request) {

        $input = $request->only('pagination','keyword','limit');

        $rules = ['pagination' => 'required'
                    ];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];

        // if validation passes
        } else {
            $code = 200;
            $pagination = false;
            if($input['pagination']) {
                $pagination = true;
            }

            $output = $this->_repository->findByAll($pagination, self::PER_PAGE, $input);
        }
        return response()->json($output, $code);
    } 
}
