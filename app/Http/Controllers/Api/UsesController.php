<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Data\Repositories\UsesRepository;
use Illuminate\Http\Request;

use \Validator, \Session, Carbon\Carbon;;

class UsesController extends Controller {

    const PER_PAGE = 10;
    /**
     *
     * This will hold the instance of UsesRepository class which is used for
     * fetching, modifying, creating and removing data from database.
     *
     * @var object
     * @access private
     *
     **/
	private $_repository;

	public function __construct() {
		$this->_repository = app()->make('UsesRepository');
	}   

    /**
     *
     * This method will create a new use
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function create(Request $request){

        $input = $request->only('name','description');

        $rules = [
                    'name'          =>  'required',
                    'description'          =>  'required'
                ];
        $messages = [
                'name.required'     => 'Please enter name.',
                'description.required'     => 'Please enter description.'

        ];

        $validator = Validator::make( $input, $rules, $messages);

        // if validation fails
        if ( $validator->fails() ) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->create($input);
            if($response == true) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Usage has been created successfully.']]];
                
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating usage.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will update an existing use
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function update(Request $request){

        $input = $request->only('id', 'name', 'description');

        $rules = [
                    'id'        =>  'required|exists:uses,id',
                    'name'      =>  'required',
                    'description'      =>  'required'
                ];
        $messages = [
                'id.required'       => 'Please enter use id.',
                'id.exists'         => 'use not found.',
                'name.required'       => 'Please enter name.',
                'description.required'       => 'Please enter description.'
        ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->update($input);
            if($response == 'success') {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Usage has been updated successfully.']]];
            } else if ($response == 'cannot_delete') {
                $code = 401;
                $output = ['error' => ['code'=>$code,'messages'=>['Sorry you cannot remove this usage as it is associated with some project.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while updating usage.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will change use status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus(Request $request) {

        // input parameters
        $input = $request->only('id', 'status');

        // define validation rules
        $rules = ['id'      => 'required | exists:uses,id',
                  'status'  => 'required |in:1,0',
                ];

        $messages = [
                'id.required'           => 'Please enter usage id.',
                'id.exists'             => 'Usage not found.',
                'status.required'       => 'Please enter status.',
                'status.in'             => 'Status can only be 1 or 0.'
        ];

        $validator = Validator::make($input,$rules, $messages);

        if($validator->fails()){
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $response = $this->_repository->updateStatus($input);
            if ($response == 'success') {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Status has been updated successfully.']]];
            } else if ($response == 'cannot_delete') {
                $code = 401;
                $output = ['error' => ['code'=>$code,'messages'=>['Sorry you cannot remove this usage as it is associated with some project.']]];
            }  else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while updating status.']]];
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will delete an existing use
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function remove(Request $request){

        $input = $request->only('id');

        $rules = ['id' => 'required|exists:uses,id'];

        $messages = ['id.required'      => 'Please enter usage id.',
                    'id.exists'         => 'Usage not found.'
                    ];

        $validator = Validator::make($input,$rules,$messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code'=>$code,'messages'=>[$validator->messages()->first()]]];
        
        // if validation passes
        } else {
            $response = $this->_repository->deleteById($input['id']);

            if($response == 'success') {   
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Usage has been deleted successfully.']]];
            } else if ($response == 'cannot_delete') {
                $code = 401;
                $output = ['error' => ['code'=>$code,'messages'=>['Sorry you cannot remove this usage as it is associated with some project.']]];
            }  else {
                $code = 405;
                $output = ['error' => ['code'=>$code,'messages'=>['An error occured while deleting this usage.']]];
            } 
        }
        
        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch data of individual use
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function view(Request $request){
        $user = Auth::user();
        dd($user);die;
        $input = $request->only('id');

        $rules = [
                    'id'=>'required|exists:uses,id'
                ];
        $messages = [
                'id.required'   => 'Please enter usage id.',
                'id.exists'     => 'Usage not found.'
                ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $code = 200;
            $response = $this->_repository->findById($input['id']);

            if ($response == NULL || $response == false) {
                $code = 404;
                $output = ['error' => ['code'=>$code,'messages'=>['Usage not found.']]];
            } else {
                $output = $response;
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch list of all uses
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function all(Request $request) {

        $input = $request->only('pagination','keyword','limit','filter_by_status', 'from');

        $rules = ['pagination' => 'required'
                    ];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];

        // if validation passes
        } else {
            $code = 200;
            $pagination = false;
            if($input['pagination']) {
                $pagination = true;
            }

            $output = $this->_repository->findByAll($pagination, self::PER_PAGE, $input);
        }
        return response()->json($output, $code);
    } 
}
