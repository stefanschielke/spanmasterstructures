<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Data\Repositories\ContactRepository;
use Illuminate\Http\Request;

use \Validator, \Session, Carbon\Carbon;;

class ContactController extends Controller {

    const PER_PAGE = 10;
    /**
     *
     * This will hold the instance of ContactRepository class which is used for
     * fetching, modifying, creating and removing data from database.
     *
     * @var object
     * @access private
     *
     **/
    private $_repository;

    public function __construct() {
        $this->_repository = app()->make('ContactRepository');
    } 

    /**
     *
     * This method will send a new contact request
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function send(Request $request) {
        
        $input = $request->only('name', 'email', 'phone', 'company', 'subject', 'message', 'best_time_to_call');
        $rules = [
                    'name'         =>'required',
                    'email'        =>'required|email',
                    'phone'         =>'required',
                    'subject'      =>'required',
                    'message'      =>'required'
                ];
        $messages = [
                    'name.required'    => 'Please enter your name',
                    'email.required'   => 'Please enter your email address',
                    'email.email'      => 'Please enter valid email address',
                    'phone.required'    => 'Please enter your phone',
                    'subject.required' => 'Please enter your subject',
                    'message.required' => 'Please enter your message',
                ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>$validator->messages()->all()]];
        } else {      
            try {

                $subject = 'Contact Query';
                if (isset($input['subject']) && $input['subject'] != '') {
                    $subject = $input['subject'];
                }
                \Mail::send('front.emails.contact-us', ['data' => $input], function ($m) use ($input, $subject) {
                    $m->from(env('MAIL_ADDRESS', 'smtp@vegatechnologies.net'), env('MAIL_USERNAME', 'SpanMaster'));
                    //$m->to(env('CONTACT_EMAIL', 'nazbushi@gmail.com'))->subject($subject);
                    $m->to('stefan@intechrity.ca', 'SpanMaster')->subject($subject);
                    $m->to('nazbushi@gmail.com', 'SpanMaster')->subject($subject);
                });

                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Contact request has been sent successfully.']]];
            } catch(\Exception $e){
                dd($e->getMessage());
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while submitting contact request.']]];
            }
        } 
        return response()->json($output, $code);
    }


    /**
     *
     * This method will create a new contact
     * and will return output back to contact as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function create(Request $request){

        $input = $request->only('name','designation', 'email');

        $rules = [
                    'name'          =>  'required',
                    'designation'   =>  'required',
                    'email'         =>  'required|email',
                ];
        $messages = [
                'name.required'             => 'Please enter name.',
                'designation.required'      => 'Please enter designation.',
                'email.required'            => 'Please enter email.',
                'email.email'               => 'Please enter valid email address.',
                'linked_in.required'        => 'Please enter linkedin address.',
        ];

        $validator = Validator::make( $input, $rules, $messages);

        // if validation fails
        if ( $validator->fails() ) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->create($input);
            if($response == true) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Contact member has been created successfully.']]];
                
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating contact member.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will update an existing contact
     * and will return output back to contact as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function update(Request $request){

        $input = $request->only('id', 'name','designation', 'email');

        $rules = [
                    'id'            =>  'required|exists:contact_members,id',
                    'name'          =>  'required',
                    'designation'   =>  'required',
                    'email'         =>  'required|email'
                ];
        $messages = [
                'id.required'               => 'Please enter contact member id.',
                'id.exists'                 => 'Contact member not found.',
                'name.required'             => 'Please enter name.',
                'designation.required'      => 'Please enter designation.',
                'email.required'            => 'Please enter email.',
                'email.email'               => 'Please enter valid email address.'
        ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->update($input);
            if($response) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Contact member has been updated successfully.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while updating contact member.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will change contact status (active, inactive)
     * and will return output back to contact as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus(Request $request) {

        // input parameters
        $input = $request->only('id', 'status');

        // define validation rules
        $rules = ['id'      => 'required | exists:contact_members,id',
                  'status'  => 'required |in:1,0',
                ];

        $messages = [
                'id.required'           => 'Please enter contact member id.',
                'id.exists'             => 'contact member not found.',
                'status.required'       => 'Please enter status.',
                'status.in'             => 'Status can only be 1 or 0.'
        ];

        $validator = Validator::make($input,$rules, $messages);

        if($validator->fails()){
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $response = $this->_repository->updateStatus($input);
            if ($response) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Status has been updated successfully.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while updating status.']]];
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will delete an existing contact member
     * and will return output back to contact as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function remove(Request $request){

        $input = $request->only('id');

        $rules = ['id' => 'required|exists:contact_members,id'];

        $messages = ['id.required'      => 'Please enter contact member id.',
                    'id.exists'         => 'contact member not found.'
                    ];

        $validator = Validator::make($input,$rules,$messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code'=>$code,'messages'=>[$validator->messages()->first()]]];
        
        // if validation passes
        } else {
            $response = $this->_repository->deleteById($input['id']);

            if($response) {   
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Contact member has been deleted successfully.']]];
            } else {
                $code = 405;
                $output = ['error' => ['code'=>$code,'messages'=>['An error occured while deleting this contact member.']]];
            } 
        }
        
        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch data of individual contact
     * and will return output back to contact as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function view(Request $request){
        $user = Auth::user();
        
        $input = $request->only('id');

        $rules = [
                    'id'=>'required|exists:contact_members,id'
                ];
        $messages = [
                'id.required'   => 'Please enter contact member id.',
                'id.exists'     => 'contact member not found.'
                ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $code = 200;
            $response = $this->_repository->findById($input['id']);

            if ($response == NULL || $response == false) {
                $code = 404;
                $output = ['error' => ['code'=>$code,'messages'=>['Contact member not found.']]];
            } else {
                $output = $response;
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch list of all contacts
     * and will return output back to contact as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function all(Request $request) {

        $input = $request->only('pagination','keyword','limit','filter_by_status');

        $rules = ['pagination' => 'required'
                    ];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];

        // if validation passes
        } else {
            $code = 200;
            $pagination = false;
            if($input['pagination']) {
                $pagination = true;
            }

            $output = $this->_repository->findByAll($pagination, self::PER_PAGE, $input);
        }
        return response()->json($output, $code);
    } 
}
