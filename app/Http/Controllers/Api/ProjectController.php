<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Data\Repositories\ProjectRepository;
use Illuminate\Http\Request;

use \Validator, \Session, Carbon\Carbon, \Exception, \File, \Image;

class ProjectController extends Controller {

    const PER_PAGE = 10;
    /**
     *
     * This will hold the instance of ProjectRepository class which is used for
     * fetching, modifying, creating and removing data from database.
     *
     * @var object
     * @access private
     *
     **/
	private $_repository;

	public function __construct() {
		$this->_repository = app()->make('ProjectRepository');
	}   

    /**
     *
     * This method will create a new project
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function create(Request $request){

        $input = $request->only('name','description', 'type_id','shape_id', 'material_ids', 'uses_ids', 'client_id', 'images', 'uses', 'size', 'start_date', 'end_date', 'testimonial', 'recent_description', 'client_name', 'company_name', 'company_logo');

        $rules = [
                    'name'                  =>  'required',
                    'description'           =>  'required',
                    'type_id'               =>  'required|exists:types,id',
                    'shape_id'              =>  'required|exists:shapes,id',
                    'material_ids'          =>  'required|array',
                    'uses_ids'              =>  'required|array',
                    'client_id'             =>  'required|exists:clients,id',
                    'images'                =>  'required|array',
                    //'uses'                  =>  'required',
                    'size'                  =>  'required',
                    'start_date'            =>  'required',
                    'end_date'              =>  'required'
                ];
        $messages = [
                'name.required'             => 'Please enter name.',
                'description.required'      => 'Please enter description.',
                'type_id.required'          => 'Please select type.',
                'type_id.exists'            => 'Type not found.',
                'shape_id.required'         => 'Please select shape.',
                'shape_id.exists'           => 'Shape not found.',
                'material_ids.required'     => 'Please select materials.',
                'material_ids.array'        => 'Materials can only be array.',
                'client_id.required'        => 'Please select client.',
                'client_id.exists'          => 'Client not found.',
                'images.required'           => 'Please select image.',
                'images.array'              => 'Images can only be array.',
                'uses.required'             => 'Please enter uses.',
                'size.required'             => 'Please enter size.',
                'start_date.required'       => 'Please select start date.',
                'end_date.required'         => 'Please select end date.',

        ];

        $validator = Validator::make( $input, $rules, $messages);

        // if validation fails
        if ( $validator->fails() ) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {

            if (count($input['images']) > 0) {
                foreach ($input['images'] as $key => $image) {
                    $localFile  = config('app.files.project.full_path').$image['image'];
                    $imageObj = Image::make($localFile);

                    $imageParts = explode('.', $image['image']);
                    $imageName = $imageParts[0];
                    $imageExtension = File::extension($localFile);
                    
                    // large image
                    $largeImageName = $imageName.'.'.$imageExtension;
                    $largeImage = config('app.files.project.full_path').$largeImageName;
                    $imageObj->resize(600,450)->save($largeImage);

                    // medium image
                    $mediumImageName = $imageName.'-medium.'.$imageExtension;
                    $mediumImage = config('app.files.project.full_path').$mediumImageName;
                    $imageObj->resize(480,260)->save($mediumImage);

                    // small image
                    $smallImageName = $imageName.'-small.'.$imageExtension;
                    $smallImage = config('app.files.project.full_path').$smallImageName;
                    $imageObj->resize(300,255)->save($smallImage);
                }
            }

            $response = $this->_repository->create($input);
            if($response == true) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Project has been created successfully.']]];
                
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating project.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will update an existing project
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function update(Request $request){

        $input = $request->only('id','name','description', 'type_id','shape_id', 'material_ids','uses_ids', 'client_id', 'images', 'uses', 'size', 'start_date', 'end_date', 'testimonial', 'recent_description', 'client_name', 'company_name', 'company_logo');

        $rules = [  'id'                    =>  'required|exists:projects,id',
                    'name'                  =>  'required',
                    'description'           =>  'required',
                    'type_id'               =>  'required|exists:types,id',
                    'shape_id'              =>  'required|exists:shapes,id',
                    'material_ids'          =>  'required|array',
                    'uses_ids'              =>  'required|array',
                    'client_id'             =>  'required|exists:clients,id',
                    'images'                =>  'required|array',
                    //'uses'                  =>  'required',
                    'size'                  =>  'required',
                    'start_date'            =>  'required',
                    'end_date'              =>  'required'
                ];
        $messages = [
                'id.required'               => 'Please enter project id.',
                'id.exists'                 => 'Project not found.',
                'name.required'             => 'Please enter name.',
                'description.required'      => 'Please enter description.',
                'type_id.required'          => 'Please select type.',
                'type_id.exists'            => 'Type not found.',
                'shape_id.required'         => 'Please select shape.',
                'shape_id.exists'           => 'Shape not found.',
                'material_ids.required'     => 'Please select materials.',
                'material_ids.array'        => 'Materials can only be array.',
                'client_id.required'        => 'Please select client.',
                'client_id.exists'          => 'Client not found.',
                'images.required'           => 'Please select image.',
                'images.array'              => 'Images can only be array.',
                'uses.required'             => 'Please enter uses.',
                'size.required'             => 'Please enter size.',
                'start_date.required'       => 'Please select start date.',
                'end_date.required'         => 'Please select end date.',

        ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $project = $this->_repository->findById($input['id']);
            if ($project != NULL) {
                if (count($input['images']) > 0) {
                    $preImages = [];
                    if (count($project->images) > 0) {
                        foreach ($project->images as $key => $image) {
                            $preImages[] = $image->image;
                        }
                    }
                    foreach ($input['images'] as $key => $image) {

                        if (!in_array($image['image'], $preImages)) {
                            $localFile  = config('app.files.project.full_path').$image['image'];
                            $imageObj = Image::make($localFile);

                            $imageParts = explode('.', $image['image']);
                            $imageName = $imageParts[0];
                            $imageExtension = File::extension($localFile);
                            
                            // large image
                            $largeImageName = $imageName.'.'.$imageExtension;
                            $largeImage = config('app.files.project.full_path').$largeImageName;
                            $imageObj->resize(600,450)->save($largeImage);

                            // medium image
                            $mediumImageName = $imageName.'-medium.'.$imageExtension;
                            $mediumImage = config('app.files.project.full_path').$mediumImageName;
                            $imageObj->resize(480,260)->save($mediumImage);

                            // small image
                            $smallImageName = $imageName.'-small.'.$imageExtension;
                            $smallImage = config('app.files.project.full_path').$smallImageName;
                            $imageObj->resize(300,255)->save($smallImage);
                        } 
                    }
                }

                $response = $this->_repository->update($input);
                if($response) {
                    $code = 200;
                    $output = ['success'=>['code'=>$code,'messages'=>['Project has been updated successfully.']]];
                } else {
                    $code = 406;
                    $output = ['error'=>['code'=>$code,'messages'=>['An error occured while updating project.']]];
                }

            } else {
                $code = 404;
                $output = ['error'=>['code'=>$code,'messages'=>['Project not found.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will change project status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus(Request $request) {

        // input parameters
        $input = $request->only('id', 'status');

        // define validation rules
        $rules = ['id'      => 'required | exists:projects,id',
                  'status'  => 'required |in:1,0',
                ];

        $messages = [
                'id.required'           => 'Please enter project id.',
                'id.exists'             => 'Project not found.',
                'status.required'       => 'Please enter status.',
                'status.in'             => 'Status can only be 1 or 0.'
        ];

        $validator = Validator::make($input,$rules, $messages);

        if($validator->fails()){
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $response = $this->_repository->updateStatus($input);
            if ($response) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Status has been updated successfully.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while updating status.']]];
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will delete an existing project
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function remove(Request $request){

        $input = $request->only('id');

        $rules = ['id' => 'required|exists:projects,id'];

        $messages = ['id.required'      => 'Please enter project id.',
                    'id.exists'         => 'Project not found.'
                    ];

        $validator = Validator::make($input,$rules,$messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code'=>$code,'messages'=>[$validator->messages()->first()]]];
        
        // if validation passes
        } else {
            $response = $this->_repository->deleteById($input['id']);

            if($response == true) {   
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Project has been deleted successfully.']]];
            } else {
                $code = 405;
                $output = ['error' => ['code'=>$code,'messages'=>['An error occured while deleting this project.']]];
            } 
        }
        
        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch data of individual project
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function view(Request $request){
        
        $input = $request->only('id');

        $rules = [
                    'id'=>'required|exists:projects,id'
                ];
        $messages = [
                'id.required'   => 'Please enter project id.',
                'id.exists'     => 'Project not found.'
                ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $code = 200;
            $response = $this->_repository->findById($input['id']);

            if ($response == NULL || $response == false) {
                $code = 404;
                $output = ['error' => ['code'=>$code,'messages'=>['Material not found.']]];
            } else {
                $output = $response;
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch list of all projects
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function all(Request $request) {

        $input = $request->only('pagination','keyword','limit','filter_by_status', 'filter_by_type', 'filter_by_material','filter_by_client', 'from');

        $rules = ['pagination' => 'required'
                    ];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];

        // if validation passes
        } else {
            $code = 200;
            $pagination = false;
            if($input['pagination']) {
                $pagination = true;
            }

            $output = $this->_repository->findByAll($pagination, self::PER_PAGE, $input);
        }
        return response()->json($output, $code);
    } 

    /**
     *
     * This method will fetch list of all merged products
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function allProducts(Request $request) {

        $input = $request->only('pagination','keyword','limit', 'filter_by_product', 'from');

        $rules = ['pagination' => 'required'
                    ];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];

        // if validation passes
        } else {
            $code = 200;
            $pagination = false;
            if($input['pagination']) {
                $pagination = true;
            }

            $output = $this->_repository->allProducts($pagination, self::PER_PAGE, $input);
        }
        return response()->json($output, $code);
    }
}
