<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Data\Repositories\TeamRepository;
use Illuminate\Http\Request;

use \Validator, \Session, Carbon\Carbon;;

class TeamController extends Controller {

    const PER_PAGE = 10;
    /**
     *
     * This will hold the instance of TeamRepository class which is used for
     * fetching, modifying, creating and removing data from database.
     *
     * @var object
     * @access private
     *
     **/
	private $_repository;

	public function __construct() {
		$this->_repository = app()->make('TeamRepository');
	}   

    /**
     *
     * This method will create a new team
     * and will return output back to team as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function create(Request $request){

        $input = $request->only('name','designation', 'description', 'email', 'linked_in', 'image');

        $rules = [
                    'name'          =>  'required',
                    'designation'   =>  'required',
                    /*'description'   =>  'required',
                    'email'         =>  'email',
                    'linked_in'     =>  '',
                    'image'         =>  'required',*/
                ];
        $messages = [
                'name.required'             => 'Please enter name.',
                'designation.required'      => 'Please enter designation.',
                'description.required'      => 'Please enter description.',
                'email.required'            => 'Please enter email.',
                'email.email'               => 'Please enter valid email address.',
                'linked_in.required'        => 'Please enter linkedin address.',
                'image.required'            => 'Please select image.'
        ];

        $validator = Validator::make( $input, $rules, $messages);

        // if validation fails
        if ( $validator->fails() ) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->create($input);
            if($response == true) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Team member has been created successfully.']]];
                
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating team member.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will update an existing team
     * and will return output back to team as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function update(Request $request){

        $input = $request->only('id', 'name','designation', 'description', 'email', 'linked_in', 'image');

        $rules = [
                    'id'            =>  'required|exists:team_members,id',
                    'name'          =>  'required',
                    'designation'   =>  'required',
                    /*'description'   =>  'required',
                    'email'         =>  'email',
                    'linked_in'     =>  '',
                    'image'         =>  'required',*/
                ];
        $messages = [
                'id.required'               => 'Please enter team member id.',
                'id.exists'                 => 'Team member not found.',
                'name.required'             => 'Please enter name.',
                'designation.required'      => 'Please enter designation.',
                'description.required'      => 'Please enter description.',
                'email.required'            => 'Please enter email.',
                'email.email'               => 'Please enter valid email address.',
                'linked_in.required'        => 'Please enter linkedin address.',
                'image.required'            => 'Please select image.'
        ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->update($input);
            if($response) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Team member has been updated successfully.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while updating team member.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will change team status (active, inactive)
     * and will return output back to team as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus(Request $request) {

        // input parameters
        $input = $request->only('id', 'status');

        // define validation rules
        $rules = ['id'      => 'required | exists:team_members,id',
                  'status'  => 'required |in:1,0',
                ];

        $messages = [
                'id.required'           => 'Please enter team member id.',
                'id.exists'             => 'Team member not found.',
                'status.required'       => 'Please enter status.',
                'status.in'             => 'Status can only be 1 or 0.'
        ];

        $validator = Validator::make($input,$rules, $messages);

        if($validator->fails()){
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $response = $this->_repository->updateStatus($input);
            if ($response) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Status has been updated successfully.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while updating status.']]];
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will delete an existing team member
     * and will return output back to team as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function remove(Request $request){

        $input = $request->only('id');

        $rules = ['id' => 'required|exists:team_members,id'];

        $messages = ['id.required'      => 'Please enter team member id.',
                    'id.exists'         => 'Team member not found.'
                    ];

        $validator = Validator::make($input,$rules,$messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code'=>$code,'messages'=>[$validator->messages()->first()]]];
        
        // if validation passes
        } else {
            $response = $this->_repository->deleteById($input['id']);

            if($response) {   
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Team member has been deleted successfully.']]];
            } else {
                $code = 405;
                $output = ['error' => ['code'=>$code,'messages'=>['An error occured while deleting this team member.']]];
            } 
        }
        
        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch data of individual team
     * and will return output back to team as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function view(Request $request){
        $user = Auth::user();
        
        $input = $request->only('id');

        $rules = [
                    'id'=>'required|exists:team_members,id'
                ];
        $messages = [
                'id.required'   => 'Please enter team member id.',
                'id.exists'     => 'Team member not found.'
                ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $code = 200;
            $response = $this->_repository->findById($input['id']);

            if ($response == NULL || $response == false) {
                $code = 404;
                $output = ['error' => ['code'=>$code,'messages'=>['Manufacturer not found.']]];
            } else {
                $output = $response;
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch list of all teams
     * and will return output back to team as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function all(Request $request) {

        $input = $request->only('pagination','keyword','limit','filter_by_status');

        $rules = ['pagination' => 'required'
                    ];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];

        // if validation passes
        } else {
            $code = 200;
            $pagination = false;
            if($input['pagination']) {
                $pagination = true;
            }

            $output = $this->_repository->findByAll($pagination, self::PER_PAGE, $input);
        }
        return response()->json($output, $code);
    } 
}
