<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Data\Repositories\TypeRepository;
use Illuminate\Http\Request;

use \Validator, \Session, Carbon\Carbon;;

class TypeController extends Controller {

    const PER_PAGE = 10;
    /**
     *
     * This will hold the instance of TypeRepository class which is used for
     * fetching, modifying, creating and removing data from database.
     *
     * @var object
     * @access private
     *
     **/
	private $_repository;

	public function __construct() {
		$this->_repository = app()->make('TypeRepository');
	}   

    /**
     *
     * This method will create a new type
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function create(Request $request){

        $input = $request->only('name','description', 'type');

        $rules = [
                    'name'          =>  'required',
                    'description'          =>  'required',
                    /*'type'          =>  'required|in:project,product,both'*/
                ];
        $messages = [
                'name.required'     => 'Please enter name.',
                'description.required'     => 'Please enter description.',
                'type.required'     => 'Please enter type.',
                'type.in'           => 'Type can only be project, product or both.'

        ];

        $validator = Validator::make( $input, $rules, $messages);

        // if validation fails
        if ( $validator->fails() ) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->create($input);
            if($response == true) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Type has been created successfully.']]];
                
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating type.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will update an existing type
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function update(Request $request){

        $input = $request->only('id', 'name', 'description', 'type');

        $rules = [
                    'id'        =>  'required|exists:types,id',
                    'name'      =>  'required',
                    'description'      =>  'required',
                    /*'type'      =>  'required|in:project,product,both'*/
                ];
        $messages = [
                'id.required'       => 'Please enter type id.',
                'id.exists'         => 'Type not found.',
                'name.required'       => 'Please enter name.',
                'description.required'       => 'Please enter description.',
                'type.required'     => 'Please enter type.',
                'type.in'           => 'Type can only be project, product or both.'
        ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>[$validator->messages()->first()]]];

        } else {
            $response = $this->_repository->update($input);
            if($response) {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Type has been updated successfully.']]];
            } else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occured while updating type.']]];
            }
        }
        return response()->json($output, $code);
    }

    /**
     *
     * This method will change type status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus(Request $request) {

        // input parameters
        $input = $request->only('id', 'status');

        // define validation rules
        $rules = ['id'      => 'required | exists:types,id',
                  'status'  => 'required |in:1,0',
                ];

        $messages = [
                'id.required'           => 'Please enter type id.',
                'id.exists'             => 'Type not found.',
                'status.required'       => 'Please enter status.',
                'status.in'             => 'Status can only be 1 or 0.'
        ];

        $validator = Validator::make($input,$rules, $messages);

        if($validator->fails()){
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $response = $this->_repository->updateStatus($input);
            if ($response == 'success') {
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Status has been updated successfully.']]];
            } else if ($response == 'cannot_delete') {
                $code = 401;
                $output = ['error' => ['code'=>$code,'messages'=>['Sorry you cannot remove this type as it is associated with some project.']]];
            }  else {
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while updating status.']]];
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will delete an existing type
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function remove(Request $request){

        $input = $request->only('id');

        $rules = ['id' => 'required|exists:types,id'];

        $messages = ['id.required'      => 'Please enter type id.',
                    'id.exists'         => 'Type not found.'
                    ];

        $validator = Validator::make($input,$rules,$messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code'=>$code,'messages'=>[$validator->messages()->first()]]];
        
        // if validation passes
        } else {
            $response = $this->_repository->deleteById($input['id']);

            if($response == 'success') {   
                $code = 200;
                $output = ['success'=>['code'=>$code,'messages'=>['Type has been deleted successfully.']]];
            } else if ($response == 'cannot_delete') {
                $code = 401;
                $output = ['error' => ['code'=>$code,'messages'=>['Sorry you cannot remove this type as it is associated with some project.']]];
            } else {
                $code = 405;
                $output = ['error' => ['code'=>$code,'messages'=>['An error occured while deleting this type.']]];
            } 
        }
        
        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch data of individual type
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function view(Request $request){
        $user = Auth::user();
        dd($user);die;
        $input = $request->only('id');

        $rules = [
                    'id'=>'required|exists:types,id'
                ];
        $messages = [
                'id.required'   => 'Please enter type id.',
                'id.exists'     => 'Type not found.'
                ];

        $validator = Validator::make( $input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];
        } else {
            $code = 200;
            $response = $this->_repository->findById($input['id']);

            if ($response == NULL || $response == false) {
                $code = 404;
                $output = ['error' => ['code'=>$code,'messages'=>['Type not found.']]];
            } else {
                $output = $response;
            }
        }

        return response()->json($output, $code);
    }

    /**
     *
     * This method will fetch list of all types
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function all(Request $request) {

        $input = $request->only('pagination','keyword','limit','filter_by_status', 'filter_by_type', 'from');

        $rules = ['pagination' => 'required'
                    ];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => [$validator->messages()->first()]]];

        // if validation passes
        } else {
            $code = 200;
            $pagination = false;
            if($input['pagination']) {
                $pagination = true;
            }

            $output = $this->_repository->findByAll($pagination, self::PER_PAGE, $input);
        }
        return response()->json($output, $code);
    } 
}
