<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Dealer;
use App\Models\Project;
use App\Models\ProjectMaterial;
use App\Models\ProjectClient;
use App\Models\ProjectUse;
use App\Models\ProjectShape;
use App\Models\ProjectType;
use App\Models\Type;
use App\Models\Setting;
use App\Models\TeamMember;
use App\Models\ContactMember;
use App\Models\HomeImage;
use App\Models\Partner;
use App\Models\ProcessModel;
use App\Models\SponsorshipImage;
use App\Models\CertificationImage;
use \Validator, \Session;
class HomeController extends Controller {

    public $dealer_model;
    public $team_model;
    public $dealers;
    public $project_model;
    public $project_repo;
    public $recent_projects;
    public $type_model;
    public $project_type_model;
    public $project_material_model;
    public $project_shape_model;
    public $project_use_model;
    public $project_client_model;
    public $contact_model;
    public $home_image_model;
    public $partner_model;
    public $process_model;

    public $type_repo;
    public $shape_repo;
    public $material_repo;
    public $uses_repo;
    public $setting_repo;

    public function __construct(Dealer $dealer, Project $project, Type $type, ProjectMaterial $projectMaterial, ProjectType $projectType , ProjectShape $projectShape, ProjectUse $projectUse, ProjectClient $projectClient, TeamMember $team, ContactMember $contactMember, HomeImage $homeImage, Partner $partner, ProcessModel $process) {
        $this->dealer_model = $dealer;
        $this->project_model = $project;
        $this->type_model = $type;
        $this->project_type_model = $projectType;
        $this->project_material_model = $projectMaterial;
        $this->project_shape_model = $projectShape;
        $this->project_use_model = $projectUse;
        $this->project_client_model = $projectClient;
        $this->team_model = $team;
        $this->contact_model = $contactMember;
        $this->home_image_model = $homeImage;
        $this->partner_model = $partner;
        $this->process_model = $process;

        $this->project_repo = app()->make('ProjectRepository');
        $this->type_repo = app()->make('TypeRepository');
        $this->shape_repo = app()->make('ShapesRepository');
        $this->material_repo = app()->make('MaterialRepository');
        $this->uses_repo = app()->make('UsesRepository');
        $this->setting_repo = app()->make('SettingRepository');

        // to get dealers
        $this->dealers = $this->dealer_model->where('is_active', '=', 1)->limit(5)->get();
        // to get recent projects
        $getProjects = $this->project_model->where('is_active', '=', 1)->limit(10)->get(['id']);
        $tmpArray = [];
        if (count($getProjects) > 0) {
            $tmpArray = [];
            foreach ($getProjects as $key => $project) {
                $projectData = $this->project_repo->findById($project->id);
                if ($projectData != NULL) {
                    $tmpArray[] = $projectData;
                }
            }
        }
        $this->recent_projects = $tmpArray;
    }

 	public function showHome(Request $request) {
        $title = 'SpanMaster | Home';
        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        $slider_images = $this->home_image_model->limit(6)->get();
        $partners = $this->partner_model->where('is_active', '=', '1')->get();
        return view('front.home',['title'=>$title, 'dealers'=>$this->dealers, 'settings'=>$settings,'recent_projects'=>$this->recent_projects, 'slider_images'=>$slider_images, 'partners'=>$partners]);
    }

    public function showAboutUs(Request $request) {
        $title = 'SpanMaster | About Us';

        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        $team_members = $this->team_model->where('is_active', '=', '1')/*->limit(4)*/->get();
        $sponsorship_images = SponsorshipImage::get();
        $certification_images = CertificationImage::get();
        $partners = $this->partner_model->where('is_active', '=', '1')->get();
        return view('front.about.index',['title'=>$title, 'dealers'=>$this->dealers, 'settings'=>$settings, 'team_members'=>$team_members, 'sponsorship_images'=>$sponsorship_images, 'certification_images'=>$certification_images, 'partners'=>$partners]);
    }

    public function showProducts(Request $request) {
        $title = 'SpanMaster | Products';
        return view('front.products.index',['title'=>$title, 'dealers'=>$this->dealers]);
    }

    public function showProduct(Request $request, $type, $id) {
        $title = 'SpanMaster | Product Details';
        $selection = $type;
        $product = NULL;
        $product_projects = [];
        if ($selection == 'types') {
            if (isset($id) && $id > 0) {
                $product = $this->type_repo->findById($id);
                if ($product == NULL) {
                    return redirect('/admin/404');
                } else {
                    $productProjects = $this->project_type_model
                                            ->where('type_id', '=', $product->id)
                                            ->where(function ($query){
                                                    $query->whereIn('project_id', function($query){
                                                                $query->select('id')
                                                                    ->from(with($this->project_model)->getTable())
                                                                    ->where('is_active','=',1)
                                                                    ->whereNull('deleted_at');
                                                    });
                                            })->get(['project_id']);

                    if (count($productProjects) > 0) {
                        foreach ($productProjects as $key => $productProject) {
                            $projectData = $this->project_repo->findById($productProject->project_id, false, 'product');
                            if ($projectData != NULL) {
                                $product_projects[] = $projectData;
                            }
                        }
                    }
                }
            }
        } else if ($selection == 'shapes') {
            if (isset($id) && $id > 0) {
                $product = $this->shape_repo->findById($id);
                if ($product == NULL) {
                    return redirect('/admin/404');
                } else {
                    $productProjects = $this->project_shape_model
                                            ->where('shape_id', '=', $product->id)
                                            ->where(function ($query){
                                                    $query->whereIn('project_id', function($query) {
                                                                $query->select('id')
                                                                    ->from(with($this->project_model)->getTable())
                                                                    ->where('is_active','=',1)
                                                                    ->whereNull('deleted_at');
                                                    });
                                            })->get(['project_id']);
                    if (count($productProjects) > 0) {
                        foreach ($productProjects as $key => $productProject) {
                            $projectData = $this->project_repo->findById($productProject->project_id);
                            if ($projectData != NULL) {
                                $product_projects[] = $projectData;
                            }
                        }
                    }
                }
            }
        } else if ($selection == 'materials') {
            if (isset($id) && $id > 0) {
                $product = $this->material_repo->findById($id);
                if ($product == NULL) {
                    return redirect('/admin/404');
                } else {
                    $productProjects = $this->project_material_model
                                            ->where('material_id', '=', $product->id)
                                            ->where(function ($query){
                                                    $query->whereIn('project_id', function($query) {
                                                                $query->select('id')
                                                                    ->from(with($this->project_model)->getTable())
                                                                    ->where('is_active','=',1)
                                                                    ->whereNull('deleted_at');
                                                    });
                                            })->get(['project_id']);
                    if (count($productProjects) > 0) {
                        foreach ($productProjects as $key => $productProject) {
                            $projectData = $this->project_repo->findById($productProject->project_id);
                            if ($projectData != NULL) {
                                $product_projects[] = $projectData;
                            }
                        }
                    }
                }
            }
        } else if ($selection == 'uses') {
            if (isset($id) && $id > 0) {
                $product = $this->uses_repo->findById($id);
                if ($product == NULL) {
                    return redirect('/admin/404');
                } else {
                    $productProjects = $this->project_use_model
                                            ->where('use_id', '=', $product->id)
                                            ->where(function ($query){
                                                    $query->whereIn('project_id', function($query) {
                                                                $query->select('id')
                                                                    ->from(with($this->project_model)->getTable())
                                                                    ->where('is_active','=',1)
                                                                    ->whereNull('deleted_at');
                                                    });
                                            })->get(['project_id']);
                    if (count($productProjects) > 0) {
                        foreach ($productProjects as $key => $productProject) {
                            $projectData = $this->project_repo->findById($productProject->project_id);
                            if ($projectData != NULL) {
                                $product_projects[] = $projectData;
                            }
                        }
                    }
                }
            }
        }
        
        return view('front.products.product.index',['title'=>$title, 'dealers'=>$this->dealers,'product'=>$product, 'product_projects'=>$product_projects]);
    }

    public function showQuote(Request $request) {
        $title = 'SpanMaster | Request A Quote';
        return view('front.request-a-quote.index',['title'=>$title, 'dealers'=>$this->dealers]);
    }

    public function showProjects(Request $request) {
        $title = 'SpanMaster | Projects';
        $projectTypes = $this->type_model->where('is_active', '=', 1)->get();
        return view('front.projects.index',['title'=>$title, 'dealers'=>$this->dealers, 'types'=>$projectTypes]);
    }

    public function showProject(Request $request, $id) {
        $title = 'SpanMaster | Project Details';

        $project = NULL;
        if (isset($id) && $id > 0) {
            $project = $this->project_repo->findById($id);
            if ($project == NULL) {
                return redirect('/admin/404');
            }
        }

        return view('front.projects.project.index',['title'=>$title, 'dealers'=>$this->dealers, 'project'=>$project]);
    }

    public function showRepairs(Request $request) {
        $title = 'SpanMaster | Repairs & Maintenance';
        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        $partners = $this->partner_model->where('is_active', '=', '1')->get();
        $processes = $this->process_model->where('is_active', '=', '1')->get();
        return view('front.repairs-maintenance.index',['title'=>$title, 'dealers'=>$this->dealers, 'settings'=>$settings, 'partners'=>$partners, 'processes'=>$processes]);
    }

    public function showContact(Request $request) {
        $title = 'SpanMaster | Contact Us';
        $contacts = $this->contact_model->where('is_active', '=', '1')->limit(4)->get();
        $settings = $this->setting_repo->findById(Setting::GLOBAL_SETTING);
        return view('front.contact.index',['title'=>$title, 'dealers'=>$this->dealers, 'contacts'=>$contacts, 'settings'=>$settings]);
    }
}
