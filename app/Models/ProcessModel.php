<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcessModel extends Model
{
    use SoftDeletes;
    protected $table = 'processes';
}
