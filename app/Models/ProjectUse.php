<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectUse extends Model
{
    protected $table = 'project_uses';
}
