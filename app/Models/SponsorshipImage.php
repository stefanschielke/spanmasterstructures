<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SponsorshipImage extends Model
{
    protected $table = 'sponsorship_images';
}
