<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectShape extends Model
{
    protected $table = 'project_shapes';
}
