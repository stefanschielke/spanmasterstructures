<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\QuoteRepository;
use App\Models\Quote;
use App\Models\User;
class QuoteRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('QuoteRepository', function(){
            return new QuoteRepository(new Quote, new User);
        });
    }
}
