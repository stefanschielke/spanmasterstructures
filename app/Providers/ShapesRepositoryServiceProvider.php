<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\ShapesRepository;
use App\Models\Shape;
use App\Models\ProjectShape;
use App\Models\ProjectImage;
use App\Models\Project;
class ShapesRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ShapesRepository', function(){
            return new ShapesRepository(new Shape, new ProjectShape, new ProjectImage, new Project);
        });
    }
}
