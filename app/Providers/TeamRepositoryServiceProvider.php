<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\TeamRepository;
use App\Models\TeamMember;
class TeamRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('TeamRepository', function(){
            return new TeamRepository(new TeamMember);
        });
    }
}
