<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\ProcessRepository;
use App\Models\ProcessModel;
class ProcessRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ProcessRepository', function(){
            return new ProcessRepository(new ProcessModel);
        });
    }
}
