<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\MaterialRepository;
use App\Models\Material;
use App\Models\ProjectMaterial;
use App\Models\ProjectImage;
use App\Models\Project;
class MaterialRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('MaterialRepository', function(){
            return new MaterialRepository(new Material, new ProjectMaterial, new ProjectImage, new Project);
        });
    }
}
