<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\ContactRepository;
use App\Models\ContactMember;
class ContactRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ContactRepository', function(){
            return new ContactRepository(new ContactMember);
        });
    }
}
