<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\TypeRepository;
use App\Models\Type;
use App\Models\ProjectType;
use App\Models\ProjectImage;
use App\Models\Project;
class TypeRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('TypeRepository', function(){
            return new TypeRepository(new Type, new ProjectType, new ProjectImage, new Project);
        });
    }
}
