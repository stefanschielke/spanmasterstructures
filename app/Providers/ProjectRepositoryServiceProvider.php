<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\ProjectRepository;
use App\Models\Project;
use App\Models\Material;
use App\Models\Type;
use App\Models\Shape;
use App\Models\Usage;
use App\Models\ProjectType;
use App\Models\ProjectMaterial;
use App\Models\ProjectClient;
use App\Models\ProjectImage;
use App\Models\ProjectUse;
use App\Models\ProjectShape;
class ProjectRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ProjectRepository', function(){
            return new ProjectRepository(new Project,new Material, new Type, new ProjectType, new ProjectMaterial, new ProjectClient, new ProjectImage, new Usage, new ProjectUse, new Shape, new ProjectShape);
        });
    }
}
