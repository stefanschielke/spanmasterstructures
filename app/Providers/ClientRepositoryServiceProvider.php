<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\ClientRepository;
use App\Models\Client;
use App\Models\ProjectClient;
use App\Models\Project;
class ClientRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ClientRepository', function(){
            return new ClientRepository(new Client, new ProjectClient, new Project);
        });
    }
}
