<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\DealerRepository;
use App\Models\Dealer;
class DealerRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DealerRepository', function(){
            return new DealerRepository(new Dealer);
        });
    }
}
