<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\PartnerRepository;
use App\Models\Partner;
class PartnerRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PartnerRepository', function(){
            return new PartnerRepository(new Partner);
        });
    }
}
