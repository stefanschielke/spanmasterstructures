<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Material;
use App\Models\Type;
use App\Models\Usage;
use App\Models\Shape;
use App\Models\Project;
use App\Models\ProjectType;
use App\Models\ProjectMaterial;
use App\Models\ProjectClient;
use App\Models\ProjectImage;
use App\Models\ProjectUse;
use App\Models\ProjectShape;

use App\Helpers\Helper;

use \StdClass, Carbon\Carbon, \Session;

class ProjectRepository {

	public $project_model;
	public $material_model;
	public $type_model;
	public $use_model;
	public $shape_model;
	public $project_material_model;
	public $project_type_model;
	public $project_client_model;
	public $project_image_model;
	public $project_use_model;
	public $project_shape_model;

	public $type_repo;
	public $shape_repo;
	public $material_repo;
	public $uses_repo;
	protected $_cacheKey = 'project-'; 

	public function __construct(Project $project, Material $material, Type $type, ProjectType $projectType, ProjectMaterial $projectMaterial, ProjectClient $projectClient, ProjectImage $projectImage, Usage $use, ProjectUse $projectUse, Shape $shape, ProjectShape $projectShape){
		$this->project_model 			= $project;
		$this->material_model 			= $material;
		$this->type_model 				= $type;
		$this->project_material_model 	= $projectMaterial;
		$this->project_type_model 		= $projectType;
		$this->project_client_model 	= $projectClient;
		$this->project_image_model 		= $projectImage;
		$this->use_model 				= $use;
		$this->project_use_model 		= $projectUse;
		$this->shape_model 				= $shape;
		$this->project_shape_model 		= $projectShape;

		$this->type_repo = app()->make('TypeRepository');
		$this->shape_repo = app()->make('ShapesRepository');
		$this->material_repo = app()->make('MaterialRepository');
		$this->uses_repo = app()->make('UsesRepository');
	}

	 /**
	 *
	 * This method will fetch data of individual project
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false, $from = '') {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$project = $this->project_model->find($id);
			if ($project != NULL) {

				$data 						= new StdClass;
				$data->id 					= $project->id;
				$data->name 				= $project->name;
				$data->description 			= $project->description;
				$data->recent_description 	= $project->recent_description;
				$data->size 				= $project->size;
				$data->client_name 			= ($project->client_name == null)?'':$project->client_name;
				$data->company_name 		= ($project->company_name == null)?'':$project->company_name;
				$data->company_logo 		= ($project->company_logo == null)?'':$project->company_logo;
				$data->is_active 			= $project->is_active;
				$data->start_date_formatted	= ($project->start_date != '')?date('M, Y', strtotime($project->start_date)):'';
				$data->end_date_formatted	= ($project->end_date != '')?date('M, Y', strtotime($project->end_date)):'';
				$data->start_date			= date('Y-m', strtotime($project->start_date));
				$data->end_date				= date('Y-m', strtotime($project->end_date));
				$data->created_at			= date('d M, Y', strtotime($project->created_at));
				$data->updated_at			= date('d M, Y', strtotime($project->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		// to get project type
		$project_type = $this->project_type_model
								->leftJoin('types',function($joins){
									$joins->on('types.id','=','project_types.type_id');
								})
								->where('project_id','=',$data->id)
								->first(['types.id','types.name','types.description']);
								
		$data->type = new StdClass;
		if ($project_type != NULL) {
			$data->type->id = $project_type->id;
			$data->type->name = $project_type->name;
			$data->type->description = $project_type->description;
		}

		// to get project shape
		$project_shape = $this->project_shape_model
								->leftJoin('shapes',function($joins){
									$joins->on('shapes.id','=','project_shapes.shape_id');
								})
								->where('project_id','=',$data->id)
								->first(['shapes.id','shapes.name','shapes.description']);
								
		$data->shape = new StdClass;
		if ($project_shape != NULL) {
			$data->shape->id = $project_shape->id;
			$data->shape->name = $project_shape->name;
			$data->shape->description = $project_shape->description;
		}

		// to get project client
		$project_client = $this->project_client_model
								->leftJoin('clients',function($joins){
									$joins->on('clients.id','=','project_clients.client_id');
								})
								->where('project_id','=',$data->id)
								->first(['clients.id','clients.name','clients.designation','project_clients.testimonial']);
								
		$data->client = new StdClass;
		if ($project_client != NULL) {
			$data->client->id = $project_client->id;
			$data->client->name = $project_client->name;
			$data->client->designation = $project_client->designation;
			$data->client->testimonial = $project_client->testimonial;
		}

		// to get project materials
		$project_materials = $this->project_material_model
								->leftJoin('materials',function($joins){
									$joins->on('materials.id','=','project_materials.material_id');
								})
								->where('project_id','=',$data->id)
								->get(['materials.id','materials.name','materials.description']);
								
		$data->materials = [];
		if (count($project_materials) > 0) {
			foreach ($project_materials as $key => $project_material) {
				$material = new StdClass;
				$material->id = $project_material->id;
				$material->name = $project_material->name;
				$material->description = $project_material->description;
				$data->materials[] = $material;
			}
		}

		// to get project uses
		$project_uses = $this->project_use_model
								->leftJoin('uses',function($joins){
									$joins->on('uses.id','=','project_uses.use_id');
								})
								->where('project_id','=',$data->id)
								->get(['uses.id','uses.name','uses.description']);
								
		$data->usages = [];
		if (count($project_uses) > 0) {
			foreach ($project_uses as $key => $project_use) {
				$use = new StdClass;
				$use->id = $project_use->id;
				$use->name = $project_use->name;
				$use->description = $project_use->description;
				$data->usages[] = $use;
			}
		}

		// to get project images
		if ($from == 'product') {
			$project_images = $this->project_image_model
								->where('project_id','=',$data->id)
								->take(2)->orderBy('id', 'ASC')->get(['id','image', 'title','real_name']);
		} else {
			$project_images = $this->project_image_model
								->where('project_id','=',$data->id)
								->get(['id','image', 'title','real_name']);
		}
		
								
		$data->images = [];
		if (count($project_images) > 0) {
			foreach ($project_images as $key => $project_image) {
				$extension = \File::extension($project_image->image);
                $mediumImage =  str_replace('.'.$extension,'-medium.'.$extension,$project_image->image);
                $smallImage =  str_replace('.'.$extension,'-small.'.$extension,$project_image->image);


				$image = new StdClass;
				$image->id = $project_image->id;
				$image->image = $project_image->image;
				$image->medium_image = $mediumImage;
				$image->small_image = $smallImage;
				$image->title = $project_image->title;
				if ($project_image->real_name == null && $project_image->real_name == '') {
					$image->real_image = $project_image->image;
                } else {
                	$image->real_image = $project_image->real_name;
                }

				$data->images[] = $image;
			}
		}


		return $data;

	}

	/**
	 *
	 * This method will create a new project
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {

		$project 					= $this->project_model;
		$project->name				= $input['name']; 

		if (isset($input['description']) && $input['description'] != '') {
			$project->description  			= $input['description'];
		} else {
			$project->description = '';
		}
		if (isset($input['recent_description']) && $input['recent_description'] != '') {
			$project->recent_description  			= $input['recent_description'];
		} else {
			$project->recent_description = '';
		}
		$project->size  			= $input['size'];
		if (isset($input['client_name']) && $input['client_name'] != '') {
			$project->client_name  			= $input['client_name'];
		} else {
			$project->client_name = '';
		}
		if (isset($input['company_name']) && $input['company_name'] != '') {
			$project->company_name  			= $input['company_name'];
		} else {
			$project->company_name = '';
		}
		if (isset($input['company_logo']) && $input['company_logo'] != '') {
			$project->company_logo  			= $input['company_logo'];
		} else {
			$project->company_logo = '';
		}
		//$project->uses  			= $input['uses'];
		$project->start_date  		= date('Y-m-1', strtotime($input['start_date']));
		$project->end_date  		= date('Y-m-1', strtotime($input['end_date']));
		$project->is_active   		= 1;
		$project->created_by  		= Auth::user()->id;
		if($project->save()) {	

			// to save project type
			$project_type = new $this->project_type_model;
			$project_type->project_id = $project->id;
			$project_type->type_id = $input['type_id'];
			if ($project_type->save()) {

				// to save project client
				$project_client = new $this->project_client_model;
				$project_client->project_id = $project->id;
				$project_client->client_id = $input['client_id'];
				if (isset($input['testimonial']) && $input['testimonial'] != '') {
					$project_client->testimonial = $input['testimonial'];
				} else {
					$project_client->testimonial = '';
				}
				if ($project_client->save()) {

					// to save project shape
					$project_shape = new $this->project_shape_model;
					$project_shape->project_id = $project->id;
					$project_shape->shape_id = $input['shape_id'];
					if ($project_shape->save()) {

						// to save project materials
						$inputMaterialIds = [];
						if (isset($input['material_ids']) && count($input['material_ids']) > 0) {
							foreach ($input['material_ids'] as $key => $materialId) {
								$inputMaterialIds[] = $materialId;
								$project_material = new $this->project_material_model;
								$project_material->project_id = $project->id;
								$project_material->material_id = $materialId;
								$project_material->save();
							}
						}
						
						// to remove previously saved materials
						$preProjectMaterials = $this->project_material_model->where('project_id', '=', $project->id)->get();
						if (count($preProjectMaterials) > 0) {
							foreach ($preProjectMaterials as $key => $preProjectMaterial) {
								if (!in_array($preProjectMaterial->material_id, $inputMaterialIds)) {
									$this->project_material_model->where('project_id', '=', $project->id)->where('material_id', '=', $preProjectMaterial->material_id)->delete();
								}
							}
						}

						// to save project uses
						$inputUseIds = [];
						if (isset($input['uses_ids']) && count($input['uses_ids']) > 0) {
							foreach ($input['uses_ids'] as $key => $usageId) {
								$inputUseIds[] = $usageId;
								$project_use = new $this->project_use_model;
								$project_use->project_id = $project->id;
								$project_use->use_id = $usageId;
								$project_use->save();
							}
						}
						
						// to remove previously saved uses
						$preProjectUses = $this->project_use_model->where('project_id', '=', $project->id)->get();
						if (count($preProjectUses) > 0) {
							foreach ($preProjectUses as $key => $preProjectUse) {
								if (!in_array($preProjectUse->use_id, $inputUseIds)) {
									$this->project_use_model->where('project_id', '=', $project->id)->where('use_id', '=', $preProjectUse->use_id)->delete();
								}
							}
						}

						// to save project images
						$inputImages = [];
						if (isset($input['images']) && count($input['images']) > 0) {
							foreach ($input['images'] as $key => $image) {
								$inputImages[] = $image['image'];
								$project_image = new $this->project_image_model;
								$project_image->project_id = $project->id;
								$project_image->image = $image['image'];
								$project_image->real_name = $image['real_image'];
								$project_image->title = $image['title'];
								$project_image->save();
							}
						}
						
						// to remove previously saved images
						$preProjectImages = $this->project_image_model->where('project_id', '=', $project->id)->get();
						if (count($preProjectImages) > 0) {
							foreach ($preProjectImages as $key => $preProjectImage) {
								if (!in_array($preProjectImage->image, $inputImages)) {
									$this->project_image_model->where('project_id', '=', $project->id)->where('image', '=', $preProjectImage->image)->delete();
								}
							}
						}

					} else {
						$project->delete();
						$project_type->delete();
						$project_client->delete();
					}

				} else {
					$project->delete();
					$project_type->delete();
				}
				
				return true;
			} else {
				$project->delete();
			}
			
		} else {
			return false;
		}
	}

	 /**
	 *
	 * This method will update existing project
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$project = $this->project_model->find($input['id']);
		if ($project != NULL) {

			if (isset($input['name']) && $input['name'] != '') {
				$project->name = $input['name'];
			}

			if (isset($input['description']) && $input['description'] != '') {
				$project->description = $input['description'];
			}

			if (isset($input['recent_description']) && $input['recent_description'] != '') {
				$project->recent_description  			= $input['recent_description'];
			} else {
				$project->recent_description = '';
			}
			if (isset($input['client_name']) && $input['client_name'] != '') {
				$project->client_name  			= $input['client_name'];
			} else {
				$project->client_name = '';
			}
			if (isset($input['company_name']) && $input['company_name'] != '') {
				$project->company_name  			= $input['company_name'];
			} else {
				$project->company_name = '';
			}
			if (isset($input['company_logo']) && $input['company_logo'] != '') {
				$project->company_logo  			= $input['company_logo'];
			} else {
				$project->company_logo = '';
			}
		
			if (isset($input['size']) && $input['size'] != '') {
				$project->size = $input['size'];
			}

			if (isset($input['uses']) && $input['uses'] != '') {
				$project->uses = $input['uses'];
			}

			if (isset($input['start_date']) && $input['start_date'] != '') {
				$project->start_date = date('Y-m-1', strtotime($input['start_date']));
			}

			if (isset($input['end_date']) && $input['end_date'] != '') {
				$project->end_date = date('Y-m-1', strtotime($input['end_date']));
			}

			$project->updated_at   = Carbon::now();
			if($project->save()) {	

				// to update project type
				$check_project_type = $this->project_type_model->where('project_id', '=', $project->id)->first();
				if ($check_project_type == NULL) {
					$project_type = new $this->project_type_model;
					$project_type->project_id = $project->id;
					$project_type->type_id = $input['type_id'];
				} else{
					$project_type = $check_project_type;
					$project_type->type_id = $input['type_id'];
				}
				
				if ($project_type->save()) {

					// to update project client
					$check_project_client = $this->project_client_model->where('project_id', '=', $project->id)->first();
					if ($check_project_client == NULL){
						$project_client = new $this->project_client_model;
						$project_client->project_id = $project->id;
						$project_client->client_id = $input['client_id'];
					} else {
						$project_client = $check_project_client;
						$project_client->client_id = $input['client_id'];
					}
					
					if (isset($input['testimonial']) && $input['testimonial'] != '') {
						$project_client->testimonial = $input['testimonial'];
					} else {
						$project_client->testimonial = '';
					}

					if ($project_client->save()) {

						// to update project shape
						$check_project_shape = $this->project_shape_model->where('project_id', '=', $project->id)->first();
						if ($check_project_shape == NULL){
							$project_shape = new $this->project_shape_model;
							$project_shape->project_id = $project->id;
							$project_shape->shape_id = $input['shape_id'];
						} else {
							$project_shape = $check_project_shape;
							$project_shape->shape_id = $input['shape_id'];
						}

						if ($project_shape->save()) {

							// to update project materials
							$inputMaterialIds = [];
							if (isset($input['material_ids']) && count($input['material_ids']) > 0) {
								foreach ($input['material_ids'] as $key => $materialId) {
									$inputMaterialIds[] = $materialId;
									$check_project_material = $this->project_material_model
																	->where('project_id', '=', $project->id)
																	->where('material_id', '=', $materialId)
																	->first();
									if ($check_project_material == NULL) {
										$project_material = new $this->project_material_model;
										$project_material->project_id = $project->id;
										$project_material->material_id = $materialId;
										$project_material->save();
									}
								}
							}
							
							// to remove previously saved materials
							$preProjectMaterials = $this->project_material_model->where('project_id', '=', $project->id)->get();
							if (count($preProjectMaterials) > 0) {
								foreach ($preProjectMaterials as $key => $preProjectMaterial) {
									if (!in_array($preProjectMaterial->material_id, $inputMaterialIds)) {
										$this->project_material_model->where('project_id', '=', $project->id)->where('material_id', '=', $preProjectMaterial->material_id)->delete();
									}
								}
							}	

							// to save project uses
							$inputUseIds = [];
							if (isset($input['uses_ids']) && count($input['uses_ids']) > 0) {
								foreach ($input['uses_ids'] as $key => $usageId) {
									$inputUseIds[] = $usageId;
									$check_project_use = $this->project_use_model
																	->where('project_id', '=', $project->id)
																	->where('use_id', '=', $usageId)
																	->first();
									if ($check_project_use == NULL) {
										$project_use = new $this->project_use_model;
										$project_use->project_id = $project->id;
										$project_use->use_id = $usageId;
										$project_use->save();
									}
								}
							}
							
							// to remove previously saved uses
							$preProjectUses = $this->project_use_model->where('project_id', '=', $project->id)->get();
							if (count($preProjectUses) > 0) {
								foreach ($preProjectUses as $key => $preProjectUse) {
									if (!in_array($preProjectUse->use_id, $inputUseIds)) {
										$this->project_use_model->where('project_id', '=', $project->id)->where('use_id', '=', $preProjectUse->use_id)->delete();
									}
								}
							}

							// to update project images
							$this->project_image_model->where('project_id', '=', $project->id)->delete();
							$inputImages = [];
							if (isset($input['images']) && count($input['images']) > 0) {
								foreach ($input['images'] as $key => $image) {
									$project_image = new $this->project_image_model;
									$project_image->project_id = $project->id;
									$project_image->image = $image['image'];
									$project_image->real_name = $image['real_image'];
									$project_image->title = $image['title'];
									$project_image->save();
								}
							}

						} else {
							$project->delete();
							$project_type->delete();
							$project_client->delete();
						}
						
					} else {
						$project->delete();
						$project_type->delete();
					}
					
					Cache::forget($this->_cacheKey.$input['id']);
					return true;
				} else {
					$project->delete();
				}
				
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing project
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$project = $this->project_model->find($id);
		if ($project != NULL) {
			if ($project->delete()) {
				$this->project_client_model->where('project_id', '=', $id)->delete();
				$this->project_material_model->where('project_id', '=', $id)->delete();
				$this->project_use_model->where('project_id', '=', $id)->delete();
				$this->project_shape_model->where('project_id', '=', $id)->delete();
				$this->project_image_model->where('project_id', '=', $id)->delete();
				Cache::forget($this->_cacheKey.$id);
				return true;
			}
		} else {
			return false;
		}
	}

	 /**
     *
     * This method will change project status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$project = $this->project_model->find($input['id']);

		if ($project != NULL) {
			$project->is_active = $input['status'];
			$project->updated_at = Carbon::now();
			if ($project->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			}
		}
	}

	/**
	 *
	 * This method will fetch list of all projects
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$from = 'admin';
		if (isset($input['from']) && $input['from'] == 'site') {
			$from = 'site';
		} else {
			$from = 'admin';
		}

		if ($from == 'admin') {
			$objectIds = $this->project_model;
		} else if ($from == 'site') {
			$objectIds = $this->project_model->where('is_active', '=', 1);
		}
		$objectIds = $objectIds->orderBy('id', 'DESC');

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$objectIds = $objectIds->where('is_active','=',$input['filter_by_status']);
		}

		if (isset($input['filter_by_type']) && $input['filter_by_type'] != '0') {
			$objectIds = $objectIds->whereIn('id',  function($query) use ($input)
											   {
											    $query->select('project_id')
											        ->from(with($this->project_type_model)->getTable())
											        ->where('type_id','=',$input['filter_by_type']);
											   });
		}

		if (isset($input['filter_by_material']) && $input['filter_by_material'] != '0') {
			$objectIds = $objectIds->whereIn('id',  function($query) use ($input)
											   {
											    $query->select('project_id')
											        ->from(with($this->project_material_model)->getTable())
											        ->where('material_id','=',$input['filter_by_material']);
											   });
		}

		if (isset($input['filter_by_client']) && $input['filter_by_client'] != '0') {
			$objectIds = $objectIds->whereIn('id',  function($query) use ($input)
											   {
											    $query->select('project_id')
											        ->from(with($this->project_client_model)->getTable())
											        ->where('client_id','=',$input['filter_by_client']);
											   });
		}

		if (isset($input['keyword']) && $input['keyword'] != '') {
			$objectIds = $objectIds->where('name','LIKE','%'.$input['keyword'].'%');
		}
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$objectIdsPaginate = $objectIds->paginate($perPage, ['id']);
			$objects = $objectIdsPaginate->items();
			
		} else {
			$objects = $objectIds->get(['id']);
		}

		$data = ['data'=>[]];
		
		if (count($objects) > 0) {
			$i = 0;
			foreach ($objects as $object) {
				$objectData = $this->findById($object->id, false, false);
				$data['data'][$i] = $objectData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $objectIdsPaginate);
		}
		return $data;
	}

	/**
	 *
	 * This method will fetch list of all projects
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function allProducts($pagination = false, $perPage = 10, array $input = []) {

		$finalData = [];
		if (isset($input['filter_by_product']) && $input['filter_by_product'] == 'all') {
			$typeData = $this->type_repo->findByAll(false, 0,$input);
			$shapeData = $this->shape_repo->findByAll(false, 0,$input);
			$usesData = $this->uses_repo->findByAll(false, 0,$input);
			$materialData = $this->material_repo->findByAll(false, 0,$input);

			$finalData = array_merge_recursive($typeData['data'], $shapeData['data'], $usesData['data'], $materialData['data']);
		} else if (isset($input['filter_by_product']) && $input['filter_by_product'] == 'types') {
			$typeData = $this->type_repo->findByAll(false, 0,$input);
			$finalData = $typeData['data'];
		} else if (isset($input['filter_by_product']) && $input['filter_by_product'] == 'shapes') {
			$shapeData = $this->shape_repo->findByAll(false, 0,$input);
			$finalData = $shapeData['data'];
		} else if (isset($input['filter_by_product']) && $input['filter_by_product'] == 'uses') {
			$usesData = $this->uses_repo->findByAll(false, 0,$input);
			$finalData = $usesData['data'];
		} else if (isset($input['filter_by_product']) && $input['filter_by_product'] == 'materials') {
			$materialData = $this->material_repo->findByAll(false, 0,$input);
			$finalData = $materialData['data'];
		}
		return $finalData;
	}
}
