<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Type;
use App\Models\ProjectType;
use App\Models\ProjectImage;
use App\Models\Project;

use App\Helpers\Helper;

use \StdClass, Carbon\Carbon, \Session;

class TypeRepository {

	public $type_model;
	public $project_model;
	public $project_type_model;
	public $project_image_model;

	protected $_cacheKey = 'type-'; 

	public function __construct(Type $type, ProjectType $projectType, ProjectImage $projectImage, Project $project){
		$this->project_model 			= $project;
		$this->type_model 			= $type;
		$this->project_type_model 	= $projectType;
		$this->project_image_model 	= $projectImage;
	}

	 /**
	 *
	 * This method will fetch data of individual type
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$type = $this->type_model->find($id);
			if ($type != NULL) {

				$data 						= new StdClass;
				$data->id 					= $type->id;
				$data->name 				= $type->name;
				$data->description 			= $type->description;
				$data->is_active 			= $type->is_active;
				$data->type 	 			= 'types';
				$data->created_at			= date('d M, Y', strtotime($type->created_at));
				$data->updated_at			= date('d M, Y', strtotime($type->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		$associatedProjects = $this->project_type_model->where('type_id', '=', $data->id)
											->where(function ($query){
                                                    $query->whereIn('project_id', function($query){
                                                                $query->select('id')
                                                                    ->from(with($this->project_model)->getTable())
                                                                    ->where('is_active','=',1)
                                                                    ->whereNull('deleted_at');
                                                    });
                                            })
											->pluck('project_id')->toArray();

		$data->associated_projects = count($associatedProjects);
		
		$projectImages = $this->project_image_model->whereIn('project_id', $associatedProjects)
													->where('image', '!=', '')
													/*->where(function ($query){
                                                    $query->whereIn('project_id', function($query){
                                                                $query->select('id')
                                                                    ->from(with($this->project_model)->getTable())
                                                                    ->where('is_active','=',1)
                                                                    ->whereNull('deleted_at');
                                                    });
                                            })*/->inRandomOrder()->get(['title', 'image']);
		$pImages = [];
		if (count($projectImages) > 0) {
			$i = 0;
			foreach ($projectImages as $key => $projectImage) {
				$extension = \File::extension($projectImage->image);
                $smallImage =  str_replace('.'.$extension,'-small.'.$extension,$projectImage->image);

                $pImages[$i]['title'] = $projectImage->title;
                $pImages[$i]['small_image'] = $smallImage;
                $pImages[$i]['image'] = $projectImage->image;
                $i++;
			}
		}
		$data->project_image = $pImages;
		return $data;

	}

	/**
	 *
	 * This method will create a new type
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {

		$type 					= $this->type_model;
		$type->name				= $input['name']; 

		if (isset($input['description']) && $input['description'] != '') {
			$type->description  			= $input['description'];
		} else {
			$type->description = '';
		}
		$type->is_active   		= 1;
		$type->created_by  		= Auth::user()->id;
		if($type->save()) {	
			return true;
		} else {
			return false;
		}
	}

	 /**
	 *
	 * This method will update existing type
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$type = $this->type_model->find($input['id']);
		if ($type != NULL) {

			if (isset($input['name']) && $input['name'] != '') {
				$type->name = $input['name'];
			}

			if (isset($input['description']) && $input['description'] != '') {
				$type->description = $input['description'];
			}

			$type->updated_at   = Carbon::now();

			if($type->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing type
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$type = $this->type_model->find($id);
		if ($type != NULL) {
			if ($type->delete()) {
				// to check for associated projects
				$associatedProjects = $this->project_type_model->where('type_id', '=', $id)->count();
				if($associatedProjects > 0) {
					return 'cannot_delete';
				} else {
					Cache::forget($this->_cacheKey.$id);
					return 'success';
				}
			}
		} else {
			return false;
		}
	}

	 /**
     *
     * This method will change type status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$type = $this->type_model->find($input['id']);

		if ($type != NULL) {
			$type->is_active = $input['status'];
			$type->updated_at = Carbon::now();
			if ($type->save()) {
				if ($type->is_active == 0) {
					// to check for associated projects
					$associatedProjects = $this->project_type_model->where('type_id', '=', $input['id'])->count();
					if($associatedProjects > 0) {
						return 'cannot_delete';
					}
				}
				Cache::forget($this->_cacheKey.$input['id']);
				return 'success';
			}
		}
	}

	/**
	 *
	 * This method will fetch list of all types
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$from = 'admin';
		if (isset($input['from']) && $input['from'] == 'site') {
			$from = 'site';
		} else {
			$from = 'admin';
		}

		if ($from == 'admin') {
			$objectIds = $this->type_model;
		} else if ($from == 'site') {
			$objectIds = $this->type_model->where('is_active', '=', 1);
		}
		
		$objectIds = $objectIds->orderBy('id', 'DESC');

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$objectIds = $objectIds->where('is_active','=',$input['filter_by_status']);
		}
		
		if (isset($input['keyword']) && $input['keyword'] != '') {
			$objectIds = $objectIds->where('name','LIKE','%'.$input['keyword'].'%');
		}
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$objectIdsPaginate = $objectIds->paginate($perPage, ['id']);
			$objects = $objectIdsPaginate->items();
			
		} else {
			$objects = $objectIds->get(['id']);
		}

		$data = ['data'=>[]];
		
		if (count($objects) > 0) {
			$i = 0;
			foreach ($objects as $object) {
				$objectData = $this->findById($object->id, false, false);
				$data['data'][$i] = $objectData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $objectIdsPaginate);
		}
		return $data;
	}
}
