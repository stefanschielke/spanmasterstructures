<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use App\Models\ProjectClient;
use App\Models\Project;
use App\Helpers\Helper;

use \StdClass, Carbon\Carbon, \Session;

class ClientRepository {

	public $client_model;
	public $project_client_model;

	protected $_cacheKey = 'client-'; 

	public function __construct(Client $client, ProjectClient $projectClient, Project $project){
		$this->project_model 			= $project;
		$this->client_model 			= $client;
		$this->project_client_model 	= $projectClient;
	}

	 /**
	 *
	 * This method will fetch data of individual client
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$client = $this->client_model->find($id);
			if ($client != NULL) {

				$data 						= new StdClass;
				$data->id 					= $client->id;
				$data->name 				= $client->name;
				$data->designation 			= $client->designation;
				$data->is_active 			= $client->is_active;
				$data->created_at			= date('d M, Y', strtotime($client->created_at));
				$data->updated_at			= date('d M, Y', strtotime($client->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		$data->associated_projects = $this->project_client_model->where('client_id', '=', $data->id)->where(function ($query){
                                                    $query->whereIn('project_id', function($query){
                                                                $query->select('id')
                                                                    ->from(with($this->project_model)->getTable())
                                                                    ->where('is_active','=',1)
                                                                    ->whereNull('deleted_at');
                                                    });
                                            })->count();
		return $data;

	}

	/**
	 *
	 * This method will create a new client
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {

		$client 					= $this->client_model;
		$client->name				= $input['name']; 

		if (isset($input['designation']) && $input['designation'] != '') {
			$client->designation  			= $input['designation'];
		} else {
			$client->designation = '';
		}
		$client->is_active   		= 1;
		$client->created_by  		= Auth::user()->id;
		if($client->save()) {	
			return true;
		} else {
			return false;
		}
	}

	 /**
	 *
	 * This method will update existing client
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$client = $this->client_model->find($input['id']);
		if ($client != NULL) {

			if (isset($input['name']) && $input['name'] != '') {
				$client->name = $input['name'];
			}

			if (isset($input['designation']) && $input['designation'] != '') {
				$client->designation = $input['designation'];
			} else {
				$client->designation = '';
			}

			$client->updated_at   = Carbon::now();

			if($client->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing client
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$client = $this->client_model->find($id);
		if ($client != NULL) {
			if ($client->delete()) {
				// to check for associated projects
				$associatedProjects = $this->project_client_model->where('client_id', '=', $id)->count();
				if($associatedProjects > 0) {
					return 'cannot_delete';
				} else {
					Cache::forget($this->_cacheKey.$id);
					return 'success';
				}
			}
		} else {
			return false;
		}
	}

	 /**
     *
     * This method will change client status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$client = $this->client_model->find($input['id']);

		if ($client != NULL) {
			$client->is_active = $input['status'];
			$client->updated_at = Carbon::now();
			if ($client->save()) {

				if ($client->is_active == 0) {
					// to check for associated projects
					$associatedProjects = $this->project_client_model->where('client_id', '=', $input['id'])->count();
					if($associatedProjects > 0) {
						return 'cannot_delete';
					}
				}
				Cache::forget($this->_cacheKey.$input['id']);
				return 'success';
			}
		}
	}

	/**
	 *
	 * This method will fetch list of all clients
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$objectIds = $this->client_model->orderBy('id', 'DESC');

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$objectIds = $objectIds->where('is_active','=',$input['filter_by_status']);
		}

		if (isset($input['keyword']) && $input['keyword'] != '') {
			$objectIds = $objectIds->where('name','LIKE','%'.$input['keyword'].'%');
		}
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$objectIdsPaginate = $objectIds->paginate($perPage, ['id']);
			$objects = $objectIdsPaginate->items();
			
		} else {
			$objects = $objectIds->get(['id']);
		}

		$data = ['data'=>[]];
		
		if (count($objects) > 0) {
			$i = 0;
			foreach ($objects as $object) {
				$objectData = $this->findById($object->id, false, false);
				$data['data'][$i] = $objectData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $objectIdsPaginate);
		}
		return $data;
	}
}
