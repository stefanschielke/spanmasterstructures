<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\ContactMember;
use App\Helpers\Helper;

use \StdClass, Carbon\Carbon, \Session;

class ContactRepository {

	public $contact_model;

	protected $_cacheKey = 'contact-'; 

	public function __construct(ContactMember $contactMember){
		$this->contact_model 			= $contactMember;
	}

	 /**
	 *
	 * This method will fetch data of individual contact
	 * and will return output back to contact as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$contact = $this->contact_model->find($id);
			if ($contact != NULL) {

				$data 						= new StdClass;
				$data->id 					= $contact->id;
				$data->name 				= $contact->name;
				$data->designation 			= $contact->designation;
				$data->email 				= $contact->email;
				$data->is_active 			= $contact->is_active;
				$data->created_at			= date('d M, Y', strtotime($contact->created_at));
				$data->updated_at			= date('d M, Y', strtotime($contact->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		return $data;

	}

	/**
	 *
	 * This method will create a new contact
	 * and will return output back to contact as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {

		$contact 					= $this->contact_model;
		$contact->name				= $input['name']; 

		if (isset($input['designation']) && $input['designation'] != '') {
			$contact->designation  			= $input['designation'];
		} else {
			$contact->designation = '';
		}

		if (isset($input['email']) && $input['email'] != '') {
			$contact->email  			= $input['email'];
		} else {
			$contact->email = '';
		}

		$contact->is_active   		= 1;
		$contact->created_by  		= Auth::user()->id;
		if($contact->save()) {	
			return true;
		} else {
			return false;
		}
	}

	 /**
	 *
	 * This method will update existing contact
	 * and will return output back to contact as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$contact = $this->contact_model->find($input['id']);
		if ($contact != NULL) {

			if (isset($input['name']) && $input['name'] != '') {
				$contact->name = $input['name'];
			}

			if (isset($input['designation']) && $input['designation'] != '') {
				$contact->designation = $input['designation'];
			}

			if (isset($input['email']) && $input['email'] != '') {
				$contact->email = $input['email'];
			}

			$contact->updated_at   = Carbon::now();

			if($contact->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing contact
	 * and will return output back to contact as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$contact = $this->contact_model->find($id);
		if ($contact != NULL) {
			if ($contact->delete()) {
				Cache::forget($this->_cacheKey.$id);
				return true;
			}
		} else {
			return false;
		}
	}

	 /**
     *
     * This method will change contact status (active, inactive)
     * and will return output back to contact as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$contact = $this->contact_model->find($input['id']);

		if ($contact != NULL) {
			$contact->is_active = $input['status'];
			$contact->updated_at = Carbon::now();
			if ($contact->save()) {

				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			}
		}
	}

	/**
	 *
	 * This method will fetch list of all contacts
	 * and will return output back to contact as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$objectIds = $this->contact_model->orderBy('id', 'DESC');

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$objectIds = $objectIds->where('is_active','=',$input['filter_by_status']);
		}

		if (isset($input['keyword']) && $input['keyword'] != '') {
			$objectIds = $objectIds->where(function ($query) use ($input) {
					                $query->orWhere('name', 'LIKE', '%'.$input['keyword'].'%')
					                		->orWhere('email', 'LIKE', '%'.$input['keyword'].'%');
					            });
		}
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$objectIdsPaginate = $objectIds->paginate($perPage, ['id']);
			$objects = $objectIdsPaginate->items();
			
		} else {
			$objects = $objectIds->get(['id']);
		}

		$data = ['data'=>[]];
		
		if (count($objects) > 0) {
			$i = 0;
			foreach ($objects as $object) {
				$objectData = $this->findById($object->id, false, false);
				$data['data'][$i] = $objectData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $objectIdsPaginate);
		}
		return $data;
	}
}
