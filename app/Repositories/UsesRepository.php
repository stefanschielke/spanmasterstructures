<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Usage;
use App\Models\ProjectUse;
use App\Models\ProjectImage;
use App\Models\Project;
use App\Helpers\Helper;

use \StdClass, Carbon\Carbon, \Session;

class UsesRepository {

	public $use_model;
	public $project_use_model;
	public $project_image_model;

	protected $_cacheKey = 'use-'; 

	public function __construct(Usage $use, ProjectUse $projectUse, ProjectImage $projectImage, Project $project){
		$this->project_model 			= $project;
		$this->use_model 	= $use;
		$this->project_use_model 	= $projectUse;
		$this->project_image_model 	= $projectImage;
	}

	 /**
	 *
	 * This method will fetch data of individual use
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$use = $this->use_model->find($id);
			if ($use != NULL) {

				$data 						= new StdClass;
				$data->id 					= $use->id;
				$data->name 				= $use->name;
				$data->description 			= $use->description;
				$data->type 	 			= 'uses';
				$data->is_active 			= $use->is_active;
				$data->created_at			= date('d M, Y', strtotime($use->created_at));
				$data->updated_at			= date('d M, Y', strtotime($use->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		$associatedProjects = $this->project_use_model
											->where('use_id', '=', $data->id)
											->where(function ($query){
                                                    $query->whereIn('project_id', function($query){
                                                                $query->select('id')
                                                                    ->from(with($this->project_model)->getTable())
                                                                    ->where('is_active','=',1)
                                                                    ->whereNull('deleted_at');
                                                    });
                                            })->pluck('project_id')->toArray();

		$data->associated_projects = count($associatedProjects);
		$projectImages = $this->project_image_model->whereIn('project_id', $associatedProjects)
													->where('image', '!=', '')
													/*->where(function ($query){
		                                                    $query->whereIn('project_id', function($query){
		                                                                $query->select('id')
		                                                                    ->from(with($this->project_model)->getTable())
		                                                                    ->where('is_active','=',1)
		                                                                    ->whereNull('deleted_at');
		                                                    });
		                                            })*/
													->inRandomOrder()->get(['title', 'image']);
		$pImages = [];
		if (count($projectImages) > 0) {
			$i = 0;
			foreach ($projectImages as $key => $projectImage) {
				$extension = \File::extension($projectImage->image);
                $smallImage =  str_replace('.'.$extension,'-small.'.$extension,$projectImage->image);

                $pImages[$i]['title'] = $projectImage->title;
                $pImages[$i]['small_image'] = $smallImage;
                $pImages[$i]['image'] = $projectImage->image;
                $i++;
			}
		}
		$data->project_image = $pImages;
		return $data;

	}

	/**
	 *
	 * This method will create a new use
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {

		$use 					= $this->use_model;
		$use->name				= $input['name']; 

		if (isset($input['description']) && $input['description'] != '') {
			$use->description  			= $input['description'];
		} else {
			$use->description = '';
		}
		$use->is_active   		= 1;
		$use->created_by  		= Auth::user()->id;
		if($use->save()) {	
			return true;
		} else {
			return false;
		}
	}

	 /**
	 *
	 * This method will update existing use
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$use = $this->use_model->find($input['id']);
		if ($use != NULL) {

			if (isset($input['name']) && $input['name'] != '') {
				$use->name = $input['name'];
			}

			if (isset($input['description']) && $input['description'] != '') {
				$use->description = $input['description'];
			}

			$use->updated_at   = Carbon::now();

			if($use->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing use
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$use = $this->use_model->find($id);
		if ($use != NULL) {
			if ($use->delete()) {
				// to check for associated projects
				$associatedProjects = $this->project_use_model->where('use_id', '=', $id)->count();
				if($associatedProjects > 0) {
					return 'cannot_delete';
				} else {
					Cache::forget($this->_cacheKey.$id);
					return 'success';
				}
			}
		} else {
			return false;
		}
	}

	 /**
     *
     * This method will change use status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$use = $this->use_model->find($input['id']);

		if ($use != NULL) {
			$use->is_active = $input['status'];
			$use->updated_at = Carbon::now();
			if ($use->save()) {
				if ($use->is_active == 0) {
					// to check for associated projects
					$associatedProjects = $this->project_use_model->where('use_id', '=', $input['id'])->count();
					if($associatedProjects > 0) {
						return 'cannot_delete';
					}
				}
				Cache::forget($this->_cacheKey.$input['id']);
				return 'success';
			}
		}
	}

	/**
	 *
	 * This method will fetch list of all uses
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$from = 'admin';
		if (isset($input['from']) && $input['from'] == 'site') {
			$from = 'site';
		} else {
			$from = 'admin';
		}

		if ($from == 'admin') {
			$objectIds = $this->use_model;
		} else if ($from == 'site') {
			$objectIds = $this->use_model->where('is_active', '=', 1);
		}
		
		$objectIds = $objectIds->orderBy('id', 'DESC');

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$objectIds = $objectIds->where('is_active','=',$input['filter_by_status']);
		}

		if (isset($input['keyword']) && $input['keyword'] != '') {
			$objectIds = $objectIds->where('name','LIKE','%'.$input['keyword'].'%');
		}
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$objectIdsPaginate = $objectIds->paginate($perPage, ['id']);
			$objects = $objectIdsPaginate->items();
			
		} else {
			$objects = $objectIds->get(['id']);
		}

		$data = ['data'=>[]];
		
		if (count($objects) > 0) {
			$i = 0;
			foreach ($objects as $object) {
				$objectData = $this->findById($object->id, false, false);
				$data['data'][$i] = $objectData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $objectIdsPaginate);
		}
		return $data;
	}
}
