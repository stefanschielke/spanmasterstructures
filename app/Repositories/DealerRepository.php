<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Dealer;

use App\Helpers\Helper;
use \StdClass, Carbon\Carbon, \Session;

class DealerRepository {

	public $dealer_model;

	protected $_cacheKey = 'dealer-'; 

	public function __construct(Dealer $dealer){
		$this->dealer_model 	= $dealer;
	}

	 /**
	 *
	 * This method will fetch data of individual dealer
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$dealer = $this->dealer_model->find($id);
			if ($dealer != NULL) {

				$data 						= new StdClass;
				$data->id 					= $dealer->id;
				$data->name 				= $dealer->name;
				$data->image 				= $dealer->image;
				$data->is_active 			= $dealer->is_active;
				$data->created_at			= date('d M, Y', strtotime($dealer->created_at));
				$data->updated_at			= date('d M, Y', strtotime($dealer->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		return $data;

	}


	/**
	 *
	 * This method will fetch list of all dealers
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$dealerIds = $this->dealer_model->orderBy('id', 'DESC');

		if (isset($input['keyword']) && $input['keyword'] != '') {
			$dealerIds = $dealerIds->where('name','LIKE','%'.$input['keyword'].'%');
		}

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$dealerIds = $dealerIds->where('is_active','=',$input['filter_by_status']);
		}
		
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$dealerIdsObj = $dealerIds->paginate($perPage, ['id']);
			$industries = $dealerIdsObj->items();
			
		} else {
			$industries = $dealerIds->get(['id']);
		}

		$data = ['data'=>[]];
		$total = count($industries);
		$data['total'] = $total;
		
		if ($total > 0) {
			$i = 0;
			foreach ($industries as $industry) {
				$dealerData = $this->findById($industry->id);
				$data['data'][$i] = $dealerData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $dealerIdsObj);
		}
		return $data;
	}

	/**
	 *
	 * This method will create a new dealer
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {
		$dealer 					= new $this->dealer_model;

		if (isset($input['name']) && $input['name'] != '') {
			$dealer->name 			= $input['name'];
		} else {
			$dealer->name 			= '';
		}
		
		$dealer->image 			= $input['image'];
		$dealer->created_by 	= Auth::user()->id;

		if($dealer->save()) {
			return true;
		} else {
			return false;
		}

	}

	 /**
	 *
	 * This method will update existing dealer
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$dealer = $this->dealer_model->find($input['id']);
		if ($dealer != NULL) {
			if (isset($input['name']) && $input['name'] != '') {
				$dealer->name = $input['name'];
			}

			if (isset($input['image']) && $input['image'] != '') {
				$dealer->image = $input['image'];
			}
			
			if ($dealer->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing dealer
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$dealer = $this->dealer_model->find($id);
		if($dealer != NULL){
		
			if ($dealer->delete()) {
				Cache::forget($this->_cacheKey.$id);
				return 'success';
			} else {
				return 'error';
			}
			
		} else {
			return 'not_found';
		}
	}

   	/**
     *
     * This method will change dealer status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$dealer = $this->dealer_model->find($input['id']);

		if ($dealer != NULL) {
			$dealer->is_active = $input['status'];	
			
			if ($dealer->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return 'success';
			}
		}
	}
}
