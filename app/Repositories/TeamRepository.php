<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\TeamMember;
use App\Helpers\Helper;

use \StdClass, Carbon\Carbon, \Session;

class TeamRepository {

	public $team_model;

	protected $_cacheKey = 'team-'; 

	public function __construct(TeamMember $teamMember){
		$this->team_model 			= $teamMember;
	}

	 /**
	 *
	 * This method will fetch data of individual team
	 * and will return output back to team as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$team = $this->team_model->find($id);
			if ($team != NULL) {

				$data 						= new StdClass;
				$data->id 					= $team->id;
				$data->name 				= $team->name;
				$data->designation 			= $team->designation;
				$data->description 			= $team->description;
				$data->email 				= $team->email;
				$data->linked_in 			= $team->linked_in;
				$data->image 				= $team->image;
				$data->is_active 			= $team->is_active;
				$data->created_at			= date('d M, Y', strtotime($team->created_at));
				$data->updated_at			= date('d M, Y', strtotime($team->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		return $data;

	}

	/**
	 *
	 * This method will create a new team
	 * and will return output back to team as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {

		$team 					= $this->team_model;
		$team->name				= $input['name']; 

		if (isset($input['designation']) && $input['designation'] != '') {
			$team->designation  			= $input['designation'];
		} else {
			$team->designation = '';
		}

		if (isset($input['description']) && $input['description'] != '') {
			$team->description  			= $input['description'];
		} else {
			$team->description = '';
		}

		if (isset($input['email']) && $input['email'] != '') {
			$team->email  			= $input['email'];
		} else {
			$team->email = '';
		}

		if (isset($input['linked_in']) && $input['linked_in'] != '') {
			$team->linked_in  			= $input['linked_in'];
		} else {
			$team->linked_in = '';
		}

		if (isset($input['image']) && $input['image'] != '') {
			$team->image  			= $input['image'];
		} else {
			$team->image = '';
		}
		$team->is_active   		= 1;
		$team->created_by  		= Auth::user()->id;
		if($team->save()) {	
			return true;
		} else {
			return false;
		}
	}

	 /**
	 *
	 * This method will update existing team
	 * and will return output back to team as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$team = $this->team_model->find($input['id']);
		if ($team != NULL) {

			if (isset($input['name']) && $input['name'] != '') {
				$team->name = $input['name'];
			}

			if (isset($input['designation']) && $input['designation'] != '') {
				$team->designation = $input['designation'];
			}

			if (isset($input['description']) && $input['description'] != '') {
				$team->description = $input['description'];
			}

			if (isset($input['email'])) {
				$team->email = $input['email'];
			}

			if (isset($input['linked_in'])) {
				$team->linked_in = $input['linked_in'];
			}

			if (isset($input['image'])) {
				$team->image = $input['image'];
			}

			$team->updated_at   = Carbon::now();

			if($team->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing team
	 * and will return output back to team as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$team = $this->team_model->find($id);
		if ($team != NULL) {
			if ($team->delete()) {
				Cache::forget($this->_cacheKey.$id);
				return true;
			}
		} else {
			return false;
		}
	}

	 /**
     *
     * This method will change team status (active, inactive)
     * and will return output back to team as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$team = $this->team_model->find($input['id']);

		if ($team != NULL) {
			$team->is_active = $input['status'];
			$team->updated_at = Carbon::now();
			if ($team->save()) {

				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			}
		}
	}

	/**
	 *
	 * This method will fetch list of all teams
	 * and will return output back to team as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$objectIds = $this->team_model->orderBy('id', 'DESC');

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$objectIds = $objectIds->where('is_active','=',$input['filter_by_status']);
		}

		if (isset($input['keyword']) && $input['keyword'] != '') {
			$objectIds = $objectIds->where(function ($query) use ($input) {
					                $query->orWhere('name', 'LIKE', '%'.$input['keyword'].'%')
					                		->orWhere('email', 'LIKE', '%'.$input['keyword'].'%')
					                		->orWhere('linked_in', 'LIKE', '%'.$input['keyword'].'%');
					            });
		}
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$objectIdsPaginate = $objectIds->paginate($perPage, ['id']);
			$objects = $objectIdsPaginate->items();
			
		} else {
			$objects = $objectIds->get(['id']);
		}

		$data = ['data'=>[]];
		
		if (count($objects) > 0) {
			$i = 0;
			foreach ($objects as $object) {
				$objectData = $this->findById($object->id, false, false);
				$data['data'][$i] = $objectData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $objectIdsPaginate);
		}
		return $data;
	}
}
