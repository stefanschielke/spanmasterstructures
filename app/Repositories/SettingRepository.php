<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Setting;
use App\Models\HomeImage;
use App\Models\SponsorshipImage;
use App\Models\CertificationImage;

use App\Helpers\Helper;

use \StdClass, Carbon\Carbon, \Session;

class SettingRepository {

	public $setting_model;
	public $home_image_model;

	protected $_cacheKey = 'setting-'; 

	public function __construct(Setting $setting, HomeImage $homeImage){
		$this->setting_model 	= $setting;
		$this->home_image_model = $homeImage;
	}

	 /**
	 *
	 * This method will fetch data of individual setting
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$setting = $this->setting_model->find($id);
			if ($setting != NULL) {
				$data 							= new StdClass;
				$data->id 						= $setting->id;
				$data->home_heading 			= $setting->home_heading;
				$data->why_choose_title 		= $setting->why_choose_title;
				$data->why_choose_description 	= $setting->why_choose_description;
				$data->about_us_title 			= $setting->about_us_title;
				$data->about_us_description 	= $setting->about_us_description;
				$data->mission_title 			= $setting->mission_title;
				$data->mission_description 		= $setting->mission_description;
				$data->sponsorship_title 		= $setting->sponsorship_title;
				$data->sponsorship_description 	= $setting->sponsorship_description;
				$data->about_us_image 			= $setting->about_us_image;
				$data->sponsorship_image 		= $setting->sponsorship_image;
				$data->certification_title 		= $setting->certification_title;
				$data->certification_description= $setting->certification_description;
				$data->certification_image 		= $setting->certification_image;
				$data->inquiry_phone 			= $setting->inquiry_phone;
				$data->inquiry_toll_free 		= $setting->inquiry_toll_free;
				$data->inquiry_email 			= $setting->inquiry_email;
				$data->repairs_heading 			= $setting->repairs_heading;
				$data->repairs_description 			= $setting->repairs_description;
				$data->repairs_who_is_title 			= $setting->repairs_who_is_title;
				$data->repairs_who_is_description 			= $setting->repairs_who_is_description;
				$data->repairs_who_is_it_for_title 			= $setting->repairs_who_is_it_for_title;
				$data->repairs_who_is_it_for_description 			= $setting->repairs_who_is_it_for_description;
				$data->repairs_why_you_love_title 			= $setting->repairs_why_you_love_title;
				$data->repairs_why_you_love_description 			= $setting->repairs_why_you_love_description;
				$data->repairs_who_is_image 			= $setting->repairs_who_is_image;
				$data->repairs_who_is_it_for_image 			= $setting->repairs_who_is_it_for_image;
				$data->repairs_why_you_love_image 			= $setting->repairs_why_you_love_image;
				Cache::forever($this->_cacheKey.$id,$data);	
			} else {
				$data = NULL;
			}
		}

		return $data;

	}

	/**
	 *
	 * This method will update settings
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update($input) {

		// check for settings
		$settings = $this->setting_model->find(Setting::GLOBAL_SETTING);

		if ($settings != NULL) {
			// update settings
			if (isset($input['home_heading']) && $input['home_heading'] != '') {
				if ($input['home_heading'] != $settings->home_heading) {
					$settings->home_heading  = $input['home_heading'];
				}
			}
			if (isset($input['why_choose_title']) && $input['why_choose_title'] != '') {
				if ($input['why_choose_title'] != $settings->why_choose_title) {
					$settings->why_choose_title  = $input['why_choose_title'];
				}
			}
			if (isset($input['why_choose_description']) && $input['why_choose_description'] != '') {
				if ($input['why_choose_description'] != $settings->why_choose_description) {
					$settings->why_choose_description  = $input['why_choose_description'];
				}
			}
			if (isset($input['about_us_title']) && $input['about_us_title'] != '') {
				if ($input['about_us_title'] != $settings->about_us_title) {
					$settings->about_us_title  = $input['about_us_title'];
				}
			}
			if (isset($input['about_us_description']) && $input['about_us_description'] != '') {
				if ($input['about_us_description'] != $settings->about_us_description) {
					$settings->about_us_description  = $input['about_us_description'];
				}
			}
			if (isset($input['about_us_image']) && $input['about_us_image'] != '') {
				if ($input['about_us_image'] != $settings->about_us_image) {
					$settings->about_us_image  = $input['about_us_image'];
				}
			}
			if (isset($input['mission_title']) && $input['mission_title'] != '') {
				if ($input['mission_title'] != $settings->mission_title) {
					$settings->mission_title  = $input['mission_title'];
				}
			}

			if (isset($input['mission_description']) && $input['mission_description'] != '') {
				if ($input['mission_description'] != $settings->mission_description) {
					$settings->mission_description  = $input['mission_description'];
				}
			}

			if (isset($input['sponsorship_title']) && $input['sponsorship_title'] != '') {
				if ($input['sponsorship_title'] != $settings->sponsorship_title) {
					$settings->sponsorship_title  = $input['sponsorship_title'];
				}
			}

			if (isset($input['sponsorship_description']) && $input['sponsorship_description'] != '') {
				if ($input['sponsorship_description'] != $settings->sponsorship_description) {
					$settings->sponsorship_description  = $input['sponsorship_description'];
				}
			}

			if (isset($input['sponsorship_image']) && $input['sponsorship_image'] != '') {
				if ($input['sponsorship_image'] != $settings->sponsorship_image) {
					$settings->sponsorship_image  = $input['sponsorship_image'];
				}
			}

			if (isset($input['certification_title']) && $input['certification_title'] != '') {
				if ($input['certification_title'] != $settings->certification_title) {
					$settings->certification_title  = $input['certification_title'];
				}
			}

			if (isset($input['certification_description']) && $input['certification_description'] != '') {
				if ($input['certification_description'] != $settings->certification_description) {
					$settings->certification_description  = $input['certification_description'];
				}
			}

			if (isset($input['certification_image']) && $input['certification_image'] != '') {
				if ($input['certification_image'] != $settings->certification_image) {
					$settings->certification_image  = $input['certification_image'];
				}
			}

			if (isset($input['inquiry_phone']) && $input['inquiry_phone'] != '') {
				if ($input['inquiry_phone'] != $settings->inquiry_phone) {
					$settings->inquiry_phone  = $input['inquiry_phone'];
				}
			}

			if (isset($input['inquiry_toll_free']) && $input['inquiry_toll_free'] != '') {
				if ($input['inquiry_toll_free'] != $settings->inquiry_toll_free) {
					$settings->inquiry_toll_free  = $input['inquiry_toll_free'];
				}
			}

			if (isset($input['inquiry_email']) && $input['inquiry_email'] != '') {
				if ($input['inquiry_email'] != $settings->inquiry_email) {
					$settings->inquiry_email  = $input['inquiry_email'];
				}
			}
				
			if (isset($input['repairs_heading']) && $input['repairs_heading'] != '') {
				if ($input['repairs_heading'] != $settings->repairs_heading) {
					$settings->repairs_heading  = $input['repairs_heading'];
				}
			}

			if (isset($input['repairs_description']) && $input['repairs_description'] != '') {
				if ($input['repairs_description'] != $settings->repairs_description) {
					$settings->repairs_description  = $input['repairs_description'];
				}
			}

			if (isset($input['repairs_who_is_title']) && $input['repairs_who_is_title'] != '') {
				if ($input['repairs_who_is_title'] != $settings->repairs_who_is_title) {
					$settings->repairs_who_is_title  = $input['repairs_who_is_title'];
				}
			}

			if (isset($input['repairs_who_is_description']) && $input['repairs_who_is_description'] != '') {
				if ($input['repairs_who_is_description'] != $settings->repairs_who_is_description) {
					$settings->repairs_who_is_description  = $input['repairs_who_is_description'];
				}
			}

			if (isset($input['repairs_who_is_it_for_title']) && $input['repairs_who_is_it_for_title'] != '') {
				if ($input['repairs_who_is_it_for_title'] != $settings->repairs_who_is_it_for_title) {
					$settings->repairs_who_is_it_for_title  = $input['repairs_who_is_it_for_title'];
				}
			}

			if (isset($input['repairs_who_is_it_for_description']) && $input['repairs_who_is_it_for_description'] != '') {
				if ($input['repairs_who_is_it_for_description'] != $settings->repairs_who_is_it_for_description) {
					$settings->repairs_who_is_it_for_description  = $input['repairs_who_is_it_for_description'];
				}
			}

			if (isset($input['repairs_why_you_love_title']) && $input['repairs_why_you_love_title'] != '') {
				if ($input['repairs_why_you_love_title'] != $settings->repairs_why_you_love_title) {
					$settings->repairs_why_you_love_title  = $input['repairs_why_you_love_title'];
				}
			}

			if (isset($input['repairs_why_you_love_description']) && $input['repairs_why_you_love_description'] != '') {
				if ($input['repairs_why_you_love_description'] != $settings->repairs_why_you_love_description) {
					$settings->repairs_why_you_love_description  = $input['repairs_why_you_love_description'];
				}
			}

			if (isset($input['repairs_who_is_image']) && $input['repairs_who_is_image'] != '') {
				if ($input['repairs_who_is_image'] != $settings->repairs_who_is_image) {
					$settings->repairs_who_is_image  = $input['repairs_who_is_image'];
				}
			}

			if (isset($input['repairs_who_is_it_for_image']) && $input['repairs_who_is_it_for_image'] != '') {
				if ($input['repairs_who_is_it_for_image'] != $settings->repairs_who_is_it_for_image) {
					$settings->repairs_who_is_it_for_image  = $input['repairs_who_is_it_for_image'];
				}
			}

			if (isset($input['repairs_why_you_love_image']) && $input['repairs_why_you_love_image'] != '') {
				if ($input['repairs_why_you_love_image'] != $settings->repairs_why_you_love_image) {
					$settings->repairs_why_you_love_image  = $input['repairs_why_you_love_image'];
				}
			}
			if ($settings->save()) {
				Cache::forget($this->_cacheKey.$settings->id);
			}
			
		} else {
			// create settings
			$settings = new $this->setting_model;
			$settings->id = Setting::GLOBAL_SETTING;

			if (isset($input['home_heading']) && $input['home_heading'] != '') {
				if ($input['home_heading'] != $settings->home_heading) {
					$settings->home_heading  = $input['home_heading'];
				}
			}

			if (isset($input['why_choose_title']) && $input['why_choose_title'] != '') {
				if ($input['why_choose_title'] != $settings->why_choose_title) {
					$settings->why_choose_title  = $input['why_choose_title'];
				}
			}
			if (isset($input['why_choose_description']) && $input['why_choose_description'] != '') {
				if ($input['why_choose_description'] != $settings->why_choose_description) {
					$settings->why_choose_description  = $input['why_choose_description'];
				}
			}
			if (isset($input['about_us_title']) && $input['about_us_title'] != '') {
				if ($input['about_us_title'] != $settings->about_us_title) {
					$settings->about_us_title  = $input['about_us_title'];
				}
			}
			if (isset($input['about_us_description']) && $input['about_us_description'] != '') {
				if ($input['about_us_description'] != $settings->about_us_description) {
					$settings->about_us_description  = $input['about_us_description'];
				}
			}
			if (isset($input['about_us_image']) && $input['about_us_image'] != '') {
				if ($input['about_us_image'] != $settings->about_us_image) {
					$settings->about_us_image  = $input['about_us_image'];
				}
			}

			if (isset($input['mission_title']) && $input['mission_title'] != '') {
				if ($input['mission_title'] != $settings->mission_title) {
					$settings->mission_title  = $input['mission_title'];
				}
			}

			if (isset($input['mission_description']) && $input['mission_description'] != '') {
				if ($input['mission_description'] != $settings->mission_description) {
					$settings->mission_description  = $input['mission_description'];
				}
			}

			if (isset($input['sponsorship_title']) && $input['sponsorship_title'] != '') {
				if ($input['sponsorship_title'] != $settings->sponsorship_title) {
					$settings->sponsorship_title  = $input['sponsorship_title'];
				}
			}

			if (isset($input['sponsorship_description']) && $input['sponsorship_description'] != '') {
				if ($input['sponsorship_description'] != $settings->sponsorship_description) {
					$settings->sponsorship_description  = $input['sponsorship_description'];
				}
			}

			if (isset($input['sponsorship_image']) && $input['sponsorship_image'] != '') {
				if ($input['sponsorship_image'] != $settings->sponsorship_image) {
					$settings->sponsorship_image  = $input['sponsorship_image'];
				}
			}

			if (isset($input['certification_title']) && $input['certification_title'] != '') {
				if ($input['certification_title'] != $settings->certification_title) {
					$settings->certification_title  = $input['certification_title'];
				}
			}

			if (isset($input['certification_description']) && $input['certification_description'] != '') {
				if ($input['certification_description'] != $settings->certification_description) {
					$settings->certification_description  = $input['certification_description'];
				}
			}

			if (isset($input['certification_image']) && $input['certification_image'] != '') {
				if ($input['certification_image'] != $settings->certification_image) {
					$settings->certification_image  = $input['certification_image'];
				}
			}

			if (isset($input['inquiry_phone']) && $input['inquiry_phone'] != '') {
				if ($input['inquiry_phone'] != $settings->inquiry_phone) {
					$settings->inquiry_phone  = $input['inquiry_phone'];
				}
			}

			if (isset($input['inquiry_toll_free']) && $input['inquiry_toll_free'] != '') {
				if ($input['inquiry_toll_free'] != $settings->inquiry_toll_free) {
					$settings->inquiry_toll_free  = $input['inquiry_toll_free'];
				}
			}

			if (isset($input['inquiry_email']) && $input['inquiry_email'] != '') {
				if ($input['inquiry_email'] != $settings->inquiry_email) {
					$settings->inquiry_email  = $input['inquiry_email'];
				}
			}


			if (isset($input['repairs_heading']) && $input['repairs_heading'] != '') {
				if ($input['repairs_heading'] != $settings->repairs_heading) {
					$settings->repairs_heading  = $input['repairs_heading'];
				}
			}

			if (isset($input['repairs_description']) && $input['repairs_description'] != '') {
				if ($input['repairs_description'] != $settings->repairs_description) {
					$settings->repairs_description  = $input['repairs_description'];
				}
			}

			if (isset($input['repairs_who_is_title']) && $input['repairs_who_is_title'] != '') {
				if ($input['repairs_who_is_title'] != $settings->repairs_who_is_title) {
					$settings->repairs_who_is_title  = $input['repairs_who_is_title'];
				}
			}

			if (isset($input['repairs_who_is_description']) && $input['repairs_who_is_description'] != '') {
				if ($input['repairs_who_is_description'] != $settings->repairs_who_is_description) {
					$settings->repairs_who_is_description  = $input['repairs_who_is_description'];
				}
			}

			if (isset($input['repairs_who_is_it_for_title']) && $input['repairs_who_is_it_for_title'] != '') {
				if ($input['repairs_who_is_it_for_title'] != $settings->repairs_who_is_it_for_title) {
					$settings->repairs_who_is_it_for_title  = $input['repairs_who_is_it_for_title'];
				}
			}

			if (isset($input['repairs_who_is_it_for_description']) && $input['repairs_who_is_it_for_description'] != '') {
				if ($input['repairs_who_is_it_for_description'] != $settings->repairs_who_is_it_for_description) {
					$settings->repairs_who_is_it_for_description  = $input['repairs_who_is_it_for_description'];
				}
			}

			if (isset($input['repairs_why_you_love_title']) && $input['repairs_why_you_love_title'] != '') {
				if ($input['repairs_why_you_love_title'] != $settings->repairs_why_you_love_title) {
					$settings->repairs_why_you_love_title  = $input['repairs_why_you_love_title'];
				}
			}

			if (isset($input['repairs_why_you_love_description']) && $input['repairs_why_you_love_description'] != '') {
				if ($input['repairs_why_you_love_description'] != $settings->repairs_why_you_love_description) {
					$settings->repairs_why_you_love_description  = $input['repairs_why_you_love_description'];
				}
			}

			if (isset($input['repairs_who_is_image']) && $input['repairs_who_is_image'] != '') {
				if ($input['repairs_who_is_image'] != $settings->repairs_who_is_image) {
					$settings->repairs_who_is_image  = $input['repairs_who_is_image'];
				}
			}

			if (isset($input['repairs_who_is_it_for_image']) && $input['repairs_who_is_it_for_image'] != '') {
				if ($input['repairs_who_is_it_for_image'] != $settings->repairs_who_is_it_for_image) {
					$settings->repairs_who_is_it_for_image  = $input['repairs_who_is_it_for_image'];
				}
			}

			if (isset($input['repairs_why_you_love_image']) && $input['repairs_why_you_love_image'] != '') {
				if ($input['repairs_why_you_love_image'] != $settings->repairs_why_you_love_image) {
					$settings->repairs_why_you_love_image  = $input['repairs_why_you_love_image'];
				}
			}

			$settings->save();
		}

		if(isset($input['home_images']) && count($input['home_images']) > 0) {
			$imageArray = [];
			foreach ($input['home_images'] as $key => $home_image) {
				$imageArray[] = $home_image['image'];
				$checkImage = $this->home_image_model->where('image', '=', $home_image['image'])->first();
				if ($checkImage == NULL) {
					$imageObj = new $this->home_image_model;
					$imageObj->title = $home_image['title'];
					$imageObj->image = $home_image['image'];
					$imageObj->real_image = $home_image['real_image'];
					$imageObj->save();
				} else {
					$checkImage->title = $home_image['title'];
					$checkImage->image = $home_image['image'];
					$checkImage->real_image = $home_image['real_image'];
					$checkImage->save();
				}
				
			}

			$dbImages = $this->home_image_model->get();
			if (count($dbImages) > 0) {
				foreach ($dbImages as $key => $dbImage) {
					if (!in_array($dbImage->image, $imageArray)) {
						$this->home_image_model->where('image', '=', $dbImage->image)->delete();
					}
				}
			}
		} else {
			$this->home_image_model->where('image', '!=', '')->delete();
		}
		if(isset($input['certification_images']) && count($input['certification_images']) > 0) {
			$imageArray = [];
			foreach ($input['certification_images'] as $key => $certification_image) {
				$imageArray[] = $certification_image['image'];
				$checkImage = CertificationImage::where('image', '=', $certification_image['image'])->first();
				if ($checkImage == NULL) {
					$imageObj = new CertificationImage;
					$imageObj->link = $certification_image['link'];
					$imageObj->image = $certification_image['image'];
					$imageObj->real_image = $certification_image['real_image'];
					$imageObj->save();
				} else {
					$checkImage->link = $certification_image['link'];
					$checkImage->image = $certification_image['image'];
					$checkImage->real_image = $certification_image['real_image'];
					$checkImage->save();
				}
				
			}

			$dbImages = CertificationImage::get();
			if (count($dbImages) > 0) {
				foreach ($dbImages as $key => $dbImage) {
					if (!in_array($dbImage->image, $imageArray)) {
						CertificationImage::where('image', '=', $dbImage->image)->delete();
					}
				}
			}
		} else {
			CertificationImage::where('image', '!=', '')->delete();
		}

		if(isset($input['sponsorship_images']) && count($input['sponsorship_images']) > 0) {
			$imageArray = [];
			foreach ($input['sponsorship_images'] as $key => $sponsorship_image) {
				$imageArray[] = $sponsorship_image['image'];
				$checkImage = SponsorshipImage::where('image', '=', $sponsorship_image['image'])->first();
				if ($checkImage == NULL) {
					$imageObj = new SponsorshipImage;
					$imageObj->link = $sponsorship_image['link'];
					$imageObj->image = $sponsorship_image['image'];
					$imageObj->real_image = $sponsorship_image['real_image'];
					$imageObj->save();
				} else {
					$checkImage->link = $sponsorship_image['link'];
					$checkImage->image = $sponsorship_image['image'];
					$checkImage->real_image = $sponsorship_image['real_image'];
					$checkImage->save();
				}
				
			}

			$dbImages = SponsorshipImage::get();
			if (count($dbImages) > 0) {
				foreach ($dbImages as $key => $dbImage) {
					if (!in_array($dbImage->image, $imageArray)) {
						SponsorshipImage::where('image', '=', $dbImage->image)->delete();
					}
				}
			}
		} else {
			SponsorshipImage::where('image', '!=', '')->delete();
		}
		return true;
	}
}
