<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProcessModel;

use App\Helpers\Helper;
use \StdClass, Carbon\Carbon, \Session;

class ProcessRepository {

	public $process_model;

	protected $_cacheKey = 'process-'; 

	public function __construct(ProcessModel $process){
		$this->process_model 	= $process;
	}

	 /**
	 *
	 * This method will fetch data of individual process
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$process = $this->process_model->find($id);
			if ($process != NULL) {

				$data 						= new StdClass;
				$data->id 					= $process->id;
				$data->name 				= $process->name;
				$data->description 			= $process->description;
				$data->image 				= $process->image;
				$data->is_active 			= $process->is_active;
				$data->created_at			= date('d M, Y', strtotime($process->created_at));
				$data->updated_at			= date('d M, Y', strtotime($process->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		return $data;

	}


	/**
	 *
	 * This method will fetch list of all processs
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$processIds = $this->process_model->orderBy('id', 'DESC');

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$processIds = $processIds->where('is_active','=',$input['filter_by_status']);
		}
			
		if (isset($input['keyword']) && $input['keyword'] != '') {
			$processIds = $processIds->where('name','LIKE','%'.$input['keyword'].'%')
										->orwhere('description','LIKE','%'.$input['keyword'].'%');
		}

		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$processIdsObj = $processIds->paginate($perPage, ['id']);
			$industries = $processIdsObj->items();
			
		} else {
			$industries = $processIds->get(['id']);
		}

		$data = ['data'=>[]];
		$total = count($industries);
		$data['total'] = $total;
		
		if ($total > 0) {
			$i = 0;
			foreach ($industries as $industry) {
				$processData = $this->findById($industry->id);
				$data['data'][$i] = $processData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $processIdsObj);
		}
		return $data;
	}

	/**
	 *
	 * This method will create a new process
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {
		$process 					= new $this->process_model;

		if (isset($input['name']) && $input['name'] != '') {
			$process->name 			= $input['name'];
		}

		if (isset($input['description']) && $input['description'] != '') {
			$process->description 			= $input['description'];
		} 
		
		$process->image 			= $input['image'];
		$process->created_by 	= Auth::user()->id;

		if($process->save()) {
			return true;
		} else {
			return false;
		}

	}

	 /**
	 *
	 * This method will update existing process
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$process = $this->process_model->find($input['id']);
		if ($process != NULL) {
			if (isset($input['name']) && $input['name'] != '') {
				$process->name = $input['name'];
			}

			if (isset($input['description']) && $input['description'] != '') {
				$process->description = $input['description'];
			}

			if (isset($input['image']) && $input['image'] != '') {
				$process->image = $input['image'];
			}
			
			if ($process->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing process
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$process = $this->process_model->find($id);
		if($process != NULL){
		
			if ($process->delete()) {
				Cache::forget($this->_cacheKey.$id);
				return 'success';
			} else {
				return 'error';
			}
			
		} else {
			return 'not_found';
		}
	}

   	/**
     *
     * This method will change process status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$process = $this->process_model->find($input['id']);

		if ($process != NULL) {
			$process->is_active = $input['status'];	
			
			if ($process->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return 'success';
			}
		}
	}
}
