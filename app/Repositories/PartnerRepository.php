<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Partner;

use App\Helpers\Helper;
use \StdClass, Carbon\Carbon, \Session;

class PartnerRepository {

	public $partner_model;

	protected $_cacheKey = 'partner-'; 

	public function __construct(Partner $partner){
		$this->partner_model 	= $partner;
	}

	 /**
	 *
	 * This method will fetch data of individual partner
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$partner = $this->partner_model->find($id);
			if ($partner != NULL) {

				$data 						= new StdClass;
				$data->id 					= $partner->id;
				$data->name 				= $partner->name;
				$data->image 				= $partner->image;
				$data->is_active 			= $partner->is_active;
				$data->created_at			= date('d M, Y', strtotime($partner->created_at));
				$data->updated_at			= date('d M, Y', strtotime($partner->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}

		return $data;

	}


	/**
	 *
	 * This method will fetch list of all partners
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$partnerIds = $this->partner_model->orderBy('id', 'DESC');

		if (isset($input['keyword']) && $input['keyword'] != '') {
			$partnerIds = $partnerIds->where('name','LIKE','%'.$input['keyword'].'%');
		}

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$partnerIds = $partnerIds->where('is_active','=',$input['filter_by_status']);
		}
		
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$partnerIdsObj = $partnerIds->paginate($perPage, ['id']);
			$industries = $partnerIdsObj->items();
			
		} else {
			$industries = $partnerIds->get(['id']);
		}

		$data = ['data'=>[]];
		$total = count($industries);
		$data['total'] = $total;
		
		if ($total > 0) {
			$i = 0;
			foreach ($industries as $industry) {
				$partnerData = $this->findById($industry->id);
				$data['data'][$i] = $partnerData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $partnerIdsObj);
		}
		return $data;
	}

	/**
	 *
	 * This method will create a new partner
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {
		$partner 					= new $this->partner_model;

		if (isset($input['name']) && $input['name'] != '') {
			$partner->name 			= $input['name'];
		} else {
			$partner->name 			= '';
		}
		
		$partner->image 			= $input['image'];
		$partner->created_by 	= Auth::user()->id;

		if($partner->save()) {
			return true;
		} else {
			return false;
		}

	}

	 /**
	 *
	 * This method will update existing partner
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function update(array $input = []) {
		$partner = $this->partner_model->find($input['id']);
		if ($partner != NULL) {
			if (isset($input['name']) && $input['name'] != '') {
				$partner->name = $input['name'];
			}

			if (isset($input['image']) && $input['image'] != '') {
				$partner->image = $input['image'];
			}
			
			if ($partner->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 *
	 * This method will delete an existing partner
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function deleteById($id) {

		$partner = $this->partner_model->find($id);
		if($partner != NULL){
		
			if ($partner->delete()) {
				Cache::forget($this->_cacheKey.$id);
				return 'success';
			} else {
				return 'error';
			}
			
		} else {
			return 'not_found';
		}
	}

   	/**
     *
     * This method will change partner status (active, inactive)
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function updateStatus($input) {

		$partner = $this->partner_model->find($input['id']);

		if ($partner != NULL) {
			$partner->is_active = $input['status'];	
			
			if ($partner->save()) {
				Cache::forget($this->_cacheKey.$input['id']);
				return 'success';
			}
		}
	}
}
