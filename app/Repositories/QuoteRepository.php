<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Quote;
use App\Models\User;

use App\Helpers\Helper;

use \StdClass, Carbon\Carbon, \Session;

class QuoteRepository {

	public $quote_model;
	public $user_model;

	protected $_cacheKey = 'quote-'; 

	public function __construct(Quote $quote, User $user){
		$this->quote_model 	= $quote;
		$this->user_model 	= $user;
	}

	 /**
	 *
	 * This method will fetch data of individual quote
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findById($id, $refresh = false) {

		$data = Cache::get($this->_cacheKey.$id);

		if ($data == NULL || $refresh == true) {
			$quote = $this->quote_model->find($id);
			if ($quote != NULL) {

				$data 						= new StdClass;
				$data->id 					= $quote->id;
				$data->name 				= $quote->name;
				$data->company_name 		= $quote->company_name;
				$data->phone 				= $quote->phone;
				$data->email 				= $quote->email;
				$data->fax 					= $quote->fax;
				$data->address 				= $quote->address;
				$data->city 				= $quote->city;
				$data->postal_code 			= $quote->postal_code;
				$data->use 					= $quote->use;
				$data->other_use 			= $quote->other_use;
				$data->installation_period 	= $quote->installation_period;
				$data->about_us 			= $quote->about_us;
				$data->comments 			= $quote->comments;
				$data->created_at			= date('d M, Y', strtotime($quote->created_at));
				$data->updated_at			= date('d M, Y', strtotime($quote->updated_at));

				Cache::forever($this->_cacheKey.$id,$data);			
				
			} else {
				$data = NULL;
			}
		}
		return $data;

	}

	/**
	 *
	 * This method will create a new quote
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function create(array $input = []) {

		try {

            $subject = 'Quote Request';
            $adminUser = $this->user_model->where('is_active', '=', 1)->first();
            \Mail::send('front.emails.quote', ['data' => $input], function ($m) use ($input, $subject, $adminUser) {
                $m->from(env('MAIL_ADDRESS', 'smtp@vegatechnologies.net'), env('MAIL_USERNAME', 'SpanMaster'));
               	$m->to('stefan@intechrity.ca', 'SpanMaster')->subject($subject);
                $m->to('nazbushi@gmail.com', $adminUser->name)->subject($subject);
            });

            $quote 						= $this->quote_model;
			$quote->name				= $input['name']; 

			if (isset($input['company_name']) && $input['company_name'] != '') {
				$quote->company_name  			= $input['company_name'];
			} else {
				$quote->company_name = '';
			}

			if (isset($input['company_name']) && $input['company_name'] != '') {
				$quote->company_name  			= $input['company_name'];
			} else {
				$quote->company_name = '';
			}

			if (isset($input['phone']) && $input['phone'] != '') {
				$quote->phone  			= $input['phone'];
			} else {
				$quote->phone = '';
			}

			if (isset($input['fax']) && $input['fax'] != '') {
				$quote->fax  			= $input['fax'];
			} else {
				$quote->fax = '';
			}

			if (isset($input['email']) && $input['email'] != '') {
				$quote->email  			= $input['email'];
			} else {
				$quote->email = '';
			}

			if (isset($input['address']) && $input['address'] != '') {
				$quote->address  			= $input['address'];
			} else {
				$quote->address = '';
			}

			if (isset($input['city']) && $input['city'] != '') {
				$quote->city  			= $input['city'];
			} else {
				$quote->city = '';
			}

			if (isset($input['postal_code']) && $input['postal_code'] != '') {
				$quote->postal_code  			= $input['postal_code'];
			} else {
				$quote->postal_code = '';
			}

			if (isset($input['use']) && $input['use'] != '') {
				$quote->use  			= $input['use'];
			} 

			if (isset($input['other_use']) && $input['other_use'] != '') {
				$quote->other_use  			= $input['other_use'];
			} else {
				$quote->other_use = '';
			}

			if (isset($input['about_us']) && $input['about_us'] != '') {
				$quote->about_us  			= $input['about_us'];
			} 

			if (isset($input['about_us_reason']) && $input['about_us_reason'] != '') {
				$quote->about_us_reason  			= $input['about_us_reason'];
			} else {
				$quote->about_us_reason = '';
			}

			if (isset($input['installation_period']) && $input['installation_period'] != '') {
				$quote->installation_period  			= $input['installation_period'];
			} 

			if (isset($input['comments']) && $input['comments'] != '') {
				$quote->comments  			= $input['comments'];
			} else {
				$quote->comments = '';
			}
			if($quote->save()) {	
				return true;
			} else {
				return false;
			}
        } catch(\Exception $e){
        	//dd($e->getMessage());
            return false;
        }
	}

	/**
	 *
	 * This method will fetch list of all quotes
	 * and will return output back to client as json
	 *
	 * @access public
	 * @return mixed
	 *
	 * @author Bushra Naz
	 *
	 **/
	public function findByAll($pagination = false, $perPage = 10, array $input = []) {

		$from = 'admin';
		if (isset($input['from']) && $input['from'] == 'site') {
			$from = 'site';
		} else {
			$from = 'admin';
		}

		if ($from == 'admin') {
			$objectIds = $this->quote_model;
		} else if ($from == 'site') {
			$objectIds = $this->quote_model->where('is_active', '=', 1);
		}
		
		$objectIds = $objectIds->orderBy('id', 'DESC');

		if (isset($input['filter_by_status']) && $input['filter_by_status'] != '') {
			$objectIds = $objectIds->where('is_active','=',$input['filter_by_status']);
		}

		if (isset($input['keyword']) && $input['keyword'] != '') {
			$objectIds = $objectIds->where('name','LIKE','%'.$input['keyword'].'%');
		}
		if(isset($input['limit']) && $input['limit'] != 0) {
			$perPage = $input['limit'];
		}

		if ($pagination == true) {
			$objectIdsPaginate = $objectIds->paginate($perPage, ['id']);
			$objects = $objectIdsPaginate->items();
			
		} else {
			$objects = $objectIds->get(['id']);
		}

		$data = ['data'=>[]];
		
		if (count($objects) > 0) {
			$i = 0;
			foreach ($objects as $object) {
				$objectData = $this->findById($object->id, false, false);
				$data['data'][$i] = $objectData;			
				$i++;
			}
		}

		if ($pagination == true) {
			// call method to paginate records
    		$data = Helper::paginator($data, $objectIdsPaginate);
		}
		return $data;
	}
}
