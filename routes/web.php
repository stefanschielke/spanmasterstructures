<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache',function(){
    Cache::flush();
    echo 'master';
});

Route::get('/hash/{name?}', function($name = null)
{
	if ($name != null) {
		echo Hash::make($name);
	} else {
		echo Hash::make('bushinaz');
	}

});

Route::get('/', function () {
    return view('welcome');
});

/****** Site Routes ******/
Route::group(['prefix'=>'','namespace'=>'Front', 'middleware' =>[]], function(){
	Route::get('/', ['uses'=>'HomeController@showHome']);
	Route::get('/about-us', ['uses'=>'HomeController@showAboutUs']);
	Route::get('/products', ['uses'=>'HomeController@showProducts']);
	Route::get('/product/{type}/{id}', ['uses'=>'HomeController@showProduct']);
	Route::get('/quote', ['uses'=>'HomeController@showQuote']);
	Route::get('/projects', ['uses'=>'HomeController@showProjects']);
	Route::get('/project/{id}', ['uses'=>'HomeController@showProject']);
	Route::get('/repairs', ['uses'=>'HomeController@showRepairs']);
	Route::get('/contact', ['uses'=>'HomeController@showContact']);
});

/****** Admin Routes ******/
Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware' =>[]], function(){
	Route::get('/',['uses'=>'HomeController@showLogin'])->middleware(['acl.admin.guest']);
	Route::get('/activation', ['uses'=>'HomeController@showActivation']);
	Route::get('/recover-password', ['uses'=>'HomeController@showRecover']);
	Route::get('/404', ['uses'=>'HomeController@showNotFound']);
	Route::get('/401', ['uses'=>'HomeController@showUnAuthorized']);

	// users route
	Route::get('/users',['uses'=>'HomeController@showUsers'])->middleware(['acl.admin']);
	Route::get('/user',['uses'=>'HomeController@showUser'])->middleware(['acl.admin']);
	Route::get('/user-profile',['uses'=>'HomeController@showUserProfile'])->middleware(['acl.admin']);
	// dealers route
	Route::get('/dealers',['uses'=>'HomeController@showDealers'])->middleware(['acl.admin']);
	Route::get('/dealer',['uses'=>'HomeController@showDealer'])->middleware(['acl.admin']);
	// types route
	Route::get('/types',['uses'=>'HomeController@showTypes'])->middleware(['acl.admin']);
	Route::get('/type',['uses'=>'HomeController@showType'])->middleware(['acl.admin']);
	// materials route
	Route::get('/materials',['uses'=>'HomeController@showMaterials'])->middleware(['acl.admin']);
	Route::get('/material',['uses'=>'HomeController@showMaterial'])->middleware(['acl.admin']);
	// shapes route
	Route::get('/shapes',['uses'=>'HomeController@showShapes'])->middleware(['acl.admin']);
	Route::get('/shape',['uses'=>'HomeController@showShape'])->middleware(['acl.admin']);
	// uses route
	Route::get('/uses',['uses'=>'HomeController@showUses'])->middleware(['acl.admin']);
	Route::get('/use',['uses'=>'HomeController@showUse'])->middleware(['acl.admin']);
	// clients route
	Route::get('/clients',['uses'=>'HomeController@showClients'])->middleware(['acl.admin']);
	Route::get('/client',['uses'=>'HomeController@showClient'])->middleware(['acl.admin']);
	// projects route
	Route::get('/projects',['uses'=>'HomeController@showProjects'])->middleware(['acl.admin']);
	Route::get('/project',['uses'=>'HomeController@showProject'])->middleware(['acl.admin']);
	Route::get('/view-project',['uses'=>'HomeController@showViewProject'])->middleware(['acl.admin']);
	// Management team route
	Route::get('/teams',['uses'=>'HomeController@showTeams'])->middleware(['acl.admin']);
	Route::get('/team',['uses'=>'HomeController@showTeam'])->middleware(['acl.admin']);
	// Contact member route
	Route::get('/contacts',['uses'=>'HomeController@showContactMembers'])->middleware(['acl.admin']);
	Route::get('/contact',['uses'=>'HomeController@showContactMember'])->middleware(['acl.admin']);
	Route::get('/inquiry',['uses'=>'HomeController@showInquiry'])->middleware(['acl.admin']);
	// Quotes route
	Route::get('/quotes',['uses'=>'HomeController@showQuotes'])->middleware(['acl.admin']);
	Route::get('/quote',['uses'=>'HomeController@showQuote'])->middleware(['acl.admin']);
	// Settings
	Route::get('/settings',['uses'=>'HomeController@showSettings'])->middleware(['acl.admin']);
	Route::get('/settings-home',['uses'=>'HomeController@showHomeSettings'])->middleware(['acl.admin']);
	Route::get('/settings-about',['uses'=>'HomeController@showAboutSettings'])->middleware(['acl.admin']);
	Route::get('/fabric-structure',['uses'=>'HomeController@showFabricStructure'])->middleware(['acl.admin']);
	// partners route
	Route::get('/partners',['uses'=>'HomeController@showPartners'])->middleware(['acl.admin']);
	Route::get('/partner',['uses'=>'HomeController@showPartner'])->middleware(['acl.admin']);
	// processes route
	Route::get('/processes',['uses'=>'HomeController@showProcesses'])->middleware(['acl.admin']);
	Route::get('/process',['uses'=>'HomeController@showProcess'])->middleware(['acl.admin']);
});

/****** API Routes ******/
Route::group(['prefix'=>'api','namespace'=>'Api','middleware' =>[]], function(){

	// User authentication Routes
	Route::post('/user/login',['as'=>'user.login', 'uses'=>'UserController@login']);
	Route::post('/user/logout',['as'=>'user.logout', 'uses'=>'UserController@logout']);
	Route::post('/user/create-password',['uses'=>'UserController@createPassword']);
	Route::post('/user/forgot-password',['uses'=>'UserController@forgotPassword']);
	Route::post('/user/reset-password',['uses'=>'UserController@resetPassword']);

	Route::post('/user/update-password',['as'=>'user.update-password', 'uses'=>'UserController@updatePassword']);
	Route::post('/user/update-account',['as'=>'user.update-account', 'uses'=>'UserController@updatePersonalInfo']);

	// User Routes
	Route::get('/user/view',['as'=>'user:view', 'uses'=>'UserController@view']);
	Route::get('/user/list',['as'=>'user:list', 'uses'=>'UserController@all'])->middleware(['acl.admin']);
	Route::post('/user/create',['as'=>'user:create', 'uses'=>'UserController@create'])->middleware(['acl.admin']);
	Route::post('/user/update',['as'=>'user:update', 'uses'=>'UserController@update'])->middleware(['acl.admin']);
	Route::post('/user/update-status',['as'=>'user.update-status', 'uses'=>'UserController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/user/remove',['as'=>'user.remove', 'uses'=>'UserController@remove'])->middleware(['acl.admin']);

	// image upload
	Route::post('/image/upload',['uses'=>'DealerController@upload']);

	// Dealers Routes
	Route::get('/dealer/view',['as'=>'dealer:view', 'uses'=>'DealerController@view'])->middleware(['acl.admin']);
	Route::get('/dealer/list',['as'=>'dealer:list', 'uses'=>'DealerController@all'])->middleware(['acl.admin']);
	Route::post('/dealer/create',['as'=>'dealer:create', 'uses'=>'DealerController@create'])->middleware(['acl.admin']);
	Route::post('/dealer/update',['as'=>'dealer:update', 'uses'=>'DealerController@update'])->middleware(['acl.admin']);
	Route::post('/dealer/update-status',['as'=>'dealer.update-status', 'uses'=>'DealerController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/dealer/remove',['as'=>'dealer.remove', 'uses'=>'DealerController@remove'])->middleware(['acl.admin']);

	// Types Routes
	Route::get('/type/view',['as'=>'type:view', 'uses'=>'TypeController@view']);
	Route::get('/type/list',['as'=>'type:list', 'uses'=>'TypeController@all'])->middleware(['acl.admin']);
	Route::post('/type/create',['as'=>'type:create', 'uses'=>'TypeController@create'])->middleware(['acl.admin']);
	Route::post('/type/update',['as'=>'type:update', 'uses'=>'TypeController@update'])->middleware(['acl.admin']);
	Route::post('/type/update-status',['as'=>'type.update-status', 'uses'=>'TypeController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/type/remove',['as'=>'type.remove', 'uses'=>'TypeController@remove'])->middleware(['acl.admin']);

	// Materials Routes
	Route::get('/material/view',['as'=>'material:view', 'uses'=>'MaterialController@view']);
	Route::get('/material/list',['as'=>'material:list', 'uses'=>'MaterialController@all'])->middleware(['acl.admin']);
	Route::post('/material/create',['as'=>'material:create', 'uses'=>'MaterialController@create'])->middleware(['acl.admin']);
	Route::post('/material/update',['as'=>'material:update', 'uses'=>'MaterialController@update'])->middleware(['acl.admin']);
	Route::post('/material/update-status',['as'=>'material.update-status', 'uses'=>'MaterialController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/material/remove',['as'=>'material.remove', 'uses'=>'MaterialController@remove'])->middleware(['acl.admin']);

	// Shapes Routes
	Route::get('/shapes/view',['as'=>'shapes:view', 'uses'=>'ShapesController@view']);
	Route::get('/shapes/list',['as'=>'shapes:list', 'uses'=>'ShapesController@all'])->middleware(['acl.admin']);
	Route::post('/shapes/create',['as'=>'shapes:create', 'uses'=>'ShapesController@create'])->middleware(['acl.admin']);
	Route::post('/shapes/update',['as'=>'shapes:update', 'uses'=>'ShapesController@update'])->middleware(['acl.admin']);
	Route::post('/shapes/update-status',['as'=>'shapes.update-status', 'uses'=>'ShapesController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/shapes/remove',['as'=>'shapes.remove', 'uses'=>'ShapesController@remove'])->middleware(['acl.admin']);

	// Uses Routes
	Route::get('/uses/view',['as'=>'uses:view', 'uses'=>'UsesController@view']);
	Route::get('/uses/list',['as'=>'uses:list', 'uses'=>'UsesController@all'])->middleware(['acl.admin']);
	Route::post('/uses/create',['as'=>'uses:create', 'uses'=>'UsesController@create'])->middleware(['acl.admin']);
	Route::post('/uses/update',['as'=>'uses:update', 'uses'=>'UsesController@update'])->middleware(['acl.admin']);
	Route::post('/uses/update-status',['as'=>'uses.update-status', 'uses'=>'UsesController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/uses/remove',['as'=>'uses.remove', 'uses'=>'UsesController@remove'])->middleware(['acl.admin']);

	// Clients Routes
	Route::get('/client/view',['as'=>'client:view', 'uses'=>'ClientController@view']);
	Route::get('/client/list',['as'=>'client:list', 'uses'=>'ClientController@all'])->middleware(['acl.admin']);
	Route::post('/client/create',['as'=>'client:create', 'uses'=>'ClientController@create'])->middleware(['acl.admin']);
	Route::post('/client/update',['as'=>'client:update', 'uses'=>'ClientController@update'])->middleware(['acl.admin']);
	Route::post('/client/update-status',['as'=>'client.update-status', 'uses'=>'ClientController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/client/remove',['as'=>'client.remove', 'uses'=>'ClientController@remove'])->middleware(['acl.admin']);

	// Projects Routes
	Route::get('/project/view',['as'=>'project:view', 'uses'=>'ProjectController@view']);
	Route::get('/project/list',['as'=>'project:list', 'uses'=>'ProjectController@all']);
	Route::post('/project/create',['as'=>'project:create', 'uses'=>'ProjectController@create'])->middleware(['acl.admin']);
	Route::post('/project/update',['as'=>'project:update', 'uses'=>'ProjectController@update'])->middleware(['acl.admin']);
	Route::post('/project/update-status',['as'=>'project.update-status', 'uses'=>'ProjectController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/project/remove',['as'=>'project.remove', 'uses'=>'ProjectController@remove'])->middleware(['acl.admin']);

	// Team Routes
	Route::get('/team/view',['as'=>'team:view', 'uses'=>'TeamController@view']);
	Route::get('/team/list',['as'=>'team:list', 'uses'=>'TeamController@all'])->middleware(['acl.admin']);
	Route::post('/team/create',['as'=>'team:create', 'uses'=>'TeamController@create'])->middleware(['acl.admin']);
	Route::post('/team/update',['as'=>'team:update', 'uses'=>'TeamController@update'])->middleware(['acl.admin']);
	Route::post('/team/update-status',['as'=>'team.update-status', 'uses'=>'TeamController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/team/remove',['as'=>'team.remove', 'uses'=>'TeamController@remove'])->middleware(['acl.admin']);

	// Contact Member Routes
	Route::get('/contact/view',['as'=>'contact:view', 'uses'=>'ContactController@view']);
	Route::get('/contact/list',['as'=>'contact:list', 'uses'=>'ContactController@all'])->middleware(['acl.admin']);
	Route::post('/contact/create',['as'=>'contact:create', 'uses'=>'ContactController@create'])->middleware(['acl.admin']);
	Route::post('/contact/update',['as'=>'contact:update', 'uses'=>'ContactController@update'])->middleware(['acl.admin']);
	Route::post('/contact/update-status',['as'=>'contact.update-status', 'uses'=>'ContactController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/contact/remove',['as'=>'contact.remove', 'uses'=>'ContactController@remove'])->middleware(['acl.admin']);

	// Settings
	Route::post('/setting/update',['as'=>'setting:update', 'uses'=>'SettingController@update'])->middleware(['acl.admin']);

	Route::post('contact/send',['uses'=>'ContactController@send']);

	// Quote Routes
	Route::post('quote/send',['uses'=>'QuoteController@create']);
	Route::get('/quote/list',['as'=>'quote:list', 'uses'=>'QuoteController@all'])->middleware(['acl.admin']);

	// merged product
	Route::get('project/listProducts',['uses'=>'ProjectController@allProducts']);

	// Partners Routes
	Route::get('/partner/view',['as'=>'partner:view', 'uses'=>'PartnerController@view'])->middleware(['acl.admin']);
	Route::get('/partner/list',['as'=>'partner:list', 'uses'=>'PartnerController@all'])->middleware(['acl.admin']);
	Route::post('/partner/create',['as'=>'partner:create', 'uses'=>'PartnerController@create'])->middleware(['acl.admin']);
	Route::post('/partner/update',['as'=>'partner:update', 'uses'=>'PartnerController@update'])->middleware(['acl.admin']);
	Route::post('/partner/update-status',['as'=>'partner.update-status', 'uses'=>'PartnerController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/partner/remove',['as'=>'partner.remove', 'uses'=>'PartnerController@remove'])->middleware(['acl.admin']);

	// Processes Routes
	Route::get('/process/view',['as'=>'process:view', 'uses'=>'ProcessController@view'])->middleware(['acl.admin']);
	Route::get('/process/list',['as'=>'process:list', 'uses'=>'ProcessController@all'])->middleware(['acl.admin']);
	Route::post('/process/create',['as'=>'process:create', 'uses'=>'ProcessController@create'])->middleware(['acl.admin']);
	Route::post('/process/update',['as'=>'process:update', 'uses'=>'ProcessController@update'])->middleware(['acl.admin']);
	Route::post('/process/update-status',['as'=>'process.update-status', 'uses'=>'ProcessController@updateStatus'])->middleware(['acl.admin']);
	Route::delete('/process/remove',['as'=>'process.remove', 'uses'=>'ProcessController@remove'])->middleware(['acl.admin']);
});

